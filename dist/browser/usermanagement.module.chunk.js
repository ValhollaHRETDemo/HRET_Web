webpackJsonp(["usermanagement.module"],{

/***/ "../../../../../src/app/pages/usermanagement/user-role-management/user-role-management.component.html":
/***/ (function(module, exports) {

module.exports = "<h2 mat-dialog-title>User Info</h2>\r\n\r\n<form>\r\n\r\n  <div *ngIf=\"showMessage\" class=\"clsErrorMessage\">\r\n    <span>{{message}}</span>\r\n  </div>\r\n  <br>\r\n\r\n  <mat-dialog-content>\r\n\r\n    <div class=\"person\" style=\"min-width:40vw\">\r\n      <div fxLayout=\"column\" fxLayout.gt-sm=\"row\" fxLayoutGap.gt-sm=\"24px\">\r\n        <mat-form-field fxFlex>\r\n          <mat-placeholder>First Name *</mat-placeholder>\r\n          <input [(ngModel)]=\"firstName\" [ngModelOptions]=\"{standalone: true}\" matInput>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n    <div class=\"person\">\r\n      <div fxLayout=\"column\" fxLayout.gt-sm=\"row\" fxLayoutGap.gt-sm=\"24px\">\r\n        <mat-form-field fxFlex>\r\n          <mat-placeholder>Last Name</mat-placeholder>\r\n          <input [(ngModel)]=\"lastName\" [ngModelOptions]=\"{standalone: true}\" matInput>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n    <div class=\"person\">\r\n      <div fxLayout=\"column\" fxLayout.gt-sm=\"row\" fxLayoutGap.gt-sm=\"24px\">\r\n        <mat-form-field fxFlex>\r\n          <mat-placeholder>Email Id *</mat-placeholder>\r\n          <input [(ngModel)]=\"emailId\" [ngModelOptions]=\"{standalone: true}\" matInput>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n    <div class=\"person\">\r\n      <div fxLayout=\"column\" fxLayout.gt-sm=\"row\" fxLayoutGap.gt-sm=\"24px\">\r\n        <mat-form-field fxFlex>\r\n          <mat-placeholder>Phone</mat-placeholder>\r\n          <input [(ngModel)]=\"phone\" [ngModelOptions]=\"{standalone: true}\" matInput>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"person\">\r\n      <div fxLayout=\"column\" fxLayout.gt-sm=\"row\" fxLayoutGap.gt-sm=\"24px\">\r\n        <mat-form-field style=\"width: 100% !important\">\r\n          <mat-placeholder>Role *</mat-placeholder>\r\n          <mat-select [(ngModel)]=\"selectedRoleId\" (change)=\"onRoleSelect($event)\" [ngModelOptions]=\"{standalone: true}\">\r\n            <mat-option *ngFor=\"let Role of Roles\" [value]=\"Role.id\">\r\n              {{ Role.name }}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n    <div class=\"person\" *ngIf=\"isStateShow\">\r\n      <div fxLayout=\"column\" fxLayout.gt-sm=\"row\" fxLayoutGap.gt-sm=\"24px\">\r\n        <mat-form-field style=\"width: 100% !important\">\r\n          <mat-placeholder>State *</mat-placeholder>\r\n          <mat-select [(ngModel)]=\"selectedStateId\" (change)=\"onStateSelect($event)\" [disabled]=\"isStateDisabled\" [ngModelOptions]=\"{standalone: true}\">\r\n            <mat-option *ngFor=\"let State of States\" [value]=\"State.id\">\r\n              {{ State.name }}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n    <div class=\"person\" *ngIf=\"isHealthSystemShow\">\r\n      <div fxLayout=\"column\" fxLayout.gt-sm=\"row\" fxLayoutGap.gt-sm=\"24px\">\r\n        <mat-form-field style=\"width: 100% !important\">\r\n          <mat-placeholder>HealthSystem *</mat-placeholder>\r\n          <mat-select [(ngModel)]=\"selectedHealthSystemId\" (change)=\"onHealthSystemSelect($event)\" [disabled]=\"isHealthSystemDisabled\"\r\n            [ngModelOptions]=\"{standalone: true}\">\r\n            <mat-option *ngFor=\"let HealthSystem of HealthSystems\" [value]=\"HealthSystem.id\">\r\n              {{ HealthSystem.name }}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n    <div class=\"person\" *ngIf=\"isHospitalShow\">\r\n      <div fxLayout=\"column\" fxLayout.gt-sm=\"row\" fxLayoutGap.gt-sm=\"24px\">\r\n        <mat-form-field style=\"width: 100% !important\">\r\n          <mat-placeholder>Hospital *</mat-placeholder>\r\n          <mat-select [(ngModel)]=\"selectedHospitalId\" (change)=\"onHospitalSelect($event)\" [disabled]=\"isHospitalDisabled\" [ngModelOptions]=\"{standalone: true}\">\r\n            <mat-option *ngFor=\"let Hospital of Hospitals\" [value]=\"Hospital.id\">\r\n              {{ Hospital.name }}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n    <div class=\"person\" *ngIf=\"isUnitShow\">\r\n      <div fxLayout=\"column\" fxLayout.gt-sm=\"row\" fxLayoutGap.gt-sm=\"24px\">\r\n        <mat-form-field style=\"width: 100% !important\">\r\n          <mat-placeholder>Units *</mat-placeholder>\r\n          <mat-select [(ngModel)]=\"selectedUnitId\" [ngModelOptions]=\"{standalone: true}\">\r\n            <mat-option *ngFor=\"let Unit of Units\" [value]=\"Unit.id\">\r\n              {{ Unit.name }}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n\r\n  </mat-dialog-content>\r\n\r\n  <mat-dialog-actions fxLayout=\"row\" fxLayoutAlign=\"end center\">\r\n\r\n    <button *ngIf=\"isCreateMode()\" (click)=\"checkAndSaveUser()\" mat-button>CREATE</button>\r\n    <button *ngIf=\"isUpdateMode()\" (click)=\"checkAndSaveUser()\" mat-button>UPDATE</button>\r\n    <button (click)=\"cancel()\" mat-button>CANCEL</button>\r\n\r\n  </mat-dialog-actions>\r\n\r\n</form>"

/***/ }),

/***/ "../../../../../src/app/pages/usermanagement/user-role-management/user-role-management.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/pages/usermanagement/user-role-management/user-role-management.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserRoleManagementComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_providers_user_service__ = __webpack_require__("../../../../../src/app/providers/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_app_providers_role_service__ = __webpack_require__("../../../../../src/app/providers/role.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_app_providers_states_service__ = __webpack_require__("../../../../../src/app/providers/states.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_app_providers_units_service__ = __webpack_require__("../../../../../src/app/providers/units.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_app_providers_hospitalinformation_service__ = __webpack_require__("../../../../../src/app/providers/hospitalinformation.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_app_providers_healthsystemmanagement_service__ = __webpack_require__("../../../../../src/app/providers/healthsystemmanagement.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_app_providers_userrole_service__ = __webpack_require__("../../../../../src/app/providers/userrole.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_app_providers_hospitalunits_service__ = __webpack_require__("../../../../../src/app/providers/hospitalunits.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};











var UserRoleManagementComponent = (function () {
    function UserRoleManagementComponent(defaults, dialogRef, userService, roleService, healthSystemManagementService, hospitalInformationService, unitsService, hospitalUnitService, stateService, userRoleService) {
        this.defaults = defaults;
        this.dialogRef = dialogRef;
        this.userService = userService;
        this.roleService = roleService;
        this.healthSystemManagementService = healthSystemManagementService;
        this.hospitalInformationService = hospitalInformationService;
        this.unitsService = unitsService;
        this.hospitalUnitService = hospitalUnitService;
        this.stateService = stateService;
        this.userRoleService = userRoleService;
        this.message = "";
        this.showMessage = false;
        this.mode = 'create';
        this.selectedTempRoleName = "";
        this.Roles = [];
        this.States = [];
        this.isStateShow = false;
        this.isStateDisabled = false;
        this.HealthSystems = [];
        this.isHealthSystemShow = false;
        this.isHealthSystemDisabled = false;
        this.Hospitals = [];
        this.isHospitalShow = false;
        this.isHospitalDisabled = false;
        this.Units = [];
        this.isUnitShow = false;
        //for set info
        this.UserRoles = [];
        this.userDetails = JSON.parse(localStorage.getItem("userDetails"));
    }
    UserRoleManagementComponent.prototype.ngOnInit = function () {
        //for first module
        if (this.defaults.status === "update") {
            this.mode = 'update';
            this.userModel = this.defaults.user;
            this.firstName = this.userModel['first_name'];
            this.lastName = this.userModel['last_name'];
            this.emailId = this.userModel['email_id'];
            this.phone = this.userModel['phone'];
            this.userModel["modified_by"] = this.userDetails.user_id;
            this.userModel["modified_on"] = new Date();
        }
        else {
            this.mode = 'create';
            this.userModel = {
                email_id: '',
                user_pwd: '',
                first_name: '',
                last_name: '',
                phone: '',
                is_locked: '1',
                password_expiry_date: new Date(),
                is_active: '1',
                created_on: new Date(),
                created_by: this.defaults.from_user_id,
                modified_on: null,
                modified_by: null,
                failure_count: '0',
                //is_auto_password: '0',
                role_id: null,
                role_name: null,
                role_rank: null,
                state_id: null,
                hs_id: null,
                hospital_id: null,
                unit_id: null
            };
            this.firstName = "";
            this.lastName = "";
            this.emailId = "";
            this.phone = "";
        }
        //for second module
        this.BindDropDowns();
    };
    UserRoleManagementComponent.prototype.BindDropDowns = function () {
        var _this = this;
        this.user_name = (this.defaults.user === undefined) ? "" : this.defaults.user.user_name;
        this.user_id = (this.defaults.user === undefined) ? "" : this.defaults.user.user_id;
        this.roleService.getRoles().subscribe(function (roleResponse) {
            roleResponse.forEach(function (element) {
                if (parseInt(element.role_rank) > parseInt(_this.userDetails.role_rank)) {
                    _this.Roles.push({ name: element.role_name, id: element.role_id });
                }
            });
            //for existing user - fetching values
            if (_this.user_id) {
                _this.userRoleService.getUserAndRolesByUserId(_this.user_id).subscribe(function (userRoleResponse) {
                    if (userRoleResponse) {
                        _this.selectedRoleId = (userRoleResponse.role_id);
                        _this.selectedTempRoleName = _this.Roles.find(function (x) { return x.id === parseInt(_this.selectedRoleId); }).name;
                        _this.selectedStateId = (userRoleResponse.state_id);
                        _this.selectedHealthSystemId = (userRoleResponse.hs_id);
                        _this.selectedHospitalId = (userRoleResponse.hospital_id);
                        _this.selectedUnitId = (userRoleResponse.unit_id);
                        var optionsField = ({
                            user_role_id: (userRoleResponse.user_role_id),
                            user_id: (userRoleResponse.user_id),
                            role_id: (_this.selectedRoleId),
                            role_name: _this.Roles.find(function (x) { return x.id === parseInt(_this.selectedRoleId); }).name,
                            hospital_id: _this.selectedHospitalId,
                            hs_id: _this.selectedHealthSystemId,
                            unit_id: _this.selectedUnitId,
                            state_id: _this.selectedStateId,
                            is_active: userRoleResponse.is_active,
                            created_by: userRoleResponse.created_by,
                            created_on: userRoleResponse.created_on,
                            modified_by: (_this.userDetails.user_id),
                            modified_on: new Date()
                        });
                        if (_this.selectedStateId !== null) {
                            _this.loadStates();
                            _this.isStateShow = true;
                        }
                        if (_this.selectedHealthSystemId !== null) {
                            _this.loadHealthSystems(_this.selectedStateId);
                            _this.isHealthSystemShow = true;
                        }
                        if (_this.selectedHospitalId !== null) {
                            _this.loadHospitals(_this.selectedStateId);
                            _this.isHospitalShow = true;
                        }
                        if (_this.selectedUnitId !== null) {
                            _this.loadUnits(_this.selectedHospitalId);
                            _this.isUnitShow = true;
                        }
                        _this.UserRoles.push(optionsField);
                    }
                });
            }
            else {
                var optionsField = ({
                    user_id: null,
                    role_id: null,
                    role_name: null,
                    hospital_id: null,
                    hs_id: null,
                    unit_id: null,
                    state_id: null,
                    is_active: "1",
                    created_by: _this.userDetails.user_id,
                    created_on: new Date(),
                    modified_by: null,
                    modified_on: null
                });
                _this.UserRoles.push(optionsField);
            }
            //if (this.userDetails.role_name === "HRET Admin") { }
            if (_this.userDetails.role_name === "State Admin") {
                _this.isStateDisabled = true;
            }
            if (_this.userDetails.role_name === "Health System Admin") {
                _this.isStateDisabled = true;
                _this.isHealthSystemDisabled = true;
            }
            if (_this.userDetails.role_name === "Hospital Admin") {
                _this.isStateDisabled = true;
                _this.isHospitalDisabled = true;
            }
            //if (this.userDetails.role_name === "Unit Admin") {}
        });
    };
    UserRoleManagementComponent.prototype.isCreateMode = function () {
        return this.mode === 'create';
    };
    UserRoleManagementComponent.prototype.isUpdateMode = function () {
        return this.mode === 'update';
    };
    UserRoleManagementComponent.prototype.checkAndSaveUser = function () {
        var _this = this;
        if (this.validation()) {
            this.userService.checkUserByEmail({ "emailId": this.emailId.trim().toLocaleLowerCase() }).subscribe(function (userResponse) {
                if (userResponse.message === "success") {
                    _this.saveUser();
                }
                else {
                    if (parseInt(userResponse.user_id) === parseInt(_this.userModel["user_id"])) {
                        _this.saveUser();
                    }
                    else {
                        _this.validationMessage(userResponse.message);
                        return false;
                    }
                }
            });
        }
    };
    UserRoleManagementComponent.prototype.saveUser = function () {
        var _this = this;
        //for first module
        this.userModel['first_name'] = this.firstName;
        this.userModel['last_name'] = this.lastName;
        this.userModel['email_id'] = this.emailId;
        this.userModel['phone'] = this.phone;
        this.userModel['role_id'] = this.selectedRoleId;
        this.userModel['role_name'] = this.Roles.find(function (x) { return x.id === parseInt(_this.selectedRoleId); }).name;
        this.userService.saveUser(this.userModel).subscribe(function (userResponse) {
            userResponse["role_id"] = _this.userModel['role_id'];
            userResponse["role_name"] = _this.userModel['role_name'];
            //for second module
            if (_this.UserRoles[0].user_role_id !== undefined) {
                _this.UserRoles[0].user_role_id = (_this.UserRoles[0].user_role_id === "") ? 0 : _this.UserRoles[0].user_role_id;
            }
            _this.UserRoles[0].user_id = userResponse.user_id;
            _this.UserRoles[0].role_id = _this.selectedRoleId;
            _this.UserRoles[0].hospital_id = _this.selectedHospitalId;
            _this.UserRoles[0].hs_id = _this.selectedHealthSystemId;
            _this.UserRoles[0].unit_id = _this.selectedUnitId;
            _this.UserRoles[0].state_id = _this.selectedStateId;
            _this.userRoleService.saveUserAndRoles(_this.UserRoles[0]).subscribe(function (p) {
                _this.UserRoles[0].role_name = _this.Roles.find(function (x) { return x.id === parseInt(_this.UserRoles[0].role_id); }).name;
                _this.dialogRef.close(userResponse);
            });
        });
    };
    UserRoleManagementComponent.prototype.cancel = function () {
        this.dialogRef.close();
    };
    UserRoleManagementComponent.prototype.validation = function () {
        if (this.firstName.trim().toString() === "") {
            this.validationMessage("Invalid first name");
            return false;
        }
        if (this.emailId.trim().toString() === "") {
            this.validationMessage("Invalid email");
            return false;
        }
        if (this.selectedRoleId === undefined) {
            this.validationMessage("Invalid role");
            return false;
        }
        if (this.selectedTempRoleName === "State Admin" && this.selectedStateId === null) {
            this.validationMessage("Invalid state");
            return false;
        }
        if (this.selectedTempRoleName === "Health System Admin" && this.selectedHealthSystemId === null) {
            this.validationMessage("Invalid health system");
            return false;
        }
        if (this.selectedTempRoleName === "Hospital Admin" && this.selectedHospitalId === null) {
            this.validationMessage("Invalid hospital");
            return false;
        }
        if (this.selectedTempRoleName === "Unit Admin" && this.selectedUnitId === null) {
            this.validationMessage("Invalid unit");
            return false;
        }
        if (!((/\S+@\S+\.\S+/).test(this.emailId.trim().toLocaleLowerCase()))) {
            this.validationMessage("Invalid email id");
            return false;
        }
        return true;
    };
    UserRoleManagementComponent.prototype.onRoleSelect = function (data) {
        //set value
        this.selectedStateId = null;
        this.States = [];
        this.selectedHealthSystemId = null;
        this.HealthSystems = [];
        this.selectedHospitalId = null;
        this.Hospitals = [];
        this.selectedUnitId = null;
        this.Units = [];
        this.selectedTempRoleName = this.Roles.find(function (x) { return x.id === parseInt(data.value); }).name;
        if (this.selectedTempRoleName === "HRET Admin") {
            this.isStateShow = false;
            this.isHealthSystemShow = false;
            this.isHospitalShow = false;
            this.isUnitShow = false;
        }
        if (this.selectedTempRoleName === "State Admin") {
            this.isStateShow = true;
            this.loadStates();
            this.isStateDisabled = false;
            this.isHealthSystemShow = false;
            this.isHospitalShow = false;
            this.isUnitShow = false;
        }
        if (this.selectedTempRoleName === "Health System Admin") {
            this.isStateShow = true;
            this.loadStates();
            if (this.userDetails.state_id !== null) {
                this.selectedStateId = this.userDetails.state_id;
            }
            this.isStateDisabled = false;
            this.isHealthSystemShow = true;
            if (this.userDetails.state_id !== null) {
                this.loadHealthSystems(this.selectedStateId);
            }
            this.isHealthSystemDisabled = false;
            this.isHospitalShow = false;
            this.isUnitShow = false;
        }
        if (this.selectedTempRoleName === "Hospital Admin") {
            this.isStateShow = true;
            this.loadStates();
            if (this.userDetails.state_id !== null) {
                this.selectedStateId = this.userDetails.state_id;
            }
            this.isStateDisabled = false;
            this.isHealthSystemShow = false;
            this.isHospitalShow = true;
            if (this.userDetails.state_id !== null) {
                this.loadHospitals(this.selectedStateId);
            }
            this.isHospitalDisabled = false;
            this.isUnitShow = false;
        }
        if (this.selectedTempRoleName === "Unit Admin") {
            this.isStateShow = true;
            this.loadStates();
            if (this.userDetails.state_id !== null) {
                this.selectedStateId = this.userDetails.state_id;
            }
            this.isStateDisabled = false;
            this.isHealthSystemShow = false;
            this.isHospitalShow = true;
            if (this.userDetails.state_id !== null) {
                this.loadHospitals(this.selectedStateId);
            }
            if (this.userDetails.state_id !== null) {
                this.selectedHospitalId = this.userDetails.hospital_id;
            }
            this.isHospitalDisabled = false;
            this.isUnitShow = true;
            if (this.userDetails.hospital_id !== null) {
                this.loadUnits(this.selectedHospitalId);
            }
        }
        //if (this.userDetails.role_name === "HRET Admin") { }
        if (this.userDetails.role_name === "State Admin") {
            this.isStateDisabled = true;
        }
        if (this.userDetails.role_name === "Health System Admin") {
            this.isStateDisabled = true;
            this.isHealthSystemDisabled = true;
        }
        if (this.userDetails.role_name === "Hospital Admin") {
            this.isStateDisabled = true;
            this.isHospitalDisabled = true;
        }
        //if (this.userDetails.role_name === "Unit Admin") {}
    };
    UserRoleManagementComponent.prototype.onStateSelect = function (data) {
        //set value
        this.selectedHealthSystemId = null;
        this.HealthSystems = [];
        this.selectedHospitalId = null;
        this.Hospitals = [];
        this.selectedUnitId = null;
        this.Units = [];
        if (this.selectedTempRoleName === "Health System Admin") {
            this.loadHealthSystems(this.selectedStateId);
        }
        if (this.selectedTempRoleName === "Hospital Admin") {
            this.loadHospitals(this.selectedStateId);
        }
        if (this.selectedTempRoleName === "Unit Admin") {
            this.loadHospitals(this.selectedStateId);
        }
    };
    UserRoleManagementComponent.prototype.onHealthSystemSelect = function (data) {
        //set value
        this.selectedHospitalId = null;
        this.Hospitals = [];
        this.selectedUnitId = null;
        this.Units = [];
        if (this.selectedTempRoleName === "Unit Admin") {
            this.loadUnits(this.selectedHospitalId);
        }
    };
    UserRoleManagementComponent.prototype.onHospitalSelect = function (data) {
        //set value
        this.selectedHealthSystemId = null;
        this.HealthSystems = [];
        this.selectedUnitId = null;
        this.Units = [];
        if (this.selectedTempRoleName === "Unit Admin") {
            this.loadUnits(this.selectedHospitalId);
        }
    };
    UserRoleManagementComponent.prototype.loadStates = function () {
        var _this = this;
        this.stateService.getStates().subscribe(function (stateResponse) {
            _this.States = [];
            stateResponse.forEach(function (element) {
                _this.States.push({ name: element.state_name, id: element.state_id });
            });
        });
    };
    UserRoleManagementComponent.prototype.loadHealthSystems = function (state_id) {
        var _this = this;
        this.healthSystemManagementService.getHealthSystemsByStateId(state_id).subscribe(function (healthSystemResponse) {
            _this.HealthSystems = [];
            healthSystemResponse.forEach(function (element) {
                _this.HealthSystems.push({ name: element.health_system_name, id: element.health_system_id });
            });
        });
    };
    UserRoleManagementComponent.prototype.loadHospitals = function (state_id) {
        var _this = this;
        this.hospitalInformationService.getHospitalInformationByStateId(state_id).subscribe(function (hospitalInformationResponse) {
            _this.Hospitals = [];
            hospitalInformationResponse.forEach(function (element) {
                _this.Hospitals.push({ name: element.hospital_name, id: element.hospital_id });
            });
        });
    };
    UserRoleManagementComponent.prototype.loadUnits = function (hosp_id) {
        var _this = this;
        this.unitsService.getUnits().subscribe(function (unitResponse) {
            var units = unitResponse;
            _this.hospitalUnitService.getHospitalUnitsByHospitalId(hosp_id).subscribe(function (hospUnitResponse) {
                _this.Units = [];
                hospUnitResponse.forEach(function (element) {
                    var unitname = units.find(function (x) { return x.unit_id === element.unit_id; }).unit_name;
                    _this.Units.push({ name: unitname, id: element.unit_id });
                });
            });
        });
    };
    UserRoleManagementComponent.prototype.validationMessage = function (message) {
        var _this = this;
        // set showloader to true to show loading div on view
        this.showMessage = true;
        this.message = message;
        this.timer = __WEBPACK_IMPORTED_MODULE_10_rxjs_Observable__["a" /* Observable */].timer(4000); // 4000 millisecond means 4 seconds
        this.subscription = this.timer.subscribe(function () {
            // set showloader to false to hide loading div from view after 5 seconds
            _this.showMessage = false;
            _this.message = "";
        });
    };
    UserRoleManagementComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'vr-user-role-management',
            template: __webpack_require__("../../../../../src/app/pages/usermanagement/user-role-management/user-role-management.component.html"),
            styles: [__webpack_require__("../../../../../src/app/pages/usermanagement/user-role-management/user-role-management.component.scss")]
        }),
        __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_1__angular_material__["a" /* MAT_DIALOG_DATA */])),
        __metadata("design:paramtypes", [Object, __WEBPACK_IMPORTED_MODULE_1__angular_material__["k" /* MatDialogRef */],
            __WEBPACK_IMPORTED_MODULE_2_app_providers_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_3_app_providers_role_service__["a" /* RoleService */],
            __WEBPACK_IMPORTED_MODULE_7_app_providers_healthsystemmanagement_service__["a" /* HealthSystemManagementService */],
            __WEBPACK_IMPORTED_MODULE_6_app_providers_hospitalinformation_service__["a" /* HospitalInformationService */],
            __WEBPACK_IMPORTED_MODULE_5_app_providers_units_service__["a" /* UnitsService */],
            __WEBPACK_IMPORTED_MODULE_9_app_providers_hospitalunits_service__["a" /* HospitalUnitsService */],
            __WEBPACK_IMPORTED_MODULE_4_app_providers_states_service__["a" /* StatesService */],
            __WEBPACK_IMPORTED_MODULE_8_app_providers_userrole_service__["a" /* UserRoleService */]])
    ], UserRoleManagementComponent);
    return UserRoleManagementComponent;
}());



/***/ }),

/***/ "../../../../../src/app/pages/usermanagement/user-role-management/user-role-management.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserRole; });
var UserRole = (function () {
    function UserRole(userrole) {
        this.user_role_id = userrole.user_role_id;
        this.user_id = userrole.user_id;
        this.role_id = userrole.role_id;
        this.hospital_id = userrole.hospital_id;
        this.unit_id = userrole.unit_id;
        this.state_id = userrole.state_id;
        this.is_active = userrole.is_active;
        this.created_by = userrole.created_by;
        this.created_on = userrole.created_on;
        this.modified_by = userrole.modified_by;
        this.modified_on = userrole.modified_on;
        this.hs_id = userrole.hs_id;
        this.role_name = userrole.role_name;
    }
    return UserRole;
}());



/***/ }),

/***/ "../../../../../src/app/pages/usermanagement/user-role-management/userModel.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserModel; });
var UserModel = (function () {
    function UserModel(user) {
        this.user_id = user.user_id;
        this.email_id = user.email_id;
        this.user_pwd = user.user_pwd;
        this.first_name = user.first_name;
        this.last_name = user.last_name;
        this.phone = user.phone;
        this.is_locked = user.is_locked;
        this.password_expiry_date = user.password_expiry_date;
        this.is_active = user.is_active;
        this.created_on = user.created_on;
        this.created_by = user.created_by;
        this.modified_on = user.modified_on;
        this.modified_by = user.modified_by;
        this.failure_count = user.failure_count;
        this.is_auto_password = user.is_auto_password;
        this.role_id = user.role_id;
        this.role_name = user.role_name;
        this.role_rank = user.role_rank;
        this.state_id = user.state_id;
        this.hs_id = user.hs_id;
        this.hospital_id = user.hospital_id;
        this.unit_id = user.unit_id;
    }
    return UserModel;
}());



/***/ }),

/***/ "../../../../../src/app/pages/usermanagement/user-role-management/userrole.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserRoleManagementModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__user_role_management_component__ = __webpack_require__("../../../../../src/app/pages/usermanagement/user-role-management/user-role-management.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_flex_layout__ = __webpack_require__("../../../flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ng_multiselect_dropdown__ = __webpack_require__("../../../../ng-multiselect-dropdown/fesm5/ng-multiselect-dropdown.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var UserRoleManagementModule = (function () {
    function UserRoleManagementModule() {
    }
    UserRoleManagementModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_forms__["e" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_forms__["j" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_flex_layout__["a" /* FlexLayoutModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["j" /* MatDialogModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["n" /* MatInputModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["c" /* MatButtonModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["m" /* MatIconModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["w" /* MatRadioModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["y" /* MatSelectModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["f" /* MatCheckboxModule */],
                __WEBPACK_IMPORTED_MODULE_6_ng_multiselect_dropdown__["a" /* NgMultiSelectDropDownModule */].forRoot()
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_2__user_role_management_component__["a" /* UserRoleManagementComponent */]],
            entryComponents: [__WEBPACK_IMPORTED_MODULE_2__user_role_management_component__["a" /* UserRoleManagementComponent */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__user_role_management_component__["a" /* UserRoleManagementComponent */]]
        })
    ], UserRoleManagementModule);
    return UserRoleManagementModule;
}());



/***/ }),

/***/ "../../../../../src/app/pages/usermanagement/usermanagement.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"all-in-one-table\">\r\n  <vr-page-header [height]=\"'180px'\" background=\"url(/assets/img/demo/headers/pattern-3.png)\" [reverse]=\"true\"></vr-page-header>\r\n\r\n  <div class=\"container\">\r\n\r\n    <vr-breadcrumbs [currentPage]=\"'User Management'\" [header]=\"'primary'\"></vr-breadcrumbs>\r\n\r\n    <div *ngIf=\"showMessage\" fxLayout=\"column\" fxLayout.gt-sm=\"row\" style=\"background-color: white;padding: 22px; text-align: center\"\r\n      fxLayoutGap.gt-sm=\"24px\">\r\n      <span style=\"text-align: center\">{{message}}</span>\r\n    </div>\r\n\r\n    <vr-list name=\"Users\" [columns]=\"columns\" (filterChange)=\"onFilterChange($event)\">\r\n      <div class=\"actions\" fxFlexAlign=\"center\">\r\n        <button class=\"create\" (click)=\"createUser()\" type=\"button\" mat-mini-fab color=\"primary\">\r\n          <mat-icon>add</mat-icon>\r\n        </button>\r\n      </div>\r\n\r\n      <mat-table #table [dataSource]=\"dataSource\" matSort>\r\n\r\n        <!--- Note that these columns can be defined in any order.\r\n              The actual rendered columns are set as a property on the row definition\" -->\r\n\r\n        <!-- Checkbox Column -->\r\n        <ng-container matColumnDef=\"checkbox\">\r\n          <mat-header-cell class=\"actions-cell\" *matHeaderCellDef>\r\n            <mat-checkbox color=\"primary\" (click)=\"$event.stopPropagation()\"></mat-checkbox>\r\n          </mat-header-cell>\r\n          <mat-cell class=\"actions-cell\" *matCellDef=\"let row\">\r\n            <mat-checkbox color=\"primary\" (click)=\"$event.stopPropagation()\"></mat-checkbox>\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <!-- ID Column -->\r\n        <ng-container matColumnDef=\"image\">\r\n          <mat-header-cell class=\"image-cell\" *matHeaderCellDef></mat-header-cell>\r\n          <mat-cell class=\"image-cell\" *matCellDef=\"let row\">\r\n            <img src=\"//placehold.it/40x40\">\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <!-- fullName Column -->\r\n        <ng-container *ngFor=\"let column of columns\">\r\n          <ng-container *ngIf=\"column.isModelProperty\" [matColumnDef]=\"column.property\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header> {{ column.name }}</mat-header-cell>\r\n            <mat-cell *matCellDef=\"let row\"> {{ row[column.property] }}</mat-cell>\r\n          </ng-container>\r\n        </ng-container>\r\n\r\n\r\n        <!-- actions Column -->\r\n        <ng-container matColumnDef=\"actions\">\r\n          <mat-header-cell class=\"actions-cell\" *matHeaderCellDef mat-sort-header></mat-header-cell>\r\n          <mat-cell class=\"actions-cell\" *matCellDef=\"let row\">\r\n            <button type=\"button\" mat-icon-button [matMenuTriggerFor]=\"actionsMenu\" (click)=\"$event.stopPropagation()\">\r\n              <mat-icon>more_horiz</mat-icon>\r\n            </button>\r\n\r\n            <mat-menu #actionsMenu=\"matMenu\" yPosition=\"below\" xPosition=\"before\">\r\n              <button (click)=\"updateUser(row)\" mat-menu-item>\r\n                <span>Modify</span>\r\n              </button>\r\n              <button (click)=\"unlockUser(row)\" *ngIf=\"row.is_locked === '1'\" mat-menu-item>\r\n                <span>Unlock</span>\r\n              </button>\r\n              <button (click)=\"deleteUser(row)\" mat-menu-item>\r\n                <span>Delete</span>\r\n              </button>\r\n            </mat-menu>\r\n          </mat-cell>\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"visibleColumns\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: visibleColumns;\" (click)=\"updateUser(row)\" class=\"clickable\"></mat-row>\r\n      </mat-table>\r\n\r\n      <mat-paginator class=\"paginator\" [length]=\"resultsLength\" [pageSize]=\"pageSize\"></mat-paginator>\r\n\r\n    </vr-list>\r\n\r\n  </div>\r\n\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/pages/usermanagement/usermanagement.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/**\n * Applies styles for users in high contrast mode. Note that this only applies\n * to Microsoft browsers. Chrome can be included by checking for the `html[hc]`\n * attribute, however Chrome handles high contrast differently.\n */\n/* Theme for the ripple elements.*/\n/** The mixins below are shared between mat-menu and mat-select */\n/**\n * This mixin adds the correct panel transform styles based\n * on the direction that the menu panel opens.\n */\n/* stylelint-disable material/no-prefixes */\n/* stylelint-enable */\n/**\n * This mixin contains shared option styles between the select and\n * autocomplete components.\n */\n.mat-elevation-z0 {\n  box-shadow: 0px 0px 0px 0px rgba(0, 0, 0, 0.2), 0px 0px 0px 0px rgba(0, 0, 0, 0.14), 0px 0px 0px 0px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z1 {\n  box-shadow: 0px 2px 1px -1px rgba(0, 0, 0, 0.2), 0px 1px 1px 0px rgba(0, 0, 0, 0.14), 0px 1px 3px 0px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z2 {\n  box-shadow: 0px 3px 1px -2px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 1px 5px 0px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z3 {\n  box-shadow: 0px 3px 3px -2px rgba(0, 0, 0, 0.2), 0px 3px 4px 0px rgba(0, 0, 0, 0.14), 0px 1px 8px 0px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z4 {\n  box-shadow: 0px 2px 4px -1px rgba(0, 0, 0, 0.2), 0px 4px 5px 0px rgba(0, 0, 0, 0.14), 0px 1px 10px 0px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z5 {\n  box-shadow: 0px 3px 5px -1px rgba(0, 0, 0, 0.2), 0px 5px 8px 0px rgba(0, 0, 0, 0.14), 0px 1px 14px 0px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z6 {\n  box-shadow: 0px 3px 5px -1px rgba(0, 0, 0, 0.2), 0px 6px 10px 0px rgba(0, 0, 0, 0.14), 0px 1px 18px 0px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z7 {\n  box-shadow: 0px 4px 5px -2px rgba(0, 0, 0, 0.2), 0px 7px 10px 1px rgba(0, 0, 0, 0.14), 0px 2px 16px 1px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z8 {\n  box-shadow: 0px 5px 5px -3px rgba(0, 0, 0, 0.2), 0px 8px 10px 1px rgba(0, 0, 0, 0.14), 0px 3px 14px 2px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z9 {\n  box-shadow: 0px 5px 6px -3px rgba(0, 0, 0, 0.2), 0px 9px 12px 1px rgba(0, 0, 0, 0.14), 0px 3px 16px 2px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z10 {\n  box-shadow: 0px 6px 6px -3px rgba(0, 0, 0, 0.2), 0px 10px 14px 1px rgba(0, 0, 0, 0.14), 0px 4px 18px 3px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z11 {\n  box-shadow: 0px 6px 7px -4px rgba(0, 0, 0, 0.2), 0px 11px 15px 1px rgba(0, 0, 0, 0.14), 0px 4px 20px 3px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z12 {\n  box-shadow: 0px 7px 8px -4px rgba(0, 0, 0, 0.2), 0px 12px 17px 2px rgba(0, 0, 0, 0.14), 0px 5px 22px 4px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z13 {\n  box-shadow: 0px 7px 8px -4px rgba(0, 0, 0, 0.2), 0px 13px 19px 2px rgba(0, 0, 0, 0.14), 0px 5px 24px 4px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z14 {\n  box-shadow: 0px 7px 9px -4px rgba(0, 0, 0, 0.2), 0px 14px 21px 2px rgba(0, 0, 0, 0.14), 0px 5px 26px 4px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z15 {\n  box-shadow: 0px 8px 9px -5px rgba(0, 0, 0, 0.2), 0px 15px 22px 2px rgba(0, 0, 0, 0.14), 0px 6px 28px 5px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z16 {\n  box-shadow: 0px 8px 10px -5px rgba(0, 0, 0, 0.2), 0px 16px 24px 2px rgba(0, 0, 0, 0.14), 0px 6px 30px 5px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z17 {\n  box-shadow: 0px 8px 11px -5px rgba(0, 0, 0, 0.2), 0px 17px 26px 2px rgba(0, 0, 0, 0.14), 0px 6px 32px 5px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z18 {\n  box-shadow: 0px 9px 11px -5px rgba(0, 0, 0, 0.2), 0px 18px 28px 2px rgba(0, 0, 0, 0.14), 0px 7px 34px 6px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z19 {\n  box-shadow: 0px 9px 12px -6px rgba(0, 0, 0, 0.2), 0px 19px 29px 2px rgba(0, 0, 0, 0.14), 0px 7px 36px 6px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z20 {\n  box-shadow: 0px 10px 13px -6px rgba(0, 0, 0, 0.2), 0px 20px 31px 3px rgba(0, 0, 0, 0.14), 0px 8px 38px 7px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z21 {\n  box-shadow: 0px 10px 13px -6px rgba(0, 0, 0, 0.2), 0px 21px 33px 3px rgba(0, 0, 0, 0.14), 0px 8px 40px 7px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z22 {\n  box-shadow: 0px 10px 14px -6px rgba(0, 0, 0, 0.2), 0px 22px 35px 3px rgba(0, 0, 0, 0.14), 0px 8px 42px 7px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z23 {\n  box-shadow: 0px 11px 14px -7px rgba(0, 0, 0, 0.2), 0px 23px 36px 3px rgba(0, 0, 0, 0.14), 0px 9px 44px 8px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z24 {\n  box-shadow: 0px 11px 15px -7px rgba(0, 0, 0, 0.2), 0px 24px 38px 3px rgba(0, 0, 0, 0.14), 0px 9px 46px 8px rgba(0, 0, 0, 0.12); }\n\n.mat-h1, .mat-headline, .mat-typography h1 {\n  font: 400 24px/32px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 16px; }\n\n.mat-h2, .mat-title, .mat-typography h2 {\n  font: 500 20px/32px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 16px; }\n\n.mat-h3, .mat-subheading-2, .mat-typography h3 {\n  font: 400 16px/28px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 16px; }\n\n.mat-h4, .mat-subheading-1, .mat-typography h4 {\n  font: 400 15px/24px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 16px; }\n\n.mat-h5, .mat-typography h5 {\n  font-size: 11.62px;\n  font-weight: 400;\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  line-height: 20px;\n  margin: 0 0 12px; }\n\n.mat-h6, .mat-typography h6 {\n  font-size: 9.38px;\n  font-weight: 400;\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  line-height: 20px;\n  margin: 0 0 12px; }\n\n.mat-body-strong, .mat-body-2 {\n  font: 500 14px/24px Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-body, .mat-body-1, .mat-typography {\n  font: 400 14px/20px Roboto, \"Helvetica Neue\", sans-serif; }\n  .mat-body p, .mat-body-1 p, .mat-typography p {\n    margin: 0 0 12px; }\n\n.mat-small, .mat-caption {\n  font: 400 12px/20px Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-display-4, .mat-typography .mat-display-4 {\n  font: 300 112px/112px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 56px;\n  letter-spacing: -0.05em; }\n\n.mat-display-3, .mat-typography .mat-display-3 {\n  font: 400 56px/56px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 64px;\n  letter-spacing: -0.02em; }\n\n.mat-display-2, .mat-typography .mat-display-2 {\n  font: 400 45px/48px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 64px;\n  letter-spacing: -0.005em; }\n\n.mat-display-1, .mat-typography .mat-display-1 {\n  font: 400 34px/40px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 64px; }\n\n.mat-button, .mat-raised-button, .mat-icon-button, .mat-fab, .mat-mini-fab {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 14px;\n  font-weight: 500; }\n\n.mat-button-toggle {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-card {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-card-title {\n  font-size: 24px;\n  font-weight: 400; }\n\n.mat-card-subtitle,\n.mat-card-content,\n.mat-card-header .mat-card-title {\n  font-size: 14px; }\n\n.mat-checkbox {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-checkbox-layout .mat-checkbox-label {\n  line-height: 24px; }\n\n.mat-chip {\n  font-size: 13px;\n  line-height: 18px; }\n  .mat-chip .mat-chip-remove.mat-icon {\n    font-size: 18px; }\n\n.mat-table {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-header-cell {\n  font-size: 12px;\n  font-weight: 500; }\n\n.mat-cell {\n  font-size: 14px; }\n\n.mat-calendar {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-calendar-body {\n  font-size: 13px; }\n\n.mat-calendar-body-label,\n.mat-calendar-period-button {\n  font-size: 14px;\n  font-weight: 500; }\n\n.mat-calendar-table-header th {\n  font-size: 11px;\n  font-weight: 400; }\n\n.mat-dialog-title {\n  font: 500 20px/32px Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-expansion-panel-header {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 15px;\n  font-weight: 400; }\n\n.mat-expansion-panel-content {\n  font: 400 14px/20px Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-form-field {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: inherit;\n  font-weight: 400;\n  line-height: 1.125; }\n\n.mat-form-field-wrapper {\n  padding-bottom: 1.25em; }\n\n.mat-form-field-prefix .mat-icon,\n.mat-form-field-suffix .mat-icon {\n  font-size: 150%;\n  line-height: 1.125; }\n\n.mat-form-field-prefix .mat-icon-button,\n.mat-form-field-suffix .mat-icon-button {\n  height: 1.5em;\n  width: 1.5em; }\n  .mat-form-field-prefix .mat-icon-button .mat-icon,\n  .mat-form-field-suffix .mat-icon-button .mat-icon {\n    height: 1.125em;\n    line-height: 1.125; }\n\n.mat-form-field-infix {\n  padding: 0.4375em 0;\n  border-top: 0.84375em solid transparent; }\n\n.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label,\n.mat-form-field-can-float .mat-input-server:focus + .mat-form-field-label-wrapper .mat-form-field-label {\n  transform: translateY(-1.28125em) scale(0.75) perspective(100px) translateZ(0.001px);\n  -ms-transform: translateY(-1.28125em) scale(0.75);\n  width: 133.33333333%; }\n\n.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill + .mat-form-field-label-wrapper\n.mat-form-field-label {\n  transform: translateY(-1.28125em) scale(0.75) perspective(100px) translateZ(0.00101px);\n  -ms-transform: translateY(-1.28124em) scale(0.75);\n  width: 133.33334333%; }\n\n.mat-form-field-can-float .mat-input-server[label]:not(:label-shown) + .mat-form-field-label-wrapper\n.mat-form-field-label {\n  transform: translateY(-1.28125em) scale(0.75) perspective(100px) translateZ(0.00102px);\n  -ms-transform: translateY(-1.28123em) scale(0.75);\n  width: 133.33335333%; }\n\n.mat-form-field-label-wrapper {\n  top: -0.84375em;\n  padding-top: 0.84375em; }\n\n.mat-form-field-label {\n  top: 1.28125em; }\n\n.mat-form-field-underline {\n  bottom: 1.25em; }\n\n.mat-form-field-subscript-wrapper {\n  font-size: 75%;\n  margin-top: 0.54166667em;\n  top: calc(100% - 1.66666667em); }\n\n.mat-grid-tile-header,\n.mat-grid-tile-footer {\n  font-size: 14px; }\n  .mat-grid-tile-header .mat-line,\n  .mat-grid-tile-footer .mat-line {\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    display: block;\n    box-sizing: border-box; }\n    .mat-grid-tile-header .mat-line:nth-child(n+2),\n    .mat-grid-tile-footer .mat-line:nth-child(n+2) {\n      font-size: 12px; }\n\ninput.mat-input-element {\n  margin-top: -0.0625em; }\n\n.mat-menu-item {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 16px;\n  font-weight: 400; }\n\n.mat-paginator,\n.mat-paginator-page-size .mat-select-trigger {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 12px; }\n\n.mat-radio-button {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-select {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-select-trigger {\n  height: 1.125em; }\n\n.mat-slide-toggle-content {\n  font: 400 14px/20px Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-slider-thumb-label-text {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 12px;\n  font-weight: 500; }\n\n.mat-stepper-vertical, .mat-stepper-horizontal {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-step-label {\n  font-size: 14px;\n  font-weight: 400; }\n\n.mat-step-label-selected {\n  font-size: 14px;\n  font-weight: 500; }\n\n.mat-tab-group {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-tab-label, .mat-tab-link {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 14px;\n  font-weight: 500; }\n\n.mat-toolbar,\n.mat-toolbar h1,\n.mat-toolbar h2,\n.mat-toolbar h3,\n.mat-toolbar h4,\n.mat-toolbar h5,\n.mat-toolbar h6 {\n  font: 500 20px/32px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0; }\n\n.mat-tooltip {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 10px;\n  padding-top: 6px;\n  padding-bottom: 6px; }\n\n.mat-list-item {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-list-option {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-list .mat-list-item, .mat-nav-list .mat-list-item, .mat-selection-list .mat-list-item {\n  font-size: 16px; }\n  .mat-list .mat-list-item .mat-line, .mat-nav-list .mat-list-item .mat-line, .mat-selection-list .mat-list-item .mat-line {\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    display: block;\n    box-sizing: border-box; }\n    .mat-list .mat-list-item .mat-line:nth-child(n+2), .mat-nav-list .mat-list-item .mat-line:nth-child(n+2), .mat-selection-list .mat-list-item .mat-line:nth-child(n+2) {\n      font-size: 14px; }\n\n.mat-list .mat-list-option, .mat-nav-list .mat-list-option, .mat-selection-list .mat-list-option {\n  font-size: 16px; }\n  .mat-list .mat-list-option .mat-line, .mat-nav-list .mat-list-option .mat-line, .mat-selection-list .mat-list-option .mat-line {\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    display: block;\n    box-sizing: border-box; }\n    .mat-list .mat-list-option .mat-line:nth-child(n+2), .mat-nav-list .mat-list-option .mat-line:nth-child(n+2), .mat-selection-list .mat-list-option .mat-line:nth-child(n+2) {\n      font-size: 14px; }\n\n.mat-list .mat-subheader, .mat-nav-list .mat-subheader, .mat-selection-list .mat-subheader {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 14px;\n  font-weight: 500; }\n\n.mat-list[dense] .mat-list-item, .mat-nav-list[dense] .mat-list-item, .mat-selection-list[dense] .mat-list-item {\n  font-size: 12px; }\n  .mat-list[dense] .mat-list-item .mat-line, .mat-nav-list[dense] .mat-list-item .mat-line, .mat-selection-list[dense] .mat-list-item .mat-line {\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    display: block;\n    box-sizing: border-box; }\n    .mat-list[dense] .mat-list-item .mat-line:nth-child(n+2), .mat-nav-list[dense] .mat-list-item .mat-line:nth-child(n+2), .mat-selection-list[dense] .mat-list-item .mat-line:nth-child(n+2) {\n      font-size: 12px; }\n\n.mat-list[dense] .mat-list-option, .mat-nav-list[dense] .mat-list-option, .mat-selection-list[dense] .mat-list-option {\n  font-size: 12px; }\n  .mat-list[dense] .mat-list-option .mat-line, .mat-nav-list[dense] .mat-list-option .mat-line, .mat-selection-list[dense] .mat-list-option .mat-line {\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    display: block;\n    box-sizing: border-box; }\n    .mat-list[dense] .mat-list-option .mat-line:nth-child(n+2), .mat-nav-list[dense] .mat-list-option .mat-line:nth-child(n+2), .mat-selection-list[dense] .mat-list-option .mat-line:nth-child(n+2) {\n      font-size: 12px; }\n\n.mat-list[dense] .mat-subheader, .mat-nav-list[dense] .mat-subheader, .mat-selection-list[dense] .mat-subheader {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 12px;\n  font-weight: 500; }\n\n.mat-option {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 16px; }\n\n.mat-optgroup-label {\n  font: 500 14px/24px Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-simple-snackbar {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 14px; }\n\n.mat-simple-snackbar-action {\n  line-height: 1;\n  font-family: inherit;\n  font-size: inherit;\n  font-weight: 500; }\n\n.mat-ripple {\n  overflow: hidden; }\n  @media screen and (-ms-high-contrast: active) {\n    .mat-ripple {\n      display: none; } }\n\n.mat-ripple.mat-ripple-unbounded {\n  overflow: visible; }\n\n.mat-ripple-element {\n  position: absolute;\n  border-radius: 50%;\n  pointer-events: none;\n  transition: opacity, transform 0ms cubic-bezier(0, 0, 0.2, 1);\n  transform: scale(0); }\n\n.mat-option {\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  display: block;\n  line-height: 48px;\n  height: 48px;\n  padding: 0 16px;\n  text-align: left;\n  text-decoration: none;\n  position: relative;\n  cursor: pointer;\n  outline: none;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  max-width: 100%;\n  box-sizing: border-box;\n  -ms-flex-align: center;\n      align-items: center; }\n  .mat-option[disabled] {\n    cursor: default; }\n  [dir='rtl'] .mat-option {\n    text-align: right; }\n  .mat-option .mat-icon {\n    margin-right: 16px; }\n    [dir='rtl'] .mat-option .mat-icon {\n      margin-left: 16px;\n      margin-right: 0; }\n  .mat-option[aria-disabled='true'] {\n    -webkit-user-select: none;\n    -moz-user-select: none;\n    -ms-user-select: none;\n    user-select: none;\n    cursor: default; }\n  .mat-optgroup .mat-option:not(.mat-option-multiple) {\n    padding-left: 32px; }\n    [dir='rtl'] .mat-optgroup .mat-option:not(.mat-option-multiple) {\n      padding-left: 16px;\n      padding-right: 32px; }\n\n.mat-option-text {\n  display: inline-block;\n  -ms-flex-positive: 1;\n      flex-grow: 1;\n  overflow: hidden;\n  text-overflow: ellipsis; }\n\n.mat-option-ripple {\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  position: absolute;\n  pointer-events: none; }\n  @media screen and (-ms-high-contrast: active) {\n    .mat-option-ripple {\n      opacity: 0.5; } }\n\n.mat-option-pseudo-checkbox {\n  margin-right: 8px; }\n  [dir='rtl'] .mat-option-pseudo-checkbox {\n    margin-left: 8px;\n    margin-right: 0; }\n\n.mat-optgroup-label {\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  display: block;\n  line-height: 48px;\n  height: 48px;\n  padding: 0 16px;\n  text-align: left;\n  text-decoration: none;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n  cursor: default; }\n  .mat-optgroup-label[disabled] {\n    cursor: default; }\n  [dir='rtl'] .mat-optgroup-label {\n    text-align: right; }\n  .mat-optgroup-label .mat-icon {\n    margin-right: 16px; }\n    [dir='rtl'] .mat-optgroup-label .mat-icon {\n      margin-left: 16px;\n      margin-right: 0; }\n\n.cdk-visually-hidden {\n  border: 0;\n  clip: rect(0 0 0 0);\n  height: 1px;\n  margin: -1px;\n  overflow: hidden;\n  padding: 0;\n  position: absolute;\n  width: 1px; }\n\n.cdk-overlay-container, .cdk-global-overlay-wrapper {\n  pointer-events: none;\n  top: 0;\n  left: 0;\n  height: 100%;\n  width: 100%; }\n\n.cdk-overlay-container {\n  position: fixed;\n  z-index: 1000; }\n\n.cdk-global-overlay-wrapper {\n  display: -ms-flexbox;\n  display: flex;\n  position: absolute;\n  z-index: 1000; }\n\n.cdk-overlay-pane {\n  position: absolute;\n  pointer-events: auto;\n  box-sizing: border-box;\n  z-index: 1000; }\n\n.cdk-overlay-backdrop {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  z-index: 1000;\n  pointer-events: auto;\n  -webkit-tap-highlight-color: transparent;\n  transition: opacity 400ms cubic-bezier(0.25, 0.8, 0.25, 1);\n  opacity: 0; }\n  .cdk-overlay-backdrop.cdk-overlay-backdrop-showing {\n    opacity: 0.48; }\n\n.cdk-overlay-dark-backdrop {\n  background: rgba(0, 0, 0, 0.6); }\n\n.cdk-overlay-transparent-backdrop {\n  background: none; }\n\n.cdk-global-scrollblock {\n  position: fixed;\n  width: 100%;\n  overflow-y: scroll; }\n\n.mat-ripple-element {\n  background-color: rgba(0, 0, 0, 0.1); }\n\n.mat-option {\n  color: rgba(0, 0, 0, 0.87); }\n  .mat-option:hover:not(.mat-option-disabled), .mat-option:focus:not(.mat-option-disabled) {\n    background: rgba(0, 0, 0, 0.04); }\n  .mat-primary .mat-option.mat-selected:not(.mat-option-disabled) {\n    color: #455a64; }\n  .mat-accent .mat-option.mat-selected:not(.mat-option-disabled) {\n    color: #2196f3; }\n  .mat-warn .mat-option.mat-selected:not(.mat-option-disabled) {\n    color: #f44336; }\n  .mat-option.mat-selected:not(.mat-option-multiple):not(.mat-option-disabled) {\n    background: rgba(0, 0, 0, 0.04); }\n  .mat-option.mat-active {\n    background: rgba(0, 0, 0, 0.04);\n    color: rgba(0, 0, 0, 0.87); }\n  .mat-option.mat-option-disabled {\n    color: rgba(0, 0, 0, 0.38); }\n\n.mat-optgroup-label {\n  color: rgba(0, 0, 0, 0.54); }\n\n.mat-optgroup-disabled .mat-optgroup-label {\n  color: rgba(0, 0, 0, 0.38); }\n\n.mat-pseudo-checkbox {\n  color: rgba(0, 0, 0, 0.54); }\n  .mat-pseudo-checkbox::after {\n    color: #fafafa; }\n\n.mat-pseudo-checkbox-checked,\n.mat-pseudo-checkbox-indeterminate,\n.mat-accent .mat-pseudo-checkbox-checked,\n.mat-accent .mat-pseudo-checkbox-indeterminate {\n  background: #2196f3; }\n\n.mat-primary .mat-pseudo-checkbox-checked,\n.mat-primary .mat-pseudo-checkbox-indeterminate {\n  background: #455a64; }\n\n.mat-warn .mat-pseudo-checkbox-checked,\n.mat-warn .mat-pseudo-checkbox-indeterminate {\n  background: #f44336; }\n\n.mat-pseudo-checkbox-checked.mat-pseudo-checkbox-disabled,\n.mat-pseudo-checkbox-indeterminate.mat-pseudo-checkbox-disabled {\n  background: #b0b0b0; }\n\n.mat-app-background {\n  background-color: #fafafa; }\n\n.mat-theme-loaded-marker {\n  display: none; }\n\n.mat-autocomplete-panel {\n  background: white;\n  color: rgba(0, 0, 0, 0.87); }\n  .mat-autocomplete-panel .mat-option.mat-selected:not(.mat-active):not(:hover) {\n    background: white; }\n    .mat-autocomplete-panel .mat-option.mat-selected:not(.mat-active):not(:hover):not(.mat-option-disabled) {\n      color: rgba(0, 0, 0, 0.87); }\n\n.mat-button, .mat-icon-button {\n  background: transparent; }\n  .mat-button.mat-primary .mat-button-focus-overlay, .mat-icon-button.mat-primary .mat-button-focus-overlay {\n    background-color: rgba(69, 90, 100, 0.12); }\n  .mat-button.mat-accent .mat-button-focus-overlay, .mat-icon-button.mat-accent .mat-button-focus-overlay {\n    background-color: rgba(33, 150, 243, 0.12); }\n  .mat-button.mat-warn .mat-button-focus-overlay, .mat-icon-button.mat-warn .mat-button-focus-overlay {\n    background-color: rgba(244, 67, 54, 0.12); }\n  .mat-button[disabled] .mat-button-focus-overlay, .mat-icon-button[disabled] .mat-button-focus-overlay {\n    background-color: transparent; }\n  .mat-button.mat-primary, .mat-icon-button.mat-primary {\n    color: #455a64; }\n  .mat-button.mat-accent, .mat-icon-button.mat-accent {\n    color: #2196f3; }\n  .mat-button.mat-warn, .mat-icon-button.mat-warn {\n    color: #f44336; }\n  .mat-button.mat-primary[disabled], .mat-button.mat-accent[disabled], .mat-button.mat-warn[disabled], .mat-button[disabled][disabled], .mat-icon-button.mat-primary[disabled], .mat-icon-button.mat-accent[disabled], .mat-icon-button.mat-warn[disabled], .mat-icon-button[disabled][disabled] {\n    color: rgba(0, 0, 0, 0.38); }\n\n.mat-raised-button, .mat-fab, .mat-mini-fab {\n  color: rgba(0, 0, 0, 0.87);\n  background-color: white; }\n  .mat-raised-button.mat-primary, .mat-fab.mat-primary, .mat-mini-fab.mat-primary {\n    color: rgba(255, 255, 255, 0.87); }\n  .mat-raised-button.mat-accent, .mat-fab.mat-accent, .mat-mini-fab.mat-accent {\n    color: white; }\n  .mat-raised-button.mat-warn, .mat-fab.mat-warn, .mat-mini-fab.mat-warn {\n    color: white; }\n  .mat-raised-button.mat-primary[disabled], .mat-raised-button.mat-accent[disabled], .mat-raised-button.mat-warn[disabled], .mat-raised-button[disabled][disabled], .mat-fab.mat-primary[disabled], .mat-fab.mat-accent[disabled], .mat-fab.mat-warn[disabled], .mat-fab[disabled][disabled], .mat-mini-fab.mat-primary[disabled], .mat-mini-fab.mat-accent[disabled], .mat-mini-fab.mat-warn[disabled], .mat-mini-fab[disabled][disabled] {\n    color: rgba(0, 0, 0, 0.38); }\n  .mat-raised-button.mat-primary, .mat-fab.mat-primary, .mat-mini-fab.mat-primary {\n    background-color: #455a64; }\n  .mat-raised-button.mat-accent, .mat-fab.mat-accent, .mat-mini-fab.mat-accent {\n    background-color: #2196f3; }\n  .mat-raised-button.mat-warn, .mat-fab.mat-warn, .mat-mini-fab.mat-warn {\n    background-color: #f44336; }\n  .mat-raised-button.mat-primary[disabled], .mat-raised-button.mat-accent[disabled], .mat-raised-button.mat-warn[disabled], .mat-raised-button[disabled][disabled], .mat-fab.mat-primary[disabled], .mat-fab.mat-accent[disabled], .mat-fab.mat-warn[disabled], .mat-fab[disabled][disabled], .mat-mini-fab.mat-primary[disabled], .mat-mini-fab.mat-accent[disabled], .mat-mini-fab.mat-warn[disabled], .mat-mini-fab[disabled][disabled] {\n    background-color: rgba(0, 0, 0, 0.12); }\n  .mat-raised-button.mat-primary .mat-ripple-element, .mat-fab.mat-primary .mat-ripple-element, .mat-mini-fab.mat-primary .mat-ripple-element {\n    background-color: rgba(255, 255, 255, 0.2); }\n  .mat-raised-button.mat-accent .mat-ripple-element, .mat-fab.mat-accent .mat-ripple-element, .mat-mini-fab.mat-accent .mat-ripple-element {\n    background-color: rgba(255, 255, 255, 0.2); }\n  .mat-raised-button.mat-warn .mat-ripple-element, .mat-fab.mat-warn .mat-ripple-element, .mat-mini-fab.mat-warn .mat-ripple-element {\n    background-color: rgba(255, 255, 255, 0.2); }\n\n.mat-button.mat-primary .mat-ripple-element {\n  background-color: rgba(69, 90, 100, 0.1); }\n\n.mat-button.mat-accent .mat-ripple-element {\n  background-color: rgba(33, 150, 243, 0.1); }\n\n.mat-button.mat-warn .mat-ripple-element {\n  background-color: rgba(244, 67, 54, 0.1); }\n\n.mat-icon-button.mat-primary .mat-ripple-element {\n  background-color: rgba(69, 90, 100, 0.2); }\n\n.mat-icon-button.mat-accent .mat-ripple-element {\n  background-color: rgba(33, 150, 243, 0.2); }\n\n.mat-icon-button.mat-warn .mat-ripple-element {\n  background-color: rgba(244, 67, 54, 0.2); }\n\n.mat-button-toggle {\n  color: rgba(0, 0, 0, 0.38); }\n  .mat-button-toggle.cdk-focused .mat-button-toggle-focus-overlay {\n    background-color: rgba(0, 0, 0, 0.06); }\n\n.mat-button-toggle-checked {\n  background-color: #e0e0e0;\n  color: rgba(0, 0, 0, 0.54); }\n\n.mat-button-toggle-disabled {\n  background-color: #eeeeee;\n  color: rgba(0, 0, 0, 0.38); }\n  .mat-button-toggle-disabled.mat-button-toggle-checked {\n    background-color: #bdbdbd; }\n\n.mat-card {\n  background: white;\n  color: rgba(0, 0, 0, 0.87); }\n\n.mat-card-subtitle {\n  color: rgba(0, 0, 0, 0.54); }\n\n.mat-checkbox-frame {\n  border-color: rgba(0, 0, 0, 0.54); }\n\n.mat-checkbox-checkmark {\n  fill: #fafafa; }\n\n.mat-checkbox-checkmark-path {\n  stroke: #fafafa !important; }\n\n.mat-checkbox-mixedmark {\n  background-color: #fafafa; }\n\n.mat-checkbox-indeterminate.mat-primary .mat-checkbox-background, .mat-checkbox-checked.mat-primary .mat-checkbox-background {\n  background-color: #455a64; }\n\n.mat-checkbox-indeterminate.mat-accent .mat-checkbox-background, .mat-checkbox-checked.mat-accent .mat-checkbox-background {\n  background-color: #2196f3; }\n\n.mat-checkbox-indeterminate.mat-warn .mat-checkbox-background, .mat-checkbox-checked.mat-warn .mat-checkbox-background {\n  background-color: #f44336; }\n\n.mat-checkbox-disabled.mat-checkbox-checked .mat-checkbox-background, .mat-checkbox-disabled.mat-checkbox-indeterminate .mat-checkbox-background {\n  background-color: #b0b0b0; }\n\n.mat-checkbox-disabled:not(.mat-checkbox-checked) .mat-checkbox-frame {\n  border-color: #b0b0b0; }\n\n.mat-checkbox-disabled .mat-checkbox-label {\n  color: #b0b0b0; }\n\n.mat-checkbox:not(.mat-checkbox-disabled).mat-primary .mat-checkbox-ripple .mat-ripple-element {\n  background-color: rgba(69, 90, 100, 0.26); }\n\n.mat-checkbox:not(.mat-checkbox-disabled).mat-accent .mat-checkbox-ripple .mat-ripple-element {\n  background-color: rgba(33, 150, 243, 0.26); }\n\n.mat-checkbox:not(.mat-checkbox-disabled).mat-warn .mat-checkbox-ripple .mat-ripple-element {\n  background-color: rgba(244, 67, 54, 0.26); }\n\n.mat-chip:not(.mat-basic-chip) {\n  background-color: #e0e0e0;\n  color: rgba(0, 0, 0, 0.87); }\n  .mat-chip:not(.mat-basic-chip) .mat-chip-remove {\n    color: rgba(0, 0, 0, 0.87);\n    opacity: 0.4; }\n  .mat-chip:not(.mat-basic-chip) .mat-chip-remove:hover {\n    opacity: 0.54; }\n\n.mat-chip.mat-chip-selected.mat-primary {\n  background-color: #455a64;\n  color: rgba(255, 255, 255, 0.87); }\n  .mat-chip.mat-chip-selected.mat-primary .mat-chip-remove {\n    color: rgba(255, 255, 255, 0.87);\n    opacity: 0.4; }\n  .mat-chip.mat-chip-selected.mat-primary .mat-chip-remove:hover {\n    opacity: 0.54; }\n\n.mat-chip.mat-chip-selected.mat-warn {\n  background-color: #f44336;\n  color: white; }\n  .mat-chip.mat-chip-selected.mat-warn .mat-chip-remove {\n    color: white;\n    opacity: 0.4; }\n  .mat-chip.mat-chip-selected.mat-warn .mat-chip-remove:hover {\n    opacity: 0.54; }\n\n.mat-chip.mat-chip-selected.mat-accent {\n  background-color: #2196f3;\n  color: white; }\n  .mat-chip.mat-chip-selected.mat-accent .mat-chip-remove {\n    color: white;\n    opacity: 0.4; }\n  .mat-chip.mat-chip-selected.mat-accent .mat-chip-remove:hover {\n    opacity: 0.54; }\n\n.mat-table {\n  background: white; }\n\n.mat-row, .mat-header-row {\n  border-bottom-color: rgba(0, 0, 0, 0.12); }\n\n.mat-header-cell {\n  color: rgba(0, 0, 0, 0.54); }\n\n.mat-cell {\n  color: rgba(0, 0, 0, 0.87); }\n\n.mat-datepicker-content {\n  background-color: white;\n  color: rgba(0, 0, 0, 0.87); }\n\n.mat-calendar-arrow {\n  border-top-color: rgba(0, 0, 0, 0.54); }\n\n.mat-calendar-next-button,\n.mat-calendar-previous-button {\n  color: rgba(0, 0, 0, 0.54); }\n\n.mat-calendar-table-header {\n  color: rgba(0, 0, 0, 0.38); }\n\n.mat-calendar-table-header-divider::after {\n  background: rgba(0, 0, 0, 0.12); }\n\n.mat-calendar-body-label {\n  color: rgba(0, 0, 0, 0.54); }\n\n.mat-calendar-body-cell-content {\n  color: rgba(0, 0, 0, 0.87);\n  border-color: transparent; }\n  .mat-calendar-body-disabled > .mat-calendar-body-cell-content:not(.mat-calendar-body-selected) {\n    color: rgba(0, 0, 0, 0.38); }\n\n:not(.mat-calendar-body-disabled):hover > .mat-calendar-body-cell-content:not(.mat-calendar-body-selected),\n.cdk-keyboard-focused .mat-calendar-body-active > .mat-calendar-body-cell-content:not(.mat-calendar-body-selected),\n.cdk-program-focused .mat-calendar-body-active > .mat-calendar-body-cell-content:not(.mat-calendar-body-selected) {\n  background-color: rgba(0, 0, 0, 0.04); }\n\n.mat-calendar-body-selected {\n  background-color: #455a64;\n  color: rgba(255, 255, 255, 0.87); }\n\n.mat-calendar-body-disabled > .mat-calendar-body-selected {\n  background-color: rgba(69, 90, 100, 0.4); }\n\n.mat-calendar-body-today:not(.mat-calendar-body-selected) {\n  border-color: rgba(0, 0, 0, 0.38); }\n\n.mat-calendar-body-today.mat-calendar-body-selected {\n  box-shadow: inset 0 0 0 1px rgba(255, 255, 255, 0.87); }\n\n.mat-calendar-body-disabled > .mat-calendar-body-today:not(.mat-calendar-body-selected) {\n  border-color: rgba(0, 0, 0, 0.18); }\n\n.mat-dialog-container {\n  background: white;\n  color: rgba(0, 0, 0, 0.87); }\n\n.mat-expansion-panel {\n  background: white;\n  color: rgba(0, 0, 0, 0.87); }\n\n.mat-action-row {\n  border-top-color: rgba(0, 0, 0, 0.12); }\n\n.mat-expansion-panel:not(.mat-expanded) .mat-expansion-panel-header:not([aria-disabled='true']).cdk-keyboard-focused, .mat-expansion-panel:not(.mat-expanded) .mat-expansion-panel-header:not([aria-disabled='true']).cdk-program-focused, .mat-expansion-panel:not(.mat-expanded) .mat-expansion-panel-header:not([aria-disabled='true']):hover {\n  background: rgba(0, 0, 0, 0.04); }\n\n.mat-expansion-panel-header-title {\n  color: rgba(0, 0, 0, 0.87); }\n\n.mat-expansion-panel-header-description,\n.mat-expansion-indicator::after {\n  color: rgba(0, 0, 0, 0.54); }\n\n.mat-expansion-panel-header[aria-disabled='true'] {\n  color: rgba(0, 0, 0, 0.38); }\n  .mat-expansion-panel-header[aria-disabled='true'] .mat-expansion-panel-header-title,\n  .mat-expansion-panel-header[aria-disabled='true'] .mat-expansion-panel-header-description {\n    color: inherit; }\n\n.mat-form-field-label {\n  color: rgba(0, 0, 0, 0.54); }\n\n.mat-hint {\n  color: rgba(0, 0, 0, 0.54); }\n\n.mat-focused .mat-form-field-label {\n  color: #455a64; }\n  .mat-focused .mat-form-field-label.mat-accent {\n    color: #2196f3; }\n  .mat-focused .mat-form-field-label.mat-warn {\n    color: #f44336; }\n\n.mat-focused .mat-form-field-required-marker {\n  color: #2196f3; }\n\n.mat-form-field-underline {\n  background-color: rgba(0, 0, 0, 0.42); }\n\n.mat-form-field-disabled .mat-form-field-underline {\n  background-image: linear-gradient(to right, rgba(0, 0, 0, 0.42) 0%, rgba(0, 0, 0, 0.42) 33%, transparent 0%);\n  background-size: 4px 1px;\n  background-repeat: repeat-x; }\n\n.mat-form-field-ripple {\n  background-color: #455a64; }\n  .mat-form-field-ripple.mat-accent {\n    background-color: #2196f3; }\n  .mat-form-field-ripple.mat-warn {\n    background-color: #f44336; }\n\n.mat-form-field-invalid .mat-form-field-label {\n  color: #f44336; }\n  .mat-form-field-invalid .mat-form-field-label.mat-accent,\n  .mat-form-field-invalid .mat-form-field-label .mat-form-field-required-marker {\n    color: #f44336; }\n\n.mat-form-field-invalid .mat-form-field-ripple {\n  background-color: #f44336; }\n\n.mat-error {\n  color: #f44336; }\n\n.mat-icon.mat-primary {\n  color: #455a64; }\n\n.mat-icon.mat-accent {\n  color: #2196f3; }\n\n.mat-icon.mat-warn {\n  color: #f44336; }\n\n.mat-input-element:disabled {\n  color: rgba(0, 0, 0, 0.38); }\n\n.mat-input-element::-webkit-input-placeholder {\n  color: rgba(0, 0, 0, 0.42); }\n\n.mat-input-element:-ms-input-placeholder {\n  color: rgba(0, 0, 0, 0.42); }\n\n.mat-input-element::placeholder {\n  color: rgba(0, 0, 0, 0.42); }\n\n.mat-input-element::-moz-placeholder {\n  color: rgba(0, 0, 0, 0.42); }\n\n.mat-input-element::-webkit-input-placeholder {\n  color: rgba(0, 0, 0, 0.42); }\n\n.mat-input-element:-ms-input-placeholder {\n  color: rgba(0, 0, 0, 0.42); }\n\n.mat-list .mat-list-item, .mat-nav-list .mat-list-item, .mat-selection-list .mat-list-item {\n  color: rgba(0, 0, 0, 0.87); }\n\n.mat-list .mat-list-option, .mat-nav-list .mat-list-option, .mat-selection-list .mat-list-option {\n  color: rgba(0, 0, 0, 0.87); }\n\n.mat-list .mat-subheader, .mat-nav-list .mat-subheader, .mat-selection-list .mat-subheader {\n  color: rgba(0, 0, 0, 0.54); }\n\n.mat-list-item-disabled {\n  background-color: #eeeeee; }\n\n.mat-divider {\n  border-top-color: rgba(0, 0, 0, 0.12); }\n\n.mat-nav-list .mat-list-item {\n  outline: none; }\n  .mat-nav-list .mat-list-item:hover, .mat-nav-list .mat-list-item.mat-list-item-focus {\n    background: rgba(0, 0, 0, 0.04); }\n\n.mat-list-option {\n  outline: none; }\n  .mat-list-option:hover, .mat-list-option.mat-list-item-focus {\n    background: rgba(0, 0, 0, 0.04); }\n\n.mat-menu-panel {\n  background: white; }\n\n.mat-menu-item {\n  background: transparent;\n  color: rgba(0, 0, 0, 0.87); }\n  .mat-menu-item[disabled] {\n    color: rgba(0, 0, 0, 0.38); }\n\n.mat-menu-item .mat-icon:not([color]),\n.mat-menu-item-submenu-trigger::after {\n  color: rgba(0, 0, 0, 0.54); }\n\n.mat-menu-item:hover:not([disabled]),\n.mat-menu-item:focus:not([disabled]),\n.mat-menu-item-highlighted:not([disabled]) {\n  background: rgba(0, 0, 0, 0.04); }\n\n.mat-paginator {\n  background: white; }\n\n.mat-paginator,\n.mat-paginator-page-size .mat-select-trigger {\n  color: rgba(0, 0, 0, 0.54); }\n\n.mat-paginator-increment,\n.mat-paginator-decrement {\n  border-top: 2px solid rgba(0, 0, 0, 0.54);\n  border-right: 2px solid rgba(0, 0, 0, 0.54); }\n\n.mat-icon-button[disabled] .mat-paginator-increment,\n.mat-icon-button[disabled] .mat-paginator-decrement {\n  border-color: rgba(0, 0, 0, 0.38); }\n\n.mat-progress-bar-background {\n  background-image: url(\"data:image/svg+xml;charset=UTF-8,%3Csvg%20version%3D%271.1%27%20xmlns%3D%27http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%27%20xmlns%3Axlink%3D%27http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%27%20x%3D%270px%27%20y%3D%270px%27%20enable-background%3D%27new%200%200%205%202%27%20xml%3Aspace%3D%27preserve%27%20viewBox%3D%270%200%205%202%27%20preserveAspectRatio%3D%27none%20slice%27%3E%3Ccircle%20cx%3D%271%27%20cy%3D%271%27%20r%3D%271%27%20fill%3D%27%23607d8b%27%2F%3E%3C%2Fsvg%3E\"); }\n\n.mat-progress-bar-buffer {\n  background-color: #607d8b; }\n\n.mat-progress-bar-fill::after {\n  background-color: #455a64; }\n\n.mat-progress-bar.mat-accent .mat-progress-bar-background {\n  background-image: url(\"data:image/svg+xml;charset=UTF-8,%3Csvg%20version%3D%271.1%27%20xmlns%3D%27http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%27%20xmlns%3Axlink%3D%27http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%27%20x%3D%270px%27%20y%3D%270px%27%20enable-background%3D%27new%200%200%205%202%27%20xml%3Aspace%3D%27preserve%27%20viewBox%3D%270%200%205%202%27%20preserveAspectRatio%3D%27none%20slice%27%3E%3Ccircle%20cx%3D%271%27%20cy%3D%271%27%20r%3D%271%27%20fill%3D%27%23bbdefb%27%2F%3E%3C%2Fsvg%3E\"); }\n\n.mat-progress-bar.mat-accent .mat-progress-bar-buffer {\n  background-color: #bbdefb; }\n\n.mat-progress-bar.mat-accent .mat-progress-bar-fill::after {\n  background-color: #2196f3; }\n\n.mat-progress-bar.mat-warn .mat-progress-bar-background {\n  background-image: url(\"data:image/svg+xml;charset=UTF-8,%3Csvg%20version%3D%271.1%27%20xmlns%3D%27http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%27%20xmlns%3Axlink%3D%27http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%27%20x%3D%270px%27%20y%3D%270px%27%20enable-background%3D%27new%200%200%205%202%27%20xml%3Aspace%3D%27preserve%27%20viewBox%3D%270%200%205%202%27%20preserveAspectRatio%3D%27none%20slice%27%3E%3Ccircle%20cx%3D%271%27%20cy%3D%271%27%20r%3D%271%27%20fill%3D%27%23ffcdd2%27%2F%3E%3C%2Fsvg%3E\"); }\n\n.mat-progress-bar.mat-warn .mat-progress-bar-buffer {\n  background-color: #ffcdd2; }\n\n.mat-progress-bar.mat-warn .mat-progress-bar-fill::after {\n  background-color: #f44336; }\n\n.mat-progress-spinner circle, .mat-spinner circle {\n  stroke: #455a64; }\n\n.mat-progress-spinner.mat-accent circle, .mat-spinner.mat-accent circle {\n  stroke: #2196f3; }\n\n.mat-progress-spinner.mat-warn circle, .mat-spinner.mat-warn circle {\n  stroke: #f44336; }\n\n.mat-radio-outer-circle {\n  border-color: rgba(0, 0, 0, 0.54); }\n\n.mat-radio-disabled .mat-radio-outer-circle {\n  border-color: rgba(0, 0, 0, 0.38); }\n\n.mat-radio-disabled .mat-radio-ripple .mat-ripple-element, .mat-radio-disabled .mat-radio-inner-circle {\n  background-color: rgba(0, 0, 0, 0.38); }\n\n.mat-radio-disabled .mat-radio-label-content {\n  color: rgba(0, 0, 0, 0.38); }\n\n.mat-radio-button.mat-primary.mat-radio-checked .mat-radio-outer-circle {\n  border-color: #455a64; }\n\n.mat-radio-button.mat-primary .mat-radio-inner-circle {\n  background-color: #455a64; }\n\n.mat-radio-button.mat-primary .mat-radio-ripple .mat-ripple-element {\n  background-color: rgba(69, 90, 100, 0.26); }\n\n.mat-radio-button.mat-accent.mat-radio-checked .mat-radio-outer-circle {\n  border-color: #2196f3; }\n\n.mat-radio-button.mat-accent .mat-radio-inner-circle {\n  background-color: #2196f3; }\n\n.mat-radio-button.mat-accent .mat-radio-ripple .mat-ripple-element {\n  background-color: rgba(33, 150, 243, 0.26); }\n\n.mat-radio-button.mat-warn.mat-radio-checked .mat-radio-outer-circle {\n  border-color: #f44336; }\n\n.mat-radio-button.mat-warn .mat-radio-inner-circle {\n  background-color: #f44336; }\n\n.mat-radio-button.mat-warn .mat-radio-ripple .mat-ripple-element {\n  background-color: rgba(244, 67, 54, 0.26); }\n\n.mat-select-content, .mat-select-panel-done-animating {\n  background: white; }\n\n.mat-select-value {\n  color: rgba(0, 0, 0, 0.87); }\n\n.mat-select-placeholder {\n  color: rgba(0, 0, 0, 0.42); }\n\n.mat-select-disabled .mat-select-value {\n  color: rgba(0, 0, 0, 0.38); }\n\n.mat-select-arrow {\n  color: rgba(0, 0, 0, 0.54); }\n\n.mat-select-panel .mat-option.mat-selected:not(.mat-option-multiple) {\n  background: rgba(0, 0, 0, 0.12); }\n\n.mat-form-field.mat-focused.mat-primary .mat-select-arrow {\n  color: #455a64; }\n\n.mat-form-field.mat-focused.mat-accent .mat-select-arrow {\n  color: #2196f3; }\n\n.mat-form-field.mat-focused.mat-warn .mat-select-arrow {\n  color: #f44336; }\n\n.mat-form-field .mat-select.mat-select-invalid .mat-select-arrow {\n  color: #f44336; }\n\n.mat-form-field .mat-select.mat-select-disabled .mat-select-arrow {\n  color: rgba(0, 0, 0, 0.38); }\n\n.mat-drawer-container {\n  background-color: #fafafa;\n  color: rgba(0, 0, 0, 0.87); }\n\n.mat-drawer {\n  background-color: white;\n  color: rgba(0, 0, 0, 0.87); }\n  .mat-drawer.mat-drawer-push {\n    background-color: white; }\n\n.mat-drawer-backdrop.mat-drawer-shown {\n  background-color: rgba(0, 0, 0, 0.6); }\n\n.mat-slide-toggle.mat-checked:not(.mat-disabled) .mat-slide-toggle-thumb {\n  background-color: #2196f3; }\n\n.mat-slide-toggle.mat-checked:not(.mat-disabled) .mat-slide-toggle-bar {\n  background-color: rgba(33, 150, 243, 0.5); }\n\n.mat-slide-toggle:not(.mat-checked) .mat-ripple-element {\n  background-color: rgba(0, 0, 0, 0.06); }\n\n.mat-slide-toggle .mat-ripple-element {\n  background-color: rgba(33, 150, 243, 0.12); }\n\n.mat-slide-toggle.mat-primary.mat-checked:not(.mat-disabled) .mat-slide-toggle-thumb {\n  background-color: #607d8b; }\n\n.mat-slide-toggle.mat-primary.mat-checked:not(.mat-disabled) .mat-slide-toggle-bar {\n  background-color: rgba(96, 125, 139, 0.5); }\n\n.mat-slide-toggle.mat-primary:not(.mat-checked) .mat-ripple-element {\n  background-color: rgba(0, 0, 0, 0.06); }\n\n.mat-slide-toggle.mat-primary .mat-ripple-element {\n  background-color: rgba(96, 125, 139, 0.12); }\n\n.mat-slide-toggle.mat-warn.mat-checked:not(.mat-disabled) .mat-slide-toggle-thumb {\n  background-color: #f44336; }\n\n.mat-slide-toggle.mat-warn.mat-checked:not(.mat-disabled) .mat-slide-toggle-bar {\n  background-color: rgba(244, 67, 54, 0.5); }\n\n.mat-slide-toggle.mat-warn:not(.mat-checked) .mat-ripple-element {\n  background-color: rgba(0, 0, 0, 0.06); }\n\n.mat-slide-toggle.mat-warn .mat-ripple-element {\n  background-color: rgba(244, 67, 54, 0.12); }\n\n.mat-disabled .mat-slide-toggle-thumb {\n  background-color: #bdbdbd; }\n\n.mat-disabled .mat-slide-toggle-bar {\n  background-color: rgba(0, 0, 0, 0.1); }\n\n.mat-slide-toggle-thumb {\n  background-color: #fafafa; }\n\n.mat-slide-toggle-bar {\n  background-color: rgba(0, 0, 0, 0.38); }\n\n.mat-slider-track-background {\n  background-color: rgba(0, 0, 0, 0.26); }\n\n.mat-primary .mat-slider-track-fill,\n.mat-primary .mat-slider-thumb,\n.mat-primary .mat-slider-thumb-label {\n  background-color: #455a64; }\n\n.mat-primary .mat-slider-thumb-label-text {\n  color: rgba(255, 255, 255, 0.87); }\n\n.mat-accent .mat-slider-track-fill,\n.mat-accent .mat-slider-thumb,\n.mat-accent .mat-slider-thumb-label {\n  background-color: #2196f3; }\n\n.mat-accent .mat-slider-thumb-label-text {\n  color: white; }\n\n.mat-warn .mat-slider-track-fill,\n.mat-warn .mat-slider-thumb,\n.mat-warn .mat-slider-thumb-label {\n  background-color: #f44336; }\n\n.mat-warn .mat-slider-thumb-label-text {\n  color: white; }\n\n.mat-slider-focus-ring {\n  background-color: rgba(33, 150, 243, 0.2); }\n\n.mat-slider:hover .mat-slider-track-background,\n.cdk-focused .mat-slider-track-background {\n  background-color: rgba(0, 0, 0, 0.38); }\n\n.mat-slider-disabled .mat-slider-track-background,\n.mat-slider-disabled .mat-slider-track-fill,\n.mat-slider-disabled .mat-slider-thumb {\n  background-color: rgba(0, 0, 0, 0.26); }\n\n.mat-slider-disabled:hover .mat-slider-track-background {\n  background-color: rgba(0, 0, 0, 0.26); }\n\n.mat-slider-min-value .mat-slider-focus-ring {\n  background-color: rgba(0, 0, 0, 0.12); }\n\n.mat-slider-min-value.mat-slider-thumb-label-showing .mat-slider-thumb,\n.mat-slider-min-value.mat-slider-thumb-label-showing .mat-slider-thumb-label {\n  background-color: rgba(0, 0, 0, 0.87); }\n\n.mat-slider-min-value.mat-slider-thumb-label-showing.cdk-focused .mat-slider-thumb,\n.mat-slider-min-value.mat-slider-thumb-label-showing.cdk-focused .mat-slider-thumb-label {\n  background-color: rgba(0, 0, 0, 0.26); }\n\n.mat-slider-min-value:not(.mat-slider-thumb-label-showing) .mat-slider-thumb {\n  border-color: rgba(0, 0, 0, 0.26);\n  background-color: transparent; }\n\n.mat-slider-min-value:not(.mat-slider-thumb-label-showing):hover .mat-slider-thumb, .mat-slider-min-value:not(.mat-slider-thumb-label-showing).cdk-focused .mat-slider-thumb {\n  border-color: rgba(0, 0, 0, 0.38); }\n\n.mat-slider-min-value:not(.mat-slider-thumb-label-showing):hover.mat-slider-disabled .mat-slider-thumb, .mat-slider-min-value:not(.mat-slider-thumb-label-showing).cdk-focused.mat-slider-disabled .mat-slider-thumb {\n  border-color: rgba(0, 0, 0, 0.26); }\n\n.mat-slider-has-ticks .mat-slider-wrapper::after {\n  border-color: rgba(0, 0, 0, 0.7); }\n\n.mat-slider-horizontal .mat-slider-ticks {\n  background-image: repeating-linear-gradient(to right, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7) 2px, transparent 0, transparent);\n  background-image: -moz-repeating-linear-gradient(0.0001deg, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7) 2px, transparent 0, transparent); }\n\n.mat-slider-vertical .mat-slider-ticks {\n  background-image: repeating-linear-gradient(to bottom, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7) 2px, transparent 0, transparent); }\n\n.mat-step-header.cdk-keyboard-focused, .mat-step-header.cdk-program-focused, .mat-step-header:hover {\n  background-color: rgba(0, 0, 0, 0.04); }\n\n.mat-step-header .mat-step-label,\n.mat-step-header .mat-step-optional {\n  color: rgba(0, 0, 0, 0.38); }\n\n.mat-step-header .mat-step-icon {\n  background-color: #455a64;\n  color: rgba(255, 255, 255, 0.87); }\n\n.mat-step-header .mat-step-icon-not-touched {\n  background-color: rgba(0, 0, 0, 0.38);\n  color: rgba(255, 255, 255, 0.87); }\n\n.mat-step-header .mat-step-label.mat-step-label-active {\n  color: rgba(0, 0, 0, 0.87); }\n\n.mat-stepper-horizontal, .mat-stepper-vertical {\n  background-color: white; }\n\n.mat-stepper-vertical-line::before {\n  border-left-color: rgba(0, 0, 0, 0.12); }\n\n.mat-stepper-horizontal-line {\n  border-top-color: rgba(0, 0, 0, 0.12); }\n\n.mat-tab-nav-bar,\n.mat-tab-header {\n  border-bottom: 1px solid rgba(0, 0, 0, 0.12); }\n\n.mat-tab-group-inverted-header .mat-tab-nav-bar,\n.mat-tab-group-inverted-header .mat-tab-header {\n  border-top: 1px solid rgba(0, 0, 0, 0.12);\n  border-bottom: none; }\n\n.mat-tab-label, .mat-tab-link {\n  color: rgba(0, 0, 0, 0.87); }\n  .mat-tab-label.mat-tab-disabled, .mat-tab-link.mat-tab-disabled {\n    color: rgba(0, 0, 0, 0.38); }\n\n.mat-tab-header-pagination-chevron {\n  border-color: rgba(0, 0, 0, 0.87); }\n\n.mat-tab-header-pagination-disabled .mat-tab-header-pagination-chevron {\n  border-color: rgba(0, 0, 0, 0.38); }\n\n.mat-tab-group[class*='mat-background-'] .mat-tab-header,\n.mat-tab-nav-bar[class*='mat-background-'] {\n  border-bottom: none;\n  border-top: none; }\n\n.mat-tab-group.mat-primary .mat-tab-label:focus, .mat-tab-group.mat-primary .mat-tab-link:focus, .mat-tab-nav-bar.mat-primary .mat-tab-label:focus, .mat-tab-nav-bar.mat-primary .mat-tab-link:focus {\n  background-color: rgba(96, 125, 139, 0.3); }\n\n.mat-tab-group.mat-primary .mat-ink-bar, .mat-tab-nav-bar.mat-primary .mat-ink-bar {\n  background-color: #455a64; }\n\n.mat-tab-group.mat-primary.mat-background-primary .mat-ink-bar, .mat-tab-nav-bar.mat-primary.mat-background-primary .mat-ink-bar {\n  background-color: rgba(255, 255, 255, 0.87); }\n\n.mat-tab-group.mat-accent .mat-tab-label:focus, .mat-tab-group.mat-accent .mat-tab-link:focus, .mat-tab-nav-bar.mat-accent .mat-tab-label:focus, .mat-tab-nav-bar.mat-accent .mat-tab-link:focus {\n  background-color: rgba(187, 222, 251, 0.3); }\n\n.mat-tab-group.mat-accent .mat-ink-bar, .mat-tab-nav-bar.mat-accent .mat-ink-bar {\n  background-color: #2196f3; }\n\n.mat-tab-group.mat-accent.mat-background-accent .mat-ink-bar, .mat-tab-nav-bar.mat-accent.mat-background-accent .mat-ink-bar {\n  background-color: white; }\n\n.mat-tab-group.mat-warn .mat-tab-label:focus, .mat-tab-group.mat-warn .mat-tab-link:focus, .mat-tab-nav-bar.mat-warn .mat-tab-label:focus, .mat-tab-nav-bar.mat-warn .mat-tab-link:focus {\n  background-color: rgba(255, 205, 210, 0.3); }\n\n.mat-tab-group.mat-warn .mat-ink-bar, .mat-tab-nav-bar.mat-warn .mat-ink-bar {\n  background-color: #f44336; }\n\n.mat-tab-group.mat-warn.mat-background-warn .mat-ink-bar, .mat-tab-nav-bar.mat-warn.mat-background-warn .mat-ink-bar {\n  background-color: white; }\n\n.mat-tab-group.mat-background-primary .mat-tab-label:focus, .mat-tab-group.mat-background-primary .mat-tab-link:focus, .mat-tab-nav-bar.mat-background-primary .mat-tab-label:focus, .mat-tab-nav-bar.mat-background-primary .mat-tab-link:focus {\n  background-color: rgba(96, 125, 139, 0.3); }\n\n.mat-tab-group.mat-background-primary .mat-tab-header, .mat-tab-group.mat-background-primary .mat-tab-links, .mat-tab-nav-bar.mat-background-primary .mat-tab-header, .mat-tab-nav-bar.mat-background-primary .mat-tab-links {\n  background-color: #455a64; }\n\n.mat-tab-group.mat-background-primary .mat-tab-label, .mat-tab-group.mat-background-primary .mat-tab-link, .mat-tab-nav-bar.mat-background-primary .mat-tab-label, .mat-tab-nav-bar.mat-background-primary .mat-tab-link {\n  color: rgba(255, 255, 255, 0.87); }\n  .mat-tab-group.mat-background-primary .mat-tab-label.mat-tab-disabled, .mat-tab-group.mat-background-primary .mat-tab-link.mat-tab-disabled, .mat-tab-nav-bar.mat-background-primary .mat-tab-label.mat-tab-disabled, .mat-tab-nav-bar.mat-background-primary .mat-tab-link.mat-tab-disabled {\n    color: rgba(255, 255, 255, 0.4); }\n\n.mat-tab-group.mat-background-primary .mat-tab-header-pagination-chevron, .mat-tab-nav-bar.mat-background-primary .mat-tab-header-pagination-chevron {\n  border-color: rgba(255, 255, 255, 0.87); }\n\n.mat-tab-group.mat-background-primary .mat-tab-header-pagination-disabled .mat-tab-header-pagination-chevron, .mat-tab-nav-bar.mat-background-primary .mat-tab-header-pagination-disabled .mat-tab-header-pagination-chevron {\n  border-color: rgba(255, 255, 255, 0.4); }\n\n.mat-tab-group.mat-background-primary .mat-ripple-element, .mat-tab-nav-bar.mat-background-primary .mat-ripple-element {\n  background-color: rgba(255, 255, 255, 0.12); }\n\n.mat-tab-group.mat-background-accent .mat-tab-label:focus, .mat-tab-group.mat-background-accent .mat-tab-link:focus, .mat-tab-nav-bar.mat-background-accent .mat-tab-label:focus, .mat-tab-nav-bar.mat-background-accent .mat-tab-link:focus {\n  background-color: rgba(187, 222, 251, 0.3); }\n\n.mat-tab-group.mat-background-accent .mat-tab-header, .mat-tab-group.mat-background-accent .mat-tab-links, .mat-tab-nav-bar.mat-background-accent .mat-tab-header, .mat-tab-nav-bar.mat-background-accent .mat-tab-links {\n  background-color: #2196f3; }\n\n.mat-tab-group.mat-background-accent .mat-tab-label, .mat-tab-group.mat-background-accent .mat-tab-link, .mat-tab-nav-bar.mat-background-accent .mat-tab-label, .mat-tab-nav-bar.mat-background-accent .mat-tab-link {\n  color: white; }\n  .mat-tab-group.mat-background-accent .mat-tab-label.mat-tab-disabled, .mat-tab-group.mat-background-accent .mat-tab-link.mat-tab-disabled, .mat-tab-nav-bar.mat-background-accent .mat-tab-label.mat-tab-disabled, .mat-tab-nav-bar.mat-background-accent .mat-tab-link.mat-tab-disabled {\n    color: rgba(255, 255, 255, 0.4); }\n\n.mat-tab-group.mat-background-accent .mat-tab-header-pagination-chevron, .mat-tab-nav-bar.mat-background-accent .mat-tab-header-pagination-chevron {\n  border-color: white; }\n\n.mat-tab-group.mat-background-accent .mat-tab-header-pagination-disabled .mat-tab-header-pagination-chevron, .mat-tab-nav-bar.mat-background-accent .mat-tab-header-pagination-disabled .mat-tab-header-pagination-chevron {\n  border-color: rgba(255, 255, 255, 0.4); }\n\n.mat-tab-group.mat-background-accent .mat-ripple-element, .mat-tab-nav-bar.mat-background-accent .mat-ripple-element {\n  background-color: rgba(255, 255, 255, 0.12); }\n\n.mat-tab-group.mat-background-warn .mat-tab-label:focus, .mat-tab-group.mat-background-warn .mat-tab-link:focus, .mat-tab-nav-bar.mat-background-warn .mat-tab-label:focus, .mat-tab-nav-bar.mat-background-warn .mat-tab-link:focus {\n  background-color: rgba(255, 205, 210, 0.3); }\n\n.mat-tab-group.mat-background-warn .mat-tab-header, .mat-tab-group.mat-background-warn .mat-tab-links, .mat-tab-nav-bar.mat-background-warn .mat-tab-header, .mat-tab-nav-bar.mat-background-warn .mat-tab-links {\n  background-color: #f44336; }\n\n.mat-tab-group.mat-background-warn .mat-tab-label, .mat-tab-group.mat-background-warn .mat-tab-link, .mat-tab-nav-bar.mat-background-warn .mat-tab-label, .mat-tab-nav-bar.mat-background-warn .mat-tab-link {\n  color: white; }\n  .mat-tab-group.mat-background-warn .mat-tab-label.mat-tab-disabled, .mat-tab-group.mat-background-warn .mat-tab-link.mat-tab-disabled, .mat-tab-nav-bar.mat-background-warn .mat-tab-label.mat-tab-disabled, .mat-tab-nav-bar.mat-background-warn .mat-tab-link.mat-tab-disabled {\n    color: rgba(255, 255, 255, 0.4); }\n\n.mat-tab-group.mat-background-warn .mat-tab-header-pagination-chevron, .mat-tab-nav-bar.mat-background-warn .mat-tab-header-pagination-chevron {\n  border-color: white; }\n\n.mat-tab-group.mat-background-warn .mat-tab-header-pagination-disabled .mat-tab-header-pagination-chevron, .mat-tab-nav-bar.mat-background-warn .mat-tab-header-pagination-disabled .mat-tab-header-pagination-chevron {\n  border-color: rgba(255, 255, 255, 0.4); }\n\n.mat-tab-group.mat-background-warn .mat-ripple-element, .mat-tab-nav-bar.mat-background-warn .mat-ripple-element {\n  background-color: rgba(255, 255, 255, 0.12); }\n\n.mat-toolbar {\n  background: whitesmoke;\n  color: rgba(0, 0, 0, 0.87); }\n  .mat-toolbar.mat-primary {\n    background: #455a64;\n    color: rgba(255, 255, 255, 0.87); }\n  .mat-toolbar.mat-accent {\n    background: #2196f3;\n    color: white; }\n  .mat-toolbar.mat-warn {\n    background: #f44336;\n    color: white; }\n\n.mat-tooltip {\n  background: rgba(97, 97, 97, 0.9); }\n\n.mat-snack-bar-container {\n  background: #323232;\n  color: white; }\n\n.mat-simple-snackbar-action {\n  color: #2196f3; }\n\n.mat-form-field {\n  font: 400 16px/1.125 Roboto, \"Helvetica Neue\", sans-serif; }\n\n/**\r\n  Core\r\n */\n/*\r\n  Logo\r\n */\n/*\r\n  Colors\r\n */\n/*\r\n  Toolbar\r\n */\n/*\r\n  Sidenav\r\n */\n/*\r\n  Sidenav: Layout Alpha\r\n */\n/*\r\n  Sidenav: Layout Beta\r\n */\n/*\r\n  Sidenav-Item: Padding\r\n */\n/*\r\n  Horizontal Navigation\r\n */\n/*\r\n  Sidenav-Item: Padding\r\n */\n.all-in-one-table {\n  padding: 24px; }\n\n.actions .create {\n  margin-left: 24px; }\n\n.clickable {\n  cursor: pointer; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/pages/usermanagement/usermanagement.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserManagementComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_takeUntil__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/takeUntil.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_filter__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/filter.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_list_list_datasource__ = __webpack_require__("../../../../../src/app/core/list/list-datasource.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_list_list_database__ = __webpack_require__("../../../../../src/app/core/list/list-database.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_utils_component_destroyed__ = __webpack_require__("../../../../../src/app/core/utils/component-destroyed.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_ReplaySubject__ = __webpack_require__("../../../../rxjs/_esm5/ReplaySubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_animation__ = __webpack_require__("../../../../../src/app/app.animation.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_user_service__ = __webpack_require__("../../../../../src/app/providers/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_app_providers_role_service__ = __webpack_require__("../../../../../src/app/providers/role.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_app_providers_userrole_service__ = __webpack_require__("../../../../../src/app/providers/userrole.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_app_pages_usermanagement_user_role_management_user_role_management_component__ = __webpack_require__("../../../../../src/app/pages/usermanagement/user-role-management/user-role-management.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_app_pages_usermanagement_user_role_management_user_role_management_model__ = __webpack_require__("../../../../../src/app/pages/usermanagement/user-role-management/user-role-management.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_app_pages_usermanagement_user_role_management_userModel_model__ = __webpack_require__("../../../../../src/app/pages/usermanagement/user-role-management/userModel.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_app_pages_dialog_box_confirmation_dialog_confirmation_dialog_component__ = __webpack_require__("../../../../../src/app/pages/dialog-box/confirmation-dialog/confirmation-dialog.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

















var UserManagementComponent = (function () {
    function UserManagementComponent(dialog, userservice, roleService, userroleService) {
        this.dialog = dialog;
        this.userservice = userservice;
        this.roleService = roleService;
        this.userroleService = userroleService;
        this.subject$ = new __WEBPACK_IMPORTED_MODULE_8_rxjs_ReplaySubject__["a" /* ReplaySubject */](1);
        this.users = [];
        this.roles = [];
        this.columns = [
            { name: 'First Name', property: 'first_name', visible: true, isModelProperty: true },
            { name: 'Last Name', property: 'last_name', visible: true, isModelProperty: true },
            { name: 'Email Id', property: 'email_id', visible: true, isModelProperty: true },
            { name: 'Role', property: 'role_name', visible: true, isModelProperty: true },
            { name: 'Actions', property: 'actions', visible: true }
        ];
        this.pageSize = 10;
        this.userDetails = (JSON.parse(localStorage.getItem("userDetails")));
    }
    Object.defineProperty(UserManagementComponent.prototype, "visibleColumns", {
        get: function () {
            return this.columns.filter(function (column) { return column.visible; }).map(function (column) { return column.property; });
        },
        enumerable: true,
        configurable: true
    });
    UserManagementComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.roleService.getRoles().subscribe(function (roleResponse) {
            roleResponse.forEach(function (element) {
                _this.roles.push({ role_name: element.role_name, role_id: element.role_id, role_rank: element.role_rank });
                //, state_id: element.state_id, hospital_id: element.hospital_id, hs_id: element.hs_id, unit_id: element.unit_id 
            });
            _this.userroleService.getUserAndRoles().subscribe(function (userroleResponse) {
                _this.userroles = userroleResponse.map(function (userrole) { return new __WEBPACK_IMPORTED_MODULE_14_app_pages_usermanagement_user_role_management_user_role_management_model__["a" /* UserRole */](userrole); });
                _this.userroles.forEach(function (element) {
                    element.role_name = _this.roles.find(function (x) { return x.role_id === element.role_id; }).role_name;
                });
                _this.userservice.getUsers().subscribe(function (userResponse) {
                    _this.totalUsers = userResponse.map(function (user) { return new __WEBPACK_IMPORTED_MODULE_15_app_pages_usermanagement_user_role_management_userModel_model__["a" /* UserModel */](user); });
                    _this.totalUsers.forEach(function (element) {
                        if (_this.userroles.find(function (x) { return x.user_id === element.user_id; }) !== undefined) {
                            element.role_id = _this.userroles.find(function (x) { return x.user_id === element.user_id; }).role_id.toString();
                            element.role_name = _this.userroles.find(function (x) { return x.user_id === element.user_id; }).role_name;
                            element.role_rank = _this.roles.find(function (x) { return x.role_id === parseInt(element.role_id); }).role_rank.toString();
                            element.state_id = (_this.userroles.find(function (x) { return x.user_id === element.user_id; }).state_id) ? _this.userroles.find(function (x) { return x.user_id === element.user_id; }).state_id.toString() : "";
                            element.hospital_id = (_this.userroles.find(function (x) { return x.user_id === element.user_id; }).hospital_id) ? _this.userroles.find(function (x) { return x.user_id === element.user_id; }).hospital_id.toString() : "";
                            element.hs_id = (_this.userroles.find(function (x) { return x.user_id === element.user_id; }).hs_id) ? _this.userroles.find(function (x) { return x.user_id === element.user_id; }).hs_id.toString() : "";
                            element.unit_id = (_this.userroles.find(function (x) { return x.user_id === element.user_id; }).unit_id) ? _this.userroles.find(function (x) { return x.user_id === element.user_id; }).unit_id.toString() : "";
                        }
                        else {
                            element.role_id = "";
                            element.role_name = "-";
                            element.role_rank = "0";
                        }
                    });
                    _this.totalUsers.forEach(function (element) {
                        if (element.role_id === "") {
                            if (element.created_by === _this.userDetails.user_id) {
                                _this.users.push(element);
                            }
                        }
                        else {
                            if (parseInt(element.role_rank) > parseInt(_this.userDetails.role_rank)) {
                                if (_this.userDetails.role_name === "State Admin"
                                    && element.state_id.toString() === _this.userDetails.state_id.toString()) {
                                    _this.users.push(element);
                                }
                                if (_this.userDetails.role_name === "Health System Admin"
                                    && element.state_id.toString() === _this.userDetails.state_id.toString()
                                    && element.hs_id.toString() === _this.userDetails.hs_id.toString()) {
                                    _this.users.push(element);
                                }
                                if (_this.userDetails.role_name === "Hospital Admin"
                                    && element.state_id.toString() === _this.userDetails.state_id.toString()
                                    && element.hospital_id.toString() === _this.userDetails.hospital_id.toString()) {
                                    _this.users.push(element);
                                }
                                if (_this.userDetails.role_name === "Unit Admin"
                                    && element.state_id.toString() === _this.userDetails.state_id.toString()
                                    && element.hospital_id.toString() === _this.userDetails.hospital_id.toString()
                                    && element.unit_id.toString() === _this.userDetails.unit_id.toString()) {
                                    _this.users.push(element);
                                }
                                if (_this.userDetails.role_name === "HRET Admin" || _this.userDetails.role_name === "Super Admin") {
                                    _this.users.push(element);
                                }
                            }
                        }
                    });
                    _this.subject$.next(_this.users);
                    _this.data$ = _this.subject$.asObservable();
                    _this.database = new __WEBPACK_IMPORTED_MODULE_6__core_list_list_database__["a" /* ListDatabase */]();
                    _this.dataSource = new __WEBPACK_IMPORTED_MODULE_5__core_list_list_datasource__["a" /* ListDataSource */](_this.database, _this.sort, _this.paginator, _this.columns);
                    _this.data$
                        .takeUntil(Object(__WEBPACK_IMPORTED_MODULE_7__core_utils_component_destroyed__["a" /* componentDestroyed */])(_this))
                        .filter(Boolean)
                        .subscribe(function (users) {
                        _this.users = users;
                        _this.database.dataChange.next(users);
                        _this.resultsLength = users.length;
                    });
                });
            });
        });
    };
    UserManagementComponent.prototype.createUser = function () {
        var _this = this;
        this.dialog.open(__WEBPACK_IMPORTED_MODULE_13_app_pages_usermanagement_user_role_management_user_role_management_component__["a" /* UserRoleManagementComponent */], {
            data: { from_role_rank: this.userDetails.role_rank, from_user_id: this.userDetails.user_id, status: "create" }
        }).afterClosed().subscribe(function (user) {
            if (user) {
                _this.users.unshift(new __WEBPACK_IMPORTED_MODULE_15_app_pages_usermanagement_user_role_management_userModel_model__["a" /* UserModel */](user));
                _this.subject$.next(_this.users);
                _this.userservice.showToast("success", "User added successfully");
            }
        });
    };
    UserManagementComponent.prototype.updateUser = function (user) {
        var _this = this;
        this.dialog.open(__WEBPACK_IMPORTED_MODULE_13_app_pages_usermanagement_user_role_management_user_role_management_component__["a" /* UserRoleManagementComponent */], {
            data: { user: user, from_role_rank: this.userDetails.role_rank, from_user_id: this.userDetails.user_id, status: "update" }
        }).afterClosed().subscribe(function (user) {
            if (user) {
                var index = _this.users.findIndex(function (existingUser) { return existingUser.user_id === user.user_id; });
                _this.users[index] = new __WEBPACK_IMPORTED_MODULE_15_app_pages_usermanagement_user_role_management_userModel_model__["a" /* UserModel */](user);
                _this.subject$.next(_this.users);
                _this.userservice.showToast("success", "User updated successfully");
            }
        });
    };
    UserManagementComponent.prototype.deleteUser = function (user) {
        var _this = this;
        this.dialog.open(__WEBPACK_IMPORTED_MODULE_16_app_pages_dialog_box_confirmation_dialog_confirmation_dialog_component__["a" /* ConfirmationDialogComponent */], {
            data: { title: "Delete User!", message: "Are you sure you want to delete this user?" }, disableClose: false
        }).afterClosed().subscribe(function (value) {
            if (value === "yes") {
                _this.userservice.deleteUser(user).subscribe(function (p) {
                    _this.users.splice(_this.users.findIndex(function (existingUser) { return existingUser.user_id === user.user_id; }), 1);
                    _this.subject$.next(_this.users);
                    _this.userservice.showToast("success", "User deleted successfully");
                });
            }
        });
    };
    UserManagementComponent.prototype.unlockUser = function (user) {
        var _this = this;
        this.dialog.open(__WEBPACK_IMPORTED_MODULE_16_app_pages_dialog_box_confirmation_dialog_confirmation_dialog_component__["a" /* ConfirmationDialogComponent */], {
            data: { title: "Unlock User!", message: "Are you sure you want to unlock this user?" }, disableClose: false
        }).afterClosed().subscribe(function (value) {
            if (value === "yes") {
                user.is_locked = '0';
                user.is_auto_password = '1';
                _this.userservice.resetPassword(user).subscribe(function (p) {
                    _this.users.unshift(new __WEBPACK_IMPORTED_MODULE_15_app_pages_usermanagement_user_role_management_userModel_model__["a" /* UserModel */](user));
                    _this.userservice.showToast("success", "User unlocked successfully");
                });
            }
        });
    };
    UserManagementComponent.prototype.onFilterChange = function (value) {
        if (!this.dataSource) {
            return;
        }
        this.dataSource.filter = value;
    };
    UserManagementComponent.prototype.ngOnDestroy = function () {
    };
    UserManagementComponent.prototype.setTimer = function (msg, isSuccess) {
        var _this = this;
        // set showloader to true to show loading div on view
        this.showMessage = true;
        this.message = msg;
        this.timer = __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["a" /* Observable */].timer(4000); // 4000 millisecond means 4 seconds
        this.subscription = this.timer.subscribe(function () {
            // set showloader to false to hide loading div from view after 5 seconds
            _this.showMessage = false;
            _this.message = "";
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Array)
    ], UserManagementComponent.prototype, "columns", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1__angular_material__["s" /* MatPaginator */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__angular_material__["s" /* MatPaginator */])
    ], UserManagementComponent.prototype, "paginator", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1__angular_material__["E" /* MatSort */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__angular_material__["E" /* MatSort */])
    ], UserManagementComponent.prototype, "sort", void 0);
    UserManagementComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'vr-all-in-one-table',
            template: __webpack_require__("../../../../../src/app/pages/usermanagement/usermanagement.component.html"),
            styles: [__webpack_require__("../../../../../src/app/pages/usermanagement/usermanagement.component.scss")],
            animations: __WEBPACK_IMPORTED_MODULE_9__app_animation__["b" /* ROUTE_TRANSITION */].slice(),
            host: { '[@routeTransition]': '' }
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_material__["i" /* MatDialog */],
            __WEBPACK_IMPORTED_MODULE_10__providers_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_11_app_providers_role_service__["a" /* RoleService */],
            __WEBPACK_IMPORTED_MODULE_12_app_providers_userrole_service__["a" /* UserRoleService */]])
    ], UserManagementComponent);
    return UserManagementComponent;
}());



/***/ }),

/***/ "../../../../../src/app/pages/usermanagement/usermanagement.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserManagementModule", function() { return UserManagementModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_flex_layout__ = __webpack_require__("../../../flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_app_core_list_list_module__ = __webpack_require__("../../../../../src/app/core/list/list.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_app_core_page_header_page_header_module__ = __webpack_require__("../../../../../src/app/core/page-header/page-header.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_app_core_breadcrumbs_breadcrumbs_module__ = __webpack_require__("../../../../../src/app/core/breadcrumbs/breadcrumbs.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_app_pages_usermanagement_usermanagement_component__ = __webpack_require__("../../../../../src/app/pages/usermanagement/usermanagement.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_app_pages_usermanagement_usermanagement_routing__ = __webpack_require__("../../../../../src/app/pages/usermanagement/usermanagement.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_app_pages_usermanagement_user_role_management_userrole_module__ = __webpack_require__("../../../../../src/app/pages/usermanagement/user-role-management/userrole.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_app_pages_dialog_box_confirmation_dialog_confirmation_dialog_module__ = __webpack_require__("../../../../../src/app/pages/dialog-box/confirmation-dialog/confirmation-dialog.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var UserManagementModule = (function () {
    function UserManagementModule() {
    }
    UserManagementModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_9_app_pages_usermanagement_usermanagement_routing__["a" /* UserManagementRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_forms__["e" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_flex_layout__["a" /* FlexLayoutModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_material__["G" /* MatTableModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_material__["F" /* MatSortModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_material__["t" /* MatPaginatorModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_material__["f" /* MatCheckboxModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_material__["n" /* MatInputModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_material__["m" /* MatIconModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_material__["p" /* MatMenuModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_material__["c" /* MatButtonModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_material__["j" /* MatDialogModule */],
                // Core
                __WEBPACK_IMPORTED_MODULE_5_app_core_list_list_module__["a" /* ListModule */],
                __WEBPACK_IMPORTED_MODULE_10_app_pages_usermanagement_user_role_management_userrole_module__["a" /* UserRoleManagementModule */],
                __WEBPACK_IMPORTED_MODULE_11_app_pages_dialog_box_confirmation_dialog_confirmation_dialog_module__["a" /* ConfirmationDialogModule */],
                __WEBPACK_IMPORTED_MODULE_6_app_core_page_header_page_header_module__["a" /* PageHeaderModule */],
                __WEBPACK_IMPORTED_MODULE_7_app_core_breadcrumbs_breadcrumbs_module__["a" /* BreadcrumbsModule */]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_8_app_pages_usermanagement_usermanagement_component__["a" /* UserManagementComponent */]],
            exports: [__WEBPACK_IMPORTED_MODULE_8_app_pages_usermanagement_usermanagement_component__["a" /* UserManagementComponent */]]
        })
    ], UserManagementModule);
    return UserManagementModule;
}());



/***/ }),

/***/ "../../../../../src/app/pages/usermanagement/usermanagement.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserManagementRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_pages_usermanagement_usermanagement_component__ = __webpack_require__("../../../../../src/app/pages/usermanagement/usermanagement.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_2_app_pages_usermanagement_usermanagement_component__["a" /* UserManagementComponent */]
    }
];
var UserManagementRoutingModule = (function () {
    function UserManagementRoutingModule() {
    }
    UserManagementRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_0__angular_router__["d" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_0__angular_router__["d" /* RouterModule */]]
        })
    ], UserManagementRoutingModule);
    return UserManagementRoutingModule;
}());



/***/ })

});
//# sourceMappingURL=usermanagement.module.chunk.js.map