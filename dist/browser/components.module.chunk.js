webpackJsonp(["components.module"],{

/***/ "../../../../../src/app/pages/components/components-autocomplete/components-autocomplete.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"autocomplete group card alternative no-border mat-elevation-z2\" fxLayout=\"column\" fxLayoutGap=\"24px\">\r\n  <div class=\"group-title\">Material Autocomplete</div>\r\n  <div class=\"divider reverse\"></div>\r\n  <div class=\"section\" fxLayout=\"column\" fxLayout.gt-md=\"row\" fxLayoutAlign=\"start stretch\" fxLayoutAlign.gt-md=\"start start\" fxLayoutGap=\"16px\" fxLayoutGap.gt-md=\"24px\">\r\n    <div fxFlex.gt-md>\r\n      <div fxLayout=\"column\">\r\n        <div class=\"section-title\">Autocomplete</div>\r\n        <div class=\"section-tag\">&lt;mat-autocomplete&gt;</div>\r\n        <p>MdAutocomplete can be used to allow for quick selection of predictable entities.</p>\r\n      </div>\r\n    </div>\r\n    <mat-tab-group class=\"preview-tabs mat-elevation-z2\" fxFlex=\"auto\" fxFlex.gt-md=\"70%\">\r\n      <mat-tab label=\"PREVIEW\">\r\n        <div class=\"preview\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n          <mat-card fxFlex=\"90%\" fxFlex.gt-sm=\"80%\">\r\n            <mat-card-content fxLayout=\"column\" fxLayoutAlign=\"center center\">\r\n              <form #autoForm=\"ngForm\">\r\n                <mat-form-field>\r\n                  <input matInput placeholder=\"State\" [matAutocomplete]=\"reactiveAuto\" [formControl]=\"stateCtrl\">\r\n                </mat-form-field>\r\n\r\n                <mat-autocomplete #reactiveAuto=\"matAutocomplete\" [displayWith]=\"displayFn\">\r\n                  <mat-option *ngFor=\"let state of reactiveStates | async\" [value]=\"state\">\r\n                    <span>{{ state.name }}</span>\r\n                    <span class=\"demo-secondary-text\"> ({{state.code}}) </span>\r\n                  </mat-option>\r\n                </mat-autocomplete>\r\n              </form>\r\n            </mat-card-content>\r\n          </mat-card>\r\n        </div>\r\n      </mat-tab>\r\n      <mat-tab label=\"HTML\">\r\n        <code [innerHTML]=\"autocompleteHTML\" vrHighlight class=\"html\"></code>\r\n      </mat-tab>\r\n    </mat-tab-group>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/pages/components/components-autocomplete/components-autocomplete.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsAutocompleteComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash_es_escape__ = __webpack_require__("../../../../lodash-es/escape.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_startWith__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/startWith.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ComponentsAutocompleteComponent = (function () {
    function ComponentsAutocompleteComponent() {
        var _this = this;
        this.autocompleteHTML = Object(__WEBPACK_IMPORTED_MODULE_2_lodash_es_escape__["a" /* default */])("\n<md-input-container>\n  <input mdInput [mdAutocomplete]=\"auto\">\n</md-input-container>\n<md-autocomplete #auto=\"mdAutocomplete\">\n  <md-option *ngFor=\"let option of options\" [value]=\"option\">\n    {{ option }}\n  </md-option>\n</md-autocomplete>\n");
        this.states = [
            { code: 'AL', name: 'Alabama' },
            { code: 'AK', name: 'Alaska' },
            { code: 'AZ', name: 'Arizona' },
            { code: 'AR', name: 'Arkansas' },
            { code: 'CA', name: 'California' },
            { code: 'CO', name: 'Colorado' },
            { code: 'CT', name: 'Connecticut' },
            { code: 'DE', name: 'Delaware' },
            { code: 'FL', name: 'Florida' },
            { code: 'GA', name: 'Georgia' },
            { code: 'HI', name: 'Hawaii' },
            { code: 'ID', name: 'Idaho' },
            { code: 'IL', name: 'Illinois' },
            { code: 'IN', name: 'Indiana' },
            { code: 'IA', name: 'Iowa' },
            { code: 'KS', name: 'Kansas' },
            { code: 'KY', name: 'Kentucky' },
            { code: 'LA', name: 'Louisiana' },
            { code: 'ME', name: 'Maine' },
            { code: 'MD', name: 'Maryland' },
            { code: 'MA', name: 'Massachusetts' },
            { code: 'MI', name: 'Michigan' },
            { code: 'MN', name: 'Minnesota' },
            { code: 'MS', name: 'Mississippi' },
            { code: 'MO', name: 'Missouri' },
            { code: 'MT', name: 'Montana' },
            { code: 'NE', name: 'Nebraska' },
            { code: 'NV', name: 'Nevada' },
            { code: 'NH', name: 'New Hampshire' },
            { code: 'NJ', name: 'New Jersey' },
            { code: 'NM', name: 'New Mexico' },
            { code: 'NY', name: 'New York' },
            { code: 'NC', name: 'North Carolina' },
            { code: 'ND', name: 'North Dakota' },
            { code: 'OH', name: 'Ohio' },
            { code: 'OK', name: 'Oklahoma' },
            { code: 'OR', name: 'Oregon' },
            { code: 'PA', name: 'Pennsylvania' },
            { code: 'RI', name: 'Rhode Island' },
            { code: 'SC', name: 'South Carolina' },
            { code: 'SD', name: 'South Dakota' },
            { code: 'TN', name: 'Tennessee' },
            { code: 'TX', name: 'Texas' },
            { code: 'UT', name: 'Utah' },
            { code: 'VT', name: 'Vermont' },
            { code: 'VA', name: 'Virginia' },
            { code: 'WA', name: 'Washington' },
            { code: 'WV', name: 'West Virginia' },
            { code: 'WI', name: 'Wisconsin' },
            { code: 'WY', name: 'Wyoming' },
        ];
        this.stateCtrl = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormControl */]({ code: '', name: '' });
        this.reactiveStates = this.stateCtrl.valueChanges
            .startWith(this.stateCtrl.value)
            .map(function (val) { return _this.displayFn(val); })
            .map(function (name) { return _this.filterStates(name); });
    }
    ComponentsAutocompleteComponent.prototype.ngOnInit = function () {
    };
    ComponentsAutocompleteComponent.prototype.filterStates = function (val) {
        return val ? this.states.filter(function (s) { return new RegExp("^" + val, 'gi').test(s.name); })
            : this.states;
    };
    ComponentsAutocompleteComponent.prototype.displayFn = function (value) {
        return value && typeof value === 'object' ? value.name : value;
    };
    ComponentsAutocompleteComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'vr-components-autocomplete',
            template: __webpack_require__("../../../../../src/app/pages/components/components-autocomplete/components-autocomplete.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], ComponentsAutocompleteComponent);
    return ComponentsAutocompleteComponent;
}());



/***/ }),

/***/ "../../../../../src/app/pages/components/components-autocomplete/components-autocomplete.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsAutocompleteModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_autocomplete_component__ = __webpack_require__("../../../../../src/app/pages/components/components-autocomplete/components-autocomplete.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_flex_layout__ = __webpack_require__("../../../flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_utils_utils_module__ = __webpack_require__("../../../../../src/app/core/utils/utils.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var ComponentsAutocompleteModule = (function () {
    function ComponentsAutocompleteModule() {
    }
    ComponentsAutocompleteModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_6__core_utils_utils_module__["a" /* UtilsModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_flex_layout__["a" /* FlexLayoutModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["e" /* MatCardModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["H" /* MatTabsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["e" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["j" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["b" /* MatAutocompleteModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["n" /* MatInputModule */],
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_2__components_autocomplete_component__["a" /* ComponentsAutocompleteComponent */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__components_autocomplete_component__["a" /* ComponentsAutocompleteComponent */]]
        })
    ], ComponentsAutocompleteModule);
    return ComponentsAutocompleteModule;
}());



/***/ }),

/***/ "../../../../../src/app/pages/components/components-button/components-button.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"buttons group card alternative no-border mat-elevation-z2\" fxLayout=\"column\" fxLayoutGap=\"24px\">\r\n  <div class=\"group-title\">Material Buttons</div>\r\n  <div class=\"divider reverse\"></div>\r\n  <div class=\"section\" fxLayout=\"column\" fxLayout.gt-md=\"row\" fxLayoutAlign=\"start stretch\" fxLayoutAlign.gt-md=\"start start\" fxLayoutGap=\"16px\" fxLayoutGap.gt-md=\"24px\">\r\n    <div fxFlex.gt-md>\r\n      <div fxLayout=\"column\">\r\n        <div class=\"section-title\">Flat Buttons</div>\r\n        <div class=\"section-tag\">&lt;mat-button&gt;</div>\r\n        <p>Flat buttons are printed on material. They do not lift, but fill with color on press.</p>\r\n        <div class=\"section-info\">Example:</div>\r\n        <code vrHighlight class=\"html\">&lt;button mat-button&gt;<br/>  Button<br/>&lt;/button&gt;</code>\r\n      </div>\r\n    </div>\r\n    <mat-tab-group class=\"preview-tabs mat-elevation-z2\" fxFlex=\"auto\" fxFlex.gt-md=\"70%\">\r\n      <mat-tab label=\"PREVIEW\">\r\n        <div class=\"preview\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n          <mat-card fxFlex=\"90%\" fxFlex.gt-sm=\"80%\">\r\n            <mat-card-content fxLayout=\"column\" fxLayout.gt-sm=\"row\" fxLayoutAlign=\"space-around center\"\r\n                             fxLayoutWrap=\"wrap\">\r\n              <button mat-button>Button</button>\r\n              <button mat-button color=\"primary\">Primary</button>\r\n              <button mat-button color=\"accent\">Accent</button>\r\n              <button mat-button color=\"warn\">Warn</button>\r\n              <button mat-button disabled>Disabled</button>\r\n            </mat-card-content>\r\n          </mat-card>\r\n        </div>\r\n      </mat-tab>\r\n      <mat-tab label=\"HTML\">\r\n        <code [innerHTML]=\"flatButtonsHTML\" vrHighlight class=\"html\"></code>\r\n      </mat-tab>\r\n    </mat-tab-group>\r\n  </div>\r\n\r\n  <div class=\"divider reverse\"></div>\r\n\r\n  <div class=\"section\" fxLayout=\"column\" fxLayout.gt-md=\"row\" fxLayoutAlign=\"start stretch\" fxLayoutAlign.gt-md=\"start start\" fxLayoutGap=\"16px\" fxLayoutGap.gt-md=\"24px\">\r\n    <div fxFlex.gt-md>\r\n      <div class=\"section-title\">Raised Buttons</div>\r\n      <div class=\"section-tag\">&lt;mat-raised-button&gt;</div>\r\n      <p>Raised buttons add dimension to mostly flat layouts. They emphasize functions on busy or wide spaces.</p>\r\n      <div class=\"section-info\">Example:</div>\r\n      <code vrHighlight class=\"html\">&lt;button mat-raised-button&gt;<br/> Button<br/>&lt;/button&gt;</code>\r\n    </div>\r\n    <mat-tab-group class=\"preview-tabs mat-elevation-z2\" fxFlex=\"auto\" fxFlex.gt-md=\"70%\">\r\n      <mat-tab label=\"PREVIEW\">\r\n        <div class=\"preview\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n          <mat-card fxFlex=\"90%\" fxFlex.gt-sm=\"80%\">\r\n            <mat-card-content fxLayout=\"column\" fxLayout.gt-sm=\"row\" fxLayoutAlign=\"space-around center\"\r\n                             fxLayoutWrap=\"wrap\">\r\n              <button mat-raised-button>Button</button>\r\n              <button mat-raised-button color=\"primary\">Primary</button>\r\n              <button mat-raised-button color=\"accent\">Accent</button>\r\n              <button mat-raised-button color=\"warn\">Warn</button>\r\n              <button mat-raised-button disabled>Disabled</button>\r\n            </mat-card-content>\r\n          </mat-card>\r\n        </div>\r\n      </mat-tab>\r\n      <mat-tab label=\"HTML\">\r\n        <code [innerHTML]=\"raisedButtonsHTML\" vrHighlight class=\"html\"></code>\r\n      </mat-tab>\r\n    </mat-tab-group>\r\n  </div>\r\n\r\n  <div class=\"divider reverse\"></div>\r\n\r\n  <div class=\"section\" fxLayout=\"column\" fxLayout.gt-md=\"row\" fxLayoutAlign=\"start stretch\" fxLayoutAlign.gt-md=\"start start\" fxLayoutGap=\"16px\" fxLayoutGap.gt-md=\"24px\">\r\n    <div fxFlex.gt-md>\r\n      <div class=\"section-title\">Floating Action Buttons (FAB)</div>\r\n      <div class=\"section-tag\">&lt;mat-fab&gt;</div>\r\n      <p>Floating action buttons are used for a promoted action. They are distinguished by a circled icon floating\r\n        above the UI and have motion behaviors that include morphing, launching, and a transferring anchor\r\n        point.</p>\r\n      <div class=\"section-info\">mat-fab Example:</div>\r\n      <code vrHighlight class=\"html\">&lt;button mat-fab&gt;<br/> &lt;mat-icon&gt;grade&lt;/mat-icon&gt;<br/>&lt;/button&gt;</code>\r\n      <div class=\"section-info\">mat-mini-fab Example:</div>\r\n      <code vrHighlight class=\"html\">&lt;button mat-mini-fab&gt;<br/> &lt;mat-icon&gt;person&lt;/mat-icon&gt;<br/>&lt;/button&gt;</code>\r\n    </div>\r\n    <mat-tab-group class=\"preview-tabs mat-elevation-z2\" fxFlex=\"auto\" fxFlex.gt-md=\"70%\">\r\n      <mat-tab label=\"PREVIEW\">\r\n        <div class=\"preview\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n          <div fxFlex=\"90%\" fxFlex.gt-sm=\"80%\" fxLayout=\"column\">\r\n            <mat-card>\r\n              <mat-card-content fxLayout=\"column\" fxLayout.gt-sm=\"row\" fxLayoutAlign=\"space-around center\"\r\n                               fxLayoutWrap=\"wrap\">\r\n                <button mat-fab color=\"primary\">\r\n                  <mat-icon>grade</mat-icon>\r\n                </button>\r\n                <button mat-fab color=\"accent\">\r\n                  <mat-icon>favorite</mat-icon>\r\n                </button>\r\n                <button mat-fab color=\"warn\">\r\n                  <mat-icon>build</mat-icon>\r\n                </button>\r\n                <button mat-fab disabled>\r\n                  <mat-icon>lock</mat-icon>\r\n                </button>\r\n              </mat-card-content>\r\n            </mat-card>\r\n            <mat-card>\r\n              <mat-card-content fxLayout=\"column\" fxLayout.gt-sm=\"row\" fxLayoutAlign=\"space-around center\"\r\n                               fxLayoutWrap=\"wrap\">\r\n                <button mat-mini-fab color=\"primary\">\r\n                  <mat-icon>favorite</mat-icon>\r\n                </button>\r\n                <button mat-mini-fab color=\"accent\">\r\n                  <mat-icon>thumb_up</mat-icon>\r\n                </button>\r\n                <button mat-mini-fab color=\"warn\">\r\n                  <mat-icon>build</mat-icon>\r\n                </button>\r\n                <button mat-mini-fab disabled>\r\n                  <mat-icon>lock</mat-icon>\r\n                </button>\r\n              </mat-card-content>\r\n            </mat-card>\r\n          </div>\r\n        </div>\r\n      </mat-tab>\r\n      <mat-tab label=\"HTML\">\r\n        <code [innerHTML]=\"fabHTML\" vrHighlight class=\"html\"></code>\r\n      </mat-tab>\r\n    </mat-tab-group>\r\n  </div>\r\n\r\n  <div class=\"divider reverse\"></div>\r\n\r\n  <div class=\"section\" fxLayout=\"column\" fxLayout.gt-md=\"row\" fxLayoutAlign=\"start stretch\" fxLayoutAlign.gt-md=\"start start\" fxLayoutGap=\"16px\" fxLayoutGap.gt-md=\"24px\">\r\n    <div fxFlex.gt-md>\r\n      <div class=\"section-title\">Button Toggle</div>\r\n      <div class=\"section-tag\">&lt;mat-button-toggle&gt;</div>\r\n      <p>Toggle buttons may be used to group related options. Arrange layout and spacing to convey that certain\r\n        toggle buttons are part of a group.</p>\r\n      <div class=\"section-info\">Example:</div>\r\n      <code vrHighlight class=\"html\">&lt;mat-button-toggle&gt;<br/> &lt;mat-icon&gt;grade&lt;/mat-icon&gt;<br/>&lt;/mat-button-toggle&gt;</code>\r\n    </div>\r\n    <mat-tab-group class=\"preview-tabs mat-elevation-z2\" fxFlex=\"auto\" fxFlex.gt-md=\"70%\">\r\n      <mat-tab label=\"PREVIEW\">\r\n        <div class=\"preview\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n          <div fxFlex=\"90%\" fxFlex.gt-sm=\"80%\">\r\n            <mat-card>\r\n              <mat-card-content fxLayout=\"column\" fxLayout.gt-sm=\"row\" fxLayoutAlign=\"space-around center\"\r\n                               fxLayoutWrap=\"wrap\">\r\n                <mat-button-toggle-group [name]=\"'alignment'\">\r\n                  <mat-button-toggle value=\"left\">\r\n                    <mat-icon>format_align_left</mat-icon>\r\n                  </mat-button-toggle>\r\n                  <mat-button-toggle value=\"center\">\r\n                    <mat-icon>format_align_center</mat-icon>\r\n                  </mat-button-toggle>\r\n                  <mat-button-toggle value=\"right\">\r\n                    <mat-icon>format_align_right</mat-icon>\r\n                  </mat-button-toggle>\r\n                  <mat-button-toggle value=\"justify\">\r\n                    <mat-icon>format_align_justify</mat-icon>\r\n                  </mat-button-toggle>\r\n                </mat-button-toggle-group>\r\n                <mat-button-toggle-group multiple>\r\n                  <mat-button-toggle>Flour</mat-button-toggle>\r\n                  <mat-button-toggle>Eggs</mat-button-toggle>\r\n                  <mat-button-toggle>Sugar</mat-button-toggle>\r\n                  <mat-button-toggle>Milk</mat-button-toggle>\r\n                </mat-button-toggle-group>\r\n              </mat-card-content>\r\n            </mat-card>\r\n          </div>\r\n        </div>\r\n      </mat-tab>\r\n      <mat-tab label=\"HTML\">\r\n        <code [innerHTML]=\"buttonToggleHTML\" vrHighlight class=\"html\"></code>\r\n      </mat-tab>\r\n    </mat-tab-group>\r\n  </div>\r\n\r\n  <div class=\"divider reverse\"></div>\r\n\r\n  <div class=\"section\" fxLayout=\"column\" fxLayout.gt-md=\"row\" fxLayoutAlign=\"start stretch\" fxLayoutAlign.gt-md=\"start start\" fxLayoutGap=\"16px\" fxLayoutGap.gt-md=\"24px\">\r\n    <div fxFlex.gt-md>\r\n      <div class=\"section-title\">Icon Buttons</div>\r\n      <div class=\"section-tag\">&lt;mat-icon-button&gt;</div>\r\n      <p>A circular material button that only contains an icon and displays an ink reaction on press.</p>\r\n      <div class=\"section-info\">Example:</div>\r\n      <code vrHighlight class=\"html\">&lt;button mat-icon-button&gt;<br/>  &lt;mat-icon&gt;grade&lt;/mat-icon&gt;<br/>&lt;/button&gt;</code>\r\n    </div>\r\n    <mat-tab-group class=\"preview-tabs mat-elevation-z2\" fxFlex=\"auto\" fxFlex.gt-md=\"70%\">\r\n      <mat-tab label=\"PREVIEW\">\r\n        <div class=\"preview\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n          <div fxFlex=\"90%\" fxFlex.gt-sm=\"80%\">\r\n            <mat-card>\r\n              <mat-card-content fxLayout=\"column\" fxLayout.gt-sm=\"row\" fxLayoutAlign=\"space-around center\"\r\n                               fxLayoutWrap=\"wrap\">\r\n                <button mat-icon-button>\r\n                  <mat-icon>menu</mat-icon>\r\n                </button>\r\n                <button mat-icon-button color=\"primary\">\r\n                  <mat-icon>grade</mat-icon>\r\n                </button>\r\n                <button mat-icon-button color=\"accent\">\r\n                  <mat-icon>favorite</mat-icon>\r\n                </button>\r\n                <button mat-icon-button color=\"warn\">\r\n                  <mat-icon>build</mat-icon>\r\n                </button>\r\n                <button mat-icon-button>\r\n                  <mat-icon>lock</mat-icon>\r\n                </button>\r\n              </mat-card-content>\r\n            </mat-card>\r\n          </div>\r\n        </div>\r\n      </mat-tab>\r\n      <mat-tab label=\"HTML\">\r\n        <code [innerHTML]=\"iconButtonHTML\" vrHighlight class=\"html\"></code>\r\n      </mat-tab>\r\n    </mat-tab-group>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/pages/components/components-button/components-button.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsButtonComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash_es_escape__ = __webpack_require__("../../../../lodash-es/escape.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ComponentsButtonComponent = (function () {
    function ComponentsButtonComponent() {
        this.flatButtonsHTML = Object(__WEBPACK_IMPORTED_MODULE_1_lodash_es_escape__["a" /* default */])("\n  <md-card>\n    <md-card-content fxLayout=\"column\" fxLayout.gt-sm=\"row\" fxLayoutAlign=\"space-around center\">\n      <button md-button>Button</button>\n      <button md-button color=\"primary\">Primary</button>\n      <button md-button color=\"accent\">Accent</button>\n      <button md-button color=\"warn\">Warn</button>\n      <button md-button disabled=\"true\">Disabled</button>\n    </md-card-content>\n  </md-card>\n  ");
        this.raisedButtonsHTML = Object(__WEBPACK_IMPORTED_MODULE_1_lodash_es_escape__["a" /* default */])("\n  <md-card>\n    <md-card-content fxLayout=\"column\" fxLayout.gt-sm=\"row\" fxLayoutAlign=\"space-around center\">\n      <button md-raised-button>Button</button>\n      <button md-raised-button color=\"primary\">Primary</button>\n      <button md-raised-button color=\"accent\">Accent</button>\n      <button md-raised-button color=\"warn\">Warn</button>\n      <button md-raised-button disabled=\"true\">Disabled</button>\n    </md-card-content>\n  </md-card>\n  ");
        this.fabHTML = Object(__WEBPACK_IMPORTED_MODULE_1_lodash_es_escape__["a" /* default */])("\n  <md-card>\n    <md-card-content fxLayout=\"column\" fxLayout.gt-sm=\"row\" fxLayoutAlign=\"space-around center\">\n      <button md-fab color=\"primary\"><md-icon>grade</md-icon></button>\n      <button md-fab color=\"accent\"><md-icon>favorite</md-icon></button>\n      <button md-fab color=\"warn\"><md-icon>build</md-icon></button>\n      <button md-fab disabled=\"true\"><md-icon>lock</md-icon></button>\n      <button md-mini-fab color=\"primary\"><md-icon>favorite</md-icon></button>\n      <button md-mini-fab color=\"accent\"><md-icon>thumb_up</md-icon></button>\n      <button md-mini-fab color=\"warn\"><md-icon>build</md-icon></button>\n      <button md-mini-fab disabled=\"true\"><md-icon>lock</md-icon></button>\n    </md-card-content>\n  </md-card>\n  ");
        this.buttonToggleHTML = Object(__WEBPACK_IMPORTED_MODULE_1_lodash_es_escape__["a" /* default */])("\n  <md-card>\n    <md-card-content fxLayout=\"column\" fxLayout.gt-sm=\"row\" fxLayoutAlign=\"space-around center\">\n      <md-button-toggle-group [name]=\"'alignment'\">\n        <md-button-toggle value=\"left\"><md-icon>format_align_left</md-icon></md-button-toggle>\n        <md-button-toggle value=\"center\"><md-icon>format_align_center</md-icon></md-button-toggle>\n        <md-button-toggle value=\"right\"><md-icon>format_align_right</md-icon></md-button-toggle>\n        <md-button-toggle value=\"justify\"><md-icon>format_align_justify</md-icon></md-button-toggle>\n      </md-button-toggle-group>\n      <md-button-toggle-group multiple>\n        <md-button-toggle>Flour</md-button-toggle>\n        <md-button-toggle>Eggs</md-button-toggle>\n        <md-button-toggle>Sugar</md-button-toggle>\n        <md-button-toggle>Milk</md-button-toggle>\n      </md-button-toggle-group>\n    </md-card-content>\n  </md-card>\n  ");
        this.iconButtonHTML = Object(__WEBPACK_IMPORTED_MODULE_1_lodash_es_escape__["a" /* default */])("\n  <md-card>\n    <md-card-content fxLayout=\"column\" fxLayout.gt-sm=\"row\" fxLayoutAlign=\"space-around center\">\n      <button md-icon-button><md-icon>menu</md-icon></button>\n      <button md-icon-button color=\"primary\"><md-icon>grade</md-icon></button>\n      <button md-icon-button color=\"accent\"><md-icon>favorite</md-icon></button>\n      <button md-icon-button color=\"warn\"><md-icon>build</md-icon></button>\n      <button md-icon-button disabled=\"true\"><md-icon>lock</md-icon></button>\n    </md-card-content>\n  </md-card>\n  ");
    }
    ComponentsButtonComponent.prototype.ngOnInit = function () {
    };
    ComponentsButtonComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'vr-components-button',
            template: __webpack_require__("../../../../../src/app/pages/components/components-button/components-button.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], ComponentsButtonComponent);
    return ComponentsButtonComponent;
}());



/***/ }),

/***/ "../../../../../src/app/pages/components/components-button/components-button.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsButtonModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_flex_layout__ = __webpack_require__("../../../flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_button_component__ = __webpack_require__("../../../../../src/app/pages/components/components-button/components-button.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_utils_utils_module__ = __webpack_require__("../../../../../src/app/core/utils/utils.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var ComponentsButtonModule = (function () {
    function ComponentsButtonModule() {
    }
    ComponentsButtonModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_5__core_utils_utils_module__["a" /* UtilsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_flex_layout__["a" /* FlexLayoutModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_material__["e" /* MatCardModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_material__["H" /* MatTabsModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_material__["c" /* MatButtonModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_material__["d" /* MatButtonToggleModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_material__["m" /* MatIconModule */],
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_4__components_button_component__["a" /* ComponentsButtonComponent */]],
            exports: [__WEBPACK_IMPORTED_MODULE_4__components_button_component__["a" /* ComponentsButtonComponent */]]
        })
    ], ComponentsButtonModule);
    return ComponentsButtonModule;
}());



/***/ }),

/***/ "../../../../../src/app/pages/components/components-card/components-card.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"card group alternative no-border mat-elevation-z2\" fxLayout=\"column\" fxLayoutGap=\"24px\">\r\n  <div class=\"group-title\">Material Card</div>\r\n  <div class=\"divider reverse\"></div>\r\n  <div class=\"section\" fxLayout=\"column\" fxLayout.gt-md=\"row\" fxLayoutAlign=\"start stretch\" fxLayoutAlign.gt-md=\"start start\" fxLayoutGap=\"16px\" fxLayoutGap.gt-md=\"24px\">\r\n    <div fxFlex.gt-md>\r\n      <div fxLayout=\"column\">\r\n        <div class=\"section-title\">Image + Action Card</div>\r\n        <div class=\"section-tag\">&lt;mat-card&gt;</div>\r\n        <p>Cards are containers for text, photos and actions. They are intented to provide information on a single subject.</p>\r\n        <div class=\"section-info\">Example:</div>\r\n        <code vrHighlight class=\"html\">&lt;mat-card&gt;<br/> &lt;mat-card-title&gt;<br/>  Card Title<br/> &lt;/mat-card-title&gt;<br/> &lt;mat-card-content&gt;<br/>  Card Content<br/> &lt;/mat-card-content&gt;<br/> &lt;mat-card-actions&gt;<br/>  &lt;button&gt;Click Me!&lt;/button&gt;<br/> &lt;/mat-card-actions&gt;<br/>&lt;/mat-card&gt;</code>\r\n      </div>\r\n    </div>\r\n    <mat-tab-group class=\"preview-tabs mat-elevation-z2\" fxFlex=\"auto\" fxFlex.gt-md=\"70%\">\r\n      <mat-tab label=\"PREVIEW\">\r\n        <div class=\"preview\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n          <mat-card fxFlex=\"80%\">\r\n            <mat-card-header>\r\n              <img mat-card-avatar src=\"assets/img/demo/avatars/3.png\">\r\n              <mat-card-subtitle>\r\n                Yesterday\r\n              </mat-card-subtitle>\r\n              <mat-card-title>Gerald Morris</mat-card-title>\r\n            </mat-card-header>\r\n            <img mat-card-image src=\"assets/img/demo/backgrounds/pattern-1.jpg\">\r\n            <mat-card-content>\r\n              <p>Piqued favour stairs it enable exeter as seeing. Remainder met improving but engrossed sincerity age. Better but length gay denied abroad are. Attachment astonished to on appearance imprudence so collecting in excellence. Tiled way blind lived whose new. The for fully had she there leave merit enjoy forth. </p>\r\n            </mat-card-content>\r\n            <mat-divider></mat-divider>\r\n            <mat-card-actions>\r\n              <div fxLayout=\"row\">\r\n                <button mat-icon-button>\r\n                  <mat-icon>share</mat-icon>\r\n                </button>\r\n                <button mat-icon-button>\r\n                  <mat-icon>favorite</mat-icon>\r\n                </button>\r\n                <span fxFlex></span>\r\n                <button mat-button>\r\n                  More Info\r\n                </button>\r\n                <button mat-button>\r\n                  Save as\r\n                </button>\r\n              </div>\r\n            </mat-card-actions>\r\n          </mat-card>\r\n        </div>\r\n      </mat-tab>\r\n      <mat-tab label=\"HTML\">\r\n        <code [innerHTML]=\"card1HTML\" vrHighlight class=\"html\"></code>\r\n      </mat-tab>\r\n    </mat-tab-group>\r\n  </div>\r\n\r\n  <div class=\"divider reverse\"></div>\r\n\r\n  <div class=\"section\" fxLayout=\"column\" fxLayout.gt-md=\"row\" fxLayoutAlign=\"start stretch\" fxLayoutAlign.gt-md=\"start start\" fxLayoutGap=\"16px\" fxLayoutGap.gt-md=\"24px\">\r\n    <div fxFlex.gt-md fxLayout=\"column\">\r\n      <div class=\"section-title\">Standard Card with Action</div>\r\n      <mat-tab-group class=\"preview-tabs mat-elevation-z2\" fxFlex=\"auto\" fxFlex.gt-md=\"70%\">\r\n        <mat-tab label=\"PREVIEW\">\r\n          <div class=\"preview\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n            <mat-card fxFlex=\"80%\">\r\n              <mat-card-title>Standard Card with Actions</mat-card-title>\r\n              <mat-card-subtitle>Subtitle</mat-card-subtitle>\r\n              <mat-card-content>\r\n                <p>Old there any widow law rooms. Agreed but expect repair she nay sir silent person. Direction\r\n                  can dependent one bed situation attempted. His she are man their spite avoid. Her pretended\r\n                  fulfilled extremely education yet. Satisfied did one admitting incommode tolerably how are. </p>\r\n              </mat-card-content>\r\n              <mat-card-actions align=\"end\">\r\n                <button mat-button>Cancel</button>\r\n                <button color=\"primary\" mat-raised-button>Action</button>\r\n              </mat-card-actions>\r\n            </mat-card>\r\n          </div>\r\n        </mat-tab>\r\n        <mat-tab label=\"HTML\">\r\n          <code [innerHTML]=\"card2HTML\" vrHighlight class=\"html\"></code>\r\n        </mat-tab>\r\n      </mat-tab-group>\r\n    </div>\r\n\r\n    <div fxFlex.gt-md fxLayout=\"column\">\r\n      <div class=\"section-title\">Standard Card</div>\r\n      <div class=\"preview-tabs mat-elevation-z2\" fxFlex=\"auto\" fxFlex.gt-md=\"70%\">\r\n        <mat-tab-group>\r\n          <mat-tab label=\"PREVIEW\">\r\n            <div class=\"preview\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              <mat-card fxFlex=\"80%\">\r\n                <mat-card-title>Standard Card</mat-card-title>\r\n                <mat-card-subtitle>Subtitle</mat-card-subtitle>\r\n                <mat-card-content>\r\n                  <p>Do play they miss give so up. Words to up style of since world. We leaf to snug on no need. Way\r\n                    own uncommonly travelling now acceptance bed compliment solicitude. Dissimilar admiration so\r\n                    terminated no in contrasted it. Advantages entreaties mr he apartments do. Limits far yet turned\r\n                    highly repair parish talked six. Draw fond rank form nor the day eat. </p>\r\n                </mat-card-content>\r\n              </mat-card>\r\n            </div>\r\n          </mat-tab>\r\n          <mat-tab label=\"HTML\">\r\n            <code [innerHTML]=\"card3HTML\" vrHighlight class=\"html\"></code>\r\n          </mat-tab>\r\n        </mat-tab-group>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/pages/components/components-card/components-card.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsCardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash_es_escape__ = __webpack_require__("../../../../lodash-es/escape.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ComponentsCardComponent = (function () {
    function ComponentsCardComponent() {
        // noinspection TsLint
        this.card1HTML = Object(__WEBPACK_IMPORTED_MODULE_1_lodash_es_escape__["a" /* default */])("\n  <md-card fxFlex=\"80%\">\n    <md-card-header>\n      <img md-card-avatar=\"\" src=\"assets/img/avatars/Avatar.png\">\n      <md-card-subtitle>\n        Yesterday\n      </md-card-subtitle>\n      <md-card-title>Gerald Morris</md-card-title>\n    </md-card-header>\n    <img md-card-image src=\"assets/img/backgrounds/1.jpg\">\n    <md-card-content>\n      <p>Piqued favour stairs it enable exeter as seeing. Remainder met improving but engrossed sincerity age. Better but length gay denied abroad are. Attachment astonished to on appearance imprudence so collecting in excellence. Tiled way blind lived whose new. The for fully had she there leave merit enjoy forth. </p>\n    </md-card-content>\n    <md-divider></md-divider>\n    <md-card-actions>\n      <div fxLayout=\"row\">\n        <button md-icon-button>\n          <md-icon>share</md-icon>\n        </button>\n        <button md-icon-button>\n          <md-icon>favorite</md-icon>\n        </button>\n        <span fxFlex></span>\n        <button md-button>\n          More Info\n        </button>\n        <button md-button>\n          Save as\n        </button>\n      </div>\n    </md-card-actions>\n  </md-card>\n  ");
        this.card2HTML = Object(__WEBPACK_IMPORTED_MODULE_1_lodash_es_escape__["a" /* default */])("\n  <md-card fxFlex=\"80%\">\n    <md-card-title>Standard Card with Actions</md-card-title>\n    <md-card-subtitle>Subtitle</md-card-subtitle>\n    <md-card-content>\n      <p>Old there any widow law rooms. Agreed but expect repair she nay sir silent person. Direction\n        can dependent one bed situation attempted. His she are man their spite avoid. Her pretended\n        fulfilled extremely education yet. Satisfied did one admitting incommode tolerably how are. </p>\n    </md-card-content>\n    <md-card-actions align=\"end\">\n      <button md-button>Cancel</button>\n      <button color=\"primary\" md-raised-button>Action</button>\n    </md-card-actions>\n  </md-card>\n  ");
        this.card3HTML = Object(__WEBPACK_IMPORTED_MODULE_1_lodash_es_escape__["a" /* default */])("\n  <md-card fxFlex=\"80%\">\n    <md-card-title>Standard Card</md-card-title>\n    <md-card-subtitle>Subtitle</md-card-subtitle>\n    <md-card-content>\n      <p>Do play they miss give so up. Words to up style of since world. We leaf to snug on no need. Way\n        own uncommonly travelling now acceptance bed compliment solicitude. Dissimilar admiration so\n        terminated no in contrasted it. Advantages entreaties mr he apartments do. Limits far yet turned\n        highly repair parish talked six. Draw fond rank form nor the day eat. </p>\n    </md-card-content>\n  </md-card>\n  ");
    }
    ComponentsCardComponent.prototype.ngOnInit = function () {
    };
    ComponentsCardComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'vr-components-card',
            template: __webpack_require__("../../../../../src/app/pages/components/components-card/components-card.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], ComponentsCardComponent);
    return ComponentsCardComponent;
}());



/***/ }),

/***/ "../../../../../src/app/pages/components/components-card/components-card.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsCardModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_card_component__ = __webpack_require__("../../../../../src/app/pages/components/components-card/components-card.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_utils_utils_module__ = __webpack_require__("../../../../../src/app/core/utils/utils.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_flex_layout__ = __webpack_require__("../../../flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var ComponentsCardModule = (function () {
    function ComponentsCardModule() {
    }
    ComponentsCardModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_3__core_utils_utils_module__["a" /* UtilsModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_flex_layout__["a" /* FlexLayoutModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["e" /* MatCardModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["H" /* MatTabsModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["c" /* MatButtonModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["m" /* MatIconModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["o" /* MatListModule */]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_2__components_card_component__["a" /* ComponentsCardComponent */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__components_card_component__["a" /* ComponentsCardComponent */]]
        })
    ], ComponentsCardModule);
    return ComponentsCardModule;
}());



/***/ }),

/***/ "../../../../../src/app/pages/components/components-dialog/components-dialog.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"dialog group card alternative no-border mat-elevation-z2\" fxLayout=\"column\" fxLayoutGap=\"24px\">\r\n  <div class=\"group-title\">Material Dialog</div>\r\n  <div class=\"divider reverse\"></div>\r\n  <div class=\"section\" fxLayout=\"column\" fxLayout.gt-md=\"row\" fxLayoutAlign=\"start stretch\" fxLayoutAlign.gt-md=\"start start\" fxLayoutGap=\"16px\" fxLayoutGap.gt-md=\"24px\">\r\n    <div fxFlex.gt-md>\r\n      <div fxLayout=\"column\">\r\n        <div class=\"section-title\">Dialog</div>\r\n        <div class=\"section-tag\">&lt;md-dialog&gt;</div>\r\n        <p>MdDialogs can be used to open modal dialogs with Material Design styling and animations.</p>\r\n      </div>\r\n    </div>\r\n    <mat-tab-group class=\"preview-tabs mat-elevation-z2\" fxFlex=\"auto\" fxFlex.gt-md=\"70%\">\r\n      <mat-tab label=\"PREVIEW\">\r\n        <div class=\"preview\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n          <mat-card fxFlex=\"80%\" fxFlex.sm=\"90%\">\r\n            <mat-card-content fxLayout=\"column\" fxLayoutAlign=\"center center\">\r\n              <button mat-raised-button type=\"button\" (click)=\"openDialog()\" color=\"primary\">Open Dialog</button>\r\n              <p *ngIf=\"result\">You chose: {{ result }}</p>\r\n            </mat-card-content>\r\n          </mat-card>\r\n        </div>\r\n      </mat-tab>\r\n      <mat-tab label=\"HTML\">\r\n        <code [innerHTML]=\"dialogHTML\" vrHighlight class=\"html\"></code>\r\n      </mat-tab>\r\n    </mat-tab-group>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/pages/components/components-dialog/components-dialog.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsDialogComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return ComponentsDialogDemoDialogComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash_es_escape__ = __webpack_require__("../../../../lodash-es/escape.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ComponentsDialogComponent = (function () {
    function ComponentsDialogComponent(dialog) {
        this.dialog = dialog;
        this.dialogHTML = Object(__WEBPACK_IMPORTED_MODULE_2_lodash_es_escape__["a" /* default */])("\n  <button md-raised-button type=\"button\" (click)=\"openDialog()\" color=\"primary\">Open Dialog</button>\n  <p *ngIf=\"result\">You chose: {{ result }}</p>\n  ");
    }
    ComponentsDialogComponent.prototype.openDialog = function () {
        var _this = this;
        this.dialogRef = this.dialog.open(ComponentsDialogDemoDialogComponent, {
            disableClose: false
        });
        this.dialogRef.afterClosed().subscribe(function (result) {
            _this.result = result;
            _this.dialogRef = null;
        });
    };
    ComponentsDialogComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'vr-components-dialog',
            template: __webpack_require__("../../../../../src/app/pages/components/components-dialog/components-dialog.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_material__["i" /* MatDialog */]])
    ], ComponentsDialogComponent);
    return ComponentsDialogComponent;
}());

var ComponentsDialogDemoDialogComponent = (function () {
    function ComponentsDialogDemoDialogComponent(dialogRef) {
        this.dialogRef = dialogRef;
    }
    ComponentsDialogDemoDialogComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'vr-component-dialog-demo-dialog',
            template: "\n  <h1>Would you like to order pizza?</h1>\n  <mat-dialog-actions align=\"end\">\n    <button mat-button (click)=\"dialogRef.close('No!')\">No</button>\n    <button mat-button color=\"primary\" (click)=\"dialogRef.close('Yes!')\">Yes</button>\n  </mat-dialog-actions>\n  "
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_material__["k" /* MatDialogRef */]])
    ], ComponentsDialogDemoDialogComponent);
    return ComponentsDialogDemoDialogComponent;
}());



/***/ }),

/***/ "../../../../../src/app/pages/components/components-dialog/components-dialog.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsDialogModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_dialog_component__ = __webpack_require__("../../../../../src/app/pages/components/components-dialog/components-dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_utils_utils_module__ = __webpack_require__("../../../../../src/app/core/utils/utils.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_flex_layout__ = __webpack_require__("../../../flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var ComponentsDialogModule = (function () {
    function ComponentsDialogModule() {
    }
    ComponentsDialogModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_3__core_utils_utils_module__["a" /* UtilsModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_flex_layout__["a" /* FlexLayoutModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["e" /* MatCardModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["H" /* MatTabsModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["j" /* MatDialogModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["c" /* MatButtonModule */]
            ],
            entryComponents: [__WEBPACK_IMPORTED_MODULE_2__components_dialog_component__["b" /* ComponentsDialogDemoDialogComponent */]],
            declarations: [__WEBPACK_IMPORTED_MODULE_2__components_dialog_component__["a" /* ComponentsDialogComponent */], __WEBPACK_IMPORTED_MODULE_2__components_dialog_component__["b" /* ComponentsDialogDemoDialogComponent */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__components_dialog_component__["a" /* ComponentsDialogComponent */]]
        })
    ], ComponentsDialogModule);
    return ComponentsDialogModule;
}());



/***/ }),

/***/ "../../../../../src/app/pages/components/components-gridlist/components-gridlist.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"gridlist group card alternative no-border mat-elevation-z2\" fxLayout=\"column\" fxLayoutGap=\"24px\">\r\n  <div class=\"group-title\">Material Grid List</div>\r\n  <div class=\"divider reverse\"></div>\r\n  <div class=\"section\" fxLayout=\"column\" fxLayout.gt-md=\"row\" fxLayoutAlign=\"start stretch\" fxLayoutAlign.gt-md=\"start start\" fxLayoutGap=\"16px\" fxLayoutGap.gt-md=\"24px\">\r\n    <div fxFlex.gt-md>\r\n      <div fxLayout=\"column\">\r\n        <div class=\"section-title\">Grid List</div>\r\n        <div class=\"section-tag\">&lt;mat-grid-list&gt;</div>\r\n        <p>An mat-grid-list is an alternative list view that arranges cells into grid-based layout.</p>\r\n        <h5>Example:</h5>\r\n        <code vrHighlight class=\"html\">&lt;mat-grid-list cols=\"4\"&gt;<br/>  &lt;mat-grid-tile&gt;Content 1&lt;/mat-grid-tile&gt;<br/>&lt;/mat-grid-list&gt;</code>\r\n      </div>\r\n    </div>\r\n    <mat-tab-group class=\"preview-tabs mat-elevation-z2\" fxFlex=\"auto\" fxFlex.gt-md=\"70%\">\r\n      <mat-tab label=\"PREVIEW\">\r\n        <div class=\"preview\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n          <mat-card fxFlex=\"90%\" fxFlex.gt-sm=\"80%\">\r\n            <mat-card-content>\r\n              <mat-grid-list cols=\"4\" rowHeight=\"100px\">\r\n                <mat-grid-tile *ngFor=\"let tile of tiles\" [colspan]=\"tile.cols\" [rowspan]=\"tile.rows\"\r\n                              [style.background]=\"tile.color\">\r\n                  {{tile.text}}\r\n                </mat-grid-tile>\r\n              </mat-grid-list>\r\n            </mat-card-content>\r\n          </mat-card>\r\n        </div>\r\n      </mat-tab>\r\n      <mat-tab label=\"HTML\">\r\n        <code [innerHTML]=\"gridListHTML\" vrHighlight class=\"html\"></code>\r\n      </mat-tab>\r\n    </mat-tab-group>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/pages/components/components-gridlist/components-gridlist.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsGridlistComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash_es_escape__ = __webpack_require__("../../../../lodash-es/escape.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ComponentsGridlistComponent = (function () {
    function ComponentsGridlistComponent() {
        this.tiles = [
            { text: 'One', cols: 3, rows: 1, color: 'lightblue' },
            { text: 'Two', cols: 1, rows: 2, color: 'lightgreen' },
            { text: 'Three', cols: 1, rows: 1, color: 'lightpink' },
            { text: 'Four', cols: 2, rows: 1, color: '#DDBDF1' },
        ];
        this.gridListHTML = Object(__WEBPACK_IMPORTED_MODULE_1_lodash_es_escape__["a" /* default */])("\n  <md-grid-list cols=\"4\" rowHeight=\"100px\">\n    <md-grid-tile *ngFor=\"let tile of tiles\" [colspan]=\"tile.cols\" [rowspan]=\"tile.rows\"\n                  [style.background]=\"tile.color\">\n      {{tile.text}}\n    </md-grid-tile>\n  </md-grid-list>\n  ");
    }
    ComponentsGridlistComponent.prototype.ngOnInit = function () {
    };
    ComponentsGridlistComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'vr-components-gridlist',
            template: __webpack_require__("../../../../../src/app/pages/components/components-gridlist/components-gridlist.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], ComponentsGridlistComponent);
    return ComponentsGridlistComponent;
}());



/***/ }),

/***/ "../../../../../src/app/pages/components/components-gridlist/components-gridlist.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsGridlistModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_utils_utils_module__ = __webpack_require__("../../../../../src/app/core/utils/utils.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_flex_layout__ = __webpack_require__("../../../flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_gridlist_component__ = __webpack_require__("../../../../../src/app/pages/components/components-gridlist/components-gridlist.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var ComponentsGridlistModule = (function () {
    function ComponentsGridlistModule() {
    }
    ComponentsGridlistModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_2__core_utils_utils_module__["a" /* UtilsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_flex_layout__["a" /* FlexLayoutModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["e" /* MatCardModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["H" /* MatTabsModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["l" /* MatGridListModule */]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_5__components_gridlist_component__["a" /* ComponentsGridlistComponent */]],
            exports: [__WEBPACK_IMPORTED_MODULE_5__components_gridlist_component__["a" /* ComponentsGridlistComponent */]]
        })
    ], ComponentsGridlistModule);
    return ComponentsGridlistModule;
}());



/***/ }),

/***/ "../../../../../src/app/pages/components/components-list/components-list.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"card group card alternative no-border mat-elevation-z2\" fxLayout=\"column\" fxLayoutGap=\"24px\">\r\n  <div class=\"group-title\">Material List</div>\r\n  <div class=\"divider reverse\"></div>\r\n  <div class=\"section\" fxLayout=\"column\" fxLayout.gt-md=\"row\" fxLayoutAlign=\"start stretch\" fxLayoutAlign.gt-md=\"start start\" fxLayoutGap=\"16px\" fxLayoutGap.gt-md=\"24px\">\r\n    <div fxFlex.gt-md>\r\n      <div fxLayout=\"column\">\r\n        <div class=\"section-title\">Two-Line List with Avatars</div>\r\n        <div class=\"section-tag\">&lt;mat-list&gt;</div>\r\n        <p>Lists are made up of a continuous column of rows. Each row contains a tile. Primary actions fill the tile, and supplemental actions are represented by icons and text.</p>\r\n        <div class=\"section-info\">Example:</div>\r\n        <code vrHighlight class=\"html\">&lt;mat-list&gt;<br/>  &lt;mat-list-item&gt;<br/>    &lt;img mat-list-avatar src=\"avatar.png\"&gt;<br/>    &lt;h3 matLine&gt;John&lt;/h3&gt;<br/>    &lt;p matLine&gt;Brunch?&lt;/p&gt;<br/>  &lt;mat-list-item&gt;<br/>&lt;/mat-list&gt;</code>\r\n      </div>\r\n    </div>\r\n    <mat-tab-group class=\"preview-tabs mat-elevation-z2\" fxFlex=\"auto\" fxFlex.gt-md=\"70%\">\r\n      <mat-tab label=\"PREVIEW\">\r\n        <div class=\"preview\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n          <mat-list class=\"demo-list mat-elevation-z2\">\r\n            <mat-list-item>\r\n              <img mat-list-avatar src=\"assets/img/demo/avatars/1.png\">\r\n              <h3 matLine>John</h3>\r\n              <p matLine>\r\n                <span>Brunch?</span>\r\n                <span class=\"subline\">-- Did you want to go on Sunday? I was thinking</span>\r\n              </p>\r\n            </mat-list-item>\r\n            <mat-list-item>\r\n              <img mat-list-avatar src=\"assets/img/demo/avatars/2.png\">\r\n              <h3 matLine>Peter</h3>\r\n              <p matLine>\r\n                <span>Summer BBQ</span>\r\n                <span class=\"subline\">-- Wish I could come, but I have some special</span>\r\n              </p>\r\n            </mat-list-item>\r\n            <mat-list-item>\r\n              <img mat-list-avatar src=\"assets/img/demo/avatars/3.png\">\r\n              <h3 matLine>Nancy</h3>\r\n              <p matLine>\r\n                <span>Oui oui</span>\r\n                <span class=\"subline\">-- Have you booked the Paris trip?</span>\r\n              </p>\r\n            </mat-list-item>\r\n          </mat-list>\r\n        </div>\r\n      </mat-tab>\r\n      <mat-tab label=\"HTML\">\r\n        <code [innerHTML]=\"twoLineListHTML\" vrHighlight class=\"html\"></code>\r\n      </mat-tab>\r\n    </mat-tab-group>\r\n  </div>\r\n\r\n  <div class=\"divider reverse\"></div>\r\n\r\n  <div class=\"section\" fxLayout=\"column\" fxLayout.gt-md=\"row\" fxLayoutAlign=\"start stretch\" fxLayoutAlign.gt-md=\"start start\" fxLayoutGap=\"16px\" fxLayoutGap.gt-md=\"24px\">\r\n    <div fxFlex.gt-md>\r\n      <div fxLayout=\"column\">\r\n        <div class=\"section-title\">Three-Line List with Avatars</div>\r\n        <p>Lists with three lines add extra content to distinguish your list-item.</p>\r\n        <div class=\"section-info\">Example:</div>\r\n        <code vrHighlight class=\"html\">&lt;mat-list&gt;<br/>  &lt;mat-list-item&gt;<br/>    &lt;img mat-list-avatar src=\"avatar.png\"&gt;<br/>    &lt;h3 matLine&gt;John&lt;/h3&gt;<br/>    &lt;p matLine&gt;Brunch?&lt;/p&gt;<br/>    &lt;p matLine class=\"subline\"&gt;<br/>      Did you want to go on Sunday?<br/>    &lt;/p&gt;<br/>  &lt;mat-list-item&gt;<br/>&lt;/mat-list&gt;</code>\r\n      </div>\r\n    </div>\r\n    <mat-tab-group class=\"preview-tabs mat-elevation-z2\" fxFlex=\"auto\" fxFlex.gt-md=\"70%\">\r\n      <mat-tab label=\"PREVIEW\">\r\n        <div class=\"preview\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n          <mat-list class=\"demo-list mat-elevation-z2\">\r\n            <mat-list-item>\r\n              <img mat-list-avatar src=\"assets/img/demo/avatars/1.png\">\r\n              <h3 matLine>John</h3>\r\n              <p matLine>Brunch?</p>\r\n              <p matLine class=\"subline\">Did you want to go on Sunday? I was thinking</p>\r\n            </mat-list-item>\r\n            <mat-list-item>\r\n              <img mat-list-avatar src=\"assets/img/demo/avatars/2.png\">\r\n              <h3 matLine>Peter</h3>\r\n              <p matLine>Summer BBQ</p>\r\n              <p matLine class=\"subline\">Wish I could come, but I have some special</p>\r\n            </mat-list-item>\r\n            <mat-list-item>\r\n              <img mat-list-avatar src=\"assets/img/demo/avatars/3.png\">\r\n              <h3 matLine>Nancy</h3>\r\n              <p matLine>Oui oui</p>\r\n              <p matLine class=\"subline\">Have you booked the Paris trip?</p>\r\n            </mat-list-item>\r\n          </mat-list>\r\n        </div>\r\n      </mat-tab>\r\n      <mat-tab label=\"HTML\">\r\n        <code [innerHTML]=\"threeLineListHTML\" vrHighlight class=\"html\"></code>\r\n      </mat-tab>\r\n    </mat-tab-group>\r\n  </div>\r\n\r\n  <div class=\"divider reverse\"></div>\r\n\r\n  <div class=\"section\" fxLayout=\"column\" fxLayout.gt-md=\"row\" fxLayoutAlign=\"start stretch\" fxLayoutAlign.gt-md=\"start start\" fxLayoutGap=\"16px\" fxLayoutGap.gt-md=\"24px\">\r\n    <div fxFlex.gt-md fxLayout=\"column\">\r\n      <div class=\"section-title\">Three-Line List</div>\r\n      <mat-tab-group class=\"preview-tabs mat-elevation-z2\" fxFlex=\"auto\" fxFlex.gt-md=\"70%\">\r\n        <mat-tab label=\"PREVIEW\">\r\n          <div class=\"preview\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n            <mat-list class=\"demo-list mat-elevation-z2\">\r\n              <mat-list-item>\r\n                <h3 matLine>John</h3>\r\n                <p matLine>Brunch?</p>\r\n                <p matLine class=\"subline\">Did you want to go on Sunday? I was thinking</p>\r\n              </mat-list-item>\r\n              <mat-list-item>\r\n                <h3 matLine>Peter</h3>\r\n                <p matLine>Summer BBQ</p>\r\n                <p matLine class=\"subline\">Wish I could come, but I have some special</p>\r\n              </mat-list-item>\r\n              <mat-list-item>\r\n                <h3 matLine>Nancy</h3>\r\n                <p matLine>Oui oui</p>\r\n                <p matLine class=\"subline\">Have you booked the Paris trip?</p>\r\n              </mat-list-item>\r\n            </mat-list>\r\n          </div>\r\n        </mat-tab>\r\n        <mat-tab label=\"HTML\">\r\n          <code [innerHTML]=\"threeLineListWithoutAvatarsHTML\" vrHighlight class=\"html\"></code>\r\n        </mat-tab>\r\n      </mat-tab-group>\r\n    </div>\r\n\r\n    <div fxFlex.gt-md fxLayout=\"column\">\r\n      <div class=\"section-title\">One-Line List</div>\r\n      <div class=\"preview-tabs mat-elevation-z2\" fxFlex=\"auto\" fxFlex.gt-md=\"70%\">\r\n        <mat-tab-group>\r\n          <mat-tab label=\"PREVIEW\">\r\n            <div class=\"preview\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              <mat-list class=\"demo-list mat-elevation-z2\">\r\n                <mat-list-item>\r\n                  <h3 matLine>John</h3>\r\n                </mat-list-item>\r\n                <mat-list-item>\r\n                  <h3 matLine>Peter</h3>\r\n                </mat-list-item>\r\n                <mat-list-item>\r\n                  <h3 matLine>Nancy</h3>\r\n                </mat-list-item>\r\n              </mat-list>\r\n            </div>\r\n          </mat-tab>\r\n          <mat-tab label=\"HTML\">\r\n            <code [innerHTML]=\"oneLineList\" vrHighlight class=\"html\"></code>\r\n          </mat-tab>\r\n        </mat-tab-group>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/pages/components/components-list/components-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsListComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash_es_escape__ = __webpack_require__("../../../../lodash-es/escape.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ComponentsListComponent = (function () {
    function ComponentsListComponent() {
        this.twoLineListHTML = Object(__WEBPACK_IMPORTED_MODULE_1_lodash_es_escape__["a" /* default */])("\n  <md-list class=\"demo-list mat-elevation-z2\">\n    <md-list-item>\n      <img md-list-avatar src=\"assets/img/avatars/John.png\">\n      <h3 md-line>John</h3>\n      <p md-line>\n        <span>Brunch?</span>\n        <span class=\"subline\">-- Did you want to go on Sunday? I was thinking</span>\n      </p>\n    </md-list-item>\n    <md-list-item>\n      <img md-list-avatar src=\"assets/img/avatars/Peter.png\">\n      <h3 md-line>Peter</h3>\n      <p md-line>\n        <span>Summer BBQ</span>\n        <span class=\"subline\">-- Wish I could come, but I have some special</span>\n      </p>\n    </md-list-item>\n    <md-list-item>\n      <img md-list-avatar src=\"assets/img/avatars/Nancy.png\">\n      <h3 md-line>Nancy</h3>\n      <p md-line>\n        <span>Oui oui</span>\n        <span class=\"subline\">-- Have you booked the Paris trip?</span>\n      </p>\n    </md-list-item>\n  </md-list>\n  ");
        this.threeLineListHTML = Object(__WEBPACK_IMPORTED_MODULE_1_lodash_es_escape__["a" /* default */])("\n  <md-list class=\"demo-list mat-elevation-z2\">\n    <md-list-item>\n      <img md-list-avatar src=\"assets/img/avatars/John.png\">\n      <h3 md-line>John</h3>\n      <p md-line>Brunch?</p>\n      <p md-line class=\"subline\">Did you want to go on Sunday? I was thinking</p>\n    </md-list-item>\n    <md-list-item>\n      <img md-list-avatar src=\"assets/img/avatars/Peter.png\">\n      <h3 md-line>Peter</h3>\n      <p md-line>Summer BBQ</p>\n      <p md-line class=\"subline\">Wish I could come, but I have some special</p>\n    </md-list-item>\n    <md-list-item>\n      <img md-list-avatar src=\"assets/img/avatars/Nancy.png\">\n      <h3 md-line>Nancy</h3>\n      <p md-line>Oui oui</p>\n      <p md-line class=\"subline\">Have you booked the Paris trip?</p>\n    </md-list-item>\n  </md-list>\n  ");
        this.threeLineListWithoutAvatarsHTML = Object(__WEBPACK_IMPORTED_MODULE_1_lodash_es_escape__["a" /* default */])("\n  <md-list class=\"demo-list mat-elevation-z2\">\n    <md-list-item>\n      <h3 md-line>John</h3>\n      <p md-line>Brunch?</p>\n      <p md-line class=\"subline\">Did you want to go on Sunday? I was thinking</p>\n    </md-list-item>\n    <md-list-item>\n      <h3 md-line>Peter</h3>\n      <p md-line>Summer BBQ</p>\n      <p md-line class=\"subline\">Wish I could come, but I have some special</p>\n    </md-list-item>\n    <md-list-item>\n      <h3 md-line>Nancy</h3>\n      <p md-line>Oui oui</p>\n      <p md-line class=\"subline\">Have you booked the Paris trip?</p>\n    </md-list-item>\n  </md-list>\n  ");
        this.oneLineList = Object(__WEBPACK_IMPORTED_MODULE_1_lodash_es_escape__["a" /* default */])("\n  <md-list class=\"demo-list mat-elevation-z2\">\n    <md-list-item>\n      <h3 md-line>John</h3>\n    </md-list-item>\n    <md-list-item>\n      <h3 md-line>Peter</h3>\n    </md-list-item>\n    <md-list-item>\n      <h3 md-line>Nancy</h3>\n    </md-list-item>\n  </md-list>\n  ");
    }
    ComponentsListComponent.prototype.ngOnInit = function () {
    };
    ComponentsListComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'vr-components-list',
            template: __webpack_require__("../../../../../src/app/pages/components/components-list/components-list.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], ComponentsListComponent);
    return ComponentsListComponent;
}());



/***/ }),

/***/ "../../../../../src/app/pages/components/components-list/components-list.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsListModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_list_component__ = __webpack_require__("../../../../../src/app/pages/components/components-list/components-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_utils_utils_module__ = __webpack_require__("../../../../../src/app/core/utils/utils.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_flex_layout__ = __webpack_require__("../../../flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var ComponentsListModule = (function () {
    function ComponentsListModule() {
    }
    ComponentsListModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_3__core_utils_utils_module__["a" /* UtilsModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_flex_layout__["a" /* FlexLayoutModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["e" /* MatCardModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["H" /* MatTabsModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["o" /* MatListModule */]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_2__components_list_component__["a" /* ComponentsListComponent */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__components_list_component__["a" /* ComponentsListComponent */]]
        })
    ], ComponentsListModule);
    return ComponentsListModule;
}());



/***/ }),

/***/ "../../../../../src/app/pages/components/components-menu/components-menu.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"autocomplete group card alternative no-border mat-elevation-z2\" fxLayout=\"column\" fxLayoutGap=\"24px\">\r\n  <div class=\"group-title\">Material Menu</div>\r\n  <div class=\"divider reverse\"></div>\r\n  <div class=\"section\" fxLayout=\"column\" fxLayout.gt-md=\"row\" fxLayoutAlign=\"start stretch\" fxLayoutAlign.gt-md=\"start start\" fxLayoutGap=\"16px\" fxLayoutGap.gt-md=\"24px\">\r\n    <div fxFlex.gt-md>\r\n      <div fxLayout=\"column\">\r\n        <div class=\"section-title\">Menu with Icons</div>\r\n        <div class=\"section-tag\">&lt;mat-menu&gt;</div>\r\n        <p>Menus offer a list of options that are displayed when triggered. The position (before, after) and (above, below) is automatically decided, but can be overridden with e.g. x-position='before'.</p>\r\n        <div class=\"section-info\">Example:</div>\r\n        <code vrHighlight class=\"html\">&lt;mat-menu&gt;<br/>  &lt;button mat-menu-item&gt;<br/>    &lt;mat-icon&gt;dialpad&lt;/mat-icon&gt;<br/>  &lt;/button&gt;<br/>&lt;/mat-menu&gt;</code>\r\n      </div>\r\n    </div>\r\n    <mat-tab-group class=\"preview-tabs mat-elevation-z2\" fxFlex=\"auto\" fxFlex.gt-md=\"70%\">\r\n      <mat-tab label=\"PREVIEW\">\r\n        <div class=\"preview\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n          <mat-card fxFlex=\"90%\" fxFlex.gt-sm=\"80%\">\r\n            <mat-card-content fxLayout=\"column\" fxLayout.gt-sm=\"row\" fxLayoutAlign=\"center center\">\r\n              <button mat-icon-button [matMenuTriggerFor]=\"menu\">\r\n                <mat-icon>more_vert</mat-icon>\r\n              </button>\r\n\r\n              <mat-menu #menu=\"matMenu\">\r\n                <button mat-menu-item>\r\n                  <mat-icon> dialpad </mat-icon>\r\n                  <span> Redial </span>\r\n                </button>\r\n                <button mat-menu-item disabled>\r\n                  <mat-icon> voicemail </mat-icon>\r\n                  <span> Check voicemail </span>\r\n                </button>\r\n                <button mat-menu-item>\r\n                  <mat-icon> notifications_off </mat-icon>\r\n                  <span> Disable alerts </span>\r\n                </button>\r\n              </mat-menu>\r\n            </mat-card-content>\r\n          </mat-card>\r\n        </div>\r\n      </mat-tab>\r\n      <mat-tab label=\"HTML\">\r\n        <code [innerHTML]=\"menuHTML\" vrHighlight class=\"html\"></code>\r\n      </mat-tab>\r\n    </mat-tab-group>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/pages/components/components-menu/components-menu.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsMenuComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash_es_escape__ = __webpack_require__("../../../../lodash-es/escape.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ComponentsMenuComponent = (function () {
    function ComponentsMenuComponent() {
        this.menuHTML = Object(__WEBPACK_IMPORTED_MODULE_1_lodash_es_escape__["a" /* default */])("\n  <button md-icon-button [mdMenuTriggerFor]=\"menu\">\n    <md-icon>more_vert</md-icon>\n  </button>\n  <md-menu #menu=\"mdMenu\">\n    <button md-menu-item>\n      <md-icon> dialpad </md-icon>\n      <span> Redial </span>\n    </button>\n    <button md-menu-item disabled>\n      <md-icon> voicemail </md-icon>\n      <span> Check voicemail </span>\n    </button>\n    <button md-menu-item>\n      <md-icon> notifications_off </md-icon>\n      <span> Disable alerts </span>\n    </button>\n  </md-menu>\n  ");
    }
    ComponentsMenuComponent.prototype.ngOnInit = function () {
    };
    ComponentsMenuComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'vr-components-menu',
            template: __webpack_require__("../../../../../src/app/pages/components/components-menu/components-menu.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], ComponentsMenuComponent);
    return ComponentsMenuComponent;
}());



/***/ }),

/***/ "../../../../../src/app/pages/components/components-menu/components-menu.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsMenuModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_menu_component__ = __webpack_require__("../../../../../src/app/pages/components/components-menu/components-menu.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_utils_utils_module__ = __webpack_require__("../../../../../src/app/core/utils/utils.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_flex_layout__ = __webpack_require__("../../../flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var ComponentsMenuModule = (function () {
    function ComponentsMenuModule() {
    }
    ComponentsMenuModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_3__core_utils_utils_module__["a" /* UtilsModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_flex_layout__["a" /* FlexLayoutModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["e" /* MatCardModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["H" /* MatTabsModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["p" /* MatMenuModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["m" /* MatIconModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["c" /* MatButtonModule */]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_2__components_menu_component__["a" /* ComponentsMenuComponent */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__components_menu_component__["a" /* ComponentsMenuComponent */]]
        })
    ], ComponentsMenuModule);
    return ComponentsMenuModule;
}());



/***/ }),

/***/ "../../../../../src/app/pages/components/components-progress/components-progress.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"card group card alternative no-border mat-elevation-z2\" fxLayout=\"column\" fxLayoutGap=\"24px\">\r\n  <div class=\"group-title\">Material Progress</div>\r\n  <div class=\"divider reverse\"></div>\r\n  <div class=\"section\" fxLayout=\"column\" fxLayout.gt-md=\"row\" fxLayoutAlign=\"start stretch\"\r\n       fxLayoutAlign.gt-md=\"start start\" fxLayoutGap=\"16px\" fxLayoutGap.gt-md=\"24px\">\r\n    <div fxFlex.gt-md>\r\n      <div fxLayout=\"column\">\r\n        <div class=\"section-title\">Progress Bar</div>\r\n        <div class=\"section-tag\">&lt;mat-progress-bar&gt;</div>\r\n        <p>Progress indicators allow you to display the progress of e.g. background tasks running.</p>\r\n        <div class=\"section-info\">Example:</div>\r\n        <p><code vrHighlight class=\"html\">&lt;mat-progress-bar<br/> mode=\"indeterminate\"&gt;<br/>&lt;/mat-progress-bar&gt;</code>\r\n        </p>\r\n      </div>\r\n    </div>\r\n    <mat-tab-group class=\"preview-tabs mat-elevation-z2\" fxFlex=\"auto\" fxFlex.gt-md=\"70%\">\r\n      <mat-tab label=\"PREVIEW\">\r\n        <div class=\"preview\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n          <mat-card fxFlex=\"80%\">\r\n            <mat-card-content>\r\n\r\n              <section fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n                <div class=\"margin\">Color:</div>\r\n                <mat-radio-group [(ngModel)]=\"color\">\r\n                  <mat-radio-button class=\"margin\" value=\"primary\">\r\n                    Primary\r\n                  </mat-radio-button>\r\n                  <mat-radio-button class=\"margin\" value=\"accent\">\r\n                    Accent\r\n                  </mat-radio-button>\r\n                  <mat-radio-button class=\"margin\" value=\"warn\">\r\n                    Warn\r\n                  </mat-radio-button>\r\n                </mat-radio-group>\r\n              </section>\r\n\r\n              <section fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n                <div class=\"margin\">Mode:</div>\r\n                <mat-radio-group [(ngModel)]=\"mode\">\r\n                  <mat-radio-button class=\"margin\" value=\"determinate\">\r\n                    Determinate\r\n                  </mat-radio-button>\r\n                  <mat-radio-button class=\"margin\" value=\"indeterminate\">\r\n                    Indeterminate\r\n                  </mat-radio-button>\r\n                  <mat-radio-button class=\"margin\" value=\"buffer\">\r\n                    Buffer\r\n                  </mat-radio-button>\r\n                  <mat-radio-button class=\"margin\" value=\"query\">\r\n                    Query\r\n                  </mat-radio-button>\r\n                </mat-radio-group>\r\n              </section>\r\n\r\n              <section fxLayout=\"row\" fxLayoutAlign=\"start center\" *ngIf=\"mode == 'determinate' || mode == 'buffer'\">\r\n                <div class=\"margin\">Progress:</div>\r\n                <mat-slider class=\"margin\" [(ngModel)]=\"progressValue\"></mat-slider>\r\n              </section>\r\n              <section fxLayout=\"row\" fxLayoutAlign=\"start center\" *ngIf=\"mode == 'buffer'\">\r\n                <div class=\"margin\">Buffer:</div>\r\n                <mat-slider class=\"margin\" [(ngModel)]=\"bufferValue\"></mat-slider>\r\n              </section>\r\n\r\n              <section class=\"margin\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n                <mat-progress-bar\r\n                  class=\"margin\"\r\n                  [color]=\"color\"\r\n                  [mode]=\"mode\"\r\n                  [value]=\"progressValue\"\r\n                  [bufferValue]=\"bufferValue\">\r\n                </mat-progress-bar>\r\n              </section>\r\n            </mat-card-content>\r\n          </mat-card>\r\n        </div>\r\n      </mat-tab>\r\n      <mat-tab label=\"HTML\">\r\n        <code [innerHTML]=\"progressbarHTML\" vrHighlight class=\"html\"></code>\r\n      </mat-tab>\r\n    </mat-tab-group>\r\n  </div>\r\n\r\n  <div class=\"divider reverse\"></div>\r\n\r\n  <div class=\"section\" fxLayout=\"column\" fxLayout.gt-md=\"row\" fxLayoutAlign=\"start stretch\"\r\n       fxLayoutAlign.gt-md=\"start start\" fxLayoutGap=\"16px\" fxLayoutGap.gt-md=\"24px\">\r\n    <div fxLayout=\"column\" fxFlex.gt-md>\r\n      <div class=\"section-title\">Progress Spinner</div>\r\n      <div class=\"section-tag\">&lt;mat-progress-spinner&gt;</div>\r\n      <p>An alternative progress indicator to allow for a blocking loader.</p>\r\n      <div class=\"section-info\">Example:</div>\r\n      <p><code vrHighlight class=\"html\">&lt;mat-progress-spinner<br/> mode=\"indeterminate\"&gt;<br/>&lt;/mat-progress-spinner&gt;</code>\r\n      </p>\r\n    </div>\r\n    <mat-tab-group class=\"preview-tabs mat-elevation-z2\" fxFlex=\"auto\" fxFlex.gt-md=\"70%\">\r\n      <mat-tab label=\"PREVIEW\">\r\n        <div class=\"preview\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n          <mat-card fxFlex=\"80%\">\r\n            <mat-card-content fxLayout=\"column\">\r\n              <section fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n                <label class=\"margin\">Color:</label>\r\n                <mat-radio-group [(ngModel)]=\"spinnerColor\">\r\n                  <mat-radio-button class=\"margin\" value=\"primary\">\r\n                    Primary\r\n                  </mat-radio-button>\r\n                  <mat-radio-button class=\"margin\" value=\"accent\">\r\n                    Accent\r\n                  </mat-radio-button>\r\n                  <mat-radio-button class=\"margin\" value=\"warn\">\r\n                    Warn\r\n                  </mat-radio-button>\r\n                </mat-radio-group>\r\n              </section>\r\n\r\n              <section fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n                <label class=\"margin\">Mode:</label>\r\n                <mat-radio-group [(ngModel)]=\"spinnerMode\">\r\n                  <mat-radio-button class=\"margin\" value=\"determinate\">\r\n                    Determinate\r\n                  </mat-radio-button>\r\n                  <mat-radio-button class=\"margin\" value=\"indeterminate\">\r\n                    Indeterminate\r\n                  </mat-radio-button>\r\n                </mat-radio-group>\r\n              </section>\r\n\r\n              <section fxLayout=\"row\" fxLayoutAlign=\"start center\" *ngIf=\"spinnerMode == 'determinate'\">\r\n                <label class=\"margin\">Progress:</label>\r\n                <mat-slider class=\"margin\" [(ngModel)]=\"spinnerValue\"></mat-slider>\r\n              </section>\r\n\r\n              <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n                <mat-progress-spinner\r\n                  class=\"margin\"\r\n                  [color]=\"spinnerColor\"\r\n                  [mode]=\"spinnerMode\"\r\n                  [value]=\"spinnerValue\">\r\n                </mat-progress-spinner>\r\n              </div>\r\n            </mat-card-content>\r\n          </mat-card>\r\n        </div>\r\n      </mat-tab>\r\n      <mat-tab label=\"HTML\">\r\n        <code [innerHTML]=\"progressSpinnerHTML\" vrHighlight class=\"html\"></code>\r\n      </mat-tab>\r\n    </mat-tab-group>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/pages/components/components-progress/components-progress.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".margin {\n  margin: 0 10px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/pages/components/components-progress/components-progress.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsProgressComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash_es_escape__ = __webpack_require__("../../../../lodash-es/escape.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ComponentsProgressComponent = (function () {
    function ComponentsProgressComponent() {
        this.color = 'accent';
        this.mode = 'indeterminate';
        this.progressValue = 50;
        this.bufferValue = 75;
        this.spinnerColor = 'accent';
        this.spinnerMode = 'indeterminate';
        this.spinnerValue = 50;
        this.progressbarHTML = Object(__WEBPACK_IMPORTED_MODULE_1_lodash_es_escape__["a" /* default */])("<section fxLayout=\"row\" fxLayoutAlign=\"start center\">\n  <div class=\"margin\">Color:</div>\n  <md-radio-group [(ngModel)]=\"color\">\n    <md-radio-button class=\"margin\" value=\"primary\">\n      Primary\n    </md-radio-button>\n    <md-radio-button class=\"margin\" value=\"accent\">\n      Accent\n    </md-radio-button>\n    <md-radio-button class=\"margin\" value=\"warn\">\n      Warn\n    </md-radio-button>\n  </md-radio-group>\n</section>\n\n<section fxLayout=\"row\" fxLayoutAlign=\"start center\">\n  <div class=\"margin\">Mode:</div>\n  <md-radio-group [(ngModel)]=\"mode\">\n    <md-radio-button class=\"margin\" value=\"determinate\">\n      Determinate\n    </md-radio-button>\n    <md-radio-button class=\"margin\" value=\"indeterminate\">\n      Indeterminate\n    </md-radio-button>\n    <md-radio-button class=\"margin\" value=\"buffer\">\n      Buffer\n    </md-radio-button>\n    <md-radio-button class=\"margin\" value=\"query\">\n      Query\n    </md-radio-button>\n  </md-radio-group>\n</section>\n\n<section fxLayout=\"row\" fxLayoutAlign=\"start center\" *ngIf=\"mode == 'determinate' || mode == 'buffer'\">\n  <div class=\"margin\">Progress:</div>\n  <md-slider class=\"margin\" [(ngModel)]=\"progressValue\"></md-slider>\n</section>\n<section fxLayout=\"row\" fxLayoutAlign=\"start center\" *ngIf=\"mode == 'buffer'\">\n  <div class=\"margin\">Buffer:</div>\n  <md-slider class=\"margin\" [(ngModel)]=\"bufferValue\"></md-slider>\n</section>\n\n<section fxLayout=\"row\" fxLayoutAlign=\"start center\">\n  <md-progress-bar\n    class=\"margin\"\n    [color]=\"color\"\n    [mode]=\"mode\"\n    [value]=\"progressValue\"\n    [bufferValue]=\"bufferValue\">\n  </md-progress-bar>\n</section>");
        this.progressSpinnerHTML = Object(__WEBPACK_IMPORTED_MODULE_1_lodash_es_escape__["a" /* default */])("<section fxLayout=\"row\" fxLayoutAlign=\"start center\">\n  <label class=\"margin\">Color:</label>\n  <md-radio-group [(ngModel)]=\"spinnerColor\">\n    <md-radio-button class=\"margin\" value=\"primary\">\n      Primary\n    </md-radio-button>\n    <md-radio-button class=\"margin\" value=\"accent\">\n      Accent\n    </md-radio-button>\n    <md-radio-button class=\"margin\" value=\"warn\">\n      Warn\n    </md-radio-button>\n  </md-radio-group>\n</section>\n\n<section fxLayout=\"row\" fxLayoutAlign=\"start center\">\n  <label class=\"margin\">Mode:</label>\n  <md-radio-group [(ngModel)]=\"spinnerMode\">\n    <md-radio-button class=\"margin\" value=\"determinate\">\n      Determinate\n    </md-radio-button>\n    <md-radio-button class=\"margin\" value=\"indeterminate\">\n      Indeterminate\n    </md-radio-button>\n  </md-radio-group>\n</section>\n\n<section fxLayout=\"row\" fxLayoutAlign=\"start center\" *ngIf=\"spinnerMode == 'determinate'\">\n  <label class=\"margin\">Progress:</label>\n  <md-slider class=\"margin\" [(ngModel)]=\"spinnerValue\"></md-slider>\n</section>\n\n<div fxLayout=\"row\" fxLayoutAlign=\"center center\">\n  <md-progress-spinner\n    class=\"margin\"\n    [color]=\"spinnerColor\"\n    [mode]=\"spinnerMode\"\n    [value]=\"spinnerValue\">\n  </md-progress-spinner>\n</div>");
    }
    ComponentsProgressComponent.prototype.ngOnInit = function () {
    };
    ComponentsProgressComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'vr-components-progress',
            template: __webpack_require__("../../../../../src/app/pages/components/components-progress/components-progress.component.html"),
            styles: [__webpack_require__("../../../../../src/app/pages/components/components-progress/components-progress.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ComponentsProgressComponent);
    return ComponentsProgressComponent;
}());



/***/ }),

/***/ "../../../../../src/app/pages/components/components-progress/components-progress.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsProgressModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_progress_component__ = __webpack_require__("../../../../../src/app/pages/components/components-progress/components-progress.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_utils_utils_module__ = __webpack_require__("../../../../../src/app/core/utils/utils.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_flex_layout__ = __webpack_require__("../../../flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var ComponentsProgressModule = (function () {
    function ComponentsProgressModule() {
    }
    ComponentsProgressModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_forms__["e" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_forms__["j" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__core_utils_utils_module__["a" /* UtilsModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_flex_layout__["a" /* FlexLayoutModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["e" /* MatCardModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["H" /* MatTabsModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["v" /* MatProgressSpinnerModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["u" /* MatProgressBarModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["w" /* MatRadioModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["B" /* MatSliderModule */]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_2__components_progress_component__["a" /* ComponentsProgressComponent */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__components_progress_component__["a" /* ComponentsProgressComponent */]]
        })
    ], ComponentsProgressModule);
    return ComponentsProgressModule;
}());



/***/ }),

/***/ "../../../../../src/app/pages/components/components-slider/components-slider.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"card alternative group no-border mat-elevation-z2\" fxLayout=\"column\" fxLayoutGap=\"24px\">\r\n  <div class=\"group-title\">Material Slider</div>\r\n  <div class=\"divider reverse\"></div>\r\n  <div class=\"section\" fxLayout=\"column\" fxLayout.gt-md=\"row\" fxLayoutAlign=\"start stretch\" fxLayoutAlign.gt-md=\"start start\" fxLayoutGap=\"16px\" fxLayoutGap.gt-md=\"24px\">\r\n    <div fxFlex.gt-md>\r\n      <div fxLayout=\"column\">\r\n        <div class=\"section-title\">Slider with Thumb Label and Ticks</div>\r\n        <div class=\"section-tag\">&lt;mat-slider&gt;</div>\r\n        <p>Sliders allow for selection of a value from a range via mouse, touch, or keyboard.</p>\r\n        <div class=\"section-info\">Example:</div>\r\n        <p><code vrHighlight class=\"html\">&lt;mat-slider<br/>  min=\"1\"<br/>  max=\"5\"<br/>  step=\"0.5\"<br/> value=\"1.5\"<br/>&gt;&lt;/mat-slider&gt;</code></p>\r\n      </div>\r\n    </div>\r\n    <mat-tab-group class=\"preview-tabs mat-elevation-z2\" fxFlex=\"auto\" fxFlex.gt-md=\"70%\">\r\n      <mat-tab label=\"PREVIEW\">\r\n        <div class=\"preview\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n          <mat-card fxFlex=\"80%\">\r\n            <mat-card-content>\r\n              <mat-slider style=\"width: 100%;\" min=\"1\" max=\"10\" thumbLabel tickInterval=\"1\" value=\"7\"></mat-slider>\r\n              <mat-slider color=\"primary\" style=\"width: 100%;\" min=\"1\" max=\"10\" value=\"2\"></mat-slider>\r\n              <mat-slider color=\"warn\" style=\"width: 100%;\" min=\"1\" max=\"10\" value=\"8\"></mat-slider>\r\n            </mat-card-content>\r\n          </mat-card>\r\n        </div>\r\n      </mat-tab>\r\n      <mat-tab label=\"HTML\">\r\n        <code [innerHTML]=\"slider1HTML\" vrHighlight class=\"html\"></code>\r\n      </mat-tab>\r\n    </mat-tab-group>\r\n  </div>\r\n\r\n  <div class=\"divider reverse\"></div>\r\n\r\n  <div class=\"section\" fxLayout=\"column\" fxLayout.gt-md=\"row\" fxLayoutAlign=\"start stretch\" fxLayoutAlign.gt-md=\"start start\" fxLayoutGap=\"16px\" fxLayoutGap.gt-md=\"24px\">\r\n    <div fxFlex.gt-md fxLayout=\"column\">\r\n      <div class=\"section-title\">Vertical Slider with Thumb-Label</div>\r\n      <mat-tab-group class=\"preview-tabs mat-elevation-z2\">\r\n        <mat-tab label=\"PREVIEW\">\r\n          <div class=\"preview\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n            <mat-card fxFlex=\"80%\">\r\n              <mat-card-content fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n                <mat-slider vertical min=\"1\" max=\"10\" thumbLabel tickInterval=\"1\" value=\"4\"></mat-slider>\r\n              </mat-card-content>\r\n            </mat-card>\r\n          </div>\r\n        </mat-tab>\r\n        <mat-tab label=\"HTML\">\r\n          <code [innerHTML]=\"slider2HTML\" vrHighlight class=\"html\"></code>\r\n        </mat-tab>\r\n      </mat-tab-group>\r\n    </div>\r\n\r\n    <div fxFlex.gt-md fxLayout=\"column\">\r\n      <div class=\"section-title\">Simple Slider without Thumb-Label and Ticks</div>\r\n      <div class=\"preview-tabs mat-elevation-z2\">\r\n        <mat-tab-group>\r\n          <mat-tab label=\"PREVIEW\">\r\n            <div class=\"preview\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              <mat-card fxFlex=\"80%\">\r\n                <mat-card-content fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n                  <mat-slider min=\"1\" max=\"10\" value=\"7\"></mat-slider>\r\n                </mat-card-content>\r\n              </mat-card>\r\n            </div>\r\n          </mat-tab>\r\n          <mat-tab label=\"HTML\">\r\n            <code [innerHTML]=\"slider3HTML\" vrHighlight class=\"html\"></code>\r\n          </mat-tab>\r\n        </mat-tab-group>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/pages/components/components-slider/components-slider.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsSliderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash_es_escape__ = __webpack_require__("../../../../lodash-es/escape.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ComponentsSliderComponent = (function () {
    function ComponentsSliderComponent() {
        this.slider1HTML = Object(__WEBPACK_IMPORTED_MODULE_1_lodash_es_escape__["a" /* default */])("\n  <md-slider min=\"1\" max=\"10\" thumbLabel tickInterval=\"1\"></md-slider>\n  ");
        this.slider2HTML = Object(__WEBPACK_IMPORTED_MODULE_1_lodash_es_escape__["a" /* default */])("<md-slider vertical min=\"1\" max=\"10\" thumbLabel tickInterval=\"1\"></md-slider>");
        this.slider3HTML = Object(__WEBPACK_IMPORTED_MODULE_1_lodash_es_escape__["a" /* default */])("<md-slider min=\"1\" max=\"10\"></md-slider>");
    }
    ComponentsSliderComponent.prototype.ngOnInit = function () {
    };
    ComponentsSliderComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'vr-components-slider',
            template: __webpack_require__("../../../../../src/app/pages/components/components-slider/components-slider.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], ComponentsSliderComponent);
    return ComponentsSliderComponent;
}());



/***/ }),

/***/ "../../../../../src/app/pages/components/components-slider/components-slider.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsSliderModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_slider_component__ = __webpack_require__("../../../../../src/app/pages/components/components-slider/components-slider.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_utils_utils_module__ = __webpack_require__("../../../../../src/app/core/utils/utils.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_flex_layout__ = __webpack_require__("../../../flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var ComponentsSliderModule = (function () {
    function ComponentsSliderModule() {
    }
    ComponentsSliderModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_3__core_utils_utils_module__["a" /* UtilsModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_flex_layout__["a" /* FlexLayoutModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["e" /* MatCardModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["H" /* MatTabsModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["B" /* MatSliderModule */]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_2__components_slider_component__["a" /* ComponentsSliderComponent */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__components_slider_component__["a" /* ComponentsSliderComponent */]]
        })
    ], ComponentsSliderModule);
    return ComponentsSliderModule;
}());



/***/ }),

/***/ "../../../../../src/app/pages/components/components-snackbar/components-snackbar.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"autocomplete group card alternative no-border mat-elevation-z2\" fxLayout=\"column\" fxLayoutGap=\"24px\">\r\n  <div class=\"group-title\">Material Snack-Bar</div>\r\n  <div class=\"divider reverse\"></div>\r\n  <div class=\"section\" fxLayout=\"column\" fxLayout.gt-md=\"row\" fxLayoutAlign=\"start stretch\" fxLayoutAlign.gt-md=\"start start\" fxLayoutGap=\"16px\" fxLayoutGap.gt-md=\"24px\">\r\n    <div fxFlex.gt-md>\r\n      <div fxLayout=\"column\">\r\n        <div class=\"section-title\">Snack-Bar Notification</div>\r\n        <p>Snack-Bars are a great way to display notifications to users or to give the user the status on something.</p>\r\n        <code [innerHTML]=\"snackbarTS\" vrHighlight class=\"typescript\"></code>\r\n      </div>\r\n    </div>\r\n    <mat-tab-group class=\"preview-tabs mat-elevation-z2\" fxFlex=\"auto\" fxFlex.gt-md=\"70%\">\r\n      <mat-tab label=\"PREVIEW\">\r\n        <div class=\"preview\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n          <mat-card fxFlex=\"90%\" fxFlex.gt-sm=\"80%\">\r\n            <mat-card-content fxLayout=\"column\" fxLayout.gt-sm=\"row\" fxLayoutAlign=\"space-around center\" fxLayoutWrap=\"wrap\">\r\n              <button mat-button (click)=\"openSnackbar()\">Trigger Snackbar</button>\r\n            </mat-card-content>\r\n          </mat-card>\r\n        </div>\r\n      </mat-tab>\r\n      <mat-tab label=\"HTML\">\r\n        <code [innerHTML]=\"snackbarHTML\" vrHighlight class=\"html\"></code>\r\n      </mat-tab>\r\n      <mat-tab label=\"TS\">\r\n        <code [innerHTML]=\"snackbarTS\" vrHighlight class=\"typescript\"></code>\r\n      </mat-tab>\r\n    </mat-tab-group>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/pages/components/components-snackbar/components-snackbar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsSnackbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash_es_escape__ = __webpack_require__("../../../../lodash-es/escape.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ComponentsSnackbarComponent = (function () {
    function ComponentsSnackbarComponent(snackBar) {
        this.snackBar = snackBar;
        this.snackbarHTML = Object(__WEBPACK_IMPORTED_MODULE_1_lodash_es_escape__["a" /* default */])("<button md-button (click)=\"openSnackbar()\">Trigger Snackbar</button>");
        this.snackbarTS = Object(__WEBPACK_IMPORTED_MODULE_1_lodash_es_escape__["a" /* default */])("this.snackBar.open(\n  'I'm a notification!',\n  'Close', {\n  duration: 3000\n});");
    }
    ComponentsSnackbarComponent.prototype.ngOnInit = function () {
    };
    ComponentsSnackbarComponent.prototype.openSnackbar = function () {
        this.snackBar.open('I\'m a notification!', 'Close', {
            duration: 3000
        });
    };
    ComponentsSnackbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'vr-components-snackbar',
            template: __webpack_require__("../../../../../src/app/pages/components/components-snackbar/components-snackbar.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_material__["C" /* MatSnackBar */]])
    ], ComponentsSnackbarComponent);
    return ComponentsSnackbarComponent;
}());



/***/ }),

/***/ "../../../../../src/app/pages/components/components-snackbar/components-snackbar.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsSnackbarModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_snackbar_component__ = __webpack_require__("../../../../../src/app/pages/components/components-snackbar/components-snackbar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_utils_utils_module__ = __webpack_require__("../../../../../src/app/core/utils/utils.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_flex_layout__ = __webpack_require__("../../../flex-layout/esm5/flex-layout.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var ComponentsSnackbarModule = (function () {
    function ComponentsSnackbarModule() {
    }
    ComponentsSnackbarModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_4__core_utils_utils_module__["a" /* UtilsModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_flex_layout__["a" /* FlexLayoutModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["e" /* MatCardModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["H" /* MatTabsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["D" /* MatSnackBarModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["c" /* MatButtonModule */]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_2__components_snackbar_component__["a" /* ComponentsSnackbarComponent */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__components_snackbar_component__["a" /* ComponentsSnackbarComponent */]]
        })
    ], ComponentsSnackbarModule);
    return ComponentsSnackbarModule;
}());



/***/ }),

/***/ "../../../../../src/app/pages/components/components-tooltip/components-tooltip.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"autocomplete group card alternative no-border mat-elevation-z2\" fxLayout=\"column\" fxLayoutGap=\"24px\">\r\n  <div class=\"group-title\">Material Tooltip</div>\r\n  <div class=\"divider reverse\"></div>\r\n  <div class=\"section\" fxLayout=\"column\" fxLayout.gt-md=\"row\" fxLayoutAlign=\"start stretch\" fxLayoutAlign.gt-md=\"start start\">\r\n    <div fxFlex.gt-md>\r\n      <div fxLayout=\"column\">\r\n        <div class=\"section-title\">Tooltip</div>\r\n        <div class=\"section-tag\">&lt;matTooltip&gt;</div>\r\n        <p>Tooltips offer space for additional information on e.g. an icon.</p>\r\n        <div class=\"section-info\">Example:</div>\r\n        <code vrHighlight class=\"html\">&lt;button mat-button matTooltip=\"I'm a Tooltip!\"&gt;<br/>  Button<br/>&lt;/button&gt;</code>\r\n      </div>\r\n    </div>\r\n    <mat-tab-group class=\"preview-tabs mat-elevation-z2\" fxFlex=\"auto\" fxFlex.gt-md=\"70%\" fxFlexOffset.gt-md=\"24px\">\r\n      <mat-tab label=\"PREVIEW\">\r\n        <div class=\"preview\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n          <mat-card fxFlex=\"90%\" fxFlex.gt-sm=\"80%\">\r\n            <mat-card-content fxLayout=\"column\" fxLayout.gt-sm=\"row\" fxLayoutAlign=\"space-around center\" fxLayoutWrap=\"wrap\">\r\n              <button mat-icon-button matTooltip=\"Favorite this\"><mat-icon>favorite</mat-icon></button>\r\n            </mat-card-content>\r\n          </mat-card>\r\n        </div>\r\n      </mat-tab>\r\n      <mat-tab label=\"HTML\">\r\n        <code [innerHTML]=\"tooltipHTML\" vrHighlight class=\"html\"></code>\r\n      </mat-tab>\r\n    </mat-tab-group>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/pages/components/components-tooltip/components-tooltip.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsTooltipComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash_es_escape__ = __webpack_require__("../../../../lodash-es/escape.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ComponentsTooltipComponent = (function () {
    function ComponentsTooltipComponent() {
        this.tooltipHTML = Object(__WEBPACK_IMPORTED_MODULE_1_lodash_es_escape__["a" /* default */])("<button md-icon-button mdTooltip=\"Favorite this\">\n  <md-icon>favorite</md-icon>\n</button>");
    }
    ComponentsTooltipComponent.prototype.ngOnInit = function () {
    };
    ComponentsTooltipComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'vr-components-tooltip',
            template: __webpack_require__("../../../../../src/app/pages/components/components-tooltip/components-tooltip.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], ComponentsTooltipComponent);
    return ComponentsTooltipComponent;
}());



/***/ }),

/***/ "../../../../../src/app/pages/components/components-tooltip/components-tooltip.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsTooltipModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_tooltip_component__ = __webpack_require__("../../../../../src/app/pages/components/components-tooltip/components-tooltip.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_utils_utils_module__ = __webpack_require__("../../../../../src/app/core/utils/utils.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_flex_layout__ = __webpack_require__("../../../flex-layout/esm5/flex-layout.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var ComponentsTooltipModule = (function () {
    function ComponentsTooltipModule() {
    }
    ComponentsTooltipModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_4__core_utils_utils_module__["a" /* UtilsModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_flex_layout__["a" /* FlexLayoutModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["e" /* MatCardModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["H" /* MatTabsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["J" /* MatTooltipModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["m" /* MatIconModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["c" /* MatButtonModule */]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_2__components_tooltip_component__["a" /* ComponentsTooltipComponent */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__components_tooltip_component__["a" /* ComponentsTooltipComponent */]]
        })
    ], ComponentsTooltipModule);
    return ComponentsTooltipModule;
}());



/***/ }),

/***/ "../../../../../src/app/pages/components/components.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"page components\">\r\n\r\n  <vr-page-header [height]=\"'176px'\" background=\"url(/assets/img/demo/headers/pattern-3.png)\" [reverse]=\"true\"></vr-page-header>\r\n\r\n  <div class=\"container\">\r\n\r\n    <div fxLayout=\"row\" fxLayoutAlign=\"start start\" fxLayoutGap=\"24px\">\r\n      <div class=\"sticky-menu\" fxFlex #sticky fxHide fxShow.gt-sm>\r\n        <mat-list class=\"list\">\r\n          <h3 matSubheader>Components</h3>\r\n          <mat-list-item class=\"list-item\" (click)=\"scrollTo(autocomplete)\" matRipple>\r\n            <h4 matLine>Autocomplete</h4>\r\n          </mat-list-item>\r\n          <mat-list-item class=\"list-item\" (click)=\"scrollTo(button)\" matRipple>\r\n            <h4 matLine>Button</h4>\r\n          </mat-list-item>\r\n          <mat-list-item class=\"list-item\" (click)=\"scrollTo(card)\" matRipple>\r\n            <h4 matLine>Card</h4>\r\n          </mat-list-item>\r\n          <mat-list-item class=\"list-item\" (click)=\"scrollTo(dialog)\" matRipple>\r\n            <h4 matLine>Dialog</h4>\r\n          </mat-list-item>\r\n          <mat-list-item class=\"list-item\" (click)=\"scrollTo(gridlist)\" matRipple>\r\n            <h4 matLine>Grid List</h4>\r\n          </mat-list-item>\r\n          <mat-list-item class=\"list-item\" (click)=\"scrollTo(list)\" matRipple>\r\n            <h4 matLine>List</h4>\r\n          </mat-list-item>\r\n          <mat-list-item class=\"list-item\" (click)=\"scrollTo(menu)\" matRipple>\r\n            <h4 matLine>Menu</h4>\r\n          </mat-list-item>\r\n          <mat-list-item class=\"list-item\" (click)=\"scrollTo(progress)\" matRipple>\r\n            <h4 matLine>Progress</h4>\r\n          </mat-list-item>\r\n          <mat-list-item class=\"list-item\" (click)=\"scrollTo(slider)\" matRipple>\r\n            <h4 matLine>Slider</h4>\r\n          </mat-list-item>\r\n          <mat-list-item class=\"list-item\" (click)=\"scrollTo(snackbar)\" matRipple>\r\n            <h4 matLine>Snack Bar</h4>\r\n          </mat-list-item>\r\n          <mat-list-item class=\"list-item\" (click)=\"scrollTo(tooltip)\" matRipple>\r\n            <h4 matLine>Tooltip</h4>\r\n          </mat-list-item>\r\n        </mat-list>\r\n      </div>\r\n\r\n      <div fxLayout=\"column\" fxFlex=\"100%\" fxFlex.gt-sm=\"85%\">\r\n        <vr-breadcrumbs [currentPage]=\"'Components'\" [header]=\"'primary'\"></vr-breadcrumbs>\r\n\r\n        <div fxLayout=\"column\" fxLayoutGap=\"24px\">\r\n          <div #autocomplete><vr-components-autocomplete></vr-components-autocomplete></div>\r\n          <div #button><vr-components-button></vr-components-button></div>\r\n          <div #card><vr-components-card></vr-components-card></div>\r\n          <div #dialog><vr-components-dialog></vr-components-dialog></div>\r\n          <div #gridlist><vr-components-gridlist></vr-components-gridlist></div>\r\n          <div #list><vr-components-list></vr-components-list></div>\r\n          <div #menu><vr-components-menu></vr-components-menu></div>\r\n          <div #progress><vr-components-progress></vr-components-progress></div>\r\n          <div #slider><vr-components-slider></vr-components-slider></div>\r\n          <div #snackbar><vr-components-snackbar></vr-components-snackbar></div>\r\n          <div #tooltip><vr-components-tooltip></vr-components-tooltip></div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/pages/components/components.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".list {\n  position: fixed; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/pages/components/components.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_animation__ = __webpack_require__("../../../../../src/app/app.animation.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ComponentsComponent = (function () {
    function ComponentsComponent() {
    }
    ComponentsComponent.prototype.ngOnInit = function () {
    };
    ComponentsComponent.prototype.ngAfterContentInit = function () {
    };
    ;
    ComponentsComponent.prototype.scrollTo = function (elem) {
        elem.scrollIntoView({
            behavior: 'smooth',
            block: 'start',
            inline: 'nearest'
        });
    };
    ComponentsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'vr-components',
            template: __webpack_require__("../../../../../src/app/pages/components/components.component.html"),
            styles: [__webpack_require__("../../../../../src/app/pages/components/components.component.scss")],
            animations: __WEBPACK_IMPORTED_MODULE_1__app_animation__["b" /* ROUTE_TRANSITION */].slice(),
            host: { '[@routeTransition]': '' }
        }),
        __metadata("design:paramtypes", [])
    ], ComponentsComponent);
    return ComponentsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/pages/components/components.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComponentsModule", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_component__ = __webpack_require__("../../../../../src/app/pages/components/components.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_page_header_page_header_module__ = __webpack_require__("../../../../../src/app/core/page-header/page-header.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_breadcrumbs_breadcrumbs_module__ = __webpack_require__("../../../../../src/app/core/breadcrumbs/breadcrumbs.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_utils_utils_module__ = __webpack_require__("../../../../../src/app/core/utils/utils.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__ = __webpack_require__("../../../flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_autocomplete_components_autocomplete_module__ = __webpack_require__("../../../../../src/app/pages/components/components-autocomplete/components-autocomplete.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_button_components_button_module__ = __webpack_require__("../../../../../src/app/pages/components/components-button/components-button.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_card_components_card_module__ = __webpack_require__("../../../../../src/app/pages/components/components-card/components-card.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__components_dialog_components_dialog_module__ = __webpack_require__("../../../../../src/app/pages/components/components-dialog/components-dialog.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_gridlist_components_gridlist_module__ = __webpack_require__("../../../../../src/app/pages/components/components-gridlist/components-gridlist.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__components_list_components_list_module__ = __webpack_require__("../../../../../src/app/pages/components/components-list/components-list.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__components_menu_components_menu_module__ = __webpack_require__("../../../../../src/app/pages/components/components-menu/components-menu.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__components_slider_components_slider_module__ = __webpack_require__("../../../../../src/app/pages/components/components-slider/components-slider.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__components_snackbar_components_snackbar_module__ = __webpack_require__("../../../../../src/app/pages/components/components-snackbar/components-snackbar.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__components_tooltip_components_tooltip_module__ = __webpack_require__("../../../../../src/app/pages/components/components-tooltip/components-tooltip.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__components_progress_components_progress_module__ = __webpack_require__("../../../../../src/app/pages/components/components-progress/components-progress.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__components_routing__ = __webpack_require__("../../../../../src/app/pages/components/components.routing.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




















var ComponentsModule = (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_19__components_routing__["a" /* ComponentsRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_3__core_page_header_page_header_module__["a" /* PageHeaderModule */],
                __WEBPACK_IMPORTED_MODULE_4__core_breadcrumbs_breadcrumbs_module__["a" /* BreadcrumbsModule */],
                __WEBPACK_IMPORTED_MODULE_5__core_utils_utils_module__["a" /* UtilsModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__["a" /* FlexLayoutModule */],
                __WEBPACK_IMPORTED_MODULE_7__angular_material__["x" /* MatRippleModule */],
                __WEBPACK_IMPORTED_MODULE_7__angular_material__["o" /* MatListModule */],
                __WEBPACK_IMPORTED_MODULE_8__components_autocomplete_components_autocomplete_module__["a" /* ComponentsAutocompleteModule */],
                __WEBPACK_IMPORTED_MODULE_9__components_button_components_button_module__["a" /* ComponentsButtonModule */],
                __WEBPACK_IMPORTED_MODULE_10__components_card_components_card_module__["a" /* ComponentsCardModule */],
                __WEBPACK_IMPORTED_MODULE_11__components_dialog_components_dialog_module__["a" /* ComponentsDialogModule */],
                __WEBPACK_IMPORTED_MODULE_12__components_gridlist_components_gridlist_module__["a" /* ComponentsGridlistModule */],
                __WEBPACK_IMPORTED_MODULE_13__components_list_components_list_module__["a" /* ComponentsListModule */],
                __WEBPACK_IMPORTED_MODULE_14__components_menu_components_menu_module__["a" /* ComponentsMenuModule */],
                __WEBPACK_IMPORTED_MODULE_18__components_progress_components_progress_module__["a" /* ComponentsProgressModule */],
                __WEBPACK_IMPORTED_MODULE_15__components_slider_components_slider_module__["a" /* ComponentsSliderModule */],
                __WEBPACK_IMPORTED_MODULE_16__components_snackbar_components_snackbar_module__["a" /* ComponentsSnackbarModule */],
                __WEBPACK_IMPORTED_MODULE_17__components_tooltip_components_tooltip_module__["a" /* ComponentsTooltipModule */]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_2__components_component__["a" /* ComponentsComponent */]]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());



/***/ }),

/***/ "../../../../../src/app/pages/components/components.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_component__ = __webpack_require__("../../../../../src/app/pages/components/components.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_1__components_component__["a" /* ComponentsComponent */]
    }
];
var ComponentsRoutingModule = (function () {
    function ComponentsRoutingModule() {
    }
    ComponentsRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_0__angular_router__["d" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_0__angular_router__["d" /* RouterModule */]]
        })
    ], ComponentsRoutingModule);
    return ComponentsRoutingModule;
}());



/***/ })

});
//# sourceMappingURL=components.module.chunk.js.map