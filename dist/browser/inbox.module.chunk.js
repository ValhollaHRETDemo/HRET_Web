webpackJsonp(["inbox.module"],{

/***/ "../../../../../src/app/pages/inbox/inbox-compose/inbox-compose.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"inbox-compose\">\r\n  <div mat-dialog-content fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\r\n    <span>New Email</span>\r\n    <button matDialogClose mat-icon-button>\r\n      <mat-icon>close</mat-icon>\r\n    </button>\r\n  </div>\r\n  <div class=\"content\" mat-dialog-content fxLayout=\"column\">\r\n    <div fxLayout=\"row\" fxLayoutGap=\"24px\">\r\n      <mat-form-field class=\"input-container\" fxFlex>\r\n        <input matInput placeholder=\"To\">\r\n      </mat-form-field>\r\n      <mat-form-field class=\"input-container\" fxFlex>\r\n        <input matInput placeholder=\"From\" disabled value=\"david.smith@demomail.com\">\r\n      </mat-form-field>\r\n    </div>\r\n\r\n    <mat-form-field class=\"input-container\">\r\n      <input matInput placeholder=\"Title\">\r\n    </mat-form-field>\r\n\r\n    <p class=\"editor\">\r\n\r\n    </p>\r\n\r\n  </div>\r\n  <div mat-dialog-actions fxLayout=\"row\">\r\n    <button matDialogClose mat-icon-button>\r\n      <mat-icon>delete</mat-icon>\r\n    </button>\r\n    <span fxFlex><!-- fill space --></span>\r\n    <button mat-icon-button>\r\n      <mat-icon>attachment</mat-icon>\r\n    </button>\r\n    <button (click)=\"send()\" class=\"send\" fxFlexOffset=\"8px\" mat-raised-button color=\"primary\">\r\n      Send <mat-icon>send</mat-icon>\r\n    </button>\r\n  </div>\r\n\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/pages/inbox/inbox-compose/inbox-compose.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InboxComposeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var InboxComposeComponent = (function () {
    function InboxComposeComponent(dialogRef) {
        this.dialogRef = dialogRef;
        this.respondOptions = {
            toolbar: [
                ['bold', 'italic', 'underline', 'strike'],
                [{ 'header': 1 }, { 'header': 2 }],
                [{ 'list': 'ordered' }, { 'list': 'bullet' }],
                [{ 'size': ['small', false, 'large', 'huge'] }],
                [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
                [{ 'color': [] }, { 'background': [] }],
                [{ 'font': [] }],
                [{ 'align': [] }],
                ['clean'],
                ['link', 'image', 'video'] // link and image, video
            ]
        };
    }
    InboxComposeComponent.prototype.ngOnInit = function () {
    };
    InboxComposeComponent.prototype.send = function () {
        this.dialogRef.close('Your message has been send.');
    };
    InboxComposeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'vr-inbox-compose',
            template: __webpack_require__("../../../../../src/app/pages/inbox/inbox-compose/inbox-compose.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_material__["k" /* MatDialogRef */]])
    ], InboxComposeComponent);
    return InboxComposeComponent;
}());



/***/ }),

/***/ "../../../../../src/app/pages/inbox/inbox.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"inbox\" fxLayout=\"column\">\r\n  <div class=\"header mat-elevation-z2\"></div>\r\n  <div class=\"container\" fxLayout=\"row\">\r\n    <div class=\"navigation\" fxFlex=\"250px\" fxHide fxShow.gt-md>\r\n\r\n      <div class=\"title-container\">\r\n        <mat-icon>mail</mat-icon>\r\n        <span class=\"title\">Inbox</span>\r\n      </div>\r\n\r\n      <div class=\"compose-container\">\r\n        <button (click)=\"openComposeDialog()\" class=\"compose\" mat-raised-button color=\"primary\">\r\n          <mat-icon>mode_edit</mat-icon>\r\n          <span>Compose</span>\r\n        </button>\r\n      </div>\r\n\r\n      <mat-nav-list class=\"nav-list\">\r\n        <h3 matSubheader>Inbox</h3>\r\n        <a class=\"nav-item mt0\" (click)=\"setActiveGroup('primary')\" [class.active]=\"activeGroup == 'primary'\" mat-list-item>\r\n          <h3 class=\"nav-title\" matLine>\r\n            <mat-icon class=\"icon\">inbox</mat-icon>\r\n            <span class=\"text\">Primary {{ unreadMailsCount('primary') }}</span>\r\n          </h3>\r\n        </a>\r\n        <a class=\"nav-item\" (click)=\"setActiveGroup('social')\" [class.active]=\"activeGroup == 'social'\" mat-list-item>\r\n          <h3 class=\"nav-title\" matLine>\r\n            <mat-icon class=\"icon\">group</mat-icon>\r\n            <span class=\"text\">Social {{ unreadMailsCount('social') }}</span>\r\n          </h3>\r\n        </a>\r\n        <a class=\"nav-item\" (click)=\"setActiveGroup('promotions')\" [class.active]=\"activeGroup == 'promotions'\" mat-list-item>\r\n          <h3 class=\"nav-title\" matLine>\r\n            <mat-icon class=\"icon\">local_offer</mat-icon>\r\n            <span class=\"text\">Promotions {{ unreadMailsCount('promotions') }}</span>\r\n          </h3>\r\n        </a>\r\n        <mat-divider></mat-divider>\r\n        <a class=\"nav-item\" (click)=\"setShowOnlyStarred()\" [class.active]=\"showOnlyStarred\" mat-list-item>\r\n          <h3 class=\"nav-title\" matLine>\r\n            <mat-icon class=\"icon\">star</mat-icon>\r\n            <span class=\"text\">Starred</span>\r\n          </h3>\r\n        </a>\r\n        <a class=\"nav-item\" (click)=\"setActiveType('sent')\" [class.active]=\"activeType == 'sent'\" mat-list-item>\r\n          <h3 class=\"nav-title\" matLine>\r\n            <mat-icon class=\"icon\">send</mat-icon>\r\n            <span class=\"text\">Sent Mails</span>\r\n          </h3>\r\n        </a>\r\n        <a class=\"nav-item\" (click)=\"setActiveType('draft')\" [class.active]=\"activeType == 'draft'\" mat-list-item>\r\n          <h3 class=\"nav-title\" matLine>\r\n            <mat-icon class=\"icon\">drafts</mat-icon>\r\n            <span class=\"text\">Drafts</span>\r\n          </h3>\r\n        </a>\r\n        <mat-divider></mat-divider>\r\n        <a class=\"nav-item\" (click)=\"setActiveType('spam')\" [class.active]=\"activeType == 'spam'\" mat-list-item>\r\n          <h3 class=\"nav-title\" matLine>\r\n            <mat-icon class=\"icon\">backspace</mat-icon>\r\n            <span class=\"text\">Spam</span>\r\n          </h3>\r\n        </a>\r\n        <a class=\"nav-item\" (click)=\"setActiveType('trash')\" [class.active]=\"activeType == 'trash'\" mat-list-item>\r\n          <h3 class=\"nav-title\" matLine>\r\n            <mat-icon class=\"icon\">delete</mat-icon>\r\n            <span class=\"text\">Trash</span>\r\n          </h3>\r\n        </a>\r\n      </mat-nav-list>\r\n    </div>\r\n\r\n    <div class=\"content-container\" fxFlex fxLayout=\"column\">\r\n\r\n      <div class=\"search mat-elevation-z2\" fxLayout=\"row\">\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n          <mat-icon>search</mat-icon>\r\n        </div>\r\n        <input fxFlex type=\"search\" placeholder=\"Search...\">\r\n      </div>\r\n\r\n      <div class=\"mails-container mat-elevation-z2\" fxFlex>\r\n        <div *ngIf=\"!currentlyOpen\" class=\"inbox-toolbar\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n          <mat-checkbox class=\"checkbox\" (change)=\"toggleSelectAllThreads($event)\"></mat-checkbox>\r\n\r\n          <div class=\"toolbar-detail\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n            <div class=\"icon-group\" fxLayout=\"row\" fxLayoutAlign=\"start center\" fxHide fxShow.gt-sm>\r\n              <button class=\"archive\" mat-icon-button matTooltip=\"Archive\">\r\n                <mat-icon>archive</mat-icon>\r\n              </button>\r\n              <button class=\"spam\" mat-icon-button matTooltip=\"Mark as Spam\">\r\n                <mat-icon>backspace</mat-icon>\r\n              </button>\r\n              <button class=\"delete\" mat-icon-button matTooltip=\"Delete\">\r\n                <mat-icon>delete</mat-icon>\r\n              </button>\r\n            </div>\r\n            <div class=\"icon-group\" fxLayout=\"row\" fxLayoutAlign=\"start center\" fxHide fxShow.gt-sm>\r\n              <button class=\"archive\" mat-icon-button matTooltip=\"Move to\">\r\n                <mat-icon>folder</mat-icon>\r\n              </button>\r\n              <button class=\"spam\" mat-icon-button matTooltip=\"Label\">\r\n                <mat-icon>label</mat-icon>\r\n              </button>\r\n            </div>\r\n            <button fxHide fxShow.gt-sm class=\"delete\" [matMenuTriggerFor]=\"mailOptions\" mat-icon-button>\r\n              <mat-icon>more_vert</mat-icon>\r\n            </button>\r\n            <button fxShow fxHide.gt-sm class=\"delete\" [matMenuTriggerFor]=\"mailOptionsMobile\" mat-icon-button>\r\n              <mat-icon>more_vert</mat-icon>\r\n            </button>\r\n          </div>\r\n\r\n          <span fxFlex><!-- fill space --></span>\r\n          <span>1 - {{ paginationCount() }} of {{ mails.length }}</span>\r\n          <div class=\"chevrons\">\r\n            <button mat-icon-button>\r\n              <mat-icon>chevron_left</mat-icon>\r\n            </button>\r\n            <button mat-icon-button>\r\n              <mat-icon>chevron_right</mat-icon>\r\n            </button>\r\n          </div>\r\n        </div>\r\n\r\n        <div *ngIf=\"currentlyOpen\" class=\"toolbar toolbar-detail\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n          <div class=\"icon-group\">\r\n            <button class=\"back\" (click)=\"closeMail()\" mat-icon-button matTooltip=\"Back\">\r\n              <mat-icon>arrow_back</mat-icon>\r\n            </button>\r\n          </div>\r\n          <div class=\"icon-group\" fxLayout=\"row\" fxLayoutAlign=\"start center\" fxHide fxShow.gt-sm>\r\n            <button class=\"archive\" mat-icon-button matTooltip=\"Archive\">\r\n              <mat-icon>archive</mat-icon>\r\n            </button>\r\n            <button class=\"spam\" mat-icon-button matTooltip=\"Mark as Spam\">\r\n              <mat-icon>backspace</mat-icon>\r\n            </button>\r\n            <button class=\"delete\" mat-icon-button matTooltip=\"Delete\">\r\n              <mat-icon>delete</mat-icon>\r\n            </button>\r\n          </div>\r\n          <div class=\"icon-group\" fxLayout=\"row\" fxLayoutAlign=\"start center\" fxHide fxShow.gt-sm>\r\n            <button class=\"archive\" mat-icon-button matTooltip=\"Move to\">\r\n              <mat-icon>folder</mat-icon>\r\n            </button>\r\n            <button class=\"spam\" mat-icon-button matTooltip=\"Label\">\r\n              <mat-icon>label</mat-icon>\r\n            </button>\r\n          </div>\r\n          <button fxHide fxShow.gt-sm class=\"delete\" [matMenuTriggerFor]=\"mailOptions\" mat-icon-button>\r\n            <mat-icon>more_vert</mat-icon>\r\n          </button>\r\n          <button fxShow fxHide.gt-sm class=\"delete\" [matMenuTriggerFor]=\"mailOptionsMobile\" mat-icon-button>\r\n            <mat-icon>more_vert</mat-icon>\r\n          </button>\r\n          <span fxFlex><!-- fill space --></span>\r\n          <span>1 - 25 of {{ mails.length }}</span>\r\n          <div class=\"chevrons\">\r\n            <button mat-icon-button>\r\n              <mat-icon>chevron_left</mat-icon>\r\n            </button>\r\n            <button mat-icon-button>\r\n              <mat-icon>chevron_right</mat-icon>\r\n            </button>\r\n          </div>\r\n        </div>\r\n\r\n        <div class=\"mails\">\r\n          <div *ngIf=\"!currentlyOpen\" class=\"mail-list\">\r\n            <a class=\"mail\" #mailList *ngFor=\"let mail of mails\" fxLayout=\"row\" [class.read]=\"mail.read\">\r\n              <div fxHide=\"true\" fxHide.gt-xs=\"false\" class=\"start-wrapper\" fxFlex=\"180px\" fxFlex.gt-sm=\"270px\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n                <mat-checkbox class=\"checkbox\" [checked]=\"isSelected(mail)\"></mat-checkbox>\r\n                <button fxHide=\"true\" fxHide.gt-sm=\"false\" mat-icon-button (click)=\"toggleStarred(mail)\">\r\n                  <mat-icon *ngIf=\"!mail.starred\" class=\"star\">star_border</mat-icon>\r\n                  <mat-icon *ngIf=\"mail.starred\" class=\"star active\">star</mat-icon>\r\n                </button>\r\n                <img fxHide=\"true\" fxHide.gt-sm=\"false\" class=\"from-avatar\" src=\"//placehold.it/40x40\">\r\n                <span class=\"from-name\">{{ mail.from.name }}</span>\r\n              </div>\r\n              <p class=\"content-wrapper\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n                <span class=\"labels\">\r\n                  <span class=\"label\" *ngFor=\"let label of mail.labels\" [style.background]=\"label.color\">{{ label.name }}</span>\r\n                </span>\r\n                <span class=\"subject\">{{ mail.subject }}</span>\r\n                <span class=\"middot\">&middot;</span>\r\n                <span class=\"content\">{{ mail.content }}</span>\r\n              </p>\r\n              <div fxHide=\"true\" fxHide.gt-xs=\"false\" fxFlex=\"100px\" fxFlex.gt-sm=\"170px\" class=\"end-wrapper\" fxLayout=\"row\" fxLayoutAlign=\"end center\">\r\n                <span class=\"when\">{{ mail.when.fromNow() }}</span>\r\n                <button fxHide=\"true\" fxHide.gt-sm=\"false\"  class=\"options\" [matMenuTriggerFor]=\"mailOptions\" mat-icon-button>\r\n                  <mat-icon>more_vert</mat-icon>\r\n                </button>\r\n              </div>\r\n            </a>\r\n          </div>\r\n\r\n          <div class=\"no-mails-container\" *ngIf=\"mails.length === 0 && !currentlyOpen\" fxLayout=\"column\" fxLayoutAlign=\"start center\">\r\n            <span class=\"no-mails\">No mails in {{ activeGroup }}</span>\r\n          </div>\r\n\r\n          <div class=\"mail-detail\" *ngIf=\"currentlyOpen\" fxLayout=\"column\">\r\n\r\n            <div class=\"mail-content-container\">\r\n              <div class=\"mail-header\" fxLayout=\"row\">\r\n                <img class=\"avatar\" src=\"//placehold.it/40x40\">\r\n                <div class=\"mail-info\" fxLayout=\"column\">\r\n                  <span class=\"from\">{{ currentlyOpen.from.name }}</span>\r\n                  <span class=\"to\">to me <mat-icon>arrow_drop_down</mat-icon></span>\r\n                </div>\r\n                <span fxFlex><!-- fill space--></span>\r\n                <div class=\"mail-extra\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n                  <span class=\"when\">{{ currentlyOpen.when.fromNow() }}</span>\r\n                  <button class=\"options\" [matMenuTriggerFor]=\"mailOptions\" mat-icon-button>\r\n                    <mat-icon>more_vert</mat-icon>\r\n                  </button>\r\n                </div>\r\n              </div>\r\n\r\n              <div class=\"mail-content\">\r\n                <div class=\"mail-content-text\">\r\n                  <p>{{ currentlyOpen.content }}</p>\r\n                </div>\r\n\r\n                <div class=\"attachments-container\" *ngIf=\"currentlyOpen.attachments\">\r\n                  <div class=\"attachments-header\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n                    <span class=\"title\">Attachments</span>\r\n                    <mat-divider fxFlex></mat-divider>\r\n                    <button class=\"download\" mat-icon-button>\r\n                      <mat-icon>file_download</mat-icon>\r\n                    </button>\r\n                  </div>\r\n\r\n                  <div class=\"attachments\" fxLayout=\"row\" fxLayoutWrap=\"wrap\">\r\n                    <div class=\"attachment\" *ngFor=\"let attachment of currentlyOpen.attachments\">\r\n                      <img class=\"pointer\" [src]=\"attachment\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"respond\" (click)=\"setRespondActive(true)\" fxLayout=\"row\" fxLayoutAlign=\"start center\" *ngIf=\"!respondActive\">\r\n              <img class=\"avatar\" src=\"assets/img/demo/avatars/noavatar.png\">\r\n              <span class=\"click-reply\">Click to <span class=\"semi-bold\">Reply</span> or <span class=\"semi-bold\">Forward</span></span>\r\n              <span fxFlex><!-- fill space --></span>\r\n              <mat-icon class=\"reply\">reply</mat-icon>\r\n              <mat-icon class=\"forward\">forward</mat-icon>\r\n            </div>\r\n\r\n            <div class=\"respond respond-active\" *ngIf=\"respondActive\">\r\n              <div class=\"respond-buttons\" fxLayout=\"row\" fxLayoutAlign=\"end stretch\">\r\n                <button mat-button (click)=\"setRespondActive(false)\">Cancel</button>\r\n                <button mat-raised-button color=\"primary\" (click)=\"setRespondActive(false)\">Send <mat-icon>send</mat-icon></button>\r\n              </div>\r\n            </div>\r\n\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n  </div>\r\n</div>\r\n\r\n<mat-menu #mailOptions=\"matMenu\">\r\n  <button mat-menu-item> <mat-icon>markunread_mailbox</mat-icon> Mark as unread </button>\r\n  <button mat-menu-item> <mat-icon>label</mat-icon> Labels </button>\r\n  <mat-divider></mat-divider>\r\n  <button mat-menu-item> <mat-icon>delete</mat-icon> Delete </button>\r\n</mat-menu>\r\n\r\n<mat-menu #mailOptionsMobile=\"matMenu\">\r\n  <button mat-menu-item> <mat-icon>archive</mat-icon> Archive </button>\r\n  <button mat-menu-item> <mat-icon>backspace</mat-icon> Mark as Spam </button>\r\n  <button mat-menu-item> <mat-icon>markunread_mailbox</mat-icon> Mark as Unread </button>\r\n  <mat-divider></mat-divider>\r\n  <button mat-menu-item> <mat-icon>folder</mat-icon> Move To </button>\r\n  <button mat-menu-item> <mat-icon>label</mat-icon> Labels </button>\r\n  <mat-divider></mat-divider>\r\n  <button mat-menu-item> <mat-icon>delete</mat-icon> Delete </button>\r\n</mat-menu>\r\n"

/***/ }),

/***/ "../../../../../src/app/pages/inbox/inbox.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/**\n * Applies styles for users in high contrast mode. Note that this only applies\n * to Microsoft browsers. Chrome can be included by checking for the `html[hc]`\n * attribute, however Chrome handles high contrast differently.\n */\n/* Theme for the ripple elements.*/\n/** The mixins below are shared between mat-menu and mat-select */\n/**\n * This mixin adds the correct panel transform styles based\n * on the direction that the menu panel opens.\n */\n/* stylelint-disable material/no-prefixes */\n/* stylelint-enable */\n/**\n * This mixin contains shared option styles between the select and\n * autocomplete components.\n */\n.mat-elevation-z0 {\n  box-shadow: 0px 0px 0px 0px rgba(0, 0, 0, 0.2), 0px 0px 0px 0px rgba(0, 0, 0, 0.14), 0px 0px 0px 0px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z1 {\n  box-shadow: 0px 2px 1px -1px rgba(0, 0, 0, 0.2), 0px 1px 1px 0px rgba(0, 0, 0, 0.14), 0px 1px 3px 0px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z2 {\n  box-shadow: 0px 3px 1px -2px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 1px 5px 0px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z3 {\n  box-shadow: 0px 3px 3px -2px rgba(0, 0, 0, 0.2), 0px 3px 4px 0px rgba(0, 0, 0, 0.14), 0px 1px 8px 0px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z4 {\n  box-shadow: 0px 2px 4px -1px rgba(0, 0, 0, 0.2), 0px 4px 5px 0px rgba(0, 0, 0, 0.14), 0px 1px 10px 0px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z5 {\n  box-shadow: 0px 3px 5px -1px rgba(0, 0, 0, 0.2), 0px 5px 8px 0px rgba(0, 0, 0, 0.14), 0px 1px 14px 0px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z6 {\n  box-shadow: 0px 3px 5px -1px rgba(0, 0, 0, 0.2), 0px 6px 10px 0px rgba(0, 0, 0, 0.14), 0px 1px 18px 0px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z7 {\n  box-shadow: 0px 4px 5px -2px rgba(0, 0, 0, 0.2), 0px 7px 10px 1px rgba(0, 0, 0, 0.14), 0px 2px 16px 1px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z8 {\n  box-shadow: 0px 5px 5px -3px rgba(0, 0, 0, 0.2), 0px 8px 10px 1px rgba(0, 0, 0, 0.14), 0px 3px 14px 2px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z9 {\n  box-shadow: 0px 5px 6px -3px rgba(0, 0, 0, 0.2), 0px 9px 12px 1px rgba(0, 0, 0, 0.14), 0px 3px 16px 2px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z10 {\n  box-shadow: 0px 6px 6px -3px rgba(0, 0, 0, 0.2), 0px 10px 14px 1px rgba(0, 0, 0, 0.14), 0px 4px 18px 3px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z11 {\n  box-shadow: 0px 6px 7px -4px rgba(0, 0, 0, 0.2), 0px 11px 15px 1px rgba(0, 0, 0, 0.14), 0px 4px 20px 3px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z12 {\n  box-shadow: 0px 7px 8px -4px rgba(0, 0, 0, 0.2), 0px 12px 17px 2px rgba(0, 0, 0, 0.14), 0px 5px 22px 4px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z13 {\n  box-shadow: 0px 7px 8px -4px rgba(0, 0, 0, 0.2), 0px 13px 19px 2px rgba(0, 0, 0, 0.14), 0px 5px 24px 4px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z14 {\n  box-shadow: 0px 7px 9px -4px rgba(0, 0, 0, 0.2), 0px 14px 21px 2px rgba(0, 0, 0, 0.14), 0px 5px 26px 4px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z15 {\n  box-shadow: 0px 8px 9px -5px rgba(0, 0, 0, 0.2), 0px 15px 22px 2px rgba(0, 0, 0, 0.14), 0px 6px 28px 5px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z16 {\n  box-shadow: 0px 8px 10px -5px rgba(0, 0, 0, 0.2), 0px 16px 24px 2px rgba(0, 0, 0, 0.14), 0px 6px 30px 5px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z17 {\n  box-shadow: 0px 8px 11px -5px rgba(0, 0, 0, 0.2), 0px 17px 26px 2px rgba(0, 0, 0, 0.14), 0px 6px 32px 5px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z18 {\n  box-shadow: 0px 9px 11px -5px rgba(0, 0, 0, 0.2), 0px 18px 28px 2px rgba(0, 0, 0, 0.14), 0px 7px 34px 6px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z19 {\n  box-shadow: 0px 9px 12px -6px rgba(0, 0, 0, 0.2), 0px 19px 29px 2px rgba(0, 0, 0, 0.14), 0px 7px 36px 6px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z20 {\n  box-shadow: 0px 10px 13px -6px rgba(0, 0, 0, 0.2), 0px 20px 31px 3px rgba(0, 0, 0, 0.14), 0px 8px 38px 7px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z21 {\n  box-shadow: 0px 10px 13px -6px rgba(0, 0, 0, 0.2), 0px 21px 33px 3px rgba(0, 0, 0, 0.14), 0px 8px 40px 7px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z22 {\n  box-shadow: 0px 10px 14px -6px rgba(0, 0, 0, 0.2), 0px 22px 35px 3px rgba(0, 0, 0, 0.14), 0px 8px 42px 7px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z23 {\n  box-shadow: 0px 11px 14px -7px rgba(0, 0, 0, 0.2), 0px 23px 36px 3px rgba(0, 0, 0, 0.14), 0px 9px 44px 8px rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z24 {\n  box-shadow: 0px 11px 15px -7px rgba(0, 0, 0, 0.2), 0px 24px 38px 3px rgba(0, 0, 0, 0.14), 0px 9px 46px 8px rgba(0, 0, 0, 0.12); }\n\n.mat-h1, .mat-headline, .mat-typography h1 {\n  font: 400 24px/32px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 16px; }\n\n.mat-h2, .mat-title, .mat-typography h2 {\n  font: 500 20px/32px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 16px; }\n\n.mat-h3, .mat-subheading-2, .mat-typography h3 {\n  font: 400 16px/28px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 16px; }\n\n.mat-h4, .mat-subheading-1, .mat-typography h4 {\n  font: 400 15px/24px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 16px; }\n\n.mat-h5, .mat-typography h5 {\n  font-size: 11.62px;\n  font-weight: 400;\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  line-height: 20px;\n  margin: 0 0 12px; }\n\n.mat-h6, .mat-typography h6 {\n  font-size: 9.38px;\n  font-weight: 400;\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  line-height: 20px;\n  margin: 0 0 12px; }\n\n.mat-body-strong, .mat-body-2 {\n  font: 500 14px/24px Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-body, .mat-body-1, .mat-typography {\n  font: 400 14px/20px Roboto, \"Helvetica Neue\", sans-serif; }\n  .mat-body p, .mat-body-1 p, .mat-typography p {\n    margin: 0 0 12px; }\n\n.mat-small, .mat-caption {\n  font: 400 12px/20px Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-display-4, .mat-typography .mat-display-4 {\n  font: 300 112px/112px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 56px;\n  letter-spacing: -0.05em; }\n\n.mat-display-3, .mat-typography .mat-display-3 {\n  font: 400 56px/56px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 64px;\n  letter-spacing: -0.02em; }\n\n.mat-display-2, .mat-typography .mat-display-2 {\n  font: 400 45px/48px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 64px;\n  letter-spacing: -0.005em; }\n\n.mat-display-1, .mat-typography .mat-display-1 {\n  font: 400 34px/40px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 64px; }\n\n.mat-button, .mat-raised-button, .mat-icon-button, .mat-fab, .mat-mini-fab {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 14px;\n  font-weight: 500; }\n\n.mat-button-toggle {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-card {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-card-title {\n  font-size: 24px;\n  font-weight: 400; }\n\n.mat-card-subtitle,\n.mat-card-content,\n.mat-card-header .mat-card-title {\n  font-size: 14px; }\n\n.mat-checkbox {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-checkbox-layout .mat-checkbox-label {\n  line-height: 24px; }\n\n.mat-chip {\n  font-size: 13px;\n  line-height: 18px; }\n  .mat-chip .mat-chip-remove.mat-icon {\n    font-size: 18px; }\n\n.mat-table {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-header-cell {\n  font-size: 12px;\n  font-weight: 500; }\n\n.mat-cell {\n  font-size: 14px; }\n\n.mat-calendar {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-calendar-body {\n  font-size: 13px; }\n\n.mat-calendar-body-label,\n.mat-calendar-period-button {\n  font-size: 14px;\n  font-weight: 500; }\n\n.mat-calendar-table-header th {\n  font-size: 11px;\n  font-weight: 400; }\n\n.mat-dialog-title {\n  font: 500 20px/32px Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-expansion-panel-header {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 15px;\n  font-weight: 400; }\n\n.mat-expansion-panel-content {\n  font: 400 14px/20px Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-form-field {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: inherit;\n  font-weight: 400;\n  line-height: 1.125; }\n\n.mat-form-field-wrapper {\n  padding-bottom: 1.25em; }\n\n.mat-form-field-prefix .mat-icon,\n.mat-form-field-suffix .mat-icon {\n  font-size: 150%;\n  line-height: 1.125; }\n\n.mat-form-field-prefix .mat-icon-button,\n.mat-form-field-suffix .mat-icon-button {\n  height: 1.5em;\n  width: 1.5em; }\n  .mat-form-field-prefix .mat-icon-button .mat-icon,\n  .mat-form-field-suffix .mat-icon-button .mat-icon {\n    height: 1.125em;\n    line-height: 1.125; }\n\n.mat-form-field-infix {\n  padding: 0.4375em 0;\n  border-top: 0.84375em solid transparent; }\n\n.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label,\n.mat-form-field-can-float .mat-input-server:focus + .mat-form-field-label-wrapper .mat-form-field-label {\n  transform: translateY(-1.28125em) scale(0.75) perspective(100px) translateZ(0.001px);\n  -ms-transform: translateY(-1.28125em) scale(0.75);\n  width: 133.33333333%; }\n\n.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill + .mat-form-field-label-wrapper\n.mat-form-field-label {\n  transform: translateY(-1.28125em) scale(0.75) perspective(100px) translateZ(0.00101px);\n  -ms-transform: translateY(-1.28124em) scale(0.75);\n  width: 133.33334333%; }\n\n.mat-form-field-can-float .mat-input-server[label]:not(:label-shown) + .mat-form-field-label-wrapper\n.mat-form-field-label {\n  transform: translateY(-1.28125em) scale(0.75) perspective(100px) translateZ(0.00102px);\n  -ms-transform: translateY(-1.28123em) scale(0.75);\n  width: 133.33335333%; }\n\n.mat-form-field-label-wrapper {\n  top: -0.84375em;\n  padding-top: 0.84375em; }\n\n.mat-form-field-label {\n  top: 1.28125em; }\n\n.mat-form-field-underline {\n  bottom: 1.25em; }\n\n.mat-form-field-subscript-wrapper {\n  font-size: 75%;\n  margin-top: 0.54166667em;\n  top: calc(100% - 1.66666667em); }\n\n.mat-grid-tile-header,\n.mat-grid-tile-footer {\n  font-size: 14px; }\n  .mat-grid-tile-header .mat-line,\n  .mat-grid-tile-footer .mat-line {\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    display: block;\n    box-sizing: border-box; }\n    .mat-grid-tile-header .mat-line:nth-child(n+2),\n    .mat-grid-tile-footer .mat-line:nth-child(n+2) {\n      font-size: 12px; }\n\ninput.mat-input-element {\n  margin-top: -0.0625em; }\n\n.mat-menu-item {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 16px;\n  font-weight: 400; }\n\n.mat-paginator,\n.mat-paginator-page-size .mat-select-trigger {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 12px; }\n\n.mat-radio-button {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-select {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-select-trigger {\n  height: 1.125em; }\n\n.mat-slide-toggle-content {\n  font: 400 14px/20px Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-slider-thumb-label-text {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 12px;\n  font-weight: 500; }\n\n.mat-stepper-vertical, .mat-stepper-horizontal {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-step-label {\n  font-size: 14px;\n  font-weight: 400; }\n\n.mat-step-label-selected {\n  font-size: 14px;\n  font-weight: 500; }\n\n.mat-tab-group {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-tab-label, .mat-tab-link {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 14px;\n  font-weight: 500; }\n\n.mat-toolbar,\n.mat-toolbar h1,\n.mat-toolbar h2,\n.mat-toolbar h3,\n.mat-toolbar h4,\n.mat-toolbar h5,\n.mat-toolbar h6 {\n  font: 500 20px/32px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0; }\n\n.mat-tooltip {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 10px;\n  padding-top: 6px;\n  padding-bottom: 6px; }\n\n.mat-list-item {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-list-option {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-list .mat-list-item, .mat-nav-list .mat-list-item, .mat-selection-list .mat-list-item {\n  font-size: 16px; }\n  .mat-list .mat-list-item .mat-line, .mat-nav-list .mat-list-item .mat-line, .mat-selection-list .mat-list-item .mat-line {\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    display: block;\n    box-sizing: border-box; }\n    .mat-list .mat-list-item .mat-line:nth-child(n+2), .mat-nav-list .mat-list-item .mat-line:nth-child(n+2), .mat-selection-list .mat-list-item .mat-line:nth-child(n+2) {\n      font-size: 14px; }\n\n.mat-list .mat-list-option, .mat-nav-list .mat-list-option, .mat-selection-list .mat-list-option {\n  font-size: 16px; }\n  .mat-list .mat-list-option .mat-line, .mat-nav-list .mat-list-option .mat-line, .mat-selection-list .mat-list-option .mat-line {\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    display: block;\n    box-sizing: border-box; }\n    .mat-list .mat-list-option .mat-line:nth-child(n+2), .mat-nav-list .mat-list-option .mat-line:nth-child(n+2), .mat-selection-list .mat-list-option .mat-line:nth-child(n+2) {\n      font-size: 14px; }\n\n.mat-list .mat-subheader, .mat-nav-list .mat-subheader, .mat-selection-list .mat-subheader {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 14px;\n  font-weight: 500; }\n\n.mat-list[dense] .mat-list-item, .mat-nav-list[dense] .mat-list-item, .mat-selection-list[dense] .mat-list-item {\n  font-size: 12px; }\n  .mat-list[dense] .mat-list-item .mat-line, .mat-nav-list[dense] .mat-list-item .mat-line, .mat-selection-list[dense] .mat-list-item .mat-line {\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    display: block;\n    box-sizing: border-box; }\n    .mat-list[dense] .mat-list-item .mat-line:nth-child(n+2), .mat-nav-list[dense] .mat-list-item .mat-line:nth-child(n+2), .mat-selection-list[dense] .mat-list-item .mat-line:nth-child(n+2) {\n      font-size: 12px; }\n\n.mat-list[dense] .mat-list-option, .mat-nav-list[dense] .mat-list-option, .mat-selection-list[dense] .mat-list-option {\n  font-size: 12px; }\n  .mat-list[dense] .mat-list-option .mat-line, .mat-nav-list[dense] .mat-list-option .mat-line, .mat-selection-list[dense] .mat-list-option .mat-line {\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    display: block;\n    box-sizing: border-box; }\n    .mat-list[dense] .mat-list-option .mat-line:nth-child(n+2), .mat-nav-list[dense] .mat-list-option .mat-line:nth-child(n+2), .mat-selection-list[dense] .mat-list-option .mat-line:nth-child(n+2) {\n      font-size: 12px; }\n\n.mat-list[dense] .mat-subheader, .mat-nav-list[dense] .mat-subheader, .mat-selection-list[dense] .mat-subheader {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 12px;\n  font-weight: 500; }\n\n.mat-option {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 16px; }\n\n.mat-optgroup-label {\n  font: 500 14px/24px Roboto, \"Helvetica Neue\", sans-serif; }\n\n.mat-simple-snackbar {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 14px; }\n\n.mat-simple-snackbar-action {\n  line-height: 1;\n  font-family: inherit;\n  font-size: inherit;\n  font-weight: 500; }\n\n.mat-ripple {\n  overflow: hidden; }\n  @media screen and (-ms-high-contrast: active) {\n    .mat-ripple {\n      display: none; } }\n\n.mat-ripple.mat-ripple-unbounded {\n  overflow: visible; }\n\n.mat-ripple-element {\n  position: absolute;\n  border-radius: 50%;\n  pointer-events: none;\n  transition: opacity, transform 0ms cubic-bezier(0, 0, 0.2, 1);\n  transform: scale(0); }\n\n.mat-option {\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  display: block;\n  line-height: 48px;\n  height: 48px;\n  padding: 0 16px;\n  text-align: left;\n  text-decoration: none;\n  position: relative;\n  cursor: pointer;\n  outline: none;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-direction: row;\n      flex-direction: row;\n  max-width: 100%;\n  box-sizing: border-box;\n  -ms-flex-align: center;\n      align-items: center; }\n  .mat-option[disabled] {\n    cursor: default; }\n  [dir='rtl'] .mat-option {\n    text-align: right; }\n  .mat-option .mat-icon {\n    margin-right: 16px; }\n    [dir='rtl'] .mat-option .mat-icon {\n      margin-left: 16px;\n      margin-right: 0; }\n  .mat-option[aria-disabled='true'] {\n    -webkit-user-select: none;\n    -moz-user-select: none;\n    -ms-user-select: none;\n    user-select: none;\n    cursor: default; }\n  .mat-optgroup .mat-option:not(.mat-option-multiple) {\n    padding-left: 32px; }\n    [dir='rtl'] .mat-optgroup .mat-option:not(.mat-option-multiple) {\n      padding-left: 16px;\n      padding-right: 32px; }\n\n.mat-option-text {\n  display: inline-block;\n  -ms-flex-positive: 1;\n      flex-grow: 1;\n  overflow: hidden;\n  text-overflow: ellipsis; }\n\n.mat-option-ripple {\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  position: absolute;\n  pointer-events: none; }\n  @media screen and (-ms-high-contrast: active) {\n    .mat-option-ripple {\n      opacity: 0.5; } }\n\n.mat-option-pseudo-checkbox {\n  margin-right: 8px; }\n  [dir='rtl'] .mat-option-pseudo-checkbox {\n    margin-left: 8px;\n    margin-right: 0; }\n\n.mat-optgroup-label {\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  display: block;\n  line-height: 48px;\n  height: 48px;\n  padding: 0 16px;\n  text-align: left;\n  text-decoration: none;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n  cursor: default; }\n  .mat-optgroup-label[disabled] {\n    cursor: default; }\n  [dir='rtl'] .mat-optgroup-label {\n    text-align: right; }\n  .mat-optgroup-label .mat-icon {\n    margin-right: 16px; }\n    [dir='rtl'] .mat-optgroup-label .mat-icon {\n      margin-left: 16px;\n      margin-right: 0; }\n\n.cdk-visually-hidden {\n  border: 0;\n  clip: rect(0 0 0 0);\n  height: 1px;\n  margin: -1px;\n  overflow: hidden;\n  padding: 0;\n  position: absolute;\n  width: 1px; }\n\n.cdk-overlay-container, .cdk-global-overlay-wrapper {\n  pointer-events: none;\n  top: 0;\n  left: 0;\n  height: 100%;\n  width: 100%; }\n\n.cdk-overlay-container {\n  position: fixed;\n  z-index: 1000; }\n\n.cdk-global-overlay-wrapper {\n  display: -ms-flexbox;\n  display: flex;\n  position: absolute;\n  z-index: 1000; }\n\n.cdk-overlay-pane {\n  position: absolute;\n  pointer-events: auto;\n  box-sizing: border-box;\n  z-index: 1000; }\n\n.cdk-overlay-backdrop {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  z-index: 1000;\n  pointer-events: auto;\n  -webkit-tap-highlight-color: transparent;\n  transition: opacity 400ms cubic-bezier(0.25, 0.8, 0.25, 1);\n  opacity: 0; }\n  .cdk-overlay-backdrop.cdk-overlay-backdrop-showing {\n    opacity: 0.48; }\n\n.cdk-overlay-dark-backdrop {\n  background: rgba(0, 0, 0, 0.6); }\n\n.cdk-overlay-transparent-backdrop {\n  background: none; }\n\n.cdk-global-scrollblock {\n  position: fixed;\n  width: 100%;\n  overflow-y: scroll; }\n\n.mat-ripple-element {\n  background-color: rgba(0, 0, 0, 0.1); }\n\n.mat-option {\n  color: rgba(0, 0, 0, 0.87); }\n  .mat-option:hover:not(.mat-option-disabled), .mat-option:focus:not(.mat-option-disabled) {\n    background: rgba(0, 0, 0, 0.04); }\n  .mat-primary .mat-option.mat-selected:not(.mat-option-disabled) {\n    color: #455a64; }\n  .mat-accent .mat-option.mat-selected:not(.mat-option-disabled) {\n    color: #2196f3; }\n  .mat-warn .mat-option.mat-selected:not(.mat-option-disabled) {\n    color: #f44336; }\n  .mat-option.mat-selected:not(.mat-option-multiple):not(.mat-option-disabled) {\n    background: rgba(0, 0, 0, 0.04); }\n  .mat-option.mat-active {\n    background: rgba(0, 0, 0, 0.04);\n    color: rgba(0, 0, 0, 0.87); }\n  .mat-option.mat-option-disabled {\n    color: rgba(0, 0, 0, 0.38); }\n\n.mat-optgroup-label {\n  color: rgba(0, 0, 0, 0.54); }\n\n.mat-optgroup-disabled .mat-optgroup-label {\n  color: rgba(0, 0, 0, 0.38); }\n\n.mat-pseudo-checkbox {\n  color: rgba(0, 0, 0, 0.54); }\n  .mat-pseudo-checkbox::after {\n    color: #fafafa; }\n\n.mat-pseudo-checkbox-checked,\n.mat-pseudo-checkbox-indeterminate,\n.mat-accent .mat-pseudo-checkbox-checked,\n.mat-accent .mat-pseudo-checkbox-indeterminate {\n  background: #2196f3; }\n\n.mat-primary .mat-pseudo-checkbox-checked,\n.mat-primary .mat-pseudo-checkbox-indeterminate {\n  background: #455a64; }\n\n.mat-warn .mat-pseudo-checkbox-checked,\n.mat-warn .mat-pseudo-checkbox-indeterminate {\n  background: #f44336; }\n\n.mat-pseudo-checkbox-checked.mat-pseudo-checkbox-disabled,\n.mat-pseudo-checkbox-indeterminate.mat-pseudo-checkbox-disabled {\n  background: #b0b0b0; }\n\n.mat-app-background {\n  background-color: #fafafa; }\n\n.mat-theme-loaded-marker {\n  display: none; }\n\n.mat-autocomplete-panel {\n  background: white;\n  color: rgba(0, 0, 0, 0.87); }\n  .mat-autocomplete-panel .mat-option.mat-selected:not(.mat-active):not(:hover) {\n    background: white; }\n    .mat-autocomplete-panel .mat-option.mat-selected:not(.mat-active):not(:hover):not(.mat-option-disabled) {\n      color: rgba(0, 0, 0, 0.87); }\n\n.mat-button, .mat-icon-button {\n  background: transparent; }\n  .mat-button.mat-primary .mat-button-focus-overlay, .mat-icon-button.mat-primary .mat-button-focus-overlay {\n    background-color: rgba(69, 90, 100, 0.12); }\n  .mat-button.mat-accent .mat-button-focus-overlay, .mat-icon-button.mat-accent .mat-button-focus-overlay {\n    background-color: rgba(33, 150, 243, 0.12); }\n  .mat-button.mat-warn .mat-button-focus-overlay, .mat-icon-button.mat-warn .mat-button-focus-overlay {\n    background-color: rgba(244, 67, 54, 0.12); }\n  .mat-button[disabled] .mat-button-focus-overlay, .mat-icon-button[disabled] .mat-button-focus-overlay {\n    background-color: transparent; }\n  .mat-button.mat-primary, .mat-icon-button.mat-primary {\n    color: #455a64; }\n  .mat-button.mat-accent, .mat-icon-button.mat-accent {\n    color: #2196f3; }\n  .mat-button.mat-warn, .mat-icon-button.mat-warn {\n    color: #f44336; }\n  .mat-button.mat-primary[disabled], .mat-button.mat-accent[disabled], .mat-button.mat-warn[disabled], .mat-button[disabled][disabled], .mat-icon-button.mat-primary[disabled], .mat-icon-button.mat-accent[disabled], .mat-icon-button.mat-warn[disabled], .mat-icon-button[disabled][disabled] {\n    color: rgba(0, 0, 0, 0.38); }\n\n.mat-raised-button, .mat-fab, .mat-mini-fab {\n  color: rgba(0, 0, 0, 0.87);\n  background-color: white; }\n  .mat-raised-button.mat-primary, .mat-fab.mat-primary, .mat-mini-fab.mat-primary {\n    color: rgba(255, 255, 255, 0.87); }\n  .mat-raised-button.mat-accent, .mat-fab.mat-accent, .mat-mini-fab.mat-accent {\n    color: white; }\n  .mat-raised-button.mat-warn, .mat-fab.mat-warn, .mat-mini-fab.mat-warn {\n    color: white; }\n  .mat-raised-button.mat-primary[disabled], .mat-raised-button.mat-accent[disabled], .mat-raised-button.mat-warn[disabled], .mat-raised-button[disabled][disabled], .mat-fab.mat-primary[disabled], .mat-fab.mat-accent[disabled], .mat-fab.mat-warn[disabled], .mat-fab[disabled][disabled], .mat-mini-fab.mat-primary[disabled], .mat-mini-fab.mat-accent[disabled], .mat-mini-fab.mat-warn[disabled], .mat-mini-fab[disabled][disabled] {\n    color: rgba(0, 0, 0, 0.38); }\n  .mat-raised-button.mat-primary, .mat-fab.mat-primary, .mat-mini-fab.mat-primary {\n    background-color: #455a64; }\n  .mat-raised-button.mat-accent, .mat-fab.mat-accent, .mat-mini-fab.mat-accent {\n    background-color: #2196f3; }\n  .mat-raised-button.mat-warn, .mat-fab.mat-warn, .mat-mini-fab.mat-warn {\n    background-color: #f44336; }\n  .mat-raised-button.mat-primary[disabled], .mat-raised-button.mat-accent[disabled], .mat-raised-button.mat-warn[disabled], .mat-raised-button[disabled][disabled], .mat-fab.mat-primary[disabled], .mat-fab.mat-accent[disabled], .mat-fab.mat-warn[disabled], .mat-fab[disabled][disabled], .mat-mini-fab.mat-primary[disabled], .mat-mini-fab.mat-accent[disabled], .mat-mini-fab.mat-warn[disabled], .mat-mini-fab[disabled][disabled] {\n    background-color: rgba(0, 0, 0, 0.12); }\n  .mat-raised-button.mat-primary .mat-ripple-element, .mat-fab.mat-primary .mat-ripple-element, .mat-mini-fab.mat-primary .mat-ripple-element {\n    background-color: rgba(255, 255, 255, 0.2); }\n  .mat-raised-button.mat-accent .mat-ripple-element, .mat-fab.mat-accent .mat-ripple-element, .mat-mini-fab.mat-accent .mat-ripple-element {\n    background-color: rgba(255, 255, 255, 0.2); }\n  .mat-raised-button.mat-warn .mat-ripple-element, .mat-fab.mat-warn .mat-ripple-element, .mat-mini-fab.mat-warn .mat-ripple-element {\n    background-color: rgba(255, 255, 255, 0.2); }\n\n.mat-button.mat-primary .mat-ripple-element {\n  background-color: rgba(69, 90, 100, 0.1); }\n\n.mat-button.mat-accent .mat-ripple-element {\n  background-color: rgba(33, 150, 243, 0.1); }\n\n.mat-button.mat-warn .mat-ripple-element {\n  background-color: rgba(244, 67, 54, 0.1); }\n\n.mat-icon-button.mat-primary .mat-ripple-element {\n  background-color: rgba(69, 90, 100, 0.2); }\n\n.mat-icon-button.mat-accent .mat-ripple-element {\n  background-color: rgba(33, 150, 243, 0.2); }\n\n.mat-icon-button.mat-warn .mat-ripple-element {\n  background-color: rgba(244, 67, 54, 0.2); }\n\n.mat-button-toggle {\n  color: rgba(0, 0, 0, 0.38); }\n  .mat-button-toggle.cdk-focused .mat-button-toggle-focus-overlay {\n    background-color: rgba(0, 0, 0, 0.06); }\n\n.mat-button-toggle-checked {\n  background-color: #e0e0e0;\n  color: rgba(0, 0, 0, 0.54); }\n\n.mat-button-toggle-disabled {\n  background-color: #eeeeee;\n  color: rgba(0, 0, 0, 0.38); }\n  .mat-button-toggle-disabled.mat-button-toggle-checked {\n    background-color: #bdbdbd; }\n\n.mat-card {\n  background: white;\n  color: rgba(0, 0, 0, 0.87); }\n\n.mat-card-subtitle {\n  color: rgba(0, 0, 0, 0.54); }\n\n.mat-checkbox-frame {\n  border-color: rgba(0, 0, 0, 0.54); }\n\n.mat-checkbox-checkmark {\n  fill: #fafafa; }\n\n.mat-checkbox-checkmark-path {\n  stroke: #fafafa !important; }\n\n.mat-checkbox-mixedmark {\n  background-color: #fafafa; }\n\n.mat-checkbox-indeterminate.mat-primary .mat-checkbox-background, .mat-checkbox-checked.mat-primary .mat-checkbox-background {\n  background-color: #455a64; }\n\n.mat-checkbox-indeterminate.mat-accent .mat-checkbox-background, .mat-checkbox-checked.mat-accent .mat-checkbox-background {\n  background-color: #2196f3; }\n\n.mat-checkbox-indeterminate.mat-warn .mat-checkbox-background, .mat-checkbox-checked.mat-warn .mat-checkbox-background {\n  background-color: #f44336; }\n\n.mat-checkbox-disabled.mat-checkbox-checked .mat-checkbox-background, .mat-checkbox-disabled.mat-checkbox-indeterminate .mat-checkbox-background {\n  background-color: #b0b0b0; }\n\n.mat-checkbox-disabled:not(.mat-checkbox-checked) .mat-checkbox-frame {\n  border-color: #b0b0b0; }\n\n.mat-checkbox-disabled .mat-checkbox-label {\n  color: #b0b0b0; }\n\n.mat-checkbox:not(.mat-checkbox-disabled).mat-primary .mat-checkbox-ripple .mat-ripple-element {\n  background-color: rgba(69, 90, 100, 0.26); }\n\n.mat-checkbox:not(.mat-checkbox-disabled).mat-accent .mat-checkbox-ripple .mat-ripple-element {\n  background-color: rgba(33, 150, 243, 0.26); }\n\n.mat-checkbox:not(.mat-checkbox-disabled).mat-warn .mat-checkbox-ripple .mat-ripple-element {\n  background-color: rgba(244, 67, 54, 0.26); }\n\n.mat-chip:not(.mat-basic-chip) {\n  background-color: #e0e0e0;\n  color: rgba(0, 0, 0, 0.87); }\n  .mat-chip:not(.mat-basic-chip) .mat-chip-remove {\n    color: rgba(0, 0, 0, 0.87);\n    opacity: 0.4; }\n  .mat-chip:not(.mat-basic-chip) .mat-chip-remove:hover {\n    opacity: 0.54; }\n\n.mat-chip.mat-chip-selected.mat-primary {\n  background-color: #455a64;\n  color: rgba(255, 255, 255, 0.87); }\n  .mat-chip.mat-chip-selected.mat-primary .mat-chip-remove {\n    color: rgba(255, 255, 255, 0.87);\n    opacity: 0.4; }\n  .mat-chip.mat-chip-selected.mat-primary .mat-chip-remove:hover {\n    opacity: 0.54; }\n\n.mat-chip.mat-chip-selected.mat-warn {\n  background-color: #f44336;\n  color: white; }\n  .mat-chip.mat-chip-selected.mat-warn .mat-chip-remove {\n    color: white;\n    opacity: 0.4; }\n  .mat-chip.mat-chip-selected.mat-warn .mat-chip-remove:hover {\n    opacity: 0.54; }\n\n.mat-chip.mat-chip-selected.mat-accent {\n  background-color: #2196f3;\n  color: white; }\n  .mat-chip.mat-chip-selected.mat-accent .mat-chip-remove {\n    color: white;\n    opacity: 0.4; }\n  .mat-chip.mat-chip-selected.mat-accent .mat-chip-remove:hover {\n    opacity: 0.54; }\n\n.mat-table {\n  background: white; }\n\n.mat-row, .mat-header-row {\n  border-bottom-color: rgba(0, 0, 0, 0.12); }\n\n.mat-header-cell {\n  color: rgba(0, 0, 0, 0.54); }\n\n.mat-cell {\n  color: rgba(0, 0, 0, 0.87); }\n\n.mat-datepicker-content {\n  background-color: white;\n  color: rgba(0, 0, 0, 0.87); }\n\n.mat-calendar-arrow {\n  border-top-color: rgba(0, 0, 0, 0.54); }\n\n.mat-calendar-next-button,\n.mat-calendar-previous-button {\n  color: rgba(0, 0, 0, 0.54); }\n\n.mat-calendar-table-header {\n  color: rgba(0, 0, 0, 0.38); }\n\n.mat-calendar-table-header-divider::after {\n  background: rgba(0, 0, 0, 0.12); }\n\n.mat-calendar-body-label {\n  color: rgba(0, 0, 0, 0.54); }\n\n.mat-calendar-body-cell-content {\n  color: rgba(0, 0, 0, 0.87);\n  border-color: transparent; }\n  .mat-calendar-body-disabled > .mat-calendar-body-cell-content:not(.mat-calendar-body-selected) {\n    color: rgba(0, 0, 0, 0.38); }\n\n:not(.mat-calendar-body-disabled):hover > .mat-calendar-body-cell-content:not(.mat-calendar-body-selected),\n.cdk-keyboard-focused .mat-calendar-body-active > .mat-calendar-body-cell-content:not(.mat-calendar-body-selected),\n.cdk-program-focused .mat-calendar-body-active > .mat-calendar-body-cell-content:not(.mat-calendar-body-selected) {\n  background-color: rgba(0, 0, 0, 0.04); }\n\n.mat-calendar-body-selected {\n  background-color: #455a64;\n  color: rgba(255, 255, 255, 0.87); }\n\n.mat-calendar-body-disabled > .mat-calendar-body-selected {\n  background-color: rgba(69, 90, 100, 0.4); }\n\n.mat-calendar-body-today:not(.mat-calendar-body-selected) {\n  border-color: rgba(0, 0, 0, 0.38); }\n\n.mat-calendar-body-today.mat-calendar-body-selected {\n  box-shadow: inset 0 0 0 1px rgba(255, 255, 255, 0.87); }\n\n.mat-calendar-body-disabled > .mat-calendar-body-today:not(.mat-calendar-body-selected) {\n  border-color: rgba(0, 0, 0, 0.18); }\n\n.mat-dialog-container {\n  background: white;\n  color: rgba(0, 0, 0, 0.87); }\n\n.mat-expansion-panel {\n  background: white;\n  color: rgba(0, 0, 0, 0.87); }\n\n.mat-action-row {\n  border-top-color: rgba(0, 0, 0, 0.12); }\n\n.mat-expansion-panel:not(.mat-expanded) .mat-expansion-panel-header:not([aria-disabled='true']).cdk-keyboard-focused, .mat-expansion-panel:not(.mat-expanded) .mat-expansion-panel-header:not([aria-disabled='true']).cdk-program-focused, .mat-expansion-panel:not(.mat-expanded) .mat-expansion-panel-header:not([aria-disabled='true']):hover {\n  background: rgba(0, 0, 0, 0.04); }\n\n.mat-expansion-panel-header-title {\n  color: rgba(0, 0, 0, 0.87); }\n\n.mat-expansion-panel-header-description,\n.mat-expansion-indicator::after {\n  color: rgba(0, 0, 0, 0.54); }\n\n.mat-expansion-panel-header[aria-disabled='true'] {\n  color: rgba(0, 0, 0, 0.38); }\n  .mat-expansion-panel-header[aria-disabled='true'] .mat-expansion-panel-header-title,\n  .mat-expansion-panel-header[aria-disabled='true'] .mat-expansion-panel-header-description {\n    color: inherit; }\n\n.mat-form-field-label {\n  color: rgba(0, 0, 0, 0.54); }\n\n.mat-hint {\n  color: rgba(0, 0, 0, 0.54); }\n\n.mat-focused .mat-form-field-label {\n  color: #455a64; }\n  .mat-focused .mat-form-field-label.mat-accent {\n    color: #2196f3; }\n  .mat-focused .mat-form-field-label.mat-warn {\n    color: #f44336; }\n\n.mat-focused .mat-form-field-required-marker {\n  color: #2196f3; }\n\n.mat-form-field-underline {\n  background-color: rgba(0, 0, 0, 0.42); }\n\n.mat-form-field-disabled .mat-form-field-underline {\n  background-image: linear-gradient(to right, rgba(0, 0, 0, 0.42) 0%, rgba(0, 0, 0, 0.42) 33%, transparent 0%);\n  background-size: 4px 1px;\n  background-repeat: repeat-x; }\n\n.mat-form-field-ripple {\n  background-color: #455a64; }\n  .mat-form-field-ripple.mat-accent {\n    background-color: #2196f3; }\n  .mat-form-field-ripple.mat-warn {\n    background-color: #f44336; }\n\n.mat-form-field-invalid .mat-form-field-label {\n  color: #f44336; }\n  .mat-form-field-invalid .mat-form-field-label.mat-accent,\n  .mat-form-field-invalid .mat-form-field-label .mat-form-field-required-marker {\n    color: #f44336; }\n\n.mat-form-field-invalid .mat-form-field-ripple {\n  background-color: #f44336; }\n\n.mat-error {\n  color: #f44336; }\n\n.mat-icon.mat-primary {\n  color: #455a64; }\n\n.mat-icon.mat-accent {\n  color: #2196f3; }\n\n.mat-icon.mat-warn {\n  color: #f44336; }\n\n.mat-input-element:disabled {\n  color: rgba(0, 0, 0, 0.38); }\n\n.mat-input-element::-webkit-input-placeholder {\n  color: rgba(0, 0, 0, 0.42); }\n\n.mat-input-element:-ms-input-placeholder {\n  color: rgba(0, 0, 0, 0.42); }\n\n.mat-input-element::placeholder {\n  color: rgba(0, 0, 0, 0.42); }\n\n.mat-input-element::-moz-placeholder {\n  color: rgba(0, 0, 0, 0.42); }\n\n.mat-input-element::-webkit-input-placeholder {\n  color: rgba(0, 0, 0, 0.42); }\n\n.mat-input-element:-ms-input-placeholder {\n  color: rgba(0, 0, 0, 0.42); }\n\n.mat-list .mat-list-item, .mat-nav-list .mat-list-item, .mat-selection-list .mat-list-item {\n  color: rgba(0, 0, 0, 0.87); }\n\n.mat-list .mat-list-option, .mat-nav-list .mat-list-option, .mat-selection-list .mat-list-option {\n  color: rgba(0, 0, 0, 0.87); }\n\n.mat-list .mat-subheader, .mat-nav-list .mat-subheader, .mat-selection-list .mat-subheader {\n  color: rgba(0, 0, 0, 0.54); }\n\n.mat-list-item-disabled {\n  background-color: #eeeeee; }\n\n.mat-divider {\n  border-top-color: rgba(0, 0, 0, 0.12); }\n\n.mat-nav-list .mat-list-item {\n  outline: none; }\n  .mat-nav-list .mat-list-item:hover, .mat-nav-list .mat-list-item.mat-list-item-focus {\n    background: rgba(0, 0, 0, 0.04); }\n\n.mat-list-option {\n  outline: none; }\n  .mat-list-option:hover, .mat-list-option.mat-list-item-focus {\n    background: rgba(0, 0, 0, 0.04); }\n\n.mat-menu-panel {\n  background: white; }\n\n.mat-menu-item {\n  background: transparent;\n  color: rgba(0, 0, 0, 0.87); }\n  .mat-menu-item[disabled] {\n    color: rgba(0, 0, 0, 0.38); }\n\n.mat-menu-item .mat-icon:not([color]),\n.mat-menu-item-submenu-trigger::after {\n  color: rgba(0, 0, 0, 0.54); }\n\n.mat-menu-item:hover:not([disabled]),\n.mat-menu-item:focus:not([disabled]),\n.mat-menu-item-highlighted:not([disabled]) {\n  background: rgba(0, 0, 0, 0.04); }\n\n.mat-paginator {\n  background: white; }\n\n.mat-paginator,\n.mat-paginator-page-size .mat-select-trigger {\n  color: rgba(0, 0, 0, 0.54); }\n\n.mat-paginator-increment,\n.mat-paginator-decrement {\n  border-top: 2px solid rgba(0, 0, 0, 0.54);\n  border-right: 2px solid rgba(0, 0, 0, 0.54); }\n\n.mat-icon-button[disabled] .mat-paginator-increment,\n.mat-icon-button[disabled] .mat-paginator-decrement {\n  border-color: rgba(0, 0, 0, 0.38); }\n\n.mat-progress-bar-background {\n  background-image: url(\"data:image/svg+xml;charset=UTF-8,%3Csvg%20version%3D%271.1%27%20xmlns%3D%27http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%27%20xmlns%3Axlink%3D%27http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%27%20x%3D%270px%27%20y%3D%270px%27%20enable-background%3D%27new%200%200%205%202%27%20xml%3Aspace%3D%27preserve%27%20viewBox%3D%270%200%205%202%27%20preserveAspectRatio%3D%27none%20slice%27%3E%3Ccircle%20cx%3D%271%27%20cy%3D%271%27%20r%3D%271%27%20fill%3D%27%23607d8b%27%2F%3E%3C%2Fsvg%3E\"); }\n\n.mat-progress-bar-buffer {\n  background-color: #607d8b; }\n\n.mat-progress-bar-fill::after {\n  background-color: #455a64; }\n\n.mat-progress-bar.mat-accent .mat-progress-bar-background {\n  background-image: url(\"data:image/svg+xml;charset=UTF-8,%3Csvg%20version%3D%271.1%27%20xmlns%3D%27http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%27%20xmlns%3Axlink%3D%27http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%27%20x%3D%270px%27%20y%3D%270px%27%20enable-background%3D%27new%200%200%205%202%27%20xml%3Aspace%3D%27preserve%27%20viewBox%3D%270%200%205%202%27%20preserveAspectRatio%3D%27none%20slice%27%3E%3Ccircle%20cx%3D%271%27%20cy%3D%271%27%20r%3D%271%27%20fill%3D%27%23bbdefb%27%2F%3E%3C%2Fsvg%3E\"); }\n\n.mat-progress-bar.mat-accent .mat-progress-bar-buffer {\n  background-color: #bbdefb; }\n\n.mat-progress-bar.mat-accent .mat-progress-bar-fill::after {\n  background-color: #2196f3; }\n\n.mat-progress-bar.mat-warn .mat-progress-bar-background {\n  background-image: url(\"data:image/svg+xml;charset=UTF-8,%3Csvg%20version%3D%271.1%27%20xmlns%3D%27http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%27%20xmlns%3Axlink%3D%27http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%27%20x%3D%270px%27%20y%3D%270px%27%20enable-background%3D%27new%200%200%205%202%27%20xml%3Aspace%3D%27preserve%27%20viewBox%3D%270%200%205%202%27%20preserveAspectRatio%3D%27none%20slice%27%3E%3Ccircle%20cx%3D%271%27%20cy%3D%271%27%20r%3D%271%27%20fill%3D%27%23ffcdd2%27%2F%3E%3C%2Fsvg%3E\"); }\n\n.mat-progress-bar.mat-warn .mat-progress-bar-buffer {\n  background-color: #ffcdd2; }\n\n.mat-progress-bar.mat-warn .mat-progress-bar-fill::after {\n  background-color: #f44336; }\n\n.mat-progress-spinner circle, .mat-spinner circle {\n  stroke: #455a64; }\n\n.mat-progress-spinner.mat-accent circle, .mat-spinner.mat-accent circle {\n  stroke: #2196f3; }\n\n.mat-progress-spinner.mat-warn circle, .mat-spinner.mat-warn circle {\n  stroke: #f44336; }\n\n.mat-radio-outer-circle {\n  border-color: rgba(0, 0, 0, 0.54); }\n\n.mat-radio-disabled .mat-radio-outer-circle {\n  border-color: rgba(0, 0, 0, 0.38); }\n\n.mat-radio-disabled .mat-radio-ripple .mat-ripple-element, .mat-radio-disabled .mat-radio-inner-circle {\n  background-color: rgba(0, 0, 0, 0.38); }\n\n.mat-radio-disabled .mat-radio-label-content {\n  color: rgba(0, 0, 0, 0.38); }\n\n.mat-radio-button.mat-primary.mat-radio-checked .mat-radio-outer-circle {\n  border-color: #455a64; }\n\n.mat-radio-button.mat-primary .mat-radio-inner-circle {\n  background-color: #455a64; }\n\n.mat-radio-button.mat-primary .mat-radio-ripple .mat-ripple-element {\n  background-color: rgba(69, 90, 100, 0.26); }\n\n.mat-radio-button.mat-accent.mat-radio-checked .mat-radio-outer-circle {\n  border-color: #2196f3; }\n\n.mat-radio-button.mat-accent .mat-radio-inner-circle {\n  background-color: #2196f3; }\n\n.mat-radio-button.mat-accent .mat-radio-ripple .mat-ripple-element {\n  background-color: rgba(33, 150, 243, 0.26); }\n\n.mat-radio-button.mat-warn.mat-radio-checked .mat-radio-outer-circle {\n  border-color: #f44336; }\n\n.mat-radio-button.mat-warn .mat-radio-inner-circle {\n  background-color: #f44336; }\n\n.mat-radio-button.mat-warn .mat-radio-ripple .mat-ripple-element {\n  background-color: rgba(244, 67, 54, 0.26); }\n\n.mat-select-content, .mat-select-panel-done-animating {\n  background: white; }\n\n.mat-select-value {\n  color: rgba(0, 0, 0, 0.87); }\n\n.mat-select-placeholder {\n  color: rgba(0, 0, 0, 0.42); }\n\n.mat-select-disabled .mat-select-value {\n  color: rgba(0, 0, 0, 0.38); }\n\n.mat-select-arrow {\n  color: rgba(0, 0, 0, 0.54); }\n\n.mat-select-panel .mat-option.mat-selected:not(.mat-option-multiple) {\n  background: rgba(0, 0, 0, 0.12); }\n\n.mat-form-field.mat-focused.mat-primary .mat-select-arrow {\n  color: #455a64; }\n\n.mat-form-field.mat-focused.mat-accent .mat-select-arrow {\n  color: #2196f3; }\n\n.mat-form-field.mat-focused.mat-warn .mat-select-arrow {\n  color: #f44336; }\n\n.mat-form-field .mat-select.mat-select-invalid .mat-select-arrow {\n  color: #f44336; }\n\n.mat-form-field .mat-select.mat-select-disabled .mat-select-arrow {\n  color: rgba(0, 0, 0, 0.38); }\n\n.mat-drawer-container {\n  background-color: #fafafa;\n  color: rgba(0, 0, 0, 0.87); }\n\n.mat-drawer {\n  background-color: white;\n  color: rgba(0, 0, 0, 0.87); }\n  .mat-drawer.mat-drawer-push {\n    background-color: white; }\n\n.mat-drawer-backdrop.mat-drawer-shown {\n  background-color: rgba(0, 0, 0, 0.6); }\n\n.mat-slide-toggle.mat-checked:not(.mat-disabled) .mat-slide-toggle-thumb {\n  background-color: #2196f3; }\n\n.mat-slide-toggle.mat-checked:not(.mat-disabled) .mat-slide-toggle-bar {\n  background-color: rgba(33, 150, 243, 0.5); }\n\n.mat-slide-toggle:not(.mat-checked) .mat-ripple-element {\n  background-color: rgba(0, 0, 0, 0.06); }\n\n.mat-slide-toggle .mat-ripple-element {\n  background-color: rgba(33, 150, 243, 0.12); }\n\n.mat-slide-toggle.mat-primary.mat-checked:not(.mat-disabled) .mat-slide-toggle-thumb {\n  background-color: #607d8b; }\n\n.mat-slide-toggle.mat-primary.mat-checked:not(.mat-disabled) .mat-slide-toggle-bar {\n  background-color: rgba(96, 125, 139, 0.5); }\n\n.mat-slide-toggle.mat-primary:not(.mat-checked) .mat-ripple-element {\n  background-color: rgba(0, 0, 0, 0.06); }\n\n.mat-slide-toggle.mat-primary .mat-ripple-element {\n  background-color: rgba(96, 125, 139, 0.12); }\n\n.mat-slide-toggle.mat-warn.mat-checked:not(.mat-disabled) .mat-slide-toggle-thumb {\n  background-color: #f44336; }\n\n.mat-slide-toggle.mat-warn.mat-checked:not(.mat-disabled) .mat-slide-toggle-bar {\n  background-color: rgba(244, 67, 54, 0.5); }\n\n.mat-slide-toggle.mat-warn:not(.mat-checked) .mat-ripple-element {\n  background-color: rgba(0, 0, 0, 0.06); }\n\n.mat-slide-toggle.mat-warn .mat-ripple-element {\n  background-color: rgba(244, 67, 54, 0.12); }\n\n.mat-disabled .mat-slide-toggle-thumb {\n  background-color: #bdbdbd; }\n\n.mat-disabled .mat-slide-toggle-bar {\n  background-color: rgba(0, 0, 0, 0.1); }\n\n.mat-slide-toggle-thumb {\n  background-color: #fafafa; }\n\n.mat-slide-toggle-bar {\n  background-color: rgba(0, 0, 0, 0.38); }\n\n.mat-slider-track-background {\n  background-color: rgba(0, 0, 0, 0.26); }\n\n.mat-primary .mat-slider-track-fill,\n.mat-primary .mat-slider-thumb,\n.mat-primary .mat-slider-thumb-label {\n  background-color: #455a64; }\n\n.mat-primary .mat-slider-thumb-label-text {\n  color: rgba(255, 255, 255, 0.87); }\n\n.mat-accent .mat-slider-track-fill,\n.mat-accent .mat-slider-thumb,\n.mat-accent .mat-slider-thumb-label {\n  background-color: #2196f3; }\n\n.mat-accent .mat-slider-thumb-label-text {\n  color: white; }\n\n.mat-warn .mat-slider-track-fill,\n.mat-warn .mat-slider-thumb,\n.mat-warn .mat-slider-thumb-label {\n  background-color: #f44336; }\n\n.mat-warn .mat-slider-thumb-label-text {\n  color: white; }\n\n.mat-slider-focus-ring {\n  background-color: rgba(33, 150, 243, 0.2); }\n\n.mat-slider:hover .mat-slider-track-background,\n.cdk-focused .mat-slider-track-background {\n  background-color: rgba(0, 0, 0, 0.38); }\n\n.mat-slider-disabled .mat-slider-track-background,\n.mat-slider-disabled .mat-slider-track-fill,\n.mat-slider-disabled .mat-slider-thumb {\n  background-color: rgba(0, 0, 0, 0.26); }\n\n.mat-slider-disabled:hover .mat-slider-track-background {\n  background-color: rgba(0, 0, 0, 0.26); }\n\n.mat-slider-min-value .mat-slider-focus-ring {\n  background-color: rgba(0, 0, 0, 0.12); }\n\n.mat-slider-min-value.mat-slider-thumb-label-showing .mat-slider-thumb,\n.mat-slider-min-value.mat-slider-thumb-label-showing .mat-slider-thumb-label {\n  background-color: rgba(0, 0, 0, 0.87); }\n\n.mat-slider-min-value.mat-slider-thumb-label-showing.cdk-focused .mat-slider-thumb,\n.mat-slider-min-value.mat-slider-thumb-label-showing.cdk-focused .mat-slider-thumb-label {\n  background-color: rgba(0, 0, 0, 0.26); }\n\n.mat-slider-min-value:not(.mat-slider-thumb-label-showing) .mat-slider-thumb {\n  border-color: rgba(0, 0, 0, 0.26);\n  background-color: transparent; }\n\n.mat-slider-min-value:not(.mat-slider-thumb-label-showing):hover .mat-slider-thumb, .mat-slider-min-value:not(.mat-slider-thumb-label-showing).cdk-focused .mat-slider-thumb {\n  border-color: rgba(0, 0, 0, 0.38); }\n\n.mat-slider-min-value:not(.mat-slider-thumb-label-showing):hover.mat-slider-disabled .mat-slider-thumb, .mat-slider-min-value:not(.mat-slider-thumb-label-showing).cdk-focused.mat-slider-disabled .mat-slider-thumb {\n  border-color: rgba(0, 0, 0, 0.26); }\n\n.mat-slider-has-ticks .mat-slider-wrapper::after {\n  border-color: rgba(0, 0, 0, 0.7); }\n\n.mat-slider-horizontal .mat-slider-ticks {\n  background-image: repeating-linear-gradient(to right, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7) 2px, transparent 0, transparent);\n  background-image: -moz-repeating-linear-gradient(0.0001deg, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7) 2px, transparent 0, transparent); }\n\n.mat-slider-vertical .mat-slider-ticks {\n  background-image: repeating-linear-gradient(to bottom, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7) 2px, transparent 0, transparent); }\n\n.mat-step-header.cdk-keyboard-focused, .mat-step-header.cdk-program-focused, .mat-step-header:hover {\n  background-color: rgba(0, 0, 0, 0.04); }\n\n.mat-step-header .mat-step-label,\n.mat-step-header .mat-step-optional {\n  color: rgba(0, 0, 0, 0.38); }\n\n.mat-step-header .mat-step-icon {\n  background-color: #455a64;\n  color: rgba(255, 255, 255, 0.87); }\n\n.mat-step-header .mat-step-icon-not-touched {\n  background-color: rgba(0, 0, 0, 0.38);\n  color: rgba(255, 255, 255, 0.87); }\n\n.mat-step-header .mat-step-label.mat-step-label-active {\n  color: rgba(0, 0, 0, 0.87); }\n\n.mat-stepper-horizontal, .mat-stepper-vertical {\n  background-color: white; }\n\n.mat-stepper-vertical-line::before {\n  border-left-color: rgba(0, 0, 0, 0.12); }\n\n.mat-stepper-horizontal-line {\n  border-top-color: rgba(0, 0, 0, 0.12); }\n\n.mat-tab-nav-bar,\n.mat-tab-header {\n  border-bottom: 1px solid rgba(0, 0, 0, 0.12); }\n\n.mat-tab-group-inverted-header .mat-tab-nav-bar,\n.mat-tab-group-inverted-header .mat-tab-header {\n  border-top: 1px solid rgba(0, 0, 0, 0.12);\n  border-bottom: none; }\n\n.mat-tab-label, .mat-tab-link {\n  color: rgba(0, 0, 0, 0.87); }\n  .mat-tab-label.mat-tab-disabled, .mat-tab-link.mat-tab-disabled {\n    color: rgba(0, 0, 0, 0.38); }\n\n.mat-tab-header-pagination-chevron {\n  border-color: rgba(0, 0, 0, 0.87); }\n\n.mat-tab-header-pagination-disabled .mat-tab-header-pagination-chevron {\n  border-color: rgba(0, 0, 0, 0.38); }\n\n.mat-tab-group[class*='mat-background-'] .mat-tab-header,\n.mat-tab-nav-bar[class*='mat-background-'] {\n  border-bottom: none;\n  border-top: none; }\n\n.mat-tab-group.mat-primary .mat-tab-label:focus, .mat-tab-group.mat-primary .mat-tab-link:focus, .mat-tab-nav-bar.mat-primary .mat-tab-label:focus, .mat-tab-nav-bar.mat-primary .mat-tab-link:focus {\n  background-color: rgba(96, 125, 139, 0.3); }\n\n.mat-tab-group.mat-primary .mat-ink-bar, .mat-tab-nav-bar.mat-primary .mat-ink-bar {\n  background-color: #455a64; }\n\n.mat-tab-group.mat-primary.mat-background-primary .mat-ink-bar, .mat-tab-nav-bar.mat-primary.mat-background-primary .mat-ink-bar {\n  background-color: rgba(255, 255, 255, 0.87); }\n\n.mat-tab-group.mat-accent .mat-tab-label:focus, .mat-tab-group.mat-accent .mat-tab-link:focus, .mat-tab-nav-bar.mat-accent .mat-tab-label:focus, .mat-tab-nav-bar.mat-accent .mat-tab-link:focus {\n  background-color: rgba(187, 222, 251, 0.3); }\n\n.mat-tab-group.mat-accent .mat-ink-bar, .mat-tab-nav-bar.mat-accent .mat-ink-bar {\n  background-color: #2196f3; }\n\n.mat-tab-group.mat-accent.mat-background-accent .mat-ink-bar, .mat-tab-nav-bar.mat-accent.mat-background-accent .mat-ink-bar {\n  background-color: white; }\n\n.mat-tab-group.mat-warn .mat-tab-label:focus, .mat-tab-group.mat-warn .mat-tab-link:focus, .mat-tab-nav-bar.mat-warn .mat-tab-label:focus, .mat-tab-nav-bar.mat-warn .mat-tab-link:focus {\n  background-color: rgba(255, 205, 210, 0.3); }\n\n.mat-tab-group.mat-warn .mat-ink-bar, .mat-tab-nav-bar.mat-warn .mat-ink-bar {\n  background-color: #f44336; }\n\n.mat-tab-group.mat-warn.mat-background-warn .mat-ink-bar, .mat-tab-nav-bar.mat-warn.mat-background-warn .mat-ink-bar {\n  background-color: white; }\n\n.mat-tab-group.mat-background-primary .mat-tab-label:focus, .mat-tab-group.mat-background-primary .mat-tab-link:focus, .mat-tab-nav-bar.mat-background-primary .mat-tab-label:focus, .mat-tab-nav-bar.mat-background-primary .mat-tab-link:focus {\n  background-color: rgba(96, 125, 139, 0.3); }\n\n.mat-tab-group.mat-background-primary .mat-tab-header, .mat-tab-group.mat-background-primary .mat-tab-links, .mat-tab-nav-bar.mat-background-primary .mat-tab-header, .mat-tab-nav-bar.mat-background-primary .mat-tab-links {\n  background-color: #455a64; }\n\n.mat-tab-group.mat-background-primary .mat-tab-label, .mat-tab-group.mat-background-primary .mat-tab-link, .mat-tab-nav-bar.mat-background-primary .mat-tab-label, .mat-tab-nav-bar.mat-background-primary .mat-tab-link {\n  color: rgba(255, 255, 255, 0.87); }\n  .mat-tab-group.mat-background-primary .mat-tab-label.mat-tab-disabled, .mat-tab-group.mat-background-primary .mat-tab-link.mat-tab-disabled, .mat-tab-nav-bar.mat-background-primary .mat-tab-label.mat-tab-disabled, .mat-tab-nav-bar.mat-background-primary .mat-tab-link.mat-tab-disabled {\n    color: rgba(255, 255, 255, 0.4); }\n\n.mat-tab-group.mat-background-primary .mat-tab-header-pagination-chevron, .mat-tab-nav-bar.mat-background-primary .mat-tab-header-pagination-chevron {\n  border-color: rgba(255, 255, 255, 0.87); }\n\n.mat-tab-group.mat-background-primary .mat-tab-header-pagination-disabled .mat-tab-header-pagination-chevron, .mat-tab-nav-bar.mat-background-primary .mat-tab-header-pagination-disabled .mat-tab-header-pagination-chevron {\n  border-color: rgba(255, 255, 255, 0.4); }\n\n.mat-tab-group.mat-background-primary .mat-ripple-element, .mat-tab-nav-bar.mat-background-primary .mat-ripple-element {\n  background-color: rgba(255, 255, 255, 0.12); }\n\n.mat-tab-group.mat-background-accent .mat-tab-label:focus, .mat-tab-group.mat-background-accent .mat-tab-link:focus, .mat-tab-nav-bar.mat-background-accent .mat-tab-label:focus, .mat-tab-nav-bar.mat-background-accent .mat-tab-link:focus {\n  background-color: rgba(187, 222, 251, 0.3); }\n\n.mat-tab-group.mat-background-accent .mat-tab-header, .mat-tab-group.mat-background-accent .mat-tab-links, .mat-tab-nav-bar.mat-background-accent .mat-tab-header, .mat-tab-nav-bar.mat-background-accent .mat-tab-links {\n  background-color: #2196f3; }\n\n.mat-tab-group.mat-background-accent .mat-tab-label, .mat-tab-group.mat-background-accent .mat-tab-link, .mat-tab-nav-bar.mat-background-accent .mat-tab-label, .mat-tab-nav-bar.mat-background-accent .mat-tab-link {\n  color: white; }\n  .mat-tab-group.mat-background-accent .mat-tab-label.mat-tab-disabled, .mat-tab-group.mat-background-accent .mat-tab-link.mat-tab-disabled, .mat-tab-nav-bar.mat-background-accent .mat-tab-label.mat-tab-disabled, .mat-tab-nav-bar.mat-background-accent .mat-tab-link.mat-tab-disabled {\n    color: rgba(255, 255, 255, 0.4); }\n\n.mat-tab-group.mat-background-accent .mat-tab-header-pagination-chevron, .mat-tab-nav-bar.mat-background-accent .mat-tab-header-pagination-chevron {\n  border-color: white; }\n\n.mat-tab-group.mat-background-accent .mat-tab-header-pagination-disabled .mat-tab-header-pagination-chevron, .mat-tab-nav-bar.mat-background-accent .mat-tab-header-pagination-disabled .mat-tab-header-pagination-chevron {\n  border-color: rgba(255, 255, 255, 0.4); }\n\n.mat-tab-group.mat-background-accent .mat-ripple-element, .mat-tab-nav-bar.mat-background-accent .mat-ripple-element {\n  background-color: rgba(255, 255, 255, 0.12); }\n\n.mat-tab-group.mat-background-warn .mat-tab-label:focus, .mat-tab-group.mat-background-warn .mat-tab-link:focus, .mat-tab-nav-bar.mat-background-warn .mat-tab-label:focus, .mat-tab-nav-bar.mat-background-warn .mat-tab-link:focus {\n  background-color: rgba(255, 205, 210, 0.3); }\n\n.mat-tab-group.mat-background-warn .mat-tab-header, .mat-tab-group.mat-background-warn .mat-tab-links, .mat-tab-nav-bar.mat-background-warn .mat-tab-header, .mat-tab-nav-bar.mat-background-warn .mat-tab-links {\n  background-color: #f44336; }\n\n.mat-tab-group.mat-background-warn .mat-tab-label, .mat-tab-group.mat-background-warn .mat-tab-link, .mat-tab-nav-bar.mat-background-warn .mat-tab-label, .mat-tab-nav-bar.mat-background-warn .mat-tab-link {\n  color: white; }\n  .mat-tab-group.mat-background-warn .mat-tab-label.mat-tab-disabled, .mat-tab-group.mat-background-warn .mat-tab-link.mat-tab-disabled, .mat-tab-nav-bar.mat-background-warn .mat-tab-label.mat-tab-disabled, .mat-tab-nav-bar.mat-background-warn .mat-tab-link.mat-tab-disabled {\n    color: rgba(255, 255, 255, 0.4); }\n\n.mat-tab-group.mat-background-warn .mat-tab-header-pagination-chevron, .mat-tab-nav-bar.mat-background-warn .mat-tab-header-pagination-chevron {\n  border-color: white; }\n\n.mat-tab-group.mat-background-warn .mat-tab-header-pagination-disabled .mat-tab-header-pagination-chevron, .mat-tab-nav-bar.mat-background-warn .mat-tab-header-pagination-disabled .mat-tab-header-pagination-chevron {\n  border-color: rgba(255, 255, 255, 0.4); }\n\n.mat-tab-group.mat-background-warn .mat-ripple-element, .mat-tab-nav-bar.mat-background-warn .mat-ripple-element {\n  background-color: rgba(255, 255, 255, 0.12); }\n\n.mat-toolbar {\n  background: whitesmoke;\n  color: rgba(0, 0, 0, 0.87); }\n  .mat-toolbar.mat-primary {\n    background: #455a64;\n    color: rgba(255, 255, 255, 0.87); }\n  .mat-toolbar.mat-accent {\n    background: #2196f3;\n    color: white; }\n  .mat-toolbar.mat-warn {\n    background: #f44336;\n    color: white; }\n\n.mat-tooltip {\n  background: rgba(97, 97, 97, 0.9); }\n\n.mat-snack-bar-container {\n  background: #323232;\n  color: white; }\n\n.mat-simple-snackbar-action {\n  color: #2196f3; }\n\n.mat-form-field {\n  font: 400 16px/1.125 Roboto, \"Helvetica Neue\", sans-serif; }\n\n/**\r\n  Core\r\n */\n/*\r\n  Logo\r\n */\n/*\r\n  Colors\r\n */\n/*\r\n  Toolbar\r\n */\n/*\r\n  Sidenav\r\n */\n/*\r\n  Sidenav: Layout Alpha\r\n */\n/*\r\n  Sidenav: Layout Beta\r\n */\n/*\r\n  Sidenav-Item: Padding\r\n */\n/*\r\n  Horizontal Navigation\r\n */\n/*\r\n  Sidenav-Item: Padding\r\n */\n:host {\n  display: block;\n  position: relative;\n  height: 100%;\n  overflow: hidden; }\n\n.inbox {\n  height: 100%; }\n  .inbox .header {\n    min-height: 200px;\n    max-height: 200px;\n    background: #fff url(\"/assets/img/demo/headers/pattern-1.png\") no-repeat center center;\n    background-size: cover; }\n  .inbox .container {\n    margin-top: -200px;\n    max-height: 100%;\n    box-sizing: border-box;\n    height: 100%;\n    padding-bottom: 24px; }\n    @media screen and (max-width: 1279px) {\n      .inbox .container {\n        padding-bottom: 16px; } }\n  .inbox .navigation {\n    padding: 42px 24px 0;\n    height: 100%;\n    overflow-y: hidden; }\n    .inbox .navigation .title-container {\n      min-height: 56px;\n      max-height: 56px;\n      padding-left: 16px;\n      padding-right: 16px;\n      color: white;\n      font-size: 28px; }\n      .inbox .navigation .title-container mat-icon {\n        margin-right: 4px;\n        height: 24px;\n        width: 24px;\n        font-size: 24px;\n        vertical-align: sub; }\n      .inbox .navigation .title-container .title {\n        vertical-align: middle; }\n    .inbox .navigation .compose-container {\n      margin: 52px 16px 0 12px; }\n      .inbox .navigation .compose-container .compose {\n        padding-left: 20px;\n        padding-right: 32px;\n        width: 100%; }\n        .inbox .navigation .compose-container .compose mat-icon {\n          margin-right: 10px; }\n    .inbox .navigation .nav-list {\n      margin-top: 28px;\n      height: calc(100% - 182px);\n      overflow-y: auto; }\n      .inbox .navigation .nav-list .nav-item {\n        color: #616161;\n        margin: 8px 0; }\n        .inbox .navigation .nav-list .nav-item .nav-title .icon {\n          margin-right: 12px;\n          font-size: 22px;\n          width: 22px;\n          height: 22px; }\n        .inbox .navigation .nav-list .nav-item .nav-title .text {\n          font-size: 14px; }\n        .inbox .navigation .nav-list .nav-item .nav-title .icon, .inbox .navigation .nav-list .nav-item .nav-title .text {\n          font-weight: 500;\n          vertical-align: middle; }\n        .inbox .navigation .nav-list .nav-item.active {\n          color: #2196f3;\n          background: #EEE; }\n    .inbox .navigation mat-divider {\n      margin: 0 16px; }\n  .inbox .content-container {\n    height: 100%;\n    max-width: 90vw; }\n    .inbox .content-container .search {\n      background: white;\n      margin-top: 42px;\n      min-height: 56px;\n      max-height: 56px; }\n      .inbox .content-container .search mat-icon {\n        vertical-align: middle;\n        margin-right: 14px;\n        margin-left: 16px; }\n      .inbox .content-container .search input {\n        border: 0;\n        height: 56px;\n        outline: none; }\n    .inbox .content-container .mails-container {\n      margin-top: 39px;\n      background: white;\n      position: relative;\n      height: calc(100% - 137px); }\n      .inbox .content-container .mails-container .inbox-toolbar {\n        box-sizing: border-box;\n        min-height: 64px;\n        max-height: 64px;\n        border-bottom: 1px solid #EEE;\n        padding: 8px 24px;\n        background: white;\n        color: #666; }\n        .inbox .content-container .mails-container .inbox-toolbar .chevrons {\n          margin-left: 16px; }\n        .inbox .content-container .mails-container .inbox-toolbar .checkbox + .toolbar-detail {\n          border-left: 1px solid #EEE;\n          margin-left: 24px; }\n      .inbox .content-container .mails-container .toolbar-detail .icon-group {\n        border-right: 1px solid #EEE; }\n      .inbox .content-container .mails-container .toolbar-detail .back {\n        margin-left: -12px;\n        margin-right: 15px; }\n      .inbox .content-container .mails-container .toolbar-detail .archive {\n        margin-left: 12px;\n        margin-right: 6px; }\n      .inbox .content-container .mails-container .toolbar-detail .spam {\n        margin-left: 6px;\n        margin-right: 6px; }\n      .inbox .content-container .mails-container .toolbar-detail .delete {\n        margin-left: 6px;\n        margin-right: 12px; }\n      .inbox .content-container .mails-container .toolbar-detail .move {\n        margin-left: 12px;\n        margin-right: 6px; }\n      .inbox .content-container .mails-container .toolbar-detail .label {\n        margin-left: 6px;\n        margin-right: 12px; }\n      .inbox .content-container .mails-container .toolbar-detail .options {\n        margin-left: 12px;\n        margin-right: 12px; }\n      .inbox .content-container .mails-container .mails {\n        height: calc(100% - 64px);\n        position: relative; }\n        .inbox .content-container .mails-container .mails .mail-list {\n          height: 100%;\n          overflow-y: auto; }\n        .inbox .content-container .mails-container .mails .mail {\n          padding: 8px 24px;\n          white-space: nowrap;\n          font-weight: 500;\n          font-size: 15px;\n          text-decoration: none;\n          color: #616161;\n          transition: background .1s ease-in-out;\n          position: relative;\n          border-bottom: 1px solid #EEE;\n          cursor: pointer;\n          min-height: 42px; }\n          .inbox .content-container .mails-container .mails .mail.read {\n            color: #9F9F9F;\n            font-weight: normal; }\n          .inbox .content-container .mails-container .mails .mail .start-wrapper {\n            padding-right: 8px; }\n            .inbox .content-container .mails-container .mails .mail .start-wrapper .checkbox {\n              margin: 0 8px 0 0;\n              color: #616161; }\n            .inbox .content-container .mails-container .mails .mail .start-wrapper .star {\n              margin: 0 8px;\n              color: #616161; }\n              .inbox .content-container .mails-container .mails .mail .start-wrapper .star.active {\n                color: #FFC107; }\n            .inbox .content-container .mails-container .mails .mail .start-wrapper .from-avatar {\n              margin: 0 8px;\n              border-radius: 50%; }\n            .inbox .content-container .mails-container .mails .mail .start-wrapper .from-name {\n              margin: 0 8px;\n              overflow-x: hidden;\n              text-overflow: ellipsis; }\n          .inbox .content-container .mails-container .mails .mail .content-wrapper {\n            white-space: nowrap;\n            overflow-x: hidden;\n            display: block;\n            margin: 0; }\n            .inbox .content-container .mails-container .mails .mail .content-wrapper .labels .label {\n              padding: 2px 8px;\n              font-size: 13px;\n              color: #FFFFFF; }\n            .inbox .content-container .mails-container .mails .mail .content-wrapper .labels .label + .label {\n              margin-left: 6px; }\n            .inbox .content-container .mails-container .mails .mail .content-wrapper .subject {\n              margin: 0 8px; }\n            .inbox .content-container .mails-container .mails .mail .content-wrapper .middot {\n              font-weight: bold; }\n            .inbox .content-container .mails-container .mails .mail .content-wrapper .content {\n              overflow-x: hidden;\n              text-overflow: ellipsis;\n              margin: 0 8px; }\n          .inbox .content-container .mails-container .mails .mail .end-wrapper {\n            margin-left: 16px; }\n            .inbox .content-container .mails-container .mails .mail .end-wrapper .when {\n              margin: 0 8px; }\n            .inbox .content-container .mails-container .mails .mail .end-wrapper .options {\n              margin: 0 0 0 8px;\n              color: #616161; }\n          .inbox .content-container .mails-container .mails .mail:hover {\n            background: #EEE; }\n        .inbox .content-container .mails-container .mails .no-mails-container {\n          margin-top: 16px; }\n          .inbox .content-container .mails-container .mails .no-mails-container .no-mails {\n            font-size: 18px; }\n        .inbox .content-container .mails-container .mails .mail-detail {\n          height: 100%; }\n          .inbox .content-container .mails-container .mails .mail-detail .mail-content-container {\n            padding: 24px 32px;\n            height: 100%;\n            overflow-y: auto; }\n          .inbox .content-container .mails-container .mails .mail-detail .mail-header .avatar {\n            height: 45px;\n            width: 45px;\n            margin-right: 20px; }\n          .inbox .content-container .mails-container .mails .mail-detail .mail-header .from {\n            font-weight: 500;\n            font-size: 15px; }\n          .inbox .content-container .mails-container .mails .mail-detail .mail-header .to {\n            color: #999;\n            font-size: 15px;\n            margin-top: 2px;\n            font-weight: 500; }\n            .inbox .content-container .mails-container .mails .mail-detail .mail-header .to mat-icon {\n              height: 16px;\n              width: 16px;\n              font-size: 16px;\n              vertical-align: middle; }\n          .inbox .content-container .mails-container .mails .mail-detail .mail-header .mail-extra {\n            color: #999;\n            font-size: 15px;\n            font-weight: 500; }\n            .inbox .content-container .mails-container .mails .mail-detail .mail-header .mail-extra .when {\n              vertical-align: middle; }\n            .inbox .content-container .mails-container .mails .mail-detail .mail-header .mail-extra .options {\n              vertical-align: middle;\n              margin-left: 16px; }\n          .inbox .content-container .mails-container .mails .mail-detail .mail-content {\n            margin-top: 32px; }\n          .inbox .content-container .mails-container .mails .mail-detail .attachments-container .attachments-header {\n            color: #999; }\n            .inbox .content-container .mails-container .mails .mail-detail .attachments-container .attachments-header .title {\n              font-size: 13px;\n              font-weight: 500;\n              margin-right: 24px; }\n            .inbox .content-container .mails-container .mails .mail-detail .attachments-container .attachments-header .download {\n              margin-left: 24px; }\n          .inbox .content-container .mails-container .mails .mail-detail .attachments-container .attachments {\n            margin-top: 12px;\n            margin-bottom: 12px; }\n            .inbox .content-container .mails-container .mails .mail-detail .attachments-container .attachments .attachment {\n              margin-right: 12px; }\n              .inbox .content-container .mails-container .mails .mail-detail .attachments-container .attachments .attachment img {\n                max-height: 140px;\n                max-width: 80px; }\n          .inbox .content-container .mails-container .mails .mail-detail .respond {\n            width: 100%;\n            padding: 24px 32px;\n            border-top: 1px solid #EEE;\n            cursor: pointer; }\n            .inbox .content-container .mails-container .mails .mail-detail .respond .avatar {\n              margin-right: 28px; }\n            .inbox .content-container .mails-container .mails .mail-detail .respond .click-reply {\n              color: #999; }\n              .inbox .content-container .mails-container .mails .mail-detail .respond .click-reply .semi-bold {\n                font-weight: 500; }\n            .inbox .content-container .mails-container .mails .mail-detail .respond .reply {\n              color: #2B303B;\n              margin-right: 24px; }\n            .inbox .content-container .mails-container .mails .mail-detail .respond .respond-buttons {\n              margin-top: 12px; }\n              .inbox .content-container .mails-container .mails .mail-detail .respond .respond-buttons mat-icon {\n                font-size: 18px;\n                height: 18px;\n                width: 18px;\n                margin-left: 6px; }\n              .inbox .content-container .mails-container .mails .mail-detail .respond .respond-buttons button {\n                margin-left: 12px; }\n\n@media only screen and (min-width: 1280px) {\n  .inbox .content-container {\n    max-width: calc(100% - 290px); } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/pages/inbox/inbox.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InboxComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_app_pages_inbox_mail_model__ = __webpack_require__("../../../../../src/app/pages/inbox/mail.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_app_pages_inbox_inbox_compose_inbox_compose_component__ = __webpack_require__("../../../../../src/app/pages/inbox/inbox-compose/inbox-compose.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngrx_store__ = __webpack_require__("../../../../@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__reducers_index__ = __webpack_require__("../../../../../src/app/reducers/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__shared_inbox_action__ = __webpack_require__("../../../../../src/app/pages/inbox/shared/inbox.action.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__inbox_demo__ = __webpack_require__("../../../../../src/app/pages/inbox/inbox.demo.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_animation__ = __webpack_require__("../../../../../src/app/app.animation.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var InboxComponent = (function () {
    function InboxComponent(cd, store, renderer, composeDialog, snackBar) {
        this.cd = cd;
        this.store = store;
        this.renderer = renderer;
        this.composeDialog = composeDialog;
        this.snackBar = snackBar;
        this.allMails = [];
        this.mails = [];
        this.selectedMails = [];
        this.clickListeners = [];
        this.respondOptions = {
            toolbar: [
                ['bold', 'italic', 'underline', 'strike'],
                [{ 'header': 1 }, { 'header': 2 }],
                [{ 'list': 'ordered' }, { 'list': 'bullet' }],
                [{ 'size': ['small', false, 'large', 'huge'] }],
                [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
                [{ 'color': [] }, { 'background': [] }],
                [{ 'font': [] }],
                [{ 'align': [] }],
                ['clean'],
                ['link', 'image', 'video'] // link and image, video
            ]
        };
    }
    InboxComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_6__shared_inbox_action__["j" /* RemoveAllMailsAction */]());
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_6__shared_inbox_action__["c" /* BulkAddMailAction */](__WEBPACK_IMPORTED_MODULE_7__inbox_demo__["a" /* demoMails */].map(function (mail) {
            return new __WEBPACK_IMPORTED_MODULE_1_app_pages_inbox_mail_model__["a" /* Mail */](mail);
        })));
        this.allMailsSubscription = this.store.select(__WEBPACK_IMPORTED_MODULE_5__reducers_index__["e" /* getInboxMails */])
            .subscribe(function (allMails) {
            _this.allMails = allMails;
            _this.cd.markForCheck();
        });
        this.mailsSubscription = this.store.select(__WEBPACK_IMPORTED_MODULE_5__reducers_index__["f" /* getInboxMailsFiltered */])
            .subscribe(function (mails) {
            _this.mails = mails;
            _this.cd.markForCheck();
        });
        this.currentlyOpenSubscription = this.store.select(__WEBPACK_IMPORTED_MODULE_5__reducers_index__["d" /* getInboxCurrentlyOpen */])
            .subscribe(function (currentlyOpen) {
            _this.currentlyOpen = currentlyOpen;
            _this.cd.markForCheck();
        });
        this.activeGroupSubscription = this.store.select(__WEBPACK_IMPORTED_MODULE_5__reducers_index__["b" /* getInboxActiveGroup */])
            .subscribe(function (activeGroup) {
            _this.activeGroup = activeGroup;
            _this.cd.markForCheck();
        });
        this.activeTypeSubscription = this.store.select(__WEBPACK_IMPORTED_MODULE_5__reducers_index__["c" /* getInboxActiveType */])
            .subscribe(function (activeType) {
            _this.activeType = activeType;
            _this.cd.markForCheck();
        });
        this.showOnlyStarredSubscription = this.store.select(__WEBPACK_IMPORTED_MODULE_5__reducers_index__["g" /* getInboxShowOnlyStarred */])
            .subscribe(function (showOnlyStarred) {
            _this.showOnlyStarred = showOnlyStarred;
            _this.cd.markForCheck();
        });
    };
    InboxComponent.prototype.ngAfterViewChecked = function () {
        this.createMailListClickListeners();
    };
    InboxComponent.prototype.openMail = function (mail) {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_6__shared_inbox_action__["g" /* OpenMailAction */](mail));
    };
    InboxComponent.prototype.closeMail = function () {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_6__shared_inbox_action__["e" /* CloseMailAction */]());
    };
    InboxComponent.prototype.setActiveGroup = function (group) {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_6__shared_inbox_action__["n" /* SetActiveGroupAction */](group));
    };
    InboxComponent.prototype.setActiveType = function (type) {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_6__shared_inbox_action__["o" /* SetActiveTypeAction */](type));
    };
    InboxComponent.prototype.setShowOnlyStarred = function () {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_6__shared_inbox_action__["p" /* ShowOnlyStarredAction */]());
    };
    InboxComponent.prototype.isSelected = function (mail) {
        return this.selectedMails.includes(mail);
    };
    InboxComponent.prototype.openComposeDialog = function () {
        var _this = this;
        var dialogRef = this.composeDialog.open(__WEBPACK_IMPORTED_MODULE_3_app_pages_inbox_inbox_compose_inbox_compose_component__["a" /* InboxComposeComponent */]);
        dialogRef.afterClosed().subscribe(function (result) {
            if (result) {
                _this.snackBar.open(result, null, {
                    duration: 3000
                });
            }
        });
    };
    InboxComponent.prototype.createMailListClickListeners = function () {
        var _this = this;
        this.clickListeners.forEach(function (listener) {
            listener();
        });
        this.mailList.forEach(function (elem, index) {
            _this.clickListeners.push(_this.renderer.listen(elem.nativeElement, 'click', function (event) {
                if (!event.target.classList.contains('mat-checkbox-inner-container') && !event.target.classList.contains('star')) {
                    _this.openMail(_this.mails[index]);
                }
            }));
        });
    };
    InboxComponent.prototype.toggleSelectAllThreads = function (event) {
        if (event.checked) {
            this.selectedMails = this.mails;
        }
        else {
            this.selectedMails = [];
        }
    };
    InboxComponent.prototype.toggleStarred = function (mail) {
        mail.starred = !mail.starred;
    };
    InboxComponent.prototype.unreadMailsCount = function (group) {
        var count = this.allMails.filter(function (mail) {
            return (mail.read === false && mail.group === group);
        }).length;
        var text = '';
        if (count > 0) {
            text = "(" + count + ")";
        }
        return text;
    };
    InboxComponent.prototype.setRespondActive = function (active) {
        this.respondActive = active;
    };
    InboxComponent.prototype.paginationCount = function () {
        if (this.mails.length < 25) {
            return this.mails.length;
        }
        else {
            return '25';
        }
    };
    InboxComponent.prototype.ngOnDestroy = function () {
        this.allMailsSubscription.unsubscribe();
        this.mailsSubscription.unsubscribe();
        this.currentlyOpenSubscription.unsubscribe();
        this.activeGroupSubscription.unsubscribe();
        this.activeTypeSubscription.unsubscribe();
        this.showOnlyStarredSubscription.unsubscribe();
        this.clickListeners.forEach(function (listener) {
            listener();
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChildren"])('mailList'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["QueryList"])
    ], InboxComponent.prototype, "mailList", void 0);
    InboxComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'vr-inbox',
            template: __webpack_require__("../../../../../src/app/pages/inbox/inbox.component.html"),
            styles: [__webpack_require__("../../../../../src/app/pages/inbox/inbox.component.scss")],
            animations: __WEBPACK_IMPORTED_MODULE_8__app_animation__["b" /* ROUTE_TRANSITION */].slice(),
            host: { '[@routeTransition]': '' }
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_4__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer2"],
            __WEBPACK_IMPORTED_MODULE_2__angular_material__["i" /* MatDialog */],
            __WEBPACK_IMPORTED_MODULE_2__angular_material__["C" /* MatSnackBar */]])
    ], InboxComponent);
    return InboxComponent;
}());



/***/ }),

/***/ "../../../../../src/app/pages/inbox/inbox.demo.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return demoMails; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_moment__ = __webpack_require__("../../../../moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_moment__);

var demoMails = [
    {
        'from': {
            'name': 'Rutledge Hammond',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'non non ex',
        'content': 'Exercitation Lorem laborum veniam ea fugiat esse fugiat commodo cupidatat. Laborum dolore labore quis consectetur ex. Magna deserunt id nostrud esse occaecat sint dolore aliqua id enim ad mollit. Cillum minim duis ut commodo eiusmod amet magna aute sunt aliquip fugiat sit.\n\nDo sint cupidatat ullamco nulla do dolor anim aliquip non consequat. Eu eiusmod ipsum nisi velit esse enim. Pariatur in eiusmod sit Lorem.\n\nEt minim incididunt dolore velit. Aliqua officia irure adipisicing magna quis consequat commodo reprehenderit excepteur ipsum cillum sit velit magna. Deserunt id sit qui ex nulla qui irure aliquip ex. Eu dolore tempor velit quis veniam commodo deserunt aliquip reprehenderit exercitation aute. Velit anim sint irure ipsum. Labore cupidatat aliquip eu dolor occaecat. Eiusmod ex minim reprehenderit elit nisi id quis dolor aliquip exercitation dolore.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(126, 'minutes'),
        'read': false,
        'starred': true,
        'labels': [],
        'group': 'social',
        'type': 'trash',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg']
    },
    {
        'from': {
            'name': 'Valentine Ray',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'nulla adipisicing consectetur',
        'content': 'Exercitation fugiat Lorem mollit dolore. Excepteur irure ullamco fugiat sunt laboris sit do consequat labore labore. Tempor id est enim laborum magna est pariatur qui et cillum commodo commodo. Culpa commodo ea aute nulla occaecat. Ut ipsum in amet culpa Lorem ipsum ipsum enim. Enim laborum exercitation do culpa eu deserunt eiusmod voluptate ut.\n\nCulpa reprehenderit aliquip nisi voluptate ullamco. Duis nisi ex et deserunt elit nostrud eu. Enim labore dolor elit consequat sint irure minim ipsum consequat nisi adipisicing. Aliquip ea irure minim deserunt ad id officia nulla excepteur labore ullamco pariatur. Consectetur amet ea aute voluptate minim veniam laboris sunt ipsum non aliquip. Exercitation enim ex velit ut incididunt cillum voluptate ullamco eu in sint.\n\nCillum deserunt duis aliqua ullamco. Nisi magna sit id fugiat aliqua consequat excepteur est incididunt et ex duis. Eiusmod et nostrud eiusmod aliquip cupidatat ad esse. Elit tempor eiusmod fugiat velit ad nostrud culpa. Incididunt eu labore aute excepteur velit nisi id magna laboris.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(132, 'hours'),
        'read': false,
        'starred': true,
        'labels': [],
        'group': 'social',
        'type': 'draft',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg']
    },
    {
        'from': {
            'name': 'Erna Clemons',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'nostrud minim veniam',
        'content': 'Minim exercitation Lorem velit reprehenderit irure nostrud tempor fugiat. Officia proident proident do ipsum fugiat nostrud. Minim consequat irure pariatur velit qui Lorem sunt officia deserunt laboris labore fugiat aliqua ad. Laborum voluptate aliquip est ullamco mollit et Lorem nostrud dolor aliquip laboris nostrud. Incididunt non tempor officia ad et laboris fugiat nisi in veniam aliquip ea. Est ipsum nulla id reprehenderit dolor id fugiat eiusmod irure voluptate non Lorem aute.\n\nFugiat commodo officia proident cupidatat cillum amet proident est in magna amet occaecat et qui. Lorem ullamco Lorem nisi proident et velit adipisicing. Eiusmod culpa est commodo cillum.\n\nDeserunt pariatur nulla fugiat laboris magna tempor nostrud tempor. Esse deserunt aliquip nisi aliquip dolore mollit Lorem ipsum amet quis eu. Id ipsum incididunt adipisicing sunt nulla dolor proident do occaecat veniam proident reprehenderit. Aliqua ut mollit officia esse. Commodo incididunt exercitation est est duis.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(167, 'days'),
        'read': true,
        'starred': true,
        'labels': [
            {
                'name': 'Private',
                'color': 'red'
            }
        ],
        'group': 'social',
        'type': 'trash',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg', 'assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Marian Newman',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'adipisicing sint commodo',
        'content': 'Ullamco veniam Lorem mollit irure enim eu nulla fugiat consectetur nisi consequat tempor sit. Velit excepteur do ad ipsum ipsum reprehenderit laboris anim enim. Dolor do eu qui et do ullamco ad laboris reprehenderit amet. Cillum occaecat cillum minim laborum veniam do nisi deserunt irure ad.\n\nEiusmod exercitation aliquip duis elit nulla non exercitation exercitation cillum qui veniam cillum reprehenderit nostrud. Sunt officia sunt minim occaecat ullamco nulla amet irure id commodo eiusmod irure. Nostrud consequat nostrud magna eu laboris sunt occaecat proident deserunt adipisicing magna excepteur ipsum. Ad veniam enim fugiat qui sunt amet. Commodo labore deserunt tempor anim veniam aute magna culpa mollit et do. Sunt nulla Lorem irure dolore reprehenderit id deserunt ut sunt voluptate fugiat.\n\nDuis consequat minim ea laboris. Elit labore qui incididunt commodo. Sit consequat aliquip proident elit magna labore est consectetur. Officia officia nisi aute amet mollit incididunt veniam elit ullamco ipsum.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(119, 'hours'),
        'read': true,
        'starred': false,
        'labels': [],
        'group': 'social',
        'type': 'spam',
        'attachments': ['assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Foley Prince',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'occaecat et eiusmod',
        'content': 'Mollit eu aliquip incididunt irure qui consequat sint commodo anim magna irure exercitation. Dolor sint aliquip enim officia ea deserunt culpa nulla ut voluptate exercitation deserunt eiusmod. Occaecat laborum nulla eu amet mollit aliquip nulla. Dolore exercitation magna do et. Dolore aliquip cillum nisi minim. Elit non enim occaecat proident commodo laborum irure laboris et adipisicing.\n\nExercitation occaecat laborum officia pariatur sunt tempor nulla sint. Commodo sunt ut irure ipsum nisi laborum officia nulla. Nulla ut nulla pariatur tempor. Laboris cupidatat aute fugiat minim deserunt nulla. Ullamco fugiat nisi tempor laboris minim adipisicing exercitation sunt dolor sunt non cupidatat cillum non. Dolor commodo do eiusmod occaecat minim.\n\nEt nulla magna nulla deserunt sint et commodo mollit in tempor cillum qui. Esse enim sit dolor ex ea elit consectetur cupidatat. Minim irure consectetur Lorem Lorem laboris laborum enim. Aute laborum ullamco consequat id magna quis eu fugiat culpa non exercitation fugiat sit. Proident voluptate pariatur aliqua non et pariatur dolor labore nostrud laborum in anim. Proident magna non ea proident labore commodo laborum aliqua nostrud occaecat id in tempor.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(24, 'days'),
        'read': true,
        'starred': true,
        'labels': [],
        'group': 'social',
        'type': 'sent',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg', 'assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Shaw Sanford',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'eu magna ut',
        'content': 'Adipisicing esse laborum aliquip tempor ullamco mollit esse est commodo nostrud amet anim minim. Adipisicing dolor quis proident eu id minim labore sunt aliquip sunt ea ea reprehenderit sunt. Do id aliquip officia aliqua ad commodo.\n\nVelit non culpa reprehenderit officia. Ullamco occaecat quis et nisi est reprehenderit excepteur ut enim dolor deserunt eu deserunt mollit. Pariatur nulla aliqua Lorem sunt veniam minim veniam excepteur. Laboris veniam Lorem irure dolor ut dolor ipsum esse incididunt. Aute ex minim ipsum aliqua quis proident. Occaecat aliquip do est irure reprehenderit adipisicing.\n\nCillum anim tempor non pariatur ad excepteur minim. Elit aliquip exercitation est magna quis anim nisi est exercitation. Nisi ex eiusmod cupidatat ullamco id. Anim cupidatat ut ut dolor proident anim aute fugiat excepteur cupidatat. Qui sit aliquip ullamco reprehenderit labore sit ad. Lorem reprehenderit aliquip labore adipisicing sit non magna amet in. Ad ipsum cillum ullamco aliqua labore amet irure proident sit consequat veniam duis cupidatat.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(167, 'days'),
        'read': false,
        'starred': false,
        'labels': [],
        'group': 'social',
        'type': 'none',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg', 'assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Fernandez Wilcox',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'ut aliqua reprehenderit',
        'content': 'Dolor voluptate do sunt irure cupidatat commodo occaecat. Deserunt consectetur Lorem amet qui ullamco mollit dolore qui in est. Eiusmod nostrud Lorem ex laboris deserunt esse culpa elit voluptate ad irure est culpa veniam. Ut ut sunt dolor exercitation. Enim incididunt sint proident incididunt duis aute magna eu duis dolor pariatur cupidatat velit magna.\n\nAmet in aute minim non nulla deserunt fugiat sint ex officia aliqua laboris. Eu deserunt deserunt ullamco officia culpa incididunt laboris irure laboris nisi nulla cupidatat. Laboris quis culpa aliquip aliquip duis occaecat duis excepteur proident nulla.\n\nAmet mollit magna sunt do enim consectetur. Labore fugiat Lorem reprehenderit veniam. Officia ea qui exercitation voluptate id occaecat cillum sunt in ipsum elit. Qui ullamco nisi tempor nostrud cupidatat. Ex occaecat laborum amet officia proident laborum culpa voluptate. Magna veniam ea velit pariatur est.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(22, 'hours'),
        'read': false,
        'starred': true,
        'labels': [],
        'group': 'promotions',
        'type': 'sent',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg']
    },
    {
        'from': {
            'name': 'Little Baird',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'eiusmod enim ad',
        'content': 'Ut labore anim cillum nostrud. Aute do amet dolore aliqua ullamco proident cupidatat deserunt dolor pariatur nulla esse anim do. Incididunt tempor amet ea commodo proident esse nostrud elit excepteur sint laborum aute sunt in. Duis mollit nisi ex irure non eiusmod ea esse. Nostrud commodo qui ea consectetur.\n\nId velit in exercitation ipsum quis. Proident veniam cupidatat sunt exercitation occaecat anim. Ex esse enim velit est duis. Enim magna Lorem laborum nostrud tempor laboris laboris. Sit reprehenderit irure nulla est anim voluptate anim eiusmod.\n\nVelit incididunt exercitation irure aliquip magna in fugiat proident. Consectetur ullamco pariatur minim commodo officia aliquip tempor. Elit id excepteur elit do irure do ad aliqua. Aliqua aliqua proident nisi sunt fugiat tempor. Laborum exercitation tempor sint commodo quis eu in commodo tempor id tempor laboris deserunt. Esse amet mollit anim culpa fugiat ea eu ad magna quis reprehenderit mollit laboris. Nulla labore cillum aliquip anim velit aute veniam cillum ad irure pariatur aliqua reprehenderit.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(89, 'minutes'),
        'read': true,
        'starred': false,
        'labels': [],
        'group': 'promotions',
        'type': 'draft',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg']
    },
    {
        'from': {
            'name': 'Dorsey Mayo',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'velit magna reprehenderit',
        'content': 'Cillum non ipsum ex voluptate velit adipisicing elit voluptate excepteur Lorem deserunt est nisi nostrud. Mollit sit veniam aliqua exercitation incididunt in ullamco mollit nisi ea qui. Laboris minim do proident voluptate pariatur eu ad enim laborum qui officia cupidatat. Sint id ad irure voluptate sint voluptate esse laborum tempor labore. Nulla nisi magna laboris sunt amet culpa anim.\n\nTempor do in reprehenderit dolore esse. Enim et id incididunt eu ex nulla quis nisi nulla dolore. Velit tempor voluptate proident culpa aliqua nisi id dolor occaecat esse ad reprehenderit non. Et mollit id nisi veniam veniam est sit aute excepteur do et. Voluptate do eu reprehenderit commodo commodo non mollit quis cillum. In ea qui adipisicing esse quis ipsum voluptate cupidatat aliqua consectetur nulla sunt laboris consequat.\n\nLabore in culpa enim elit cillum Lorem labore quis do veniam est mollit officia mollit. Nostrud reprehenderit consequat ullamco quis deserunt. Sit consequat aliquip ad ad proident quis occaecat nisi nisi id. Qui deserunt id enim eiusmod consectetur magna et cillum. Duis enim commodo anim qui aute consequat consequat elit laboris Lorem enim adipisicing sit. Dolor duis ipsum ut aliquip dolore proident. Reprehenderit ex sunt nisi culpa nisi cupidatat enim nostrud amet aliqua in amet.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(83, 'hours'),
        'read': false,
        'starred': false,
        'labels': [],
        'group': 'promotions',
        'type': 'spam',
        'attachments': ['assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Castaneda Hunt',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'aute nostrud deserunt',
        'content': 'Adipisicing irure ad aliqua est irure. Labore duis proident aute laboris do enim. Elit eiusmod ea ad minim irure reprehenderit dolore duis ipsum.\n\nDo qui non eu labore culpa ipsum excepteur anim sunt aute ut magna nisi commodo. Est reprehenderit non amet culpa Lorem elit dolore elit pariatur adipisicing voluptate ut. Est velit excepteur commodo irure ad ad laboris in consectetur ad sint eu incididunt. Nisi non dolore id enim ad laboris reprehenderit id irure. Tempor deserunt est nostrud do exercitation culpa quis ad id officia adipisicing commodo elit. Id sunt Lorem et magna quis.\n\nEa ipsum labore ea reprehenderit quis nostrud est labore ut in amet id. Aute esse cillum eu proident consectetur elit fugiat sunt velit velit in. Reprehenderit ea nisi nisi consequat non. Duis exercitation anim quis sunt sint elit magna voluptate et cillum deserunt. Est eu reprehenderit pariatur dolore exercitation proident eu non minim adipisicing cupidatat. Excepteur consequat irure sunt dolor laborum esse nulla culpa minim eu cupidatat consectetur aute culpa. Reprehenderit aliquip elit ex occaecat eu mollit non culpa ut.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(180, 'minutes'),
        'read': true,
        'starred': false,
        'labels': [],
        'group': 'promotions',
        'type': 'spam',
        'attachments': ['assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Helen Roberson',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'incididunt irure magna',
        'content': 'Cillum cillum sint reprehenderit mollit dolor cillum ullamco laborum ullamco est duis laborum culpa. Est aute non irure esse officia. Velit culpa cillum nulla sunt adipisicing consequat sit labore est elit aute laboris irure quis. Est cupidatat voluptate ullamco cillum minim ex consectetur anim dolore exercitation. Laborum deserunt labore est nostrud. Consequat deserunt magna esse irure sit eiusmod esse veniam consectetur ad tempor voluptate. Fugiat laboris quis enim labore incididunt minim et fugiat dolor sit magna laborum sit dolor.\n\nAliqua eu veniam velit elit nisi adipisicing proident nulla enim ipsum tempor aliqua. Incididunt ullamco eu aute officia. Magna eu cupidatat aute exercitation labore do.\n\nOfficia aliquip consectetur do ut sit. Nulla cillum amet mollit commodo et ea non anim voluptate duis dolore sunt. Laborum labore cupidatat sint mollit consequat duis ad tempor velit consequat duis nisi id.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(117, 'hours'),
        'read': false,
        'starred': true,
        'labels': [],
        'group': 'promotions',
        'type': 'spam',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg']
    },
    {
        'from': {
            'name': 'Oliver Short',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'laboris do sunt',
        'content': 'Officia deserunt Lorem id ex deserunt fugiat ex consequat laborum cupidatat ullamco. Ut excepteur quis quis velit est. Dolor veniam Lorem pariatur adipisicing. Ad consectetur commodo veniam fugiat excepteur qui elit tempor ipsum laborum. Elit nisi tempor exercitation anim esse.\n\nCommodo do anim fugiat magna. Est non eu occaecat magna eiusmod eu dolor eiusmod sint nulla ullamco incididunt pariatur. Anim ea eiusmod dolor Lorem elit commodo esse do fugiat amet esse cillum eu. Ut velit minim mollit do cupidatat dolore nisi reprehenderit est commodo anim mollit in pariatur. Esse commodo laboris amet reprehenderit.\n\nOccaecat ea commodo ullamco consectetur sunt eu officia duis irure. Nostrud laboris voluptate ex labore do dolore non. Aliquip veniam nisi reprehenderit exercitation ut reprehenderit.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(193, 'hours'),
        'read': true,
        'starred': true,
        'labels': [],
        'group': 'promotions',
        'type': 'spam',
        'attachments': ['assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Hendricks Whitley',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'Lorem ut sit',
        'content': 'Laborum exercitation esse ad elit pariatur ullamco dolor nisi. Magna non ea labore mollit eu fugiat. Qui occaecat nostrud quis veniam ipsum proident ea proident. Officia esse excepteur qui non sit dolore mollit aliqua deserunt officia. Occaecat sit enim fugiat veniam sunt. Quis sit duis do cupidatat cillum officia irure exercitation deserunt.\n\nDeserunt nostrud anim sit aute eiusmod do consequat ipsum proident ex. Do nisi do elit consequat. Mollit aute duis ut quis amet ea excepteur laboris ea deserunt. Cillum fugiat sunt incididunt velit pariatur cillum quis reprehenderit ut ex sunt reprehenderit.\n\nVelit ipsum qui labore ullamco sit do sit cupidatat ea in. Incididunt commodo commodo Lorem qui eiusmod incididunt incididunt irure veniam adipisicing eiusmod adipisicing. Cillum dolore in enim irure enim duis nostrud sit ad qui Lorem. Excepteur amet est dolor minim aliqua commodo consequat labore ea dolore sint commodo magna.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(194, 'days'),
        'read': false,
        'starred': false,
        'labels': [],
        'group': 'promotions',
        'type': 'sent',
        'attachments': ['assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Herring Palmer',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'dolor do consectetur',
        'content': 'Commodo tempor nisi cupidatat sit laborum duis in sunt et sint. Minim eiusmod incididunt magna anim officia. Commodo officia aliquip incididunt aliquip elit ullamco tempor et ex minim veniam veniam nisi. Dolor sunt eiusmod ipsum laborum Lorem esse eu ullamco amet deserunt non pariatur.\n\nEiusmod et laborum sit irure amet ad occaecat est occaecat voluptate sunt reprehenderit. Eiusmod et labore ut sit aute ut adipisicing. Ea cillum id cupidatat velit non velit. Commodo do esse est adipisicing consequat occaecat cupidatat culpa. Ut ut consectetur velit nulla fugiat ut ex quis nostrud pariatur. Ea ullamco sit aliquip exercitation consequat voluptate laborum enim excepteur ad aute deserunt consectetur.\n\nProident ad tempor reprehenderit eu. Laborum ipsum non eu sit sunt cupidatat quis. Nisi excepteur deserunt veniam quis cillum voluptate deserunt culpa elit aliquip esse et incididunt. Fugiat aliquip est dolore nisi ullamco exercitation anim veniam. Esse aliquip esse dolore esse sunt sint aute aliquip et elit irure. Minim id eiusmod reprehenderit proident qui in non aliqua do adipisicing.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(181, 'days'),
        'read': false,
        'starred': false,
        'labels': [],
        'group': 'social',
        'type': 'draft',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg']
    },
    {
        'from': {
            'name': 'Candy Kline',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'reprehenderit et enim',
        'content': 'Lorem ea consequat non nisi do do deserunt excepteur do ex. Est laboris nulla officia laborum incididunt pariatur exercitation incididunt. Incididunt minim laboris ex amet est in in. Aliquip aliquip commodo ex dolor quis. Ex commodo magna nostrud tempor occaecat fugiat excepteur et pariatur dolor ut. Officia nulla et eu ipsum proident labore cupidatat laboris. Commodo aliquip esse elit velit cupidatat veniam anim quis nulla officia esse velit proident.\n\nCulpa id non et in commodo et eiusmod do labore. Labore proident id sint et esse mollit officia nisi voluptate occaecat pariatur amet do magna. Ea exercitation irure est magna excepteur consectetur voluptate est quis qui. Aliqua proident duis officia occaecat dolor id culpa ex qui velit ullamco. Nostrud eu duis anim reprehenderit consequat laboris mollit reprehenderit eu esse.\n\nDo nulla magna aute culpa enim qui consequat elit eu enim duis non cupidatat. Aute non ipsum officia consectetur. Sint id quis do irure adipisicing incididunt ea tempor aliqua deserunt consectetur quis fugiat. Officia est eiusmod cillum nostrud reprehenderit occaecat commodo non dolore irure. Eiusmod ut sit labore ea anim cupidatat nostrud ex laborum commodo. Aute reprehenderit reprehenderit ut proident adipisicing eiusmod dolor in veniam.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(148, 'minutes'),
        'read': true,
        'starred': false,
        'labels': [],
        'group': 'social',
        'type': 'spam',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg', 'assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Marjorie Rivas',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'aliqua id veniam',
        'content': 'Officia laborum exercitation nostrud aliquip minim est duis velit veniam eiusmod reprehenderit. Dolor aliqua commodo enim id dolore est ut proident sit dolor ut amet. Ad cillum non consequat non anim. Esse magna in aute culpa et proident culpa reprehenderit ullamco commodo ut excepteur velit. Officia laboris nulla do consequat et culpa aliqua nulla dolor ex. Lorem exercitation dolor qui labore in.\n\nLaborum id Lorem adipisicing pariatur. Ea commodo eu nisi ullamco cupidatat cillum incididunt aute ad incididunt id tempor minim. Ad quis culpa elit et ex in laboris dolore culpa culpa fugiat consequat anim.\n\nEiusmod nostrud duis nostrud do consectetur nostrud do non ullamco nulla ad pariatur. Magna adipisicing culpa laborum labore esse laboris in officia. Labore fugiat Lorem consequat incididunt aute ea aliqua eu pariatur cupidatat.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(112, 'days'),
        'read': true,
        'starred': true,
        'labels': [
            {
                'name': 'Business',
                'color': '#3E50B4'
            },
            {
                'name': 'Priority',
                'color': '#4CA9BB'
            }
        ],
        'group': 'promotions',
        'type': 'none',
        'attachments': ['assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Maldonado Bentley',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'irure velit ex',
        'content': 'Proident mollit elit adipisicing voluptate ex eiusmod Lorem aute ipsum incididunt quis qui reprehenderit. Commodo dolore commodo excepteur ea adipisicing cupidatat eiusmod consequat. Enim magna ea sit nulla ipsum fugiat aliqua sit exercitation. Ullamco adipisicing cillum tempor ea cillum. Anim ea do sint id deserunt do consectetur eu enim. Reprehenderit ullamco aliqua veniam sunt aute consequat enim id.\n\nIrure deserunt anim enim laboris qui nulla cillum. Ut ex aliqua non voluptate tempor ipsum pariatur cupidatat est enim sit tempor. Mollit irure veniam anim qui ipsum magna. Consequat in sint ea fugiat id est adipisicing culpa minim labore minim sint labore fugiat.\n\nId aliqua cillum aliqua irure Lorem occaecat consectetur. Consequat duis in ut dolor consectetur aliqua nostrud consectetur officia non aliquip anim. Do nisi magna dolore officia duis consequat reprehenderit sunt minim ea tempor. Reprehenderit minim et anim labore nulla excepteur deserunt minim enim tempor sunt laboris incididunt mollit.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(53, 'hours'),
        'read': true,
        'starred': false,
        'labels': [
            {
                'name': 'Outreach',
                'color': '#3E50B4'
            }
        ],
        'group': 'promotions',
        'type': 'sent',
        'attachments': ['assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Ballard Oneill',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'proident et esse',
        'content': 'Commodo velit quis ex elit anim dolor Lorem magna. Labore ad nulla laboris id exercitation. Nostrud est deserunt esse elit commodo nulla aliqua aute nostrud. Enim ullamco et eiusmod ea mollit Lorem.\n\nDo minim sint sint occaecat ex incididunt veniam. Magna dolore enim eiusmod est officia laborum occaecat ut. Qui duis cupidatat et magna incididunt nostrud esse ex mollit proident Lorem nulla ipsum. Velit adipisicing tempor magna ipsum exercitation commodo anim ut. Do voluptate qui ea esse Lorem dolore quis.\n\nIrure nisi mollit in ea velit irure aliqua. Enim ea consequat fugiat aliqua in aliqua Lorem nostrud adipisicing nulla et ex qui ullamco. Qui Lorem Lorem Lorem minim. In excepteur culpa commodo eu eu ad consectetur pariatur ullamco sunt esse ipsum consequat.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(139, 'days'),
        'read': false,
        'starred': false,
        'labels': [
            {
                'name': 'Private',
                'color': '#3E50B4'
            }
        ],
        'group': 'promotions',
        'type': 'none',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg']
    },
    {
        'from': {
            'name': 'Strickland Battle',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'id fugiat velit',
        'content': 'Commodo id non anim exercitation sint aliqua culpa esse. Nulla fugiat culpa sit Lorem quis veniam do laborum adipisicing veniam excepteur est velit reprehenderit. Aliqua et id eiusmod excepteur. Officia sunt eu ea velit cupidatat sit mollit incididunt aliqua elit adipisicing incididunt. Lorem consequat elit adipisicing ullamco in ad nostrud do elit enim.\n\nIrure laborum velit excepteur veniam do ex. Veniam aliquip est aute pariatur sint deserunt aliquip culpa sint aliqua. Sint consectetur laboris ea labore excepteur sit ad sit aute sunt elit duis ipsum. Non fugiat officia qui id excepteur ad nostrud sint laboris ipsum esse.\n\nVeniam consectetur ex ipsum magna Lorem sint ut consequat elit deserunt magna ex. Esse proident voluptate proident deserunt commodo proident ea Lorem qui velit do. Ex mollit quis pariatur culpa excepteur. Aliqua tempor ea quis commodo consectetur qui Lorem occaecat. Non nulla reprehenderit tempor irure culpa exercitation esse fugiat ad est aliqua.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(18, 'days'),
        'read': false,
        'starred': false,
        'labels': [],
        'group': 'social',
        'type': 'sent',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg']
    },
    {
        'from': {
            'name': 'Tanner Leach',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'nisi excepteur ex',
        'content': 'Elit nisi ut nulla et culpa velit ut quis quis eu qui. Proident voluptate dolor consectetur proident id culpa esse non. Ipsum pariatur ipsum aliqua ex consequat reprehenderit irure est esse. Sunt veniam velit deserunt exercitation Lorem ad et occaecat. Consequat fugiat fugiat nisi adipisicing Lorem laborum in do sunt aute Lorem labore et occaecat. Quis proident in ex tempor. Irure et culpa proident cupidatat do nisi ullamco adipisicing aliqua labore ipsum veniam duis et.\n\nTempor in commodo nostrud deserunt officia. Ipsum deserunt dolor anim do exercitation velit est Lorem nisi officia exercitation velit. Duis anim et mollit exercitation ad laboris ipsum commodo.\n\nLabore elit minim nulla aliquip officia incididunt anim non. Proident dolor velit fugiat in ex sint eu consequat proident anim ipsum veniam sint. Lorem incididunt eu dolore pariatur. Velit magna et occaecat ad eiusmod dolore mollit tempor aliqua laborum sit eu elit et. Sint elit consequat pariatur esse enim tempor esse nisi reprehenderit laboris esse sit eu aliqua.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(42, 'days'),
        'read': true,
        'starred': false,
        'labels': [],
        'group': 'promotions',
        'type': 'spam',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg']
    },
    {
        'from': {
            'name': 'Kimberly Weaver',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'exercitation ea pariatur',
        'content': 'Do aliquip est consequat id adipisicing dolor cillum exercitation proident fugiat occaecat. Ex mollit aliquip dolor sit qui cillum non. Commodo officia Lorem excepteur aliqua cillum aliqua nisi dolore deserunt laboris officia.\n\nEu elit enim excepteur aliqua occaecat. Pariatur consequat nostrud in reprehenderit mollit cupidatat anim elit amet. Eu proident cupidatat ut labore. Eiusmod reprehenderit voluptate quis eiusmod nostrud voluptate quis et incididunt amet. Dolor sit eiusmod Lorem officia ad ad ad est duis dolor nulla. Laboris eiusmod cupidatat commodo pariatur esse enim. Consectetur culpa amet minim labore.\n\nLaboris enim adipisicing tempor reprehenderit tempor et laboris sit pariatur velit sint quis non. Quis veniam fugiat cillum sit est nisi ex incididunt exercitation labore esse. Sint ea tempor minim nulla eiusmod nisi tempor voluptate adipisicing. Minim dolor commodo consequat incididunt consequat adipisicing cupidatat ullamco tempor pariatur dolore do.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(165, 'days'),
        'read': false,
        'starred': true,
        'labels': [],
        'group': 'promotions',
        'type': 'sent',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg', 'assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Addie Barber',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'eiusmod elit sunt',
        'content': 'Labore veniam nisi non cupidatat nostrud enim ullamco esse culpa nostrud laboris labore voluptate duis. Ex deserunt proident Lorem reprehenderit ut. Tempor irure anim ad sit dolore in cillum ut proident et. Eiusmod officia irure deserunt occaecat anim ullamco ex enim adipisicing tempor occaecat ut reprehenderit. Fugiat labore voluptate adipisicing elit nisi voluptate proident qui sunt. Enim culpa aliqua reprehenderit qui.\n\nMinim tempor minim ex duis aliqua id enim do aute laboris nisi dolore dolor quis. Ea nisi amet ea pariatur culpa non ullamco do cupidatat aliqua qui commodo nulla. Fugiat excepteur mollit adipisicing eu aliqua veniam in non ullamco laborum. Elit nulla elit adipisicing elit aliqua dolore non consectetur. Ut ullamco consectetur consequat eiusmod consequat eu tempor aliquip ex veniam pariatur in. Ullamco tempor ullamco nisi ad reprehenderit. Culpa eiusmod exercitation in labore incididunt esse.\n\nVeniam qui fugiat anim aute ullamco cillum laborum do amet esse occaecat eu ea labore. Et nisi laboris et in ex nulla consectetur Lorem cillum consequat minim nisi reprehenderit elit. Incididunt excepteur eu tempor enim excepteur in minim.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(100, 'days'),
        'read': false,
        'starred': false,
        'labels': [],
        'group': 'promotions',
        'type': 'spam',
        'attachments': ['assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Rena Hanson',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'deserunt aliqua dolore',
        'content': 'Aliquip esse ullamco culpa ullamco irure deserunt velit non enim id ipsum. Amet laborum ipsum enim nostrud est ad. Fugiat cupidatat deserunt enim anim do quis dolore ad duis laboris consequat eiusmod aliquip ipsum. Et nostrud deserunt et ex. Cillum voluptate laborum proident sit deserunt consectetur labore.\n\nConsectetur adipisicing officia ea ipsum dolor qui Lorem veniam ullamco incididunt dolore do pariatur dolore. Voluptate aliquip esse quis labore esse sit quis ullamco irure veniam consequat sit Lorem. Adipisicing consectetur enim nulla excepteur. Incididunt proident ullamco laborum laboris mollit pariatur anim quis est. Ex sunt aliquip esse reprehenderit et nulla cillum duis qui cillum velit laboris ea.\n\nDuis consequat reprehenderit in aute dolor enim. Est et do cupidatat commodo dolor excepteur anim aliqua sunt. Velit reprehenderit consectetur magna excepteur et enim duis elit reprehenderit eu fugiat proident qui nulla. Duis laboris deserunt magna culpa duis eiusmod Lorem voluptate ea. Magna tempor adipisicing labore in id magna proident ad excepteur voluptate esse eu. Eu sunt cillum amet ut laborum quis. Tempor cupidatat aliquip elit sint et incididunt occaecat.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(47, 'days'),
        'read': true,
        'starred': false,
        'labels': [],
        'group': 'social',
        'type': 'sent',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg', 'assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Kim Garrison',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'adipisicing quis ex',
        'content': 'Esse cillum voluptate mollit deserunt aute. Cupidatat duis aliqua eu dolor nulla ad anim aliquip. Labore ex proident consectetur ad nostrud laborum sint aute anim voluptate amet veniam ex. Lorem consectetur tempor nulla reprehenderit. Elit reprehenderit sit sit laboris.\n\nNisi mollit ex consectetur nulla enim est. Sit fugiat mollit elit est deserunt. Voluptate sint ea ad aliquip ex exercitation mollit nulla et sunt dolor consequat aute aliqua. Labore culpa ipsum ullamco aute dolore adipisicing adipisicing cillum ut excepteur est dolor excepteur.\n\nExercitation consequat enim quis tempor fugiat esse sit et pariatur sunt adipisicing ad cillum incididunt. Ea est dolore in consectetur mollit consequat proident commodo. Mollit velit Lorem veniam qui nulla est labore anim ipsum nulla laboris excepteur labore amet. Fugiat nostrud ipsum aliqua excepteur nostrud adipisicing elit ad aute est velit incididunt commodo proident. Quis laborum labore nisi Lorem ipsum anim quis laboris ut velit esse consectetur aute dolore. Ad amet ea consectetur cillum.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(73, 'minutes'),
        'read': false,
        'starred': false,
        'labels': [],
        'group': 'social',
        'type': 'draft',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg', 'assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Vanessa Ramos',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'laborum do aute',
        'content': 'Ullamco et excepteur aliqua excepteur. Adipisicing id aliquip excepteur ipsum nisi mollit tempor ut Lorem. Non Lorem veniam incididunt anim mollit nostrud do fugiat sit anim est amet. Pariatur culpa quis sit irure nisi aute sit labore consequat commodo aliqua deserunt consectetur ipsum. Dolore est ea fugiat voluptate aliquip Lorem officia ad cupidatat ipsum laborum officia labore magna.\n\nMollit mollit est in eiusmod id amet occaecat eu culpa eu laboris non ea. Ad sunt mollit aute sunt elit ea. Exercitation fugiat occaecat elit ut ex deserunt id irure officia ut eu. Qui Lorem reprehenderit nulla labore officia tempor occaecat ut sunt ad officia aliqua consectetur enim. Fugiat elit laborum non occaecat reprehenderit ex ex pariatur non. Do ipsum velit proident eiusmod esse velit ipsum aute occaecat consequat excepteur irure reprehenderit.\n\nNulla Lorem occaecat pariatur magna quis. Duis tempor sit sunt quis dolore. Do eiusmod magna cillum irure culpa sit tempor laborum ipsum do. Eu laboris duis ea nisi minim ex eu veniam. Amet dolor veniam aliqua amet proident non commodo voluptate magna in anim laboris ipsum laborum. Duis minim qui Lorem laborum enim pariatur. Eu laboris ullamco minim culpa labore deserunt laboris cupidatat sit excepteur fugiat non nisi.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(6, 'days'),
        'read': true,
        'starred': false,
        'labels': [],
        'group': 'promotions',
        'type': 'trash',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg']
    },
    {
        'from': {
            'name': 'Fry Berg',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'et incididunt incididunt',
        'content': 'Cupidatat ullamco sint cillum cupidatat ea id. Lorem proident aliquip labore quis consectetur sint qui eiusmod esse est. Laborum qui laborum ea eu eiusmod magna labore consectetur duis amet. Sit voluptate esse nulla labore adipisicing voluptate. Velit qui qui adipisicing qui consequat culpa voluptate eiusmod excepteur eiusmod cillum. Cillum cupidatat laborum irure et irure eiusmod deserunt fugiat elit consequat dolore voluptate. Excepteur eu aliqua irure velit laboris Lorem reprehenderit qui mollit nostrud.\n\nDo aliqua proident aliqua officia eu veniam Lorem nulla amet non magna nostrud. Aute incididunt labore magna ex ipsum elit in in ad ex velit cupidatat incididunt pariatur. Ad incididunt deserunt occaecat reprehenderit sint aliquip cillum ea deserunt irure. Est fugiat minim velit laborum ipsum sunt velit ex minim eu cupidatat exercitation ea. Ex ea excepteur consequat deserunt enim.\n\nCulpa anim magna et consequat non. Reprehenderit aliquip et do nulla dolore ut consectetur reprehenderit cillum ut sit cupidatat est. Enim eu Lorem quis elit sunt cupidatat labore culpa nisi tempor commodo. Consectetur adipisicing dolor mollit ad tempor irure. Aute qui id voluptate non tempor Lorem nostrud sit minim sint cillum cillum est Lorem.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(165, 'hours'),
        'read': true,
        'starred': true,
        'labels': [],
        'group': 'social',
        'type': 'none',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg']
    },
    {
        'from': {
            'name': 'Daisy Gallagher',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'quis sunt magna',
        'content': 'Et cupidatat elit reprehenderit ex laborum enim proident aliqua aliquip est. Reprehenderit dolor ad veniam laborum. Ipsum nostrud elit duis consequat incididunt. Tempor nisi excepteur mollit incididunt ea ipsum amet sunt. Non velit proident consequat duis enim commodo ipsum esse minim. Ea aute pariatur do qui sit non magna consequat do. Duis laboris aliquip labore ea adipisicing cupidatat quis dolore elit eiusmod qui.\n\nOccaecat ut commodo consequat nisi nisi culpa. Pariatur consectetur duis proident laboris dolor mollit esse ea culpa deserunt. In sint nostrud sunt do magna elit. Cupidatat excepteur labore est occaecat dolor velit cillum duis ad. Velit irure sit enim eiusmod ad aute exercitation laborum magna. Sit anim ex esse cupidatat consequat est.\n\nEnim eiusmod dolor nostrud elit aliquip officia dolore cillum pariatur sint sunt amet elit aliqua. Officia sint culpa irure consequat elit est irure officia reprehenderit adipisicing est amet cupidatat. Magna mollit officia voluptate commodo duis et quis nostrud. Lorem pariatur ullamco elit aute voluptate do qui incididunt excepteur mollit. Lorem ut amet laborum magna pariatur proident cupidatat nulla. Elit qui irure tempor ad veniam nulla occaecat sunt id elit irure exercitation. Consequat qui tempor sit laboris dolor.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(66, 'hours'),
        'read': true,
        'starred': true,
        'labels': [],
        'group': 'social',
        'type': 'none',
        'attachments': ['assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Schwartz Patterson',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'et est est',
        'content': 'Ipsum reprehenderit esse ex tempor reprehenderit. Deserunt adipisicing veniam Lorem laboris tempor nulla tempor. Nulla mollit nulla enim culpa eu laborum et aute. Amet sit esse excepteur voluptate pariatur labore dolore nostrud occaecat laboris exercitation duis fugiat.\n\nAliquip labore voluptate ullamco in irure nisi anim dolor adipisicing ut nulla proident labore est. Pariatur sunt amet aliquip irure aliquip esse do nostrud anim esse cupidatat fugiat eiusmod. Quis et aliquip excepteur nulla ut in irure.\n\nExcepteur irure dolore consectetur nulla ad aliquip in irure deserunt commodo laboris ea exercitation. In ad incididunt irure laboris voluptate cupidatat sunt. Duis duis mollit incididunt irure eu sunt esse. Id ullamco enim labore reprehenderit laboris sunt exercitation quis excepteur dolore. Est ex dolore est ullamco laboris cillum qui cillum ad consequat ad nulla reprehenderit esse. Lorem voluptate incididunt magna in consequat. Laborum velit amet minim ullamco Lorem duis culpa pariatur.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(200, 'minutes'),
        'read': true,
        'starred': true,
        'labels': [],
        'group': 'promotions',
        'type': 'draft',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg']
    },
    {
        'from': {
            'name': 'Lavonne Sykes',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'nulla deserunt occaecat',
        'content': 'Velit duis commodo aliquip officia duis ad nulla aliqua officia enim dolore excepteur sit velit. Velit fugiat amet occaecat nulla nostrud ipsum. Labore adipisicing irure eu veniam. Ad nostrud incididunt adipisicing cillum veniam adipisicing enim qui aliquip ad incididunt deserunt excepteur quis. Amet quis laboris cillum ad tempor ullamco eiusmod proident ullamco dolor ipsum velit.\n\nLorem elit quis consectetur culpa ex. Eiusmod cupidatat sit ea sunt non incididunt pariatur do sunt Lorem dolore. Excepteur aliqua proident aliqua excepteur aliqua excepteur amet deserunt esse officia pariatur aliqua eiusmod minim. Nulla nostrud et dolore cillum laboris sit. Dolore non officia cupidatat amet magna officia culpa reprehenderit consequat amet deserunt do officia. Consequat id magna amet reprehenderit Lorem.\n\nEsse laborum irure consequat qui enim ipsum velit aute quis do nisi qui. Nostrud sit commodo do in sunt amet sit aliquip cillum deserunt cupidatat sint anim. In minim Lorem officia cupidatat. Mollit sit id consectetur Lorem esse culpa culpa deserunt elit duis sint sint pariatur. Quis sunt labore et occaecat esse nisi elit Lorem esse et. Minim qui esse eu do voluptate aute qui ex dolor magna.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(135, 'hours'),
        'read': false,
        'starred': true,
        'labels': [],
        'group': 'social',
        'type': 'none',
        'attachments': ['assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Hester Haney',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'excepteur cupidatat est',
        'content': 'Non consequat fugiat do quis eiusmod exercitation culpa amet fugiat. Tempor aute consectetur quis esse id non eu mollit. Ad laboris laboris consectetur eu duis anim culpa non id aliquip ullamco.\n\nExercitation in veniam non veniam reprehenderit dolor ut. Ullamco sint voluptate magna aliqua qui. Cupidatat commodo do quis amet reprehenderit ullamco elit nulla non velit. Consequat culpa voluptate cillum officia laboris reprehenderit nisi occaecat qui deserunt velit id fugiat.\n\nSint laborum veniam anim culpa labore cillum est eu ex excepteur laboris exercitation laborum. Deserunt ipsum ad aliqua nostrud aliquip quis sint labore culpa exercitation deserunt culpa. Pariatur eu excepteur id irure reprehenderit excepteur pariatur quis eu. Do culpa sit duis velit ipsum irure amet in dolor. Proident laborum excepteur qui non officia est voluptate nulla duis veniam culpa amet minim.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(54, 'days'),
        'read': false,
        'starred': false,
        'labels': [],
        'group': 'promotions',
        'type': 'none',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg', 'assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Sims Mcguire',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'ipsum minim do',
        'content': 'Aute tempor amet aute adipisicing. Cupidatat deserunt culpa laboris anim in eu in qui culpa ipsum. Dolore in cupidatat quis non ut ullamco cillum tempor. Anim pariatur consequat exercitation nisi nulla.\n\nNon laboris anim pariatur labore reprehenderit officia irure in ipsum. Ipsum non amet sunt mollit proident. Incididunt sit amet laboris mollit. Qui qui elit incididunt nostrud ea officia consequat eiusmod reprehenderit id dolor amet.\n\nAd culpa fugiat quis voluptate mollit consectetur occaecat occaecat id. Aute ad pariatur eiusmod veniam eu minim culpa commodo commodo ex cillum deserunt. Velit ex laboris culpa ad eiusmod anim aliqua Lorem amet commodo ipsum consectetur do duis. Qui in aute qui aliquip duis dolor veniam excepteur nostrud pariatur commodo eu incididunt. Consectetur nisi ipsum proident reprehenderit. Ipsum proident sint voluptate deserunt magna ad ipsum aliquip fugiat enim exercitation exercitation cupidatat. Irure aliquip culpa consectetur Lorem nulla magna nostrud laborum irure.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(70, 'hours'),
        'read': false,
        'starred': false,
        'labels': [
            {
                'name': 'Project X',
                'color': 'red'
            }
        ],
        'group': 'promotions',
        'type': 'draft',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg']
    },
    {
        'from': {
            'name': 'Evangelina Fleming',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'ad ut excepteur',
        'content': 'Occaecat aute ea non fugiat amet esse eu cillum commodo ea dolor et. Dolor proident nulla dolore adipisicing nisi. Esse adipisicing velit nulla velit sunt anim magna deserunt voluptate eu enim deserunt.\n\nFugiat consectetur exercitation non est. Magna exercitation nulla do commodo veniam anim. Esse incididunt tempor voluptate excepteur ea. Deserunt reprehenderit mollit adipisicing sit deserunt enim dolor officia labore magna consequat ea nostrud. Excepteur ex esse veniam deserunt sint eiusmod. Nisi amet sunt labore occaecat voluptate excepteur. Sit cupidatat est pariatur exercitation esse irure sit tempor deserunt non cupidatat velit consequat laboris.\n\nAnim labore duis ut sunt sunt ut enim consectetur exercitation ipsum et nisi. Tempor consequat tempor ullamco proident nostrud anim. Minim adipisicing irure cupidatat nisi dolor aliquip voluptate cillum do sit ea dolor nulla. Consequat fugiat magna consectetur magna. Ullamco esse mollit exercitation mollit proident do magna ullamco non duis exercitation culpa et. Ullamco eiusmod mollit reprehenderit eiusmod qui irure id irure velit veniam nulla fugiat commodo id. Nostrud ipsum aliqua esse ipsum consequat nisi pariatur reprehenderit culpa anim ipsum.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(185, 'minutes'),
        'read': false,
        'starred': false,
        'labels': [],
        'group': 'promotions',
        'type': 'trash',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg', 'assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Jenna Sanchez',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'tempor aute sint',
        'content': 'Ex nulla consequat velit commodo commodo enim ipsum magna cupidatat exercitation excepteur ullamco qui occaecat. Consectetur cupidatat est irure nisi mollit cillum culpa veniam nostrud. Non incididunt excepteur cupidatat irure exercitation non adipisicing sint voluptate nulla fugiat sunt. Sit id est velit ad. Officia enim voluptate amet dolor ad do Lorem exercitation non do. Veniam excepteur anim elit occaecat quis labore irure tempor ad adipisicing consequat consequat.\n\nOfficia id non aliquip ullamco ad pariatur eu ad commodo ut. Excepteur dolor consectetur tempor magna ex nostrud. Culpa in aute aliqua ad sit amet magna occaecat dolor Lorem qui laboris nostrud consequat. Aute laborum ut est cillum minim ad pariatur.\n\nOfficia ut culpa est occaecat do. Id Lorem nulla adipisicing voluptate deserunt est voluptate culpa nulla ullamco commodo. Nulla cillum aliqua ipsum sint duis sunt exercitation exercitation non cupidatat non. Ad pariatur esse eiusmod sunt voluptate sit nostrud nostrud laboris occaecat sunt officia. Occaecat veniam Lorem pariatur incididunt elit incididunt excepteur laboris. Adipisicing ex mollit qui aliqua velit quis quis Lorem proident officia sit dolor dolor.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(12, 'hours'),
        'read': true,
        'starred': false,
        'labels': [],
        'group': 'primary',
        'type': 'draft',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg', 'assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Montoya Davenport',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'commodo ad aliquip',
        'content': 'Sint veniam ipsum ea id nostrud non tempor ipsum ad cupidatat id. Amet minim qui proident proident eu labore qui culpa. Eu id dolore sit et laborum aliqua in velit. Voluptate commodo esse mollit Lorem nulla pariatur cupidatat pariatur adipisicing duis aliquip.\n\nFugiat excepteur ut proident tempor cillum aute non Lorem sunt consectetur pariatur non tempor. Irure Lorem irure tempor aliquip velit dolore mollit magna sunt sit in eu. Incididunt cillum pariatur consequat do esse irure cupidatat consectetur officia consequat velit duis nulla. Duis sunt consequat sit elit excepteur excepteur nulla eu excepteur. Nisi ipsum adipisicing ad ut magna. Magna pariatur pariatur commodo officia duis. Anim aute commodo nostrud eiusmod reprehenderit officia esse adipisicing.\n\nAmet ut cillum ex laborum. Laboris nulla velit ad labore enim tempor sint tempor excepteur id. Sint fugiat sunt irure qui dolor irure velit ad non culpa nulla eiusmod excepteur. Fugiat mollit dolor consequat officia ex enim ullamco officia reprehenderit dolore velit voluptate.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(74, 'days'),
        'read': false,
        'starred': true,
        'labels': [],
        'group': 'primary',
        'type': 'sent',
        'attachments': ['assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Lucia Talley',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'Lorem sint pariatur',
        'content': 'Ex ex aute laborum elit reprehenderit commodo anim fugiat ipsum deserunt. Ex consequat culpa Lorem cillum voluptate officia aliqua aliquip consectetur tempor incididunt. Voluptate ipsum ea do consectetur cupidatat laboris amet ea pariatur adipisicing nostrud culpa et nostrud. Incididunt est elit qui nisi excepteur esse esse pariatur officia esse veniam et minim. Reprehenderit enim nostrud eu consectetur cupidatat id laboris et velit. Incididunt est et nostrud qui.\n\nNon amet culpa culpa et irure est minim. Commodo aute duis sunt ut pariatur tempor consequat officia incididunt adipisicing. Lorem cupidatat ea mollit consequat aliquip consequat magna cupidatat.\n\nLorem esse dolore labore veniam pariatur minim occaecat. Lorem exercitation labore quis minim pariatur consectetur duis velit aliqua tempor consectetur sint. Quis fugiat officia ea laborum ipsum velit.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(192, 'minutes'),
        'read': true,
        'starred': true,
        'labels': [],
        'group': 'primary',
        'type': 'sent',
        'attachments': ['assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Staci Puckett',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'eiusmod commodo eu',
        'content': 'Duis excepteur magna mollit nostrud aliquip velit minim deserunt laboris enim. Anim sunt est dolor nulla nisi ipsum tempor. Mollit veniam qui non aliquip consequat proident. Aliquip laborum officia voluptate culpa eiusmod in veniam.\n\nMinim adipisicing excepteur ullamco proident velit enim nulla minim dolor dolore cillum laborum minim est. Ullamco nisi voluptate nisi ipsum. Consectetur sunt magna tempor aliqua voluptate veniam eu veniam non officia est reprehenderit. Aliquip duis aliquip occaecat do sunt incididunt tempor enim. Ut voluptate consectetur sit eu adipisicing ea consequat dolore voluptate ea. Ex in magna nostrud duis dolor sint Lorem exercitation dolore occaecat sit minim nisi Lorem. Cupidatat consectetur cillum voluptate tempor irure labore proident nostrud in aliqua ullamco cillum.\n\nPariatur ut ex est eu qui. Laborum id amet proident esse sit duis eiusmod et. Reprehenderit cillum adipisicing in ad nulla velit elit pariatur veniam ad ex reprehenderit. Quis cillum non dolor eu incididunt est sit excepteur voluptate.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(118, 'hours'),
        'read': false,
        'starred': false,
        'labels': [],
        'group': 'primary',
        'type': 'draft',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg', 'assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Della Morse',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'id adipisicing cupidatat',
        'content': 'Nulla quis dolor culpa proident qui mollit consequat consectetur ad voluptate et. Anim cillum consectetur cillum deserunt irure sint veniam reprehenderit excepteur. Incididunt aliquip aute ex Lorem commodo deserunt elit amet. Ex magna fugiat commodo incididunt sit et ipsum sit non qui. Amet reprehenderit cillum commodo ullamco do exercitation. Incididunt do ad anim occaecat dolor dolor tempor non deserunt. Amet est sint quis non voluptate et adipisicing velit irure eiusmod mollit.\n\nCommodo ex id eu id do incididunt laborum voluptate aute ex incididunt eiusmod eiusmod. Aliquip occaecat aute in aliqua est labore ut. Fugiat pariatur in irure pariatur deserunt aute incididunt ad.\n\nConsectetur voluptate reprehenderit nostrud ut deserunt exercitation consequat sunt. Pariatur aliquip velit non ut officia elit enim duis Lorem fugiat. Aliquip enim esse amet do do magna voluptate do. Aliquip anim id aute incididunt voluptate aliqua consectetur exercitation excepteur est. Elit anim esse enim non nostrud consequat magna pariatur labore id nostrud. Anim esse incididunt non incididunt sint.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(137, 'days'),
        'read': true,
        'starred': false,
        'labels': [],
        'group': 'promotions',
        'type': 'none',
        'attachments': ['assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Lillian Wilson',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'commodo sunt enim',
        'content': 'Mollit aliquip ad mollit consectetur incididunt excepteur. Aliquip commodo laboris ea commodo. Consequat aliquip ullamco proident commodo nulla et. In in et magna tempor laboris sit exercitation Lorem quis. Magna est esse tempor veniam ex nisi ad. Sunt sunt Lorem velit id ullamco culpa.\n\nSunt non quis enim qui sunt. Aliquip nulla sint incididunt tempor id fugiat minim officia occaecat. Adipisicing non officia nulla elit deserunt. Consectetur cillum deserunt qui cupidatat nostrud irure in eiusmod ullamco duis incididunt aliqua aute occaecat. Aliqua est est culpa ullamco mollit laborum irure quis enim incididunt veniam. Mollit officia fugiat labore est irure nulla nostrud quis ullamco Lorem dolor ad velit aliquip. Nisi do sint ut magna esse minim quis fugiat id nisi id dolor.\n\nQui magna eu sint exercitation labore cillum voluptate fugiat do amet magna labore. Commodo minim anim culpa ad mollit labore laborum cillum culpa excepteur mollit dolor anim et. Fugiat ullamco cupidatat cillum irure ullamco velit minim consectetur. Amet exercitation aliquip ullamco irure mollit aliquip nulla sint velit esse sunt deserunt consequat aute. Sit duis reprehenderit duis eu qui sit quis.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(93, 'days'),
        'read': false,
        'starred': false,
        'labels': [
            {
                'name': 'Project X',
                'color': '#4BAE4F'
            }
        ],
        'group': 'promotions',
        'type': 'draft',
        'attachments': ['assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Mercer Barnett',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'culpa incididunt aliquip',
        'content': 'Incididunt ut culpa nulla culpa quis sint ipsum aliqua occaecat nulla deserunt sunt. Quis sint minim id amet aliqua. Ut nulla id fugiat veniam excepteur quis minim voluptate reprehenderit.\n\nEt excepteur laborum in culpa cillum ut irure eu qui adipisicing sunt sint ipsum deserunt. Sint nulla magna amet esse consectetur ullamco non velit non ex minim irure. Do sit nisi occaecat consectetur ut voluptate anim elit tempor et et exercitation culpa deserunt. Reprehenderit ea voluptate sint consectetur ipsum adipisicing ad elit ut esse in sint sit.\n\nLaboris in amet quis irure laboris. Enim et esse in id proident deserunt laboris adipisicing id labore commodo minim sit. Ut esse deserunt deserunt sint officia ipsum consectetur.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(15, 'days'),
        'read': false,
        'starred': true,
        'labels': [
            {
                'name': 'Business',
                'color': 'red'
            }
        ],
        'group': 'primary',
        'type': 'spam',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg', 'assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Herrera Knowles',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'mollit aliqua in',
        'content': 'Nostrud enim ea ipsum non duis. Incididunt minim occaecat eu esse aliquip cillum quis magna. Incididunt ea cupidatat non occaecat excepteur non.\n\nConsectetur tempor ullamco officia id ea ad irure non anim sunt cillum. Nostrud tempor pariatur occaecat minim eu dolore. Voluptate cupidatat velit elit ut nulla dolore enim ipsum commodo ad officia reprehenderit labore. Laborum ad elit duis ullamco anim adipisicing. Reprehenderit enim aute cupidatat laboris est dolor Lorem in. Ad sit proident ea sit cupidatat pariatur. Eu esse nisi sunt ut velit nisi.\n\nDolore deserunt nulla eiusmod in. Consectetur quis proident eu Lorem duis excepteur dolor nisi sit velit fugiat sint. Laboris id dolore dolore irure occaecat minim qui amet enim sit nulla aliqua. Magna incididunt veniam consectetur laboris cillum laboris.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(74, 'days'),
        'read': false,
        'starred': true,
        'labels': [
            {
                'name': 'Project X',
                'color': '#4BAE4F'
            }
        ],
        'group': 'primary',
        'type': 'trash',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg', 'assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Ruiz Berry',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'veniam ipsum sint',
        'content': 'Reprehenderit deserunt incididunt aliquip velit pariatur adipisicing aliquip ipsum duis adipisicing. Officia ut anim tempor amet ex labore velit minim sunt pariatur eiusmod anim. Do enim aute qui ut nisi ut sint sit culpa magna eiusmod ex deserunt incididunt. Ea qui laboris ea mollit labore fugiat est. Adipisicing ea officia cupidatat ipsum. Elit officia magna ipsum non. Commodo magna ex minim tempor consectetur minim qui aliqua cillum culpa sunt adipisicing nostrud tempor.\n\nSit reprehenderit in excepteur sit deserunt sit id ea eu non ad voluptate. Consequat pariatur ullamco reprehenderit fugiat aute do irure ipsum aute. Aliquip sunt pariatur in elit ad veniam consectetur. Id eiusmod proident fugiat excepteur esse Lorem esse. In voluptate do aliqua mollit tempor ipsum et quis exercitation fugiat do.\n\nEsse pariatur voluptate exercitation duis fugiat nostrud pariatur dolor officia. Ullamco quis ex dolore id ex ad duis aute pariatur deserunt. Elit cillum est fugiat proident. Quis nostrud ullamco id in non ullamco mollit officia est est. Aliqua laboris nisi aliqua ut eiusmod occaecat ipsum pariatur cillum eiusmod occaecat. Eu officia ipsum minim officia culpa consectetur.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(21, 'hours'),
        'read': false,
        'starred': false,
        'labels': [
            {
                'name': 'Project X',
                'color': 'darkgrey'
            }
        ],
        'group': 'primary',
        'type': 'sent',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg', 'assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Lupe Pugh',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'minim dolor consectetur',
        'content': 'Ad proident incididunt veniam aute aliquip voluptate irure veniam id consequat eu esse. Tempor labore aute nulla ea do tempor nulla. Exercitation anim cillum nulla ut minim anim deserunt anim enim id irure aute. Labore sunt fugiat aliquip exercitation exercitation mollit est exercitation. Nostrud amet commodo reprehenderit magna laboris occaecat. Aliqua sunt reprehenderit duis deserunt duis consequat id exercitation non id voluptate consectetur amet.\n\nDolor aute aliquip cillum tempor velit pariatur ipsum sit cupidatat quis aliquip. Deserunt pariatur eiusmod magna occaecat mollit nulla dolor proident et deserunt laboris et do. Et elit esse sunt nulla eiusmod labore veniam est ex. Fugiat irure proident tempor aliqua laboris magna culpa ullamco ullamco cupidatat aute. Irure duis esse ut cillum cillum occaecat sunt id ut occaecat officia velit. Aliqua cillum amet magna officia velit magna mollit dolor.\n\nEu pariatur et non pariatur in pariatur laboris anim irure occaecat labore ex. Sint excepteur ad aliquip enim fugiat minim commodo dolore esse veniam. Anim nisi ipsum ullamco aute. Id mollit sunt ut exercitation qui aute magna eu labore cillum.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(150, 'hours'),
        'read': false,
        'starred': true,
        'labels': [
            {
                'name': 'Priority!',
                'color': 'red'
            }
        ],
        'group': 'primary',
        'type': 'draft',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg']
    },
    {
        'from': {
            'name': 'Becky Cote',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'magna magna in',
        'content': 'Lorem elit adipisicing pariatur pariatur exercitation sint excepteur anim. Aliquip consectetur ut mollit anim fugiat ut. Ullamco minim consequat ea velit laboris. Aute enim sit incididunt commodo dolore anim cupidatat labore ipsum.\n\nFugiat anim est sit irure adipisicing excepteur ullamco sit voluptate et occaecat irure. Et tempor commodo enim adipisicing nulla exercitation culpa consequat fugiat proident anim. Qui sint amet aute elit eu aute ullamco Lorem pariatur ad commodo. Culpa officia velit dolore fugiat adipisicing commodo id fugiat est magna. Ex tempor enim deserunt ut elit cupidatat pariatur reprehenderit exercitation officia enim duis aute incididunt.\n\nDuis culpa aliqua excepteur elit exercitation quis ad. Irure ullamco cillum minim do. Veniam deserunt pariatur est Lorem in. Est exercitation magna aliquip elit eiusmod duis velit amet enim. Velit irure excepteur laboris ut adipisicing tempor ut exercitation nostrud. Ea irure aute elit et.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(120, 'days'),
        'read': true,
        'starred': false,
        'labels': [
            {
                'name': 'Business',
                'color': 'darkgrey'
            }
        ],
        'group': 'promotions',
        'type': 'none',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg', 'assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Floyd Dyer',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'laborum culpa ut',
        'content': 'Nisi Lorem duis minim in. Ex Lorem labore nisi ad ex proident veniam consectetur. Quis aliqua id in eiusmod tempor voluptate tempor magna reprehenderit occaecat enim. Voluptate ut Lorem nulla fugiat est aliqua labore mollit nostrud. Consectetur exercitation elit irure officia adipisicing ea ipsum. Ullamco minim id excepteur eu excepteur excepteur dolor. Excepteur duis fugiat qui proident.\n\nDeserunt mollit occaecat nisi excepteur amet culpa occaecat eiusmod magna non. Irure nulla in deserunt veniam est nulla nulla eu amet do consequat eiusmod qui. Magna id sunt amet nisi esse labore officia ut aliqua qui proident non ea. Non aliqua consequat velit in proident excepteur sint aute minim proident minim laboris aliquip. Nostrud dolore incididunt ullamco eiusmod et.\n\nCommodo laborum non Lorem dolor sint adipisicing voluptate est labore aliquip cupidatat. Irure amet incididunt elit eiusmod tempor proident esse id laborum culpa. Ipsum dolore est ullamco fugiat ad anim commodo consectetur non. Fugiat est ea sit minim officia consectetur commodo culpa sunt.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(143, 'days'),
        'read': true,
        'starred': true,
        'labels': [
            {
                'name': 'Private',
                'color': 'red'
            }
        ],
        'group': 'primary',
        'type': 'sent',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg', 'assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Berta Lawson',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'dolor cupidatat adipisicing',
        'content': 'Eiusmod nisi occaecat proident do qui cupidatat pariatur. Amet mollit laboris irure sint anim aute duis sit exercitation dolore. Adipisicing ea anim aute enim duis exercitation dolor do aliquip est exercitation duis veniam ex. Incididunt proident cupidatat fugiat aliquip ad velit cillum sint excepteur do cupidatat ut. Duis qui non dolor excepteur veniam adipisicing esse excepteur veniam minim ex ad commodo in. Laboris tempor do exercitation cillum cillum voluptate consectetur incididunt sunt nisi incididunt. Magna et eiusmod cillum incididunt proident nisi est cupidatat occaecat ad velit ullamco.\n\nDo minim voluptate sit sit et sit mollit. Est eu non tempor ut excepteur sit veniam adipisicing velit est. Cillum esse non voluptate minim laborum sunt ex deserunt in adipisicing laboris sit duis. Reprehenderit laborum pariatur nostrud eiusmod dolore anim in ea anim. Exercitation aliqua deserunt qui fugiat commodo id ea culpa quis incididunt. Pariatur eu ut nisi in eu nostrud irure tempor.\n\nAdipisicing occaecat ipsum aute incididunt. Est fugiat nostrud duis nostrud reprehenderit ut. Exercitation do excepteur ex magna non et amet ut cupidatat voluptate incididunt minim. Laboris nisi aliquip ullamco irure ex proident consectetur do id do dolore sit aute dolor. Do deserunt incididunt consectetur non magna do anim duis. Eu voluptate eu occaecat eiusmod. Adipisicing nisi ex laborum minim in tempor qui qui enim et aliqua consequat.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(33, 'days'),
        'read': false,
        'starred': true,
        'labels': [
            {
                'name': 'Business',
                'color': 'red'
            }
        ],
        'group': 'primary',
        'type': 'sent',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg']
    },
    {
        'from': {
            'name': 'Johanna Rice',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'eu eiusmod irure',
        'content': 'In ad sit consequat cupidatat do enim tempor ut officia laboris duis Lorem nulla aute. Amet ipsum veniam pariatur enim laborum anim ex non pariatur cillum consectetur tempor voluptate. Ullamco cupidatat nisi anim elit labore et nulla eu ad irure in officia culpa. Eiusmod eu proident amet nostrud et elit consequat amet voluptate Lorem. Occaecat culpa laboris in ullamco amet ex consectetur proident culpa eiusmod.\n\nDo sunt elit laborum ea mollit. Nulla veniam sit aliquip quis elit ullamco. Ut voluptate reprehenderit duis eu in ullamco deserunt dolore anim dolore excepteur incididunt. Officia ad commodo veniam mollit in duis mollit.\n\nOfficia exercitation pariatur labore voluptate reprehenderit nostrud cupidatat nostrud. Nisi adipisicing officia minim sunt eu nostrud cillum. Laborum id eiusmod ipsum deserunt aute nisi cillum dolore.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(133, 'hours'),
        'read': false,
        'starred': true,
        'labels': [
            {
                'name': 'Business',
                'color': '#3E50B4'
            }
        ],
        'group': 'primary',
        'type': 'trash',
        'attachments': ['assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Schneider Bean',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'voluptate pariatur nulla',
        'content': 'Ad mollit est eiusmod incididunt incididunt enim ad ullamco enim labore est sint. Anim laboris sunt sit adipisicing cillum id deserunt do consequat aliqua proident. Deserunt sint reprehenderit ut incididunt et mollit velit incididunt dolore et ex tempor irure. Excepteur commodo ea officia exercitation occaecat deserunt commodo laborum aliqua. Non eiusmod excepteur enim aliquip laborum elit aute culpa. Occaecat labore ea ex irure officia reprehenderit laboris amet pariatur laboris veniam eu ex elit. Proident ad sint velit nisi sint.\n\nMinim nostrud nulla fugiat est ullamco ipsum ut occaecat dolore magna ad ut exercitation. Fugiat et cupidatat duis consequat mollit minim mollit anim quis amet. Mollit do minim excepteur non veniam ullamco esse. Occaecat ad nostrud et consequat occaecat mollit sit consectetur velit nostrud elit cupidatat. Reprehenderit cillum irure et ipsum esse amet exercitation nisi nostrud esse elit sint. Sunt occaecat amet consequat proident id Lorem ut elit anim nostrud sit labore duis amet. Duis id elit dolor laboris pariatur proident pariatur.\n\nExcepteur nisi excepteur aliqua cillum dolor duis mollit mollit Lorem veniam labore culpa ullamco. Fugiat occaecat velit ipsum est ad ad. In sit cupidatat sunt minim.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(68, 'minutes'),
        'read': false,
        'starred': false,
        'labels': [
            {
                'name': 'Business',
                'color': '#3E50B4'
            }
        ],
        'group': 'social',
        'type': 'none',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg']
    },
    {
        'from': {
            'name': 'Brooks Warner',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'in deserunt veniam',
        'content': 'Quis reprehenderit duis aute sit anim duis qui sint. Et ipsum dolore minim excepteur veniam proident. Quis officia aute ipsum id deserunt officia sunt pariatur est. Dolore incididunt est non exercitation in deserunt quis elit magna commodo eiusmod commodo. Aliqua duis enim deserunt incididunt. Est consequat ipsum exercitation fugiat Lorem irure ad nostrud minim reprehenderit velit voluptate.\n\nLorem consequat aliqua culpa sint ea nostrud consectetur aute. Nostrud anim adipisicing dolor amet laborum occaecat. Ex pariatur minim esse est in deserunt. Est velit enim cupidatat ad deserunt nostrud. Eu proident sunt culpa sit dolor minim occaecat Lorem do.\n\nCupidatat eu fugiat nostrud laboris amet deserunt ut sint nulla eiusmod. Exercitation elit Lorem elit irure nostrud deserunt tempor aliqua. Consequat labore nostrud deserunt commodo elit id ad.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(141, 'minutes'),
        'read': false,
        'starred': false,
        'labels': [
            {
                'name': 'Project X',
                'color': 'darkgrey'
            }
        ],
        'group': 'social',
        'type': 'trash',
        'attachments': ['assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Madeline Hawkins',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'eu pariatur amet',
        'content': 'Id proident sit laboris veniam mollit non exercitation qui enim occaecat aliquip aliquip. Nulla in aliquip in amet adipisicing culpa ut ad non. Commodo ex sint magna adipisicing id irure elit do. Nisi culpa nostrud consectetur do eu. Quis irure incididunt incididunt reprehenderit aliqua laborum sint sunt aliqua est elit tempor.\n\nVelit aliqua tempor ullamco nostrud proident sit labore proident eiusmod. Veniam nulla consectetur enim est et non mollit voluptate officia adipisicing exercitation et ullamco in. Labore occaecat est pariatur incididunt aute reprehenderit Lorem. Enim consectetur eu aliquip proident qui laborum ipsum ut exercitation ut ipsum deserunt. Id sint quis consectetur aliqua consequat ullamco qui sint. Amet duis aliquip nostrud anim nostrud magna exercitation aute ad ipsum adipisicing. Esse fugiat velit in officia exercitation.\n\nSint nisi irure excepteur sint exercitation exercitation. Est irure aute cupidatat fugiat anim ipsum sunt minim laboris. Commodo adipisicing veniam nostrud non amet sit magna qui voluptate. Consectetur adipisicing excepteur ut deserunt minim ut qui tempor nisi. Voluptate et consectetur deserunt id eiusmod aliquip sint. Velit incididunt occaecat labore aliqua ad ullamco irure Lorem. Nisi do labore id consectetur ut mollit.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(179, 'days'),
        'read': false,
        'starred': true,
        'labels': [
            {
                'name': 'Private',
                'color': '#4BAE4F'
            }
        ],
        'group': 'primary',
        'type': 'trash',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg']
    },
    {
        'from': {
            'name': 'Lenora Boyle',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'id quis est',
        'content': 'Occaecat minim tempor irure magna. Esse ut incididunt et labore ipsum qui minim cillum elit do excepteur occaecat quis et. Id nostrud exercitation ipsum nisi aute sunt amet tempor eu consectetur qui reprehenderit commodo velit. Incididunt Lorem eu deserunt pariatur.\n\nCulpa Lorem voluptate sit occaecat nostrud laboris veniam est esse eu id sunt non nisi. Adipisicing duis aliquip fugiat ad occaecat dolore enim quis elit. Occaecat consectetur quis aliqua cupidatat laborum culpa.\n\nDo labore sit duis sint occaecat elit nulla ullamco commodo ut. Fugiat fugiat esse duis deserunt quis labore. Commodo eu fugiat ipsum est laboris nulla. Aute consectetur do sunt culpa aliquip ut est commodo nostrud elit enim. Laborum nostrud aute dolore cillum. Sint et ipsum non tempor esse quis nulla ullamco.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(175, 'days'),
        'read': false,
        'starred': false,
        'labels': [
            {
                'name': 'Business',
                'color': '#4BAE4F'
            }
        ],
        'group': 'primary',
        'type': 'draft',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg', 'assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Lyons Callahan',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'officia deserunt nulla',
        'content': 'Cillum sit et anim veniam qui in minim officia incididunt velit dolor dolor. Excepteur cupidatat sunt do anim. Est nisi ea dolore et ad do enim minim nulla fugiat aliqua.\n\nId laborum quis magna laborum esse nulla culpa minim dolore culpa esse aliquip occaecat. Eiusmod sit eiusmod magna est. Cillum dolore occaecat eiusmod velit Lorem ad esse magna esse consequat. Sunt do commodo velit dolor id quis cillum nisi eu. Ea minim dolor in ad deserunt dolor reprehenderit. Minim et ad commodo anim eu sunt reprehenderit aute excepteur irure.\n\nEst non eiusmod excepteur mollit veniam non. Reprehenderit minim ipsum sint proident commodo ipsum fugiat ex minim. Excepteur tempor ad enim esse nulla velit dolore id tempor ut dolore in ad cupidatat. Nulla voluptate velit dolor do. Laborum eiusmod voluptate et eiusmod laboris minim proident aliqua quis Lorem. Laborum deserunt laboris cillum labore ex consequat nostrud nostrud nostrud minim ullamco magna.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(155, 'minutes'),
        'read': true,
        'starred': true,
        'labels': [
            {
                'name': 'Outreach',
                'color': '#4BAE4F'
            }
        ],
        'group': 'primary',
        'type': 'none',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg', 'assets/img/demo/backgrounds/pattern-1.jpg']
    },
    {
        'from': {
            'name': 'Moss Howe',
            'mail': 'demo@justademomail.com'
        },
        'subject': 'duis do Lorem',
        'content': 'Exercitation consequat labore deserunt consectetur. Est aute do dolore fugiat sunt dolor mollit culpa. Mollit sit sit enim cillum dolore consequat et occaecat sit commodo nulla culpa. Tempor aliquip esse cupidatat pariatur duis quis fugiat duis dolore esse cillum amet consectetur. Proident non id sunt elit officia consectetur aliqua quis elit sit proident magna sit sint. Est pariatur pariatur laborum eiusmod eiusmod est et aliquip ipsum consequat in labore ut fugiat.\n\nAnim deserunt voluptate duis sint qui ad sint culpa est id ex incididunt. Anim nostrud cupidatat et fugiat in eiusmod qui magna minim. Enim sint duis aute eiusmod do qui ut culpa amet ut anim voluptate.\n\nEx proident eu voluptate id occaecat ad ad elit esse magna. Labore in mollit est sunt. Duis veniam consequat sunt incididunt consequat deserunt Lorem dolor pariatur elit deserunt culpa dolor exercitation. Incididunt cupidatat fugiat fugiat consectetur minim deserunt aute culpa nulla veniam elit.',
        'when': __WEBPACK_IMPORTED_MODULE_0_moment__().subtract(173, 'days'),
        'read': true,
        'starred': false,
        'labels': [
            {
                'name': 'Priority!',
                'color': '#4CA9BB'
            }
        ],
        'group': 'social',
        'type': 'spam',
        'attachments': ['assets/img/demo/backgrounds/mac-2.jpg']
    }
];


/***/ }),

/***/ "../../../../../src/app/pages/inbox/inbox.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InboxModule", function() { return InboxModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__inbox_component__ = __webpack_require__("../../../../../src/app/pages/inbox/inbox.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__inbox_compose_inbox_compose_component__ = __webpack_require__("../../../../../src/app/pages/inbox/inbox-compose/inbox-compose.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__ = __webpack_require__("../../../flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_scrollbar_scrollbar_module__ = __webpack_require__("../../../../../src/app/core/scrollbar/scrollbar.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__inbox_routing__ = __webpack_require__("../../../../../src/app/pages/inbox/inbox.routing.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var InboxModule = (function () {
    function InboxModule() {
    }
    InboxModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_8__inbox_routing__["a" /* InboxRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__["a" /* FlexLayoutModule */],
                __WEBPACK_IMPORTED_MODULE_7__core_scrollbar_scrollbar_module__["a" /* ScrollbarModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_forms__["e" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["c" /* MatButtonModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["o" /* MatListModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["m" /* MatIconModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["f" /* MatCheckboxModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["x" /* MatRippleModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["j" /* MatDialogModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["n" /* MatInputModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["p" /* MatMenuModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["D" /* MatSnackBarModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material__["J" /* MatTooltipModule */]
            ],
            entryComponents: [__WEBPACK_IMPORTED_MODULE_3__inbox_compose_inbox_compose_component__["a" /* InboxComposeComponent */]],
            declarations: [__WEBPACK_IMPORTED_MODULE_2__inbox_component__["a" /* InboxComponent */], __WEBPACK_IMPORTED_MODULE_3__inbox_compose_inbox_compose_component__["a" /* InboxComposeComponent */]]
        })
    ], InboxModule);
    return InboxModule;
}());



/***/ }),

/***/ "../../../../../src/app/pages/inbox/inbox.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InboxRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__inbox_component__ = __webpack_require__("../../../../../src/app/pages/inbox/inbox.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_1__inbox_component__["a" /* InboxComponent */]
    }
];
var InboxRoutingModule = (function () {
    function InboxRoutingModule() {
    }
    InboxRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_0__angular_router__["d" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_0__angular_router__["d" /* RouterModule */]]
        })
    ], InboxRoutingModule);
    return InboxRoutingModule;
}());



/***/ }),

/***/ "../../../../../src/app/pages/inbox/mail.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Mail; });
var Mail = (function () {
    function Mail(model) {
        if (model === void 0) { model = null; }
        this.from = model.from;
        this.subject = model.subject;
        this.content = model.content;
        this.when = model.when;
        this.read = model.read;
        this.starred = model.starred;
        this.labels = model.labels;
        this.group = model.group;
        this.type = model.type;
        this.attachments = model.attachments;
    }
    return Mail;
}());



/***/ })

});
//# sourceMappingURL=inbox.module.chunk.js.map