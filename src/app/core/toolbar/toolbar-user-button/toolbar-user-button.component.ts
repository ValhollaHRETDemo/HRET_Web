import { AfterViewInit, Component, OnInit } from '@angular/core';

@Component({
  selector: 'vr-toolbar-user-button',
  templateUrl: './toolbar-user-button.component.html',
  styleUrls: ['./toolbar-user-button.component.scss']
})
export class ToolbarUserButtonComponent implements OnInit, AfterViewInit {

  isOpen: boolean;
  userName : string;

  constructor() { }

  ngOnInit() {
    var userDetails = JSON.parse(localStorage.getItem("userDetails"));
    this.userName = userDetails.first_name + ' ' + userDetails.last_name;
  }

  ngAfterViewInit() {
  }

  toggleDropdown() {
    this.isOpen = !this.isOpen;
  }

  onClickOutside() {
    this.isOpen = false;
  }
}
