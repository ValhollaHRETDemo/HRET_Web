import { Component, OnInit } from '@angular/core';
import { SidenavItem } from '../sidenav/sidenav-item/sidenav-item.model';
import * as fromRoot from '../../reducers/index';
import * as fromSidenav from '../sidenav/shared/sidenav.action';
import { Store } from '@ngrx/store';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { SetCurrentlyOpenByRouteAction } from '../sidenav/shared/sidenav.action';
import { SelectLayoutAction, SetCardElevationAction } from '../layout/shared/layout.action';

@Component({
  selector: 'vr-route-handler',
  templateUrl: './route-handler.component.html',
  styleUrls: ['./route-handler.component.scss']
})
export class RouteHandlerComponent implements OnInit {

  constructor(
    private store: Store<fromRoot.State>,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  _roleRank: number;

  ngOnInit() {
    // Set Sidenav Currently Open on Page load
    this.router.events.subscribe((event) => {
      this._roleRank = 1;
      if (event instanceof NavigationEnd) {
        this.store.dispatch(new SetCurrentlyOpenByRouteAction(event.urlAfterRedirects));
      }
    });

    // You can use ?layout=beta to load the page with Layout Beta as default
    // Same with ?elevation=5 (anything from 0 to 24)
    this.route.queryParamMap.subscribe((params) => {
      const layout = params.get('layout');

      switch (layout) {
        case 'alpha': {
          this.store.dispatch(new SelectLayoutAction('alpha'));
          break
        }

        case 'beta': {
          this.store.dispatch(new SelectLayoutAction('beta'));
          break
        }

        case 'gamma': {
          this.store.dispatch(new SelectLayoutAction('gamma'));
          break
        }
      }

      const elevation = params.get('elevation');

      if (elevation) {
        this.store.dispatch(new SetCardElevationAction('card-elevation-z' + elevation))
      }
    });

    // Define Menu Items here
    // Top Level Item (The item to click on so the dropdown opens)
    const dashboard = new SidenavItem({
      icon: 'dashboard',
      name: 'Dashboard',
      route: '/dashboard',
      subItems: [],
      position: 1,
      routerLinkActiveOptions: {
        exact: true
      }
    });

    const adminConsole = new SidenavItem({
      name: 'Admin Console',
      icon: 'library_books',
      subItems: [],
      position: 1
    });

    const pagesSubItems = [

      new SidenavItem({
        name: 'Unit Management',
        route: '/pages/unitmanagement',
        parent: adminConsole,
        subItems: [],
        position: 2
      }),
      new SidenavItem({
        name: 'State Management',
        route: '/pages/statemanagement',
        parent: adminConsole,
        subItems: [],
        position: 1
      }),
      new SidenavItem({
        name: 'Hospital Management',
        route: '/pages/hospitalmanagement',
        parent: adminConsole,
        subItems: [],
        position: 1
      }),

      new SidenavItem({
        name: 'Health System Management',
        route: '/pages/healthsystemmanagement',
        parent: adminConsole,
        subItems: [],
        position: 1
      }),

      new SidenavItem({
        name: 'User Management',
        route: '/pages/usermanagement',
        parent: adminConsole,
        subItems: [],
        position: 1
      }),
      new SidenavItem({
        name: 'Reports Usage Statistics',
        parent: adminConsole,
        subItems: [],
        route: '/pages/reportsusagestatistics',
        position: 1
      }),
      new SidenavItem({
        name: 'Domain Management',
        parent: adminConsole,
        subItems: [],
        route: '/pages/domainmanagement',
        position: 1
      })];

    adminConsole.subItems.push(...pagesSubItems);


    const reportsUsageStatistics = new SidenavItem({
      name: 'Reports',
      icon: 'insert_chart',
      subItems: [],
      position: 1
    });

    const reportsSubItems = [


      new SidenavItem({
        name: 'Adverse Drug Events',
        route: '/pages/reports/1',
        parent: reportsUsageStatistics,
        subItems: [],
        position: 2
      }),
      new SidenavItem({
        name: 'CAUTI',
        route: '/pages/reports/2',
        parent: reportsUsageStatistics,
        subItems: [],
        position: 1
      }),
      new SidenavItem({
        name: 'CLABSI',
        route: '/pages/reports/3',
        parent: reportsUsageStatistics,
        subItems: [],
        position: 1
      }),

      new SidenavItem({
        name: 'Difficile',
        route: '/pages/reports/14',
        parent: reportsUsageStatistics,
        subItems: [],
        position: 1
      }),

      new SidenavItem({
        name: 'Falls',
        route: '/pages/reports/5',
        parent: reportsUsageStatistics,
        subItems: [],
        position: 1
      }),
      new SidenavItem({
        name: 'MRSA',
        route: '/pages/reports/6',
        parent: reportsUsageStatistics,
        subItems: [],
        position: 1
      }),
      new SidenavItem({
        name: 'Pressure Ulcers',
        route: '/pages/reports/7',
        parent: reportsUsageStatistics,
        subItems: [],
        position: 1
      }),
      new SidenavItem({
        name: 'Readminssion',
        route: '/pages/reports/8',
        parent: reportsUsageStatistics,
        subItems: [],
        position: 1
      }),
      new SidenavItem({
        name: 'Sepsis',
        route: '/pages/reports/9',
        parent: reportsUsageStatistics,
        subItems: [],
        position: 1
      }),
      new SidenavItem({
        name: 'Surgical Site Infection',
        route: '/pages/reports/10',
        parent: reportsUsageStatistics,
        subItems: [],
        position: 1
      }),
      new SidenavItem({
        name: 'VAE',
        route: '/pages/reports/12',
        parent: reportsUsageStatistics,
        subItems: [],
        position: 1
      }),
      new SidenavItem({
        name: 'VTE',
        route: '/pages/reports/11',
        parent: reportsUsageStatistics,
        subItems: [],
        position: 1
      }),
      new SidenavItem({
        name: 'Culture of Safety - Worker Saftey',
        route: '/pages/reports/13',
        parent: reportsUsageStatistics,
        subItems: [],
        position: 1
      }),
      new SidenavItem({
        name: 'Post Intervention Reports',
        route: '/pages/reports/14',
        parent: reportsUsageStatistics,
        subItems: [],
        position: 1
      })];

    reportsUsageStatistics.subItems.push(...reportsSubItems);

    // Send the created Menu structure to Redux/ngrx (you only need to send the Top Level Item, all dropdown items will be added automatically)
    this.store.dispatch(new fromSidenav.AddSidenavItemAction(dashboard));
    this.store.dispatch(new fromSidenav.AddSidenavItemAction(adminConsole));
    this.store.dispatch(new fromSidenav.AddSidenavItemAction(reportsUsageStatistics));

  }

}
