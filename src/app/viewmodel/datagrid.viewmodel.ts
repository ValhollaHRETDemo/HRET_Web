import { UserEvent } from "./user.event";
import { Subject } from "../../../node_modules/rxjs";

export class DataGridViewModel {
    public rows: Array<Row>;
    public columns: Array<Column>;
    public keyColumnIndex: Number;
    public events: Subject<UserEvent>;

    public constructor() {
        this.events = new Subject<UserEvent>();
    }
    public hideRow(rowId: any) {
        console.log("hide Row");
    }

    public showRow(rowId: any) {

    }


}

export interface Column {
    id: String;
    name: String;
    isHidden?: Boolean
}

export interface Row {
    data: Array<any>;
    isSelected: boolean;
}
