import { UserEvent } from "./user.event";
import { Subject } from "../../../node_modules/rxjs";
import * as moment from "moment";

export class FilterViewModel {

    public events: Subject<UserEvent>;
    public filters: Array<Filter>;
    public fromDate: Date;
    public toDate: Date;
    public selectedBedRange: string;
    public selectedHospitalGroup: string;
    public currentReportLevel: string;

    public constructor() {
        this.events = new Subject<UserEvent>();
        this.fromDate = moment().subtract(1, "year").toDate();
        this.toDate = moment().toDate();
    }

    public applyFilter() {
        this.events.next({ eventData: null, eventType: "ApplyFilter" })

    }
    public reset() {
        this.filters.forEach(element => element.selectedId = element.defaultValue)
        this.events.next({ eventData: null, eventType: "ResetFilter" })
    }

    public getFilterValues() {

        var values: any = {};
        this.filters.forEach(element => {
            values[element.id.toString()] =
                {
                    selectedValue: element.selectedId, display_name: element.name, data: element
                };
        });
        values["fromDate"] = { selectedValue: this.fromDate, display_name: this.fromDate.toDateString(), data: null };
        values["toDate"] = { selectedValue: this.toDate, display_name: this.fromDate.toDateString(), data: null };
        return values;
    }
    public filterChanged() {
        this.events.next({ eventData: undefined, eventType: "FilterChanged" })
    }
}

export interface Filter {
    id: String;
    name: String;
    data: Array<any>;
    type: String;
    selectedId: string;
    defaultValue: string;
}


