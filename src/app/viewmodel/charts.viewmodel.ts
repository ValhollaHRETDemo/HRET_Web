import { UserEvent } from "./user.event";
import { Subject } from "../../../node_modules/rxjs";

export class ChartViewModel {
    public chartSeries: Array<Series>;
    public xData: Array<any>;
    public events: Subject<UserEvent>;

    public constructor() {
        this.events = new Subject<UserEvent>();
    }
    public hideSeries(seriesId: any) {

    }

    public showSeries(seriesId: any) {

    }

}

export interface Series {
    name: String;
    seriesId: String;
    dashStyle?: String;
    data: Array<any>;
    isHidden: boolean;
    lineWidth?: Number;
}
