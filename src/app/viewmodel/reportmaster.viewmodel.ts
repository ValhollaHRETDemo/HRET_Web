import { DataGridViewModel } from "./datagrid.viewmodel";
import { UserEvent } from "./user.event";
import { ChartViewModel } from "./charts.viewmodel";
import { FilterViewModel } from "./filter.viewmodel";
import { ReportsDataAdapter } from "../adapters/reports.data.adapter";
import { ReportsService } from "../providers/reports.service";
import { ReportsFilterAdapter } from "../adapters/reports.filters.adapter";
import { Subject, Observable } from "../../../node_modules/rxjs";
import * as _ from "underscore";

export class ReportMasterViewModel {

    public dataGridViewModel: DataGridViewModel;
    public chartViewModel: ChartViewModel;
    public filterViewModel: FilterViewModel;
    public events: Subject<UserEvent>;
    public isFilterClicked: boolean;
    public selectedState: string;
    public selectedHospital: string;
    public currentReportLevel: string;
    public currentReportId: String;
    public enableLoader: boolean = true;
    public enableChart: boolean = true;
    public enableGrid: boolean = true;
    public userReportLevel: string;
    private hretReport: any;
    private currentStateName: string;
    private currentHospital: string;

    public constructor(private reportsService: ReportsService) {
        this.dataGridViewModel = new DataGridViewModel();
        this.chartViewModel = new ChartViewModel();
        this.filterViewModel = new FilterViewModel();
    }

    getReportsData() {

        const reportsDataAdapter = new ReportsDataAdapter();
        let reportLevel = "";
        var comparableValue = null;

        var filterValues = this.filterViewModel.getFilterValues();


        if (filterValues.comparables != null && filterValues.comparables.selectedId != undefined) {
            filterValues.comparables.data.forEach(comparable => {
                comparable.id === comparable.selectedId; comparableValue = comparable.comparable_value;
            });
        }

        this.setLoading(true);

        var user_details = (JSON.parse(localStorage.getItem("userDetails")));

        this.reportsService.getReports(user_details.user_id, this.currentReportId,
            filterValues.measureCodes.selectedValue,
            filterValues.fromDate.selectedValue, filterValues.toDate.selectedValue,
            this.selectedState,
            this.selectedHospital,
            this.currentReportLevel,
            filterValues.ruralValues.selectedValue,
            filterValues.bedRange.selectedValue,
            filterValues.hospitalGroupsData.selectedValue,
            filterValues.comparables != undefined ? filterValues.comparables.selectedValue : undefined,
            filterValues.randomNumbers != undefined ? filterValues.randomNumbers.selectedValue : undefined)

            .catch(ex => { this.setLoading(false); return Observable.throw(ex) })
            .subscribe(p => {
                debugger;
                this.hretReport = p;
                this.dataGridViewModel = reportsDataAdapter.transformToDataGridViewModel(p);
                this.chartViewModel = reportsDataAdapter.transformToChartViewModel(p);
                this.setLoading(false);
                this.dataGridViewModel.events.filter(e => e.eventType === "DataGridRowClicked").subscribe(e => {
                    debugger;
                    this.isFilterClicked = false;
                    if (e.eventData.toString().includes("National Level") ||
                        e.eventData.toString().includes("State Level")) return;

                    if (this.currentReportLevel === "National") {
                        this.selectedState = e.eventData;
                        this.selectedHospital = undefined;
                        this.currentReportLevel = "State"
                        this.currentStateName = e.eventData;
                    } else if (this.currentReportLevel === "State") {
                        this.selectedHospital = e.eventData;
                        this.currentHospital = e.eventData;
                        this.currentReportLevel = "Hospital"
                    }
                    else if (this.isFilterClicked) {
                        this.currentReportLevel = (this.currentReportLevel == "") ? "National" : this.currentReportLevel;
                    }
                    this.filterViewModel.currentReportLevel = this.currentReportLevel;

                    this.getReportsData();
                });
                this.dataGridViewModel.events.filter(e => e.eventType === "ExportToExcel").subscribe(e => {
                    this.reportsService.exportToExcel(this.hretReport, {});
                });

            });
    }

    getFilterData() {
        const reportsFilterDataAdapter = new ReportsFilterAdapter();
        this.reportsService.getFiltersData(this.currentReportId, this.currentReportLevel, this.currentHospital).subscribe(p => {
            this.filterViewModel = new FilterViewModel();
            this.filterViewModel.filters = reportsFilterDataAdapter.transform(_.sortBy(p, 'position'));
            this.setDefaultReportLevel();
            this.filterViewModel.currentReportLevel = this.currentReportLevel;

            this.filterViewModel.events.filter(e => e.eventType == "ResetFilter").subscribe(e => {
                this.currentStateName = null;
                this.currentHospital = null;
            });

            this.filterViewModel.events.filter(e => e.eventType == "ApplyFilter").subscribe(e => {
                this.getReportsData();
            });

            this.filterViewModel.events.filter(e => e.eventType === "FilterChanged").subscribe(e => {

                var values = this.filterViewModel.getFilterValues();
                if (values.comparables) {

                }

            });

            this.getReportsData();
        });
    }

    setLoading(state) {
        this.enableLoader = state;
        this.enableGrid = !state;
        this.enableChart = !state;
    }


    setDefaultReportLevel() {
        var user_details = (JSON.parse(localStorage.getItem("userDetails")));
        console.log(user_details);
        if (user_details.role_name == "HRET Admin") {
            this.currentReportLevel = "National";
        }
        else if (user_details.role_name == "State Admin") {
            this.currentReportLevel = "State";
            this.selectedState = user_details.state_code;
            this.currentStateName = user_details.state_code;
        }
        else {
            this.currentReportLevel = "Hospital";
            this.selectedState = user_details.state_code;
            this.selectedHospital = user_details.hospital_code;
            this.currentStateName = user_details.state_code;
            this.currentHospital = user_details.hospital_code;
        }
    }
}
