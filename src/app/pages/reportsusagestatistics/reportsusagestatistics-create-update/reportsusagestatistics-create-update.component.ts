import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../../reducers/index';
import { ReportsUsageStatistics } from './reportsusagestatistics.model';

@Component({
  selector: 'vr-reportsusagestatistics-create-update',
  templateUrl: './reportsusagestatistics-create-update.component.html',
  styleUrls: ['./reportsusagestatistics-create-update.component.scss']
})
export class ReportsUsageStatisticsCreateUpdateComponent implements OnInit {

  static id = 100;

  form: FormGroup;
  mode: 'create' | 'update' = 'create';

  constructor( @Inject(MAT_DIALOG_DATA) public defaults: any,
    private dialogRef: MatDialogRef<ReportsUsageStatisticsCreateUpdateComponent>,
    private fb: FormBuilder,
    private store: Store<fromRoot.State>) {
  }

  ngOnInit() {
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as ReportsUsageStatistics;
    }

    this.form = this.fb.group({
      id: [ReportsUsageStatisticsCreateUpdateComponent.id++],
      ReportName: [this.defaults.ReportName || '',],
      CreatedDate: [this.defaults.CreatedDate || '',],
      AccessedBy: [this.defaults.AccessedBy || '',],
      ModifiedDate: [this.defaults.ModifiedDate || '',],
      ErrorMessage: [this.defaults.ErrorMessage || '',],
      Status: [this.defaults.Status || '',],
      GenerationTime: [this.defaults.GenerationTime || '',],
      Parameters: [this.defaults.Parameters || '',]
    });
  }

  save() {
    if (this.mode === 'create') {
      this.createReportsUsageStatistics();
    } else if (this.mode === 'update') {
      this.updateReportsUsageStatistics();
    }
  }

  createReportsUsageStatistics() {
    const reportsusagestatistics = this.form.value;
    this.dialogRef.close(reportsusagestatistics);
  }

  updateReportsUsageStatistics() {
    const reportsusagestatistics = this.form.value;
    reportsusagestatistics.id = this.defaults.id;

    this.dialogRef.close(reportsusagestatistics);
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }
}
