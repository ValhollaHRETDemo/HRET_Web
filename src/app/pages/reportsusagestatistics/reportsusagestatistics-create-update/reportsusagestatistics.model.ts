export class ReportsUsageStatistics {
  id: number;
  ReportName: string;
  CreatedDate: string;
  AccessedBy: string;
  ModifiedDate: string;
  ErrorMessage: string;
  Status: string;
  GenerationTime: string;
  Parameters: string;

  constructor(reportsusagestatistics) {
    this.id = reportsusagestatistics.id;
    this.ReportName = reportsusagestatistics.ReportName;
    this.CreatedDate = reportsusagestatistics.CreatedDate;
    this.AccessedBy = reportsusagestatistics.AccessedBy;
    this.ModifiedDate = reportsusagestatistics.ModifiedDate;
    this.ErrorMessage = reportsusagestatistics.ErrorMessage;
    this.Status = reportsusagestatistics.Status;
    this.GenerationTime = reportsusagestatistics.GenerationTime;
    this.Parameters = reportsusagestatistics.Parameters;
  }

}
