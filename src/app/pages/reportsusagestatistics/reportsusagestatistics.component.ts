import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort } from '@angular/material';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/filter';
import { Observable } from 'rxjs/Observable';
import { List } from '../../core/list/list.interface';
import { ReportsUsageStatistics } from './reportsusagestatistics-create-update/reportsusagestatistics.model';
import { ListColumn } from '../../core/list/list-column.model';
import { ListDataSource } from '../../core/list/list-datasource';
import { ListDatabase } from '../../core/list/list-database';
import { componentDestroyed } from '../../core/utils/component-destroyed';
import { ReportsUsageStatisticsCreateUpdateComponent } from './reportsusagestatistics-create-update/reportsusagestatistics-create-update.component';
import { ReportsUsageStatistics_Data } from './reportsusagestatistics.demo';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { ROUTE_TRANSITION } from '../../app.animation';

@Component({
  selector: 'vr-all-in-one-table',
  templateUrl: './reportsusagestatistics.component.html',
  styleUrls: ['./reportsusagestatistics.component.scss'],
  animations: [...ROUTE_TRANSITION],
  host: { '[@routeTransition]': '' }
})
export class ReportsUsageStatisticsComponent implements List<ReportsUsageStatistics>, OnInit, OnDestroy {

  subject$: ReplaySubject<ReportsUsageStatistics[]> = new ReplaySubject<ReportsUsageStatistics[]>(1);
  data$: Observable<ReportsUsageStatistics[]>;
  ReportsUsageStatisticsList: ReportsUsageStatistics[];

  @Input()
  columns: ListColumn[] = [
    { name: 'Report Name', property: 'ReportName', visible: true, isModelProperty: true },
    { name: 'Created Date', property: 'CreatedDate', visible: true, isModelProperty: true },
    { name: 'Accessed By', property: 'AccessedBy', visible: true, isModelProperty: true },
    { name: 'Modified Date', property: 'ModifiedDate', visible: true, isModelProperty: true },
    { name: 'Error Message', property: 'ErrorMessage', visible: true, isModelProperty: true },
    { name: 'Status', property: 'Status', visible: true, isModelProperty: true },
    { name: 'Generation Time', property: 'GenerationTime', visible: true, isModelProperty: true },
    { name: 'Parameters', property: 'Parameters', visible: true, isModelProperty: true },
    { name: 'Actions', property: 'actions', visible: true },
  ] as ListColumn[];
  pageSize = 10;
  resultsLength: number;
  dataSource: ListDataSource<ReportsUsageStatistics> | null;
  database: ListDatabase<ReportsUsageStatistics>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  constructor(private dialog: MatDialog) {
  }

  ngOnInit() {
    this.ReportsUsageStatisticsList = ReportsUsageStatistics_Data.map(reportsusagestatistics => new ReportsUsageStatistics(reportsusagestatistics));

    this.subject$.next(this.ReportsUsageStatisticsList);
    this.data$ = this.subject$.asObservable();

    this.database = new ListDatabase<ReportsUsageStatistics>();
    this.data$
      .takeUntil(componentDestroyed(this))
      .filter(Boolean)
      .subscribe((ReportsUsageStatisticsList) => {
        this.ReportsUsageStatisticsList = ReportsUsageStatisticsList;
        this.database.dataChange.next(ReportsUsageStatisticsList);
        this.resultsLength = ReportsUsageStatisticsList.length;
      });

    this.dataSource = new ListDataSource<ReportsUsageStatistics>(this.database, this.sort, this.paginator, this.columns);
  }

  createReportsUsageStatistics() {
    this.dialog.open(ReportsUsageStatisticsCreateUpdateComponent).afterClosed().subscribe((reportsusagestatistics: ReportsUsageStatistics) => {
      if (reportsusagestatistics) {
        this.ReportsUsageStatisticsList.unshift(new ReportsUsageStatistics(reportsusagestatistics));
        this.subject$.next(this.ReportsUsageStatisticsList);
      }
    });
  }

  updateReportsUsageStatistics(reportsusagestatistics) {
    this.dialog.open(ReportsUsageStatisticsCreateUpdateComponent, {
      data: reportsusagestatistics
    }).afterClosed().subscribe((reportsusagestatistics) => {
      if (reportsusagestatistics) {
        const index = this.ReportsUsageStatisticsList.findIndex((existingReportsUsageStatistics) => existingReportsUsageStatistics.id === reportsusagestatistics.id);
        this.ReportsUsageStatisticsList[index] = new ReportsUsageStatistics(reportsusagestatistics);
        this.subject$.next(this.ReportsUsageStatisticsList);
      }
    });
  }

  deleteReportsUsageStatistics(reportsusagestatistics) {
    this.ReportsUsageStatisticsList.splice(this.ReportsUsageStatisticsList.findIndex((existingReportsUsageStatistics) => existingReportsUsageStatistics.id === reportsusagestatistics.id), 1);
    this.subject$.next(this.ReportsUsageStatisticsList);
  }

  onFilterChange(value) {
    if (!this.dataSource) {
      return;
    }
    this.dataSource.filter = value;
  }

  ngOnDestroy() {
  }
}
