import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatDialogModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatPaginatorModule,
  MatSortModule,
  MatTableModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { ReportsUsageStatisticsCreateUpdateModule } from './reportsusagestatistics-create-update/reportsusagestatistics-create-update.module';
import { ListModule } from 'app/core/list/list.module';
import { PageHeaderModule } from 'app/core/page-header/page-header.module';
import { BreadcrumbsModule } from 'app/core/breadcrumbs/breadcrumbs.module';
import { ReportsUsageStatisticsComponent } from 'app/pages/reportsusagestatistics/reportsusagestatistics.component';
import { ReportsUsageStatisticsRoutingModule } from 'app/pages/reportsusagestatistics/reportsusagestatistics.routing';


@NgModule({
  imports: [
    CommonModule,
    ReportsUsageStatisticsRoutingModule,
    FormsModule,
    FlexLayoutModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatInputModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    MatDialogModule,

    // Core
    ListModule,
    ReportsUsageStatisticsCreateUpdateModule,
    PageHeaderModule,
    BreadcrumbsModule
  ],
  declarations: [ReportsUsageStatisticsComponent],
  exports: [ReportsUsageStatisticsComponent]
})
export class ReportsUsageStatisticsModule { }
