import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ReportsUsageStatisticsComponent } from 'app/pages/reportsusagestatistics/reportsusagestatistics.component';

const routes: Routes = [
  {
    path: '',
    component: ReportsUsageStatisticsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsUsageStatisticsRoutingModule { }
