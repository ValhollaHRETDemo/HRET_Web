import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ReportMasterViewModel } from '../../viewmodel/reportmaster.viewmodel';
import { ReportsService } from '../../providers/reports.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'vr-reports-master',
  templateUrl: './reports-master.component.html',
  styleUrls: ['./reports-master.component.scss']
})

export class ReportsMasterComponent implements OnInit {
  viewModel: ReportMasterViewModel;
  reportId: number;
  route: ActivatedRoute;

  constructor(private reportsService: ReportsService, route: ActivatedRoute) {
    this.route = route;
  }

  ngOnInit() {

    this.viewModel = new ReportMasterViewModel(this.reportsService);
    this.viewModel.currentReportLevel = "";
    this.viewModel.currentReportId = this.route.snapshot.params['id'];
    this.viewModel.setDefaultReportLevel();
    this.viewModel.getFilterData();

  }

}
