import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    MatDatepickerModule, MatNativeDateModule, MatOptionModule, MatButtonModule, MatSelectModule, MatCheckboxModule, MatDialogModule, MatIconModule, MatInputModule, MatMenuModule, MatPaginatorModule, MatSortModule, MatTableModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { ListModule } from '../../core/list/list.module';
import { PageHeaderModule } from '../../core/page-header/page-header.module';
import { BreadcrumbsModule } from '../../core/breadcrumbs/breadcrumbs.module';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { ReportsMasterComponent } from './reports-master.component';
import { ReportsMasterComponentRoutingModule } from './reports-master.routing';
import { ChartsModule } from '../charts/charts.module'
import { FiltersComponent } from '../filters/filters.component'
import { DatagridModule } from '../datagrid/datagrid.module'

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        FlexLayoutModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatCheckboxModule,
        MatInputModule,
        MatIconModule,
        MatMenuModule,
        MatButtonModule,
        MatDialogModule,
        AngularFontAwesomeModule,
        ReportsMasterComponentRoutingModule, MatSelectModule, MatNativeDateModule, MatDatepickerModule, MatOptionModule,
        // Core
        ListModule,
        PageHeaderModule,
        BreadcrumbsModule, DatagridModule, ChartsModule
    ],
    declarations: [ReportsMasterComponent, FiltersComponent],
    exports: [ReportsMasterComponent, FiltersComponent]
})
export class ReportsMasterModule { }
