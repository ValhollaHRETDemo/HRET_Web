import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ReportsMasterComponent } from './reports-master.component';

const routes: Routes = [
  {
    path: '',
    component: ReportsMasterComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class ReportsMasterComponentRoutingModule { }
