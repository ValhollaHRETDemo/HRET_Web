import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { HealthSystemManagementComponent } from 'app/pages/healthsystemmanagement/healthsystemmanagement.component';

const routes: Routes = [
  {
    path: '',
    component: HealthSystemManagementComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HealthSystemManagementRoutingModule { }
