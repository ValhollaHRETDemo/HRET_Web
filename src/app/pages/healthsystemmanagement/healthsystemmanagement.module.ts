import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatDialogModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatPaginatorModule,
  MatSortModule,
  MatTableModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { HealthSystemCreateUpdateModule } from './healthsystem-create-update/healthsystem-create-update.module';
import { LinkModule } from './link/link.module';
import { ListModule } from 'app/core/list/list.module';
import { PageHeaderModule } from 'app/core/page-header/page-header.module';
import { BreadcrumbsModule } from 'app/core/breadcrumbs/breadcrumbs.module';
import { HealthSystemManagementComponent } from 'app/pages/healthsystemmanagement/healthsystemmanagement.component';
import { HealthSystemManagementRoutingModule } from 'app/pages/healthsystemmanagement/healthsystemmanagement.routing';


@NgModule({
  imports: [
    CommonModule,
    HealthSystemManagementRoutingModule,
    FormsModule,
    FlexLayoutModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatInputModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    MatDialogModule,

    // Core
    ListModule,
    HealthSystemCreateUpdateModule,
    LinkModule,
    PageHeaderModule,
    BreadcrumbsModule
  ],
  declarations: [HealthSystemManagementComponent],
  exports: [HealthSystemManagementComponent]
})
export class HealthSystemManagementModule { }
