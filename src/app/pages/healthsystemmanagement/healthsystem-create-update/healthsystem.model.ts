import { State } from "../../statemanagement/state-create-update/state.model";

export class HealthSystem {
  health_system_id: number;
  health_system_name: string;
  contact_person: string;
  email_id: string;
  phone: string;
  address_line_1: string;
  address_line_2: string;
  city: string;
  zip_code: string;
  is_active: string;
  created_by: string;
  created_on: string;
  modified_by: string;
  modified_on: string;
  state_id: number;
  state_name: string;
  constructor(healthsystem) {
    this.health_system_id = healthsystem.health_system_id;
    this.health_system_name = healthsystem.health_system_name;
    this.contact_person = healthsystem.contact_person;
    this.email_id = healthsystem.email_id;
    this.phone = healthsystem.phone;
    this.address_line_1 = healthsystem.address_line_1;
    this.address_line_2 = healthsystem.address_line_2;
    this.city = healthsystem.city;
    this.zip_code = healthsystem.zip_code;
    this.state_id = healthsystem.state_id;
    this.is_active = healthsystem.is_active;
    this.created_by = healthsystem.created_by;
    this.created_on = healthsystem.created_on;
    this.modified_on = healthsystem.modified_on;
    this.modified_by = healthsystem.modified_by;
    if (healthsystem.state_ !== undefined) {
      this.state_name = healthsystem.state_.state_name;
    } else {
      this.state_name = healthsystem.state_name;
    }
  }

}
