import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../../reducers/index';
import { HealthSystem } from './healthsystem.model';
import { HealthSystemManagementService } from '../../../providers/healthsystemmanagement.service';
import { StatesService } from '../../../providers/states.service';
import { validateFormData } from '../../validateformdata';
import { genericFunctions } from '../../genericfunctions';

@Component({
  selector: 'vr-healthsystem-create-update',
  templateUrl: './healthsystem-create-update.component.html',
  styleUrls: ['./healthsystem-create-update.component.scss']
})
export class HealthSystemCreateUpdateComponent implements OnInit {

  form: FormGroup;
  public states = [];
  public selectedStateId: any;
  mode: 'create' | 'update' = 'create';
  _validateformdata: validateFormData; 
  _genericfunction:genericFunctions;

  showMessage:boolean;
  message:string;
  systemNameValidationMessage:string;
  emailValidationMessage:string;
  stateValidationMessage:string;
  contactValidationMessage:string;
  cityValidationMessage:string;
  zipValidationMessage:string;
  phoneValidationMessage:string;  

  timer: Observable<number>;
  subscription: any;

  constructor( @Inject(MAT_DIALOG_DATA) public defaults: any,
    private dialogRef: MatDialogRef<HealthSystemCreateUpdateComponent>,
    private fb: FormBuilder,
    private healthsystemmanagementservice: HealthSystemManagementService,
    private stateservice: StatesService,
    private store: Store<fromRoot.State>) {
    this._validateformdata = new validateFormData();
    this._genericfunction = new genericFunctions();
  }

  ngOnInit() {
    this.fillDefaults();
  }

  fillDefaults() {

    this.stateservice.getAllStates().subscribe(p => {
      p.forEach(element => {
        this.states.push({ name: element.state_name, id: element.state_id });
      });      
    });

    if (this.defaults) {

      this.mode = 'update';
      this.form = this.fb.group({
        health_system_id: [this.defaults.health_system_id],
        health_system_name: [this.defaults.health_system_name || '',],
        address_line_1: [this.defaults.address_line_1 || '',],
        address_line_2: [this.defaults.address_line_2 || '',],
        city: [this.defaults.city || '',],
        zip_code: [this.defaults.zip_code || '',],
        email_id: [this.defaults.email_id || '',],
        phone: [this.defaults.phone || '',],
        contact_person: [this.defaults.contact_person || '',],
        is_active: [this.defaults.is_active || '1'],
        created_by: [this.defaults.created_by || '1'],
        created_on: [this.defaults.created_on || new Date()],
        modified_by: [this.defaults.modified_by || null],
        modified_on: [this.defaults.modified_on || null],
        state_id: [this.defaults.state_id || null]
      });
      this.selectedStateId = this.defaults.state_id;
      
    } else {

      this.defaults = {} as HealthSystem;
      this.form = this.fb.group({
        health_system_name: [this.defaults.health_system_name || '',],
        address_line_1: [this.defaults.address_line_1 || '',],
        address_line_2: [this.defaults.address_line_2 || '',],
        city: [this.defaults.city || '',],
        zip_code: [this.defaults.zip_code || '',],
        email_id: [this.defaults.email_id || '',],
        phone: [this.defaults.phone || '',],
        contact_person: [this.defaults.contact_person || '',],
        is_active: [this.defaults.is_active || '1'],
        created_by: [this.defaults.created_by || '1'],
        created_on: [this.defaults.created_on || new Date()],
        modified_by: [this.defaults.modified_by || null],
        modified_on: [this.defaults.modified_on || null]
      });

    }
  }

  save() {
    if (this.mode === 'create') {
      this.createHealthSystem();

    } else if (this.mode === 'update') {
      this.updateHealthSystem();
    }
  }

  createHealthSystem() {
    const healthsystem = this.form.value;
    healthsystem.state_id = this.selectedStateId;
    healthsystem.state_name = this.states.find(x => x.id === this.selectedStateId).name;
    healthsystem.health_system_name = this._genericfunction.trimData(healthsystem.health_system_name);
    healthsystem.city = this._genericfunction.trimData(healthsystem.city);
    healthsystem.email_id = this._genericfunction.trimData(healthsystem.email_id);
    healthsystem.contact_person = this._genericfunction.trimData(healthsystem.contact_person);
    if (this.validateModelData(healthsystem)) {
      this.healthsystemmanagementservice.saveHealthSystem(healthsystem).subscribe(p => {
        this.dialogRef.close(p);
      });
    }
  }

  updateHealthSystem() {
    const healthsystem = this.form.value;
    healthsystem.state_id = this.selectedStateId;
    healthsystem.state_name = this.states.find(x => x.id === this.selectedStateId).name;
    healthsystem.health_system_name = this._genericfunction.trimData(healthsystem.health_system_name);
    healthsystem.city = this._genericfunction.trimData(healthsystem.city);
    healthsystem.email_id = this._genericfunction.trimData(healthsystem.email_id);
    healthsystem.contact_person = this._genericfunction.trimData(healthsystem.contact_person);
    if (this.validateModelData(healthsystem)) {
      this.healthsystemmanagementservice.saveHealthSystem(healthsystem).subscribe(p => {
        this.dialogRef.close(p);
      });
    }
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  setTimer(model, msg, isSuccess) {

    // set showloader to true to show loading div on view
    this.showMessage = true;
    if (model == "email") {
      this.emailValidationMessage = msg;
    }
    if (model == "name") {
      this.systemNameValidationMessage = msg;
    }
    if (model == "statecode") {
      this.stateValidationMessage = msg;
    }
    if (model == "contact") {
      this.contactValidationMessage = msg;
    }
    if (model == "city") {
      this.cityValidationMessage = msg;
    }
    if(model == "zip")
    {
      this.zipValidationMessage = msg;
    }
    if(model == "phone")
    {
      this.phoneValidationMessage = msg;
    }
    else
    {
    this.message = msg;
    }
    this.timer = Observable.timer(5000); // 4000 millisecond means 4 seconds
    this.subscription = this.timer.subscribe(() => {
      // set showloader to false to hide loading div from view after 5 seconds
      this.showMessage = false;
      this.message = "";
    });
  }

  validateModelData(healthsystem)
  {
     var status = true;
     if(!(this._validateformdata.validateEmail(healthsystem.email_id).status))
     {
       this.setTimer("email",(this._validateformdata.validateEmail(healthsystem.email_id).message),true);     
     }
     if(!(this._validateformdata.validateString(healthsystem.health_system_name).status))
     {
       this.setTimer("name",(this._validateformdata.validateString(healthsystem.health_system_name).message),true);     
     }
     if(!(this._validateformdata.validateDropdownSelection(healthsystem.state_code).status))
     {
       this.setTimer("statecode",(this._validateformdata.validateDropdownSelection(healthsystem.state_code).message),true);     
     }
     if(!(this._validateformdata.validateString(healthsystem.contact_person).status))
     {
       this.setTimer("contact",(this._validateformdata.validateString(healthsystem.contact_person).message),true);     
     }
     if(!(this._validateformdata.validateString(healthsystem.city).status))
     {
       this.setTimer("city",(this._validateformdata.validateString(healthsystem.city).message),true);     
     }
     if(!(this._validateformdata.validatePhoneNumber(healthsystem.phone).status))
     {
       this.setTimer("phone",(this._validateformdata.validatePhoneNumber(healthsystem.phone).message),true);     
     }
     if(!(this._validateformdata.validateZipCode(healthsystem.zip_code).status))
     {
       this.setTimer("zip",(this._validateformdata.validateZipCode(healthsystem.zip_code).message),true);     
     }
     
    if(
    (this._validateformdata.validateEmail(healthsystem.email_id).status)&&
    (this._validateformdata.validateString(healthsystem.health_system_name).status)&&
    (this._validateformdata.validateString(healthsystem.city).status)&&
    (this._validateformdata.validateString(healthsystem.contact_person).status)&&
    (this._validateformdata.validateDropdownSelection(healthsystem.state_code).status)&&
    (this._validateformdata.validatePhoneNumber(healthsystem.phone).status)&&
    (this._validateformdata.validateZipCode(healthsystem.zip_code).status))
    {
      return true;
    }
    else {
      return false;
    }
  }

}
