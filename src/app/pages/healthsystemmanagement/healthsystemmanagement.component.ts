import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';

import { MatDialog, MatPaginator, MatSort } from '@angular/material';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/filter';
import { Observable } from 'rxjs/Observable';
import { List } from '../../core/list/list.interface';
import { HealthSystem } from './healthsystem-create-update/healthsystem.model';
import { Link } from 'app/pages/healthsystemmanagement/link/link.model';
import { ListColumn } from '../../core/list/list-column.model';
import { ListDataSource } from '../../core/list/list-datasource';
import { ListDatabase } from '../../core/list/list-database';
import { componentDestroyed } from '../../core/utils/component-destroyed';
import { HealthSystemCreateUpdateComponent } from './healthsystem-create-update/healthsystem-create-update.component';
import { LinkComponent } from 'app/pages/healthsystemmanagement/link/link.component';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { ROUTE_TRANSITION } from '../../app.animation';
import { HealthSystemManagementService } from '../../providers/healthsystemmanagement.service';
import { StatesService } from 'app/providers/states.service';

@Component({
  selector: 'vr-all-in-one-table',
  templateUrl: './healthsystemmanagement.component.html',
  styleUrls: ['./healthsystemmanagement.component.scss'],
  animations: [...ROUTE_TRANSITION],
  host: { '[@routeTransition]': '' }
})

export class HealthSystemManagementComponent implements List<HealthSystem>, OnInit, OnDestroy {

  subject$: ReplaySubject<HealthSystem[]> = new ReplaySubject<HealthSystem[]>(1);
  subject_Hospital$: ReplaySubject<Link[]> = new ReplaySubject<Link[]>(1);
  data$: Observable<HealthSystem[]>;
  healthsystems: HealthSystem[];
  link: Link[];
  public states = [];


  @Input()
  columns: ListColumn[] = [
    { name: 'Health System Id', property: 'health_system_id', visible: false, isModelProperty: true },
    { name: 'Health System Name', property: 'health_system_name', visible: true, isModelProperty: true },
    { name: 'Contact Person', property: 'contact_person', visible: false, isModelProperty: true },
    { name: 'Email', property: 'email_id', visible: true, isModelProperty: true },
    { name: 'Phone Number', property: 'phone', visible: true, isModelProperty: true },
    { name: 'Address 1', property: 'address_line_1', visible: true, isModelProperty: true },
    { name: 'Address 2', property: 'address_line_2', visible: true, isModelProperty: true },
    { name: 'City', property: 'city', visible: true, isModelProperty: true },
    { name: 'Zip Code', property: 'zip_code', visible: true, isModelProperty: true },
    { name: 'isActive', property: 'is_active', visible: false, isModelProperty: true },
    { name: 'Created By', property: 'created_by', visible: false, isModelProperty: true },
    { name: 'Created On', property: 'created_on', visible: false, isModelProperty: true },
    { name: 'Modified By', property: 'modified_by', visible: false, isModelProperty: true },
    { name: 'Modified On', property: 'modified_on', visible: false, isModelProperty: true },
    { name: 'State', property: 'state_name', visible: true, isModelProperty: true },
    { name: 'Actions', property: 'actions', visible: true },

  ] as ListColumn[];
  pageSize = 10;
  resultsLength: number;
  dataSource: ListDataSource<HealthSystem> | null;
  database: ListDatabase<HealthSystem>;

  showMessage: boolean;
  message: string;
  timer: Observable<number>;
  subscription: any;


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  constructor(
    private dialog: MatDialog,
    private healthsystemservice: HealthSystemManagementService,
    private stateservice: StatesService) {
  }

  ngOnInit() {
    
    var data = this.healthsystemservice.getHealthSystems().subscribe(p => {
      this.healthsystems = p.map(healthsystem => new HealthSystem(healthsystem));
      this.subject$.next(this.healthsystems);
      this.data$ = this.subject$.asObservable();
      this.database = new ListDatabase<HealthSystem>();

      this.dataSource = new ListDataSource<HealthSystem>(this.database, this.sort, this.paginator, this.columns);
      this.data$
        .takeUntil(componentDestroyed(this))
        .filter(Boolean)
        .subscribe((healthsystems) => {
          this.healthsystems = healthsystems;
          this.database.dataChange.next(healthsystems);
          this.resultsLength = healthsystems.length;
        });
      this.dataSource = new ListDataSource<HealthSystem>(this.database, this.sort, this.paginator, this.columns);
    });

    
    this.stateservice.getAllStates().subscribe(p => {
      p.forEach(element => {
        this.states.push({ name: element.state_name, id: element.state_id });
      });      
    });


  }

  createHealthSystem() {
    this.dialog.open(HealthSystemCreateUpdateComponent).afterClosed().subscribe((healthsystem: HealthSystem) => {
      if (healthsystem) {
        healthsystem.state_name = this.states.find(x => x.id === healthsystem.state_id).name;
        this.healthsystems.unshift(new HealthSystem(healthsystem));
        this.subject$.next(this.healthsystems);
        this.healthsystemservice.showToast("success", "Health Sytem saved successfully");
      }
    });
  }

  updateHealthSystem(healthsystem) {
    this.dialog.open(HealthSystemCreateUpdateComponent, {
      data: healthsystem
    }).afterClosed().subscribe((healthsystem) => {
      if (healthsystem) {
        healthsystem.state_name = this.states.find(x => x.id === healthsystem.state_id).name;
        const index = this.healthsystems.findIndex((existingHealthSystem) => existingHealthSystem.health_system_id === healthsystem.health_system_id);
        this.healthsystems[index] = new HealthSystem(healthsystem);
        this.subject$.next(this.healthsystems);
        this.healthsystemservice.showToast("success", "Health Sytem modified successfully");
      }
    });
  }

  linkHealthSystem(healthsystem) {

    this.dialog.open(LinkComponent, { data: healthsystem.health_system_id });

  }

  deleteHealthSystem(healthsystem) {
    this.healthsystemservice.deleteHealthSystem(healthsystem).subscribe(p => {
      this.healthsystems.splice(this.healthsystems.findIndex((existingHealthSystem) => existingHealthSystem.health_system_id === healthsystem.health_system_id), 1);
      this.subject$.next(this.healthsystems);
    });
    this.setTimer("Health System deleted successfully", true);
  }

  onFilterChange(value) {
    if (!this.dataSource) {
      return;
    }
    this.dataSource.filter = value;
  }

  ngOnDestroy() {
  }

  public setTimer(msg, isSuccess) {

    // set showloader to true to show loading div on view
    this.showMessage = true;
    this.message = msg;

    this.timer = Observable.timer(1000); // 4000 millisecond means 4 seconds
    this.subscription = this.timer.subscribe(() => {
      // set showloader to false to hide loading div from view after 5 seconds
      this.showMessage = false;
      this.message = "";
    });
  }

}
