import { State } from "../../statemanagement/state-create-update/state.model";

export class Link {
hs_hosp_id:number;
health_system_id:number;
hospital_id:number;
is_active:boolean;
created_by:number;
created_on:Date;
modified_by:number | null;
modified_on:Date | null;
  constructor(healthsystem) {
    this.hs_hosp_id = healthsystem.hs_hosp_id;
    this.health_system_id = healthsystem.health_system_id;
    this.hospital_id = healthsystem.hospital_id;
    this.is_active = healthsystem.is_active;
    this.created_by = healthsystem.created_by;
    this.created_on = healthsystem.created_on;
    this.modified_by  = healthsystem.modified_by;
    this.modified_on = healthsystem.modified_on;
  }
}
