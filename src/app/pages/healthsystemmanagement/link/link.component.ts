import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../../reducers/index';
import { Link } from './link.model';
import { HealthSystemManagementService } from 'app/providers/healthsystemmanagement.service';
import { HealthSystemHospitalService } from 'app/providers/healthsystem-hospital.service';
import { HospitalInformationService } from 'app/providers/hospitalinformation.service';

@Component({
  selector: 'vr-link',
  templateUrl: './link.component.html',
  styleUrls: ['./link.component.scss']
})
export class LinkComponent implements OnInit {

  form: FormGroup;
  HealthSystemHospitals = [];
  health_system_id: string;
  health_system_name: string;
  hospitalInfomations = [];

  dropdownList = [];
  dropdownListData = [];
  selectedItems = [];
  selectedItemsData = [];
  dropdownSettings = {};

  constructor( @Inject(MAT_DIALOG_DATA) public defaults: any,
    private dialogRef: MatDialogRef<LinkComponent>,
    private fb: FormBuilder,
    private store: Store<fromRoot.State>,
    private healthSystemManagementService: HealthSystemManagementService,
    private healthSystemHospitalService: HealthSystemHospitalService,
    private hospitalInformationService: HospitalInformationService) {
  }

  ngOnInit() {

    this.health_system_id = this.defaults;

    this.healthSystemManagementService.getHealthSystemById(this.health_system_id).subscribe(p => {
      this.health_system_name = p.health_system_name
    });

    this.hospitalInformationService.getHospitalInformation()
      .subscribe(hospitalInformations => {

        hospitalInformations.forEach(hospitalInformation => {
          this.hospitalInfomations.push({
            hospital_id: hospitalInformation.hospital_id,
            hospital_name: hospitalInformation.hospital_name,
            selected: false,
            hs_hosp_id: 0
          });
        });

        this.healthSystemHospitalService.getHealthSystemHospitalByHealthSystemId(this.health_system_id)
          .subscribe(healthSystemHospitals => {
            healthSystemHospitals.forEach(healthSystemHospital => {
              this.hospitalInfomations.forEach(hospital_infomation => {
                if (hospital_infomation.hospital_id === healthSystemHospital.hospital_id) {
                  hospital_infomation.hs_hosp_id = healthSystemHospital.hs_hosp_id;
                  if (healthSystemHospital.is_active === '1') {
                    hospital_infomation.selected = true;
                  }
                }
              });
            });


            // for drop down - auto complete
            this.hospitalInfomations.forEach(hospital_infomation => {
              var data = { hospital_id: hospital_infomation.hospital_id, hospital_name: hospital_infomation.hospital_name };
              this.dropdownListData.push(data);
              if (hospital_infomation.selected === true) {
                this.selectedItemsData.push(data);
              }
            });
            this.dropdownList = this.dropdownListData;
            this.selectedItems = this.selectedItemsData;
            this.dropdownSettings = {
              singleSelection: false,
              idField: 'hospital_id',
              textField: 'hospital_name',
              selectAllText: 'Select All',
              unSelectAllText: 'UnSelect All',
              itemsShowLimit: 4,
              allowSearchFilter: true
            };


          });

      });

  }

  save() {

    this.hospitalInfomations.forEach(element => {
      this.HealthSystemHospitals.push({
        hs_hosp_id: element.hs_hosp_id !== 0 ? element.hs_hosp_id : 0,
        health_system_id: this.health_system_id,
        hospital_id: element.hospital_id,
        is_active: element.selected === true ? '1' : '0',
        created_by: '1',
        created_on: new Date,
        modified_by: '1',
        modified_on: new Date
      });
    });    
    this.healthSystemHospitalService.saveHealthSystemHospital(this.HealthSystemHospitals).subscribe(p=>{
      this.dialogRef.close(this.HealthSystemHospitals);
    });

  }

  cancel() {
    this.dialogRef.close();
  }

  onItemSelect(item: any) {
    this.hospitalInfomations.forEach(hospital_infomation => {
      if (item.hospital_id === hospital_infomation.hospital_id) {
        hospital_infomation.selected = true;
      }
    });
  }

  onItemDeSelect(item: any) {
    this.hospitalInfomations.forEach(hospital_infomation => {
      if (item.hospital_id === hospital_infomation.hospital_id) {
        hospital_infomation.selected = false;
      }
    });
  }

  onSelectAll() {
    this.hospitalInfomations.forEach(hospital_infomation => {
      hospital_infomation.selected = true;
    });
  }

  onDeSelectAll() {
    this.hospitalInfomations.forEach(hospital_infomation => {
      hospital_infomation.selected = false;
    });
  }

}
