import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  MatButtonModule,
  MatCheckboxModule,
  MatDialogModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatPaginatorModule,
  MatSortModule,
  MatTableModule,
  MatSelectModule, MatNativeDateModule, MatOptionModule,
  // MatDatepicker,
  MatDatepickerModule
} from '@angular/material';

import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { ListModule } from 'app/core/list/list.module';
import { PageHeaderModule } from 'app/core/page-header/page-header.module';
import { BreadcrumbsModule } from 'app/core/breadcrumbs/breadcrumbs.module';
import { FiltersComponent } from './filters.component';
import { FiltersRoutingModule } from './filters.routing';
import { ChartModule } from 'angular-highcharts';


@NgModule({
  imports: [
    CommonModule,
    FiltersRoutingModule,
    FormsModule,
    FlexLayoutModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatInputModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    MatDialogModule,
    ChartModule,
    MatSelectModule, MatOptionModule,
    // MatDatepicker,
    MatDatepickerModule,
    MatNativeDateModule,
    // Core
    ListModule,
    PageHeaderModule,
    BreadcrumbsModule
  ],
  declarations: [FiltersComponent],
  exports: [FiltersComponent]
})
export class FiltersModule {

}
