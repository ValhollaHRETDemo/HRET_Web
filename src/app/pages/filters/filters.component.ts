import { Component, OnInit, Input, OnChanges, SimpleChanges, EventEmitter, Output, NgModule, ViewChild } from '@angular/core';
import { FilterViewModel } from '../../viewmodel/filter.viewmodel'
//import { MatDatepicker } from '@angular/material';

@Component({
  selector: 'hret-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})

export class FiltersComponent implements OnInit, OnChanges {

  constructor() { }
  @Input() viewModel: FilterViewModel;
  currentDate = new Date();

  ngOnInit() {

  }

  ngOnChanges(changes: SimpleChanges) {
  }

  filterChanged() { this.viewModel.filterChanged() }

  filter() {
    this.viewModel.applyFilter();
  }

  clearFilters() {
    this.viewModel.reset();
  }
}
