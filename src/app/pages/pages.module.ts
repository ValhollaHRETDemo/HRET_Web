import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthModule } from './auth/auth.module';
import { GoogleMapsComponent } from './google-maps/google-maps.component';

import { PageHeaderModule } from 'app/core/page-header/page-header.module';
import { BreadcrumbsModule } from 'app/core/breadcrumbs/breadcrumbs.module';
//import { DomainComponent } from './domain/domain.component';

@NgModule({
  imports: [
    CommonModule,
    AuthModule,PageHeaderModule,BreadcrumbsModule
  ],
 // declarations: [DomainComponent]
})
export class PagesModule { }
