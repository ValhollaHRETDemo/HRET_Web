import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort } from '@angular/material';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/filter';
import { Observable } from 'rxjs/Observable';
import { List } from '../../core/list/list.interface';
import { Domain } from './domain-create-update/domain.model';
import { ListColumn } from '../../core/list/list-column.model';
import { ListDataSource } from '../../core/list/list-datasource';
import { ListDatabase } from '../../core/list/list-database';
import { componentDestroyed } from '../../core/utils/component-destroyed';
import { DomainCreateUpdateComponent } from './domain-create-update/domain-create-update.component';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { ROUTE_TRANSITION } from '../../app.animation';
import { Subscription } from 'rxjs/Subscription';
import { DomainManagementService } from 'app/providers/domainmanagement.service';


@Component({
  selector: 'vr-domain',
  templateUrl: './domain.component.html',
  styleUrls: ['./domain.component.scss'],
  animations: [...ROUTE_TRANSITION],
  host: { '[@routeTransition]': '' }
})

export class DomainComponent implements List<Domain>, OnInit, OnDestroy  {
  subject$: ReplaySubject<Domain[]> = new ReplaySubject<Domain[]>(1);
  data$: Observable<Domain[]>;
  domains: Domain[];

  message: string = "";
  showMessage: boolean = false;
  private subscription: Subscription;
  private timer: Observable<any>;

  @Input()
  columns: ListColumn[] = [
    { name: 'Domain', property: 'domain', visible: true, isModelProperty: true },
    { name: 'Domain Description', property: 'domain_desc', visible: true, isModelProperty: true },
    { name: 'Actions', property: 'actions', visible: true }
  ] as ListColumn[];
  pageSize = 10;
  resultsLength: number;
  dataSource: ListDataSource<Domain> | null;
  database: ListDatabase<Domain>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  constructor(private dialog: MatDialog, private domainManagementService: DomainManagementService) {
  }

  ngOnInit() {
    this.getDomain();
  }

  getDomain() {
    this.domainManagementService.getDomain().subscribe(Domain_Data => {
      this.domains = Domain_Data.map(domain => new Domain(domain));

      this.subject$.next(this.domains);
      this.data$ = this.subject$.asObservable();
      this.database = new ListDatabase<Domain>();
      this.data$
        .takeUntil(componentDestroyed(this))
        .filter(Boolean)
        .subscribe((domains) => {
          this.domains = domains;
          this.database.dataChange.next(domains);
          this.resultsLength = domains.length;
        });

      this.dataSource = new ListDataSource<Domain>(this.database, this.sort, this.paginator, this.columns);
    });
  }

  createDomain() {
    this.dialog.open(DomainCreateUpdateComponent).afterClosed().subscribe((domain: Domain) => {
      if (domain) {
        this.domains.unshift(new Domain(domain));
        this.subject$.next(this.domains);
        this.domainManagementService.showToast("success", "Domain saved successfully");
      }
    });
  }

  updateDomain(domain) {
    this.dialog.open(DomainCreateUpdateComponent, {
      data: domain
    }).afterClosed().subscribe((domain) => {
      if (domain) {
        const index = this.domains.findIndex((existingDomain) => existingDomain.domain_id === domain.domain_id);
        this.domains[index] = new Domain(domain);
        this.subject$.next(this.domains);
        this.domainManagementService.showToast("success", "Domain updated successfully");
      }
    });
  }

  deleteDomain(domain) {
    this.domainManagementService.deleteDomain(domain).subscribe(p => {
      this.domains.splice(this.domains.findIndex((existingDomain) => existingDomain.domain_id === domain.domain_id), 1);
      this.subject$.next(this.domains);
    this.domainManagementService.showToast("success", "Domain deleted successfully");
    });
  }

  onFilterChange(value) {
    if (!this.dataSource) {
      return;
    }
    this.dataSource.filter = value;
  }

  ngOnDestroy() {
  }

  setTimer(msg, isSuccess) {

    // set showloader to true to show loading div on view
    this.showMessage = true;
    this.message = msg;

    this.timer = Observable.timer(4000); // 4000 millisecond means 4 seconds
    this.subscription = this.timer.subscribe(() => {
      // set showloader to false to hide loading div from view after 5 seconds
      this.showMessage = false;
      this.message = "";
    });
  }

}
