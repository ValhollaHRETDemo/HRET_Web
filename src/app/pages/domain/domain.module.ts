import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatDialogModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatPaginatorModule,
  MatSortModule,
  MatTableModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { DomainCreateUpdateModule } from './domain-create-update/domain-create-update.module';
import { ListModule } from 'app/core/list/list.module';
import { PageHeaderModule } from 'app/core/page-header/page-header.module';
import { BreadcrumbsModule } from 'app/core/breadcrumbs/breadcrumbs.module';
import { DomainComponent } from 'app/pages/domain/domain.component';
import { DomainRoutingModule } from 'app/pages/domain/domain.routing';
//import { DomainCreateUpdateComponent } from './domain-create-update/domain-create-update.component';


@NgModule({
  imports: [
    CommonModule,
    DomainRoutingModule,
    FormsModule,
    FlexLayoutModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatInputModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    MatDialogModule,

    // Core
    ListModule,
    DomainCreateUpdateModule,
    PageHeaderModule,
    BreadcrumbsModule
  ],
  declarations: [DomainComponent],
  exports: [DomainComponent]
})
export class DomainModule { }
