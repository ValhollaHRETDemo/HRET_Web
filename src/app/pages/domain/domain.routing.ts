import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { DomainComponent } from 'app/pages/domain/domain.component';

const routes: Routes = [
  {
    path: '',
    component: DomainComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DomainRoutingModule { }
