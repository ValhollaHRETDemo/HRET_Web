import { Component, Inject, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../../reducers/index';
import { Domain } from './domain.model';
import { DomainManagementService } from 'app/providers/domainmanagement.service';
import { validateFormData } from '../../validateformdata';
import { genericFunctions } from '../../genericfunctions';

@Component({
  selector: 'vr-domain-create-update',
  templateUrl: './domain-create-update.component.html',
  styleUrls: ['./domain-create-update.component.scss']
})
export class DomainCreateUpdateComponent implements OnInit {

  form: FormGroup;
  mode: 'create' | 'update' = 'create';
  _validateformdata: any;
  _genericfunction: genericFunctions;
  responseMessage:string;

  showMessage:boolean;
  message:string;
  timer:Observable<number>;
  subscription:any;

  constructor(@Inject(MAT_DIALOG_DATA) public defaults: any,
    private dialogRef: MatDialogRef<DomainCreateUpdateComponent>,
    private fb: FormBuilder,
    private store: Store<fromRoot.State>,
    private domainManagementService: DomainManagementService,
  ) {

    this._validateformdata = new validateFormData();
    this._genericfunction = new genericFunctions();
  }

  ngOnInit() {
    this.fillDefaults();
  }

  fillDefaults() {

    if (this.defaults) {
      this.mode = 'update';
      
      this.form = this.fb.group({
        domain_id: [this.defaults.domain_id || 0],
        domain: [this.defaults.domain || '',],
        is_active: this.defaults.is_active || '0',
        created_by: this.defaults.created_by || '',
        created_on: this.defaults.created_on || '',
        modified_by: this.defaults.modified_by || null,
        modified_on: this.defaults.modified_on || null,
        domain_desc: this.defaults.domain_desc || ''
      });

    } else {
      this.defaults = {} as Domain;

      this.form = this.fb.group({
        //domain_id: [this.defaults.domain_id || 0],
        domain: [this.defaults.domain || '',],
        is_active: this.defaults.is_active || '',
        created_by: this.defaults.created_by || '',
        created_on: this.defaults.created_on || '',
        domain_desc: this.defaults.domain_desc || ''
      });

    }
  }

  save() {
    if (this.mode === 'create') {
      this.createDomain();
    } else if (this.mode === 'update') {
      this.updateDomain();
    }
  }

  createDomain() {
    const domain = this.form.value;
    domain.is_active = "1";
    domain.created_by = 1;
    domain.created_on = new Date();
    domain.domain = this._genericfunction.trimData(domain.domain);
    if(this._validateformdata.validateDomain(domain.domain).status == true)
    {
      this.domainManagementService.saveDomain(domain).subscribe(p => {
        this.dialogRef.close(p);
      });  
    }
    else
    {
      this.setTimer(this._validateformdata.validateDomain(domain.domain).message, true);  
    }
  }

  public setTimer(msg, isSuccess) {

    // set showloader to true to show loading div on view
    this.showMessage = true;
    this.message = msg;


    this.timer = Observable.timer(3000); // 4000 millisecond means 4 seconds
    this.subscription = this.timer.subscribe(() => {
      // set showloader to false to hide loading div from view after 5 seconds
      this.showMessage = false;
      this.message = "";
    });
  }

  updateDomain() {
    const domain = this.form.value;
    domain.is_active = "1";
    domain.modified_by = 1;
    domain.modified_on = new Date();
    domain.domain = this._genericfunction.trimData(domain.domain);
    if(this._validateformdata.validateDomain(domain.domain).status == true)
    {
      this.domainManagementService.saveDomain(domain).subscribe(p => {
        this.dialogRef.close(p);
      }); 
    }
    else
    {
      this.setTimer(this._validateformdata.validateDomain(domain.domain).message, true);  
    }
    
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }


}
