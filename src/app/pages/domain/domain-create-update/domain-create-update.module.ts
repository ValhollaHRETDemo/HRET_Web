import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DomainCreateUpdateComponent } from './domain-create-update.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatDialogModule, MatIconModule, MatInputModule, MatRadioModule, MatSelectModule, MatCheckboxModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatDialogModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatRadioModule,
    MatSelectModule,
    MatCheckboxModule
  ],
  declarations: [DomainCreateUpdateComponent],
  entryComponents: [DomainCreateUpdateComponent],
  exports: [DomainCreateUpdateComponent]
})
export class DomainCreateUpdateModule {
}
