export class Domain {
  domain_id: number;
  domain: string;
  is_active: string;
  created_by: number;
  created_on: Date;
  modified_by: number;
  modified_on: Date;
  domain_desc: string;

  constructor(domainInformation) {
    this.domain_id = domainInformation.domain_id;
    this.domain = domainInformation.domain;
    this.is_active = domainInformation.is_active;
    this.created_by = domainInformation.created_by;
    this.created_on = domainInformation.created_on;
    this.modified_by = domainInformation.modified_by;
    this.modified_on = domainInformation.modified_on;
    this.domain_desc = domainInformation.domain_desc;
  }

}
