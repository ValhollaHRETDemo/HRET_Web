
export class validateFormData {
  constructor() {

  }

  validateDropdownSelection(text: String) {
    console.log(text);
    if (text != undefined || text != null || text != "")
      return { status: true, message: "Dropdown is selected" };
    else
      return { status: false, message: "Select a valid item" };
  }

  validateString(text: string): any {
    if ((/^[a-zA-Z ]+$/.test(text)))
      return { status: true, message: "Valid String" };
    else
      return { status: false, message: "Please enter alphabetical string" };

  }
  validateTextLength(text: string, max: number): any {
    if (text.length >= 1 && text.length <= max)
      return { status: true, message: "Text of acceptable length" };
    else
      return { status: false, message: "Maximum of " + max + "chars is allowed" };
  }


  validateEmail(email: string): any {
    if ((/\S+@\S+\.\S+/).test(email))
      return { status: true, message: "" };
    else
      return { status: false, message: "Please enter valid email" };
  }
  validateDomain(domain: string): any {
    var domainName = (/\S+\.\S+/).test(domain);
    if (domainName == true)
      return { status: true, message: "" };

    else
      return { status: false, message: "Please enter valid domain" };
  }

  validatePhoneNumber(text: number) {
    var textStringified = text.toString();
    if (textStringified.length != 10) {
      return { status: false, message: "Please enter 10 digits" };
    }
    else {
      return { status: true, message: "Valid phone number" };
    }
  }

  validateZipCode(text: number) {
    var textStringified = text.toString();
    if (textStringified.length != 6) {
      return { status: false, message: "Please enter 6 digits" };
    }
    else {
      return { status: true, message: "Valid Zip Code" };
    }
  }
  validateUnitCode(text: String) {
    if (text.length >= 1 && text.length <= 5) {
      return { status: true, message: "Valid Unit Code" };
    }
    else {
      return { status: false, message: "Maximum of 5 chars is allowed" };
    }
  }
}



