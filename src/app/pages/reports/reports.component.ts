import { Component, OnInit } from '@angular/core';
import { Chart } from 'angular-highcharts';
import { ReportsService } from '../../providers/reports.service'
import { Input, OnDestroy, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort } from '@angular/material';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/filter';
import { Observable } from 'rxjs/Observable';
import { Customer } from 'app/pages/tables/all-in-one-table/customer-create-update/customer.model';
import { ListColumn } from 'app/core/list/list-column.model';
import { ListDataSource } from 'app/core/list/list-datasource';
import { ListDatabase } from 'app/core/list/list-database';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { ROUTE_TRANSITION } from 'app/app.animation';
import * as _ from "underscore";

@Component({
  selector: 'vr-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss'],
  animations: [...ROUTE_TRANSITION],
  host: { '[@routeTransition]': '' }
})
export class ReportsComponent implements OnInit {

  constructor(private reportsService: ReportsService, private dialog: MatDialog) { }

  public isLoaderContainer: boolean = false;
  public isChartContainer: boolean = false;

  stateCode: string;
  hospitalId: string;


  subject$: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);
  data$: Observable<any[]>;
  customers: any[];

  comparables = [];
  selectedComparable = null;

  numberOfHospitals = [];
  selectedNumberOfHospitals = null;

  percentiles = [];
  selectedPercentile = null;

  teachingList = [];
  selectedTeaching = null;

  rurals = [];
  selectedRural = null;

  measureId = [];
  selectedMeasure = null;
  selectedMeasureText = "";

  occupancies = [];
  selectedOccupancy = null;


  @Input()
  columns: ListColumn[] = [] as ListColumn[];

  pageSize = 10;
  resultsLength: number;
  dataSource: ListDataSource<Customer> | null;
  database: ListDatabase<Customer>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  ngOnInit() {
    // this.getComparables();
    // this.getMeasures();
    // this.getHospitals();
    // this.getOccupancies();
    // this.getPercentiles();
    // this.getTeachingData();
    // this.getRuralDropdownData();
    // this.clearFilters();
  }


  ngOnDestroy() {

  }

  chart = new Chart({
    chart: {
      type: 'line',
    },
    title: {
      text: ''
    },
    subtitle: {
      text: ''
    },
    xAxis: {
      categories: [
        "BL",
        "2016-10",
        "2016-11",
        "2016-12",
        "2017-01",
        "2017-02",
        "2017-03",
        "2017-04",
        "2017-05",
        "2017-06",
        "2017-07",
        "2017-08",
        "2017-09",
        "2017-10"
      ]
    },
    yAxis: {
      title: {
        text: 'Average'
      }
    },
    plotOptions: {
      line: {
        dataLabels: {
          enabled: true
        },
        enableMouseTracking: true
      }
    },
    series: [],
    exporting: {
      enabled: false
    }
  });

  initChart() {

    this.chart = new Chart({
      chart: {
        type: 'line',
      },
      title: {
        text: ''
      },
      subtitle: {
        text: ''
      },
      xAxis: {
        categories: [
          "BL",
          "2016-10",
          "2016-11",
          "2016-12",
          "2017-01",
          "2017-02",
          "2017-03",
          "2017-04",
          "2017-05",
          "2017-06",
          "2017-07",
          "2017-08",
          "2017-09",
          "2017-10"
        ]
      },
      yAxis: {
        title: {
          text: 'Average'
        }
      },
      plotOptions: {
        line: {
          dataLabels: {
            enabled: true
          },
          enableMouseTracking: true
        }
      },
      series: []
    });

  }


  //  getReports() {

  //   var valueMeasure = this.selectedMeasure;
  //   this.selectedMeasureText = this.measureId.filter(function (object) { return object.id == valueMeasure; })[0].name;

  //   this.isChartContainer = false;
  //   this.isLoaderContainer = true;
  //   var reportLevel = "";
  //   this.chart.options.series = [];

  //   var userDetails = JSON.parse(localStorage.getItem("userDetails"));
  //   this.stateCode = userDetails.stateCode;
  //   this.hospitalId = userDetails.hospitalId;


  //   if (userDetails.role == "Hospital Admin") {

  //     reportLevel = "Hospital";
  //     var data = this.reportsService.getReports(this.selectedMeasure, null, null, "yearmonth,round",
  //       "National", this.selectedRural, this.selectedOccupancy,
  //       this.selectedTeaching).subscribe(p => {

  //         this.addSeries("National", p.round.map(parseFloat));

  //         this.isLoaderContainer = false;
  //         this.isChartContainer = true;
  //         this.fillDatatable(p);
  //       });

  //     var data = this.reportsService.getReports(this.selectedMeasure, userDetails.stateCode, null,
  //       "yearmonth,round", "State", this.selectedRural,
  //       this.selectedOccupancy, this.selectedTeaching).subscribe(p => {
  //         this.addSeries("State", p.round.map(parseFloat));
  //       });

  //     var data = this.reportsService.getReports(this.selectedMeasure, userDetails.stateCode, userDetails.hospitalId,
  //       "yearmonth,double", "Hospital", this.selectedRural, this.selectedOccupancy,
  //       this.selectedTeaching).subscribe(p => {
  //         this.addSeries("Hospital", p.double.map(parseFloat));
  //       });
  //   }
  //   else if (userDetails.role == "State Admin") {
  //     reportLevel = "State";

  //     this.navigationDetailsAdmin(userDetails.stateCode, null, true);

  //   } else {
  //     reportLevel = "National";

  //     var data = this.reportsService.getReports(this.selectedMeasure, null, null, "yearmonth,round", reportLevel,
  //       this.selectedRural, this.selectedOccupancy,
  //       this.selectedTeaching).subscribe(p => {
  //         this.isLoaderContainer = false;
  //         this.isChartContainer = true;
  //         this.addSeries("National", p.round.map(parseFloat));
  //       });

  //     var data = this.reportsService.getReports(this.selectedMeasure, null, null, "yearmonth,round,state_code",
  //       "State", this.selectedRural, this.selectedOccupancy,
  //       this.selectedTeaching).subscribe(p => {
  //         this.populateGraph(p, "state_code", "State", "round")
  //       });
  //   }

  // }


  // navigationDetailsAdmin(stateCode, hospitalCode, isState) {

  //   if (isState) {
  //     var data = this.reportsService.getReports(this.selectedMeasure, null, null, "yearmonth,round", "National",
  //       this.selectedRural, this.selectedOccupancy,
  //       this.selectedTeaching).subscribe(p => {
  //         this.addSeries("National", p.round.map(parseFloat));
  //         this.isLoaderContainer = false;
  //         this.isChartContainer = true;
  //       });

  //     var data = this.reportsService.getReports(this.selectedMeasure, stateCode, null, "yearmonth,round",
  //       "State", this.selectedRural, this.selectedOccupancy,
  //       this.selectedTeaching).subscribe(p => {
  //         this.addSeries(stateCode, p.round.map(parseFloat));

  //       });

  //     var data = this.reportsService.getReports(this.selectedMeasure, stateCode, null,
  //       "yearmonth,hiin_id,double", "Hospital", this.selectedRural, this.selectedOccupancy,
  //       this.selectedTeaching).subscribe(p => {
  //         this.populateGraph(p, "hiin_id", "Hospital", "double");
  //       });
  //   }
  //   else {
  //     var data = this.reportsService.getReports(this.selectedMeasure, stateCode, hospitalCode,
  //       "yearmonth,hiin_id,double", "Hospital", this.selectedRural, this.selectedOccupancy,
  //       this.selectedTeaching).subscribe(p => {
  //         this.populateGraph(p, "hiin_id", "Hospital", "double");
  //       });

  //     var data = this.reportsService.getReports(this.selectedMeasure, null, null, "yearmonth,round",
  //       "National", this.selectedRural, this.selectedOccupancy,
  //       this.selectedTeaching).subscribe(p => {
  //         var nationalFlag = false;
  //         this.chart.ref.series.forEach(data => {
  //           if (data.name == "National") {
  //             nationalFlag = true;
  //           }
  //         });
  //         if (!nationalFlag) {
  //           this.addSeries("National", p.round.map(parseFloat));
  //         }

  //         // this.addSeries("National3333", p.round.map(parseFloat));
  //         //this.isLoaderContainer = false;
  //         //this.isChartContainer = true;
  //         //this.fillDatatable(p);
  //       });

  //     var data = this.reportsService.getReports(this.selectedMeasure, stateCode, null,
  //       "yearmonth,round", "State", this.selectedRural,
  //       this.selectedOccupancy, this.selectedTeaching).subscribe(p => {
  //         this.addSeries("State", p.round.map(parseFloat));
  //       });


  //   }
  // }


  // populateGraph(p, groupByColumnName, entityName, valueColumn) {
  //   debugger;
  //   var datatable = { columns: [], rows: [] };
  //   var row = { seriesName: "", data: [] }
  //   var groupByColValue = p[groupByColumnName][0];

  //   datatable.columns = _(p.yearmonth).chain().uniq().value();

  //   p.yearmonth.forEach((element, index) => {
  //     if (p[groupByColumnName][index] != groupByColValue) {
  //       row.seriesName = groupByColValue;
  //       datatable.rows.push(row)
  //       row = { seriesName: "", data: [] };
  //       groupByColValue = p[groupByColumnName][index];
  //     }
  //     row.data.push(parseFloat(p[valueColumn][index]));
  //   });

  //   if (row.data.length != 0) { datatable.rows.push(row); row.seriesName = groupByColValue; }

  //   datatable.rows.forEach(element => {
  //     this.addSeries(element.seriesName, element.data);
  //   })
  //   this.fillDetailDatatable(datatable, entityName);
  //   //this.hideTransactionalData();
  // }



  // fillDetailDatatable(datatable, entityName) {
  //   this.columns = [];

  //   var nameList = new ListColumn();
  //   nameList.name = entityName;
  //   nameList.property = entityName;
  //   nameList.visible = true;
  //   nameList.isModelProperty = true;
  //   this.columns.push(nameList);

  //   datatable.columns.forEach(element => {
  //     var list = new ListColumn();
  //     list.name = element;
  //     list.property = element;
  //     list.visible = true;
  //     list.isModelProperty = true;
  //     this.columns.push(list);
  //   });

  //   var table = [];
  //   var row = {};
  //   datatable.rows.forEach((element, index) => {
  //     row = {};
  //     row[entityName] = element.seriesName;
  //     datatable.columns.forEach((column, index) => {
  //       row[column] = element.data[index];
  //     });
  //     table.push(row)
  //   });

  //   this.subject$.next(table);
  //   this.data$ = this.subject$.asObservable();

  //   this.database = new ListDatabase<any>();
  //   this.data$.filter(Boolean)
  //     .subscribe((customers) => {
  //       this.customers = customers;
  //       this.database.dataChange.next(customers);
  //       this.resultsLength = customers.length;
  //     });

  //   this.dataSource = new ListDataSource<Customer>(this.database, this.sort, this.paginator, this.columns);
  // }

  // navigate(rowData) {
  //   this.initChart();

  //   if (rowData["State"] != null) {
  //     this.stateCode = rowData["State"];
  //     this.navigationDetailsAdmin(rowData["State"], null, true);
  //   }
  //   else if (rowData["Hospital"] != null) {
  //     this.hospitalId = rowData["Hospital"];
  //     this.navigationDetailsAdmin(this.stateCode, rowData["Hospital"], false);
  //   }
  // }

  // addSeries(name, data) {
  //   this.chart.addSerie({
  //     name: name,
  //     data: data,
  //     dashStyle: (name === "National" || name === "State") ? "DashDot" : "Solid",
  //     lineWidth: (name === "National" || name === "State") ? 3 : 1,
  //   });
  // }


  // getComparables() {
  //   //Comparables options
  //   this.comparables.push({ id: "1", name: "Random Hospitals within State" });
  //   this.comparables.push({ id: "2", name: "Random Hospitals based on Occupancy" });
  //   this.comparables.push({ id: "3", name: "Random Hospitals based in Rural Areas" });
  //   this.comparables.push({ id: "4", name: "Random teaching Hospitals" });
  //   this.comparables.push({ id: "5", name: "95 Percentile" });
  //   this.comparables.push({ id: "6", name: "80 Percentile" });
  //   this.comparables.push({ id: "7", name: "20 Percentile" });
  //   this.comparables.push({ id: "8", name: "5 Percentile" });
  // }


  // getMeasures() {
  //   this.measureId.push({ id: "HIIN-ADE-1a", name: "ADE - Excessive Anticoagulation" });
  //   this.measureId.push({ id: "HIIN-ADE-1b", name: "ADE - Hypoglycemia" });
  //   this.measureId.push({ id: "HIIN-ADE-1c", name: "ADE - Opioids" });
  // }


  // getHospitals() {
  //   this.numberOfHospitals.push({ id: "1", name: "3 Hospitals" });
  //   this.numberOfHospitals.push({ id: "2", name: "5 Hospitals" });
  //   this.numberOfHospitals.push({ id: "3", name: "10 Hospitals" });
  // }


  // getOccupancies() {
  //   this.occupancies.push({ id: "'< 25'", name: "< 25" });
  //   this.occupancies.push({ id: "'25 - 99'", name: "25 - 99" });
  //   this.occupancies.push({ id: "'100 - 299'", name: "100 - 299" });
  //   this.occupancies.push({ id: "'> 300'", name: "> 300" });
  // }

  // getPercentiles() {
  //   this.percentiles.push({ id: "1", name: "95" });
  //   this.percentiles.push({ id: "2", name: "80" });
  //   this.percentiles.push({ id: "3", name: "20" });
  //   this.percentiles.push({ id: "4", name: "5" });
  // }

  // getTeachingData() {
  //   this.teachingList.push({ id: "'Critical access hospitals'", name: "Critical access hospitals" });
  //   this.teachingList.push({ id: "'Medium hospitals'", name: "Medium hospitals" });
  //   this.teachingList.push({ id: "'Teaching hospitals'", name: "Teaching hospitals" });
  //   this.teachingList.push({ id: "'Large hospitals'", name: "Large hospitals" });
  //   this.teachingList.push({ id: "'Small hospitals'", name: "Small hospitals" });
  //   this.teachingList.push({ id: "'Rural hospitals'", name: "Rural hospitals" });
  //   this.teachingList.push({ id: "'Children's hospitals'", name: "Children's hospitals" });
  // }

  // getRuralDropdownData() {
  //   this.rurals.push({ id: "'Y'", name: "Yes" });
  //   this.rurals.push({ id: "'N'", name: "No" });
  //   //this.rurals.push({ id: "3", name: "Not Available" }
  // }


  // fillDatatable(data) {

  //   this.columns = [];

  //   var nameList = new ListColumn();
  //   nameList.name = "Name";
  //   nameList.property = "Name";
  //   nameList.visible = true;
  //   nameList.isModelProperty = true;
  //   this.columns.push(nameList);


  //   data.yearmonth.forEach(element => {
  //     var list = new ListColumn();
  //     list.name = element;
  //     list.property = element;
  //     list.visible = true;
  //     list.isModelProperty = true;
  //     this.columns.push(list);
  //   });

  //   var datatable = [];
  //   var table = {};
  //   data.yearmonth.forEach((element, index) => {
  //     table[element] = data.round[index];
  //   });

  //   table["Name"] = this.hospitalId;

  //   datatable.push(table);
  //   this.subject$.next(datatable);
  //   this.data$ = this.subject$.asObservable();

  //   this.database = new ListDatabase<any>();
  //   this.data$.filter(Boolean)
  //     .subscribe((customers) => {
  //       this.customers = customers;
  //       this.database.dataChange.next(customers);
  //       this.resultsLength = customers.length;
  //     });

  //   this.dataSource = new ListDataSource<Customer>(this.database, this.sort, this.paginator, this.columns);
  // }

  // clearFilters() {
  //   this.selectedComparable = null;
  //   this.selectedNumberOfHospitals = null;
  //   this.selectedOccupancy = null;
  //   this.selectedPercentile = null;
  //   this.selectedRural = null;
  //   this.selectedTeaching = null;
  //   this.selectedMeasure = this.measureId[0].id;
  //   this.selectedMeasureText = this.measureId[0].name;
  //   this.getReports();
  // }


  // showTransactionalData() {
  //   this.chart.ref.series.forEach(serie => {
  //     serie.show();
  //   });
  // }

  // hideTransactionalData() {
  //   this.chart.ref.series.forEach(serie => {
  //     if (serie.name !== "National" && serie.name !== "State" && serie.name !== "Hospital" && serie.name !== this.stateCode && serie.name !== this.hospitalId) {
  //       serie.hide();
  //     }
  //   });
  // }

  // onFilterChange(value) {
  //   if (!this.dataSource) {
  //     return;
  //   }
  //   this.dataSource.filter = value;
  // }

}
