import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { ROUTE_TRANSITION } from '../../../app.animation';
import * as fromRoot from '../../../reducers/index';
import { Store } from '@ngrx/store';

declare var Plotly: any;

@Component({
  selector: 'vr-dashboard-statistics',
  templateUrl: './dashboard-statistics.component.html',
  styleUrls: ['./dashboard-statistics.component.scss'],
  animations: [...ROUTE_TRANSITION],
  host: { '[@routeTransition]': '' }
})

export class DashboardStatisticsComponent implements OnInit {

  @Input() hideHeader: boolean;

  layoutGap = {
    'lt-md': '16px',
    'gt-md': '24px'
  };

  flexWidth = {
    'lt-sm': 'auto',
    'gt-sm': `calc(50% - ${this.layoutGap['lt-md']}`,
    'gt-md': `calc(50% - ${this.layoutGap['gt-md']}`
  };

  layout: string;

  layoutColumnOnBoxed = 'row';

  constructor(
    private store: Store<fromRoot.State>,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit() {

    this.store.select(fromRoot.getLayout).subscribe((layout) => {
      this.layout = layout;

      if (layout === 'gamma') {
        this.layoutColumnOnBoxed = 'column';
      } else {
        this.layoutColumnOnBoxed = 'row';
      }

      this.cd.markForCheck();
    });

    Plotly.d3.csv('https://raw.githubusercontent.com/plotly/datasets/master/2011_us_ag_exports.csv', function (err, rows) {
      function unpack(rows, key) {
        return rows.map(function (row) { return row[key]; });
      }

      var data = [{
        type: 'choropleth',
        locationmode: 'USA-states',
        locations: unpack(rows, 'code'),
        //z: unpack(rows, 'beef'),
        text: unpack(rows, 'state'),
        zmin: 0,
        zmax: 1000,
        colorscale: [
          [0, 'rgb(242,240,247)'], [0.2, 'rgb(218,218,235)'],
          [0.4, 'rgb(188,189,220)'], [0.6, 'rgb(158,154,200)'],
          [0.8, 'rgb(117,107,177)'], [1, 'rgb(84,39,143)']
        ],
        colorbar: {
          title: 'HRET Participants',
          thickness: 0.2
        },
        marker: {
          line: {
            color: 'rgb(255,255,255)',
            width: 2
          }
        }
      }];

      var layout = {
        title: 'National Data',
        //width: "1200",
        geo: {
          scope: 'usa',
          showlakes: true,
          lakecolor: 'rgb(255,255,255)'
        }
      };

      Plotly.plot("divNationalDataChart", data, layout, { showLink: false });

    });

  }
}