import { Component, Input, OnInit } from '@angular/core';
import { MatCard, MatCardContent, MatCardSubtitle } from '@angular/material';
import { ROUTE_TRANSITION } from '../../../app.animation';
import { RouterModule, Routes } from '@angular/router';

@Component({
  selector: 'vr-dashboard-admin',
  templateUrl: './dashboard-admin.component.html',
  styleUrls: ['./dashboard-admin.component.scss'],
  animations: [...ROUTE_TRANSITION],
  host: { '[@routeTransition]': '' }
})
export class DashboardAdminComponent implements OnInit {

  @Input() hideHeader: boolean;

  layoutGap = {
    'lt-md': '16px',
    'gt-md': '24px'
  };

  flexWidth = {
    'lt-sm': 'auto',
    'gt-sm': `calc(50% - ${this.layoutGap['lt-md']}`,
    'gt-md': `calc(50% - ${this.layoutGap['gt-md']}`
  };

  constructor() { }

  ngOnInit() {
  }

}
