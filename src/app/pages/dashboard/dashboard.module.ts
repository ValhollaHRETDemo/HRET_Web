import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatIconModule, MatTabsModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { ScrollbarModule } from '../../core/scrollbar/scrollbar.module';
import { DashboardCrmModule } from './dashboard-crm/dashboard-crm.module';
import { DashboardStatisticsModule } from './dashboard-statistics/dashboard-statistics.module';
import { DashboardRoutingModule } from './dashboard.routing';
import { DashboardAdminModule } from './dashboard-admin/dashboard-admin.module';
import { StateWidgetModule } from '../dashboard/dashboard-statistics/state-widget/state-widget.module';
import { BarchartWidgetModule } from '../dashboard/dashboard-statistics/barchart-widget/barchart-widget.module';
import { SalesWidgetModule } from '../dashboard/dashboard-statistics/sales-widget/sales-widget.module';
import { RecentsalesWidgetModule } from '../dashboard/dashboard-statistics/recentsales-widget/recentsales-widget.module';
import { MarketWidgetModule } from '../dashboard/dashboard-crm/market-widget/market-widget.module';
import { AreachartWidgetModule } from 'app/pages/dashboard/dashboard-statistics/areachart-widget/areachart-widget.module';
import { AdvancedPieChartWidgetModule } from 'app/pages/dashboard/dashboard-statistics/advanced-pie-chart-widget/advanced-pie-chart-widget.module';
import { MapsWidgetModule } from 'app/pages/dashboard/dashboard-statistics/maps-widget/maps-widget.module';
import { ProjectWidgetModule } from 'app/pages/dashboard/dashboard-crm/project-widget/project-widget.module';

@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    FormsModule,
    FlexLayoutModule,
    MatIconModule,
    MatTabsModule,
    MatButtonModule,
    ScrollbarModule,

    // Dashboards
    DashboardCrmModule,
    DashboardStatisticsModule,
    DashboardAdminModule,

    // Widgets
    StateWidgetModule,
    BarchartWidgetModule,
    AreachartWidgetModule,
    RecentsalesWidgetModule,
    AdvancedPieChartWidgetModule,
    MarketWidgetModule,
    MapsWidgetModule,
    SalesWidgetModule,
    ProjectWidgetModule
  ],
  declarations: [
    DashboardComponent
  ]
})
export class DashboardModule { }
