import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ROUTE_TRANSITION } from '../../../app.animation';
import { UserService } from '../../../providers/user.service';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs';
import { sha256 } from 'hash.js';

@Component({
    selector: 'vr-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [...ROUTE_TRANSITION],
    host: { '[@routeTransition]': '' }
})


export class LoginComponent implements OnInit {

    email: string;
    password: string;
    message: string = "";
    showMessage: boolean = false;
    private subscription: Subscription;
    private timer: Observable<any>;

    constructor(private authRoutes: Router, private userService: UserService) { }

    ngOnInit() {
        localStorage.clear();
    }

    login() {
        debugger;
        var hashedPassword = sha256().update(this.password).digest('hex')
        var credentials = { "email_id": this.email.toLocaleLowerCase(), "user_pwd": hashedPassword };
        this.userService.login(credentials).subscribe(
            response => {
                localStorage.setItem("userDetails", JSON.stringify(response));
                if (response.is_auto_password === "1") {
                    this.authRoutes.navigate(['/auth/changepassword']);
                } else {
                    this.authRoutes.navigate(['/auth/verification']);
                }
            },
            error => {
                this.validationMessage(error.error.message);
            });
    }

    public validationMessage(message) {

        // set showloader to true to show loading div on view
        this.showMessage = true;
        this.message = message;

        this.timer = Observable.timer(4000); // 4000 millisecond means 4 seconds
        this.subscription = this.timer.subscribe(() => {
            // set showloader to false to hide loading div from view after 5 seconds
            this.showMessage = false;
            this.message = "";
        });
    }

}
