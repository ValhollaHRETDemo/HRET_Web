import { NgModule } from '@angular/core';
import { RouterModule } from "@angular/router";
import { CommonModule } from '@angular/common';
import { LoginModule } from './login/login.module';
import { RegisterModule } from './register/register.module';
import { ForgotPasswordModule } from './forgot-password/forgot-password.module';
import { VerificationModule } from './verification/verification.module';
import { ChangePasswordModule } from './change-password/change-password.module';
import { authRoutes } from 'app/pages/auth/auth.routing';

@NgModule({
  imports: [
    CommonModule,
    LoginModule,
    RegisterModule,
    ForgotPasswordModule,
    VerificationModule,
    ChangePasswordModule,
    RouterModule.forRoot(authRoutes, { useHash: true })
  ],
  declarations: []
})
export class AuthModule { }
