import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'app/providers/user.service';
import { UserRoleService } from 'app/providers/userrole.service';
import { RoleService } from 'app/providers/role.service';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs';

const appConfig = require("../../../../config")
@Component({
  selector: 'vr-verification',
  templateUrl: './verification.component.html',
  styleUrls: ['./verification.component.scss']
})
export class VerificationComponent implements OnInit {

  authCode: string;
  message: string = "";
  showMessage: boolean = false;
  private subscription: Subscription;
  private timer: Observable<any>;

  constructor(private router: Router,
    private userService: UserService,
    private userRoleService: UserRoleService,
    private roleService: RoleService) { }

  ngOnInit() {
  }

  verify() {

    debugger;
    var user_details = (JSON.parse(localStorage.getItem("userDetails")));
    var verificationRespose = this.userService.verification({ user_id: user_details.user_id, auth_code: this.authCode }).subscribe(
      response => {
        if (response.result) {
          localStorage.setItem("authToken", JSON.stringify(response.token));
          localStorage.setItem("userDetails", JSON.stringify(appConfig.decodeToken(response.token)));
          this.router.navigate(['/dashboard']);
        }
      },
      error => {
        this.validationMessage(error.error.message);
      }
    )
  }

  generateOTP() {

    debugger;
    var user_details = (JSON.parse(localStorage.getItem("userDetails")));
    var verificationRespose = this.userService.generateOTP({ user_id: user_details.user_id }).subscribe(
      response => {
        debugger;
        if (response.result) {

        }
      },
      error => {
        this.validationMessage(error.error.message);
      }
    )
  }

  public validationMessage(message) {

    // set showloader to true to show loading div on view
    this.showMessage = true;
    this.message = message;

    this.timer = Observable.timer(4000); // 4000 millisecond means 4 seconds
    this.subscription = this.timer.subscribe(() => {
      // set showloader to false to hide loading div from view after 5 seconds
      this.showMessage = false;
      this.message = "";
    });
  }
}
