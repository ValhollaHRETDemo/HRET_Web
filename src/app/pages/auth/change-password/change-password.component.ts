import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs';
import { UserService } from 'app/providers/user.service';
@Component({
  selector: 'vr-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  oldPassword: string;
  newPassword: string;
  confirmPassword: string;
  message: string = "";
  showMessage: boolean = false;
  private subscription: Subscription;
  private timer: Observable<any>;

  constructor(private router: Router,
    private userService: UserService) { }

  ngOnInit() {
  }

  changePassword() {

    if (this.newPassword.length < 6) {
      this.validationMessage("Password should be atleast 6 charachters.");
    }
    else if (this.newPassword !== this.confirmPassword) {
      this.validationMessage("Passwords do not match. Please try again");
    }
    else {
      debugger;
      var user_details = JSON.parse(localStorage.getItem("userDetails"));
      this.userService.changePassword({ "user_id": user_details.user_id, "user_pwd": this.oldPassword, "new_pwd": this.newPassword }).subscribe(
        response => {
          debugger;
          if (response) {
            localStorage.setItem("userDetails", JSON.stringify(response));
            this.router.navigate(['/auth/login']);
          }
        },
        error => {
          this.validationMessage(error.error.message);
        }
      );
    }
  }

  public validationMessage(message) {

    // set showloader to true to show loading div on view
    this.showMessage = true;
    this.message = message;

    this.timer = Observable.timer(4000); // 4000 millisecond means 4 seconds
    this.subscription = this.timer.subscribe(() => {
      // set showloader to false to hide loading div from view after 5 seconds
      this.showMessage = false;
      this.message = "";
    });
  }
}
