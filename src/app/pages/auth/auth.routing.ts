import { Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { VerificationComponent } from './verification/verification.component';
import { ChangePasswordComponent } from 'app/pages/auth/change-password/change-password.component';

export const authRoutes: Routes = [
  {
    path: 'auth/login',
    component: LoginComponent
  },
  {
    path: 'auth/register',
    component: RegisterComponent
  },
  {
    path: 'auth/forgot-password',
    component: ForgotPasswordComponent
  },
  {
    path: 'auth/verification',
    component: VerificationComponent
  },
  {
    path: 'auth/changepassword',
    component: ChangePasswordComponent
  }
];
