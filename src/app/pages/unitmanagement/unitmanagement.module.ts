import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatDialogModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatPaginatorModule,
  MatSortModule,
  MatTableModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { UnitCreateUpdateModule } from './unit-create-update/unit-create-update.module';
import { ListModule } from 'app/core/list/list.module';
import { PageHeaderModule } from 'app/core/page-header/page-header.module';
import { BreadcrumbsModule } from 'app/core/breadcrumbs/breadcrumbs.module';
import { UnitManagementComponent } from 'app/pages/unitmanagement/unitmanagement.component';
import { UnitManagementRoutingModule } from 'app/pages/unitmanagement/unitmanagement.routing';


@NgModule({
  imports: [
    CommonModule,
    UnitManagementRoutingModule,
    FormsModule,
    FlexLayoutModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatInputModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    MatDialogModule,

    // Core
    ListModule,
    UnitCreateUpdateModule,
    PageHeaderModule,
    BreadcrumbsModule
  ],
  declarations: [UnitManagementComponent],
  exports: [UnitManagementComponent]
})
export class UnitManagementModule { }
