import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../../reducers/index';
import { Unit } from './unit.model';
import { UnitsService } from '../../../providers/units.service';
import { validateFormData } from '../../validateformdata';
import { Observable } from 'rxjs/Observable';
import { genericFunctions } from '../../genericfunctions';

@Component({
  selector: 'vr-unit-create-update',
  templateUrl: './unit-create-update.component.html',
  styleUrls: ['./unit-create-update.component.scss']
})
export class UnitCreateUpdateComponent implements OnInit {

  form: FormGroup;
  mode: 'Create' | 'Update' = 'Create';
  isEdit: boolean = false;

  _validateformdata: validateFormData;
  _genericfunction: genericFunctions;

  unitNameValidationMessage: string;
  unitCodeValidationMessage: string;
  unitCodeLengthValidationMessage: string;

  showMessage: boolean;
  message: string;
  timer: Observable<number>;
  subscription: any;

  constructor( @Inject(MAT_DIALOG_DATA) public defaults: any,
    private dialogRef: MatDialogRef<UnitCreateUpdateComponent>,
    private fb: FormBuilder,
    private store: Store<fromRoot.State>, private unitService: UnitsService) {
    this._validateformdata = new validateFormData();
    this._genericfunction = new genericFunctions();
  }

  ngOnInit() {
    if (this.defaults) {
      this.mode = 'Update';
      this.isEdit = true;
      this.form = this.fb.group({
        unit_id: [this.defaults.unit_id || ''],
        unit_code: [this.defaults.unit_code || ''],
        unit_name: [this.defaults.unit_name || ''],
        is_active: [this.defaults.is_active || '1'],
        created_by: [this.defaults.created_by || '1'],
        created_on: [this.defaults.created_on || new Date()],
        modified_by: [this.defaults.modified_by || '1'],
        modified_on: [this.defaults.modified_on || new Date()]
      });
    } else {
      this.form = this.fb.group({
        unit_code: [''],
        unit_name: [''],
        is_active: ['1'],
        created_by: ['1'],
        created_on: [new Date()],
        modified_by: [null],
        modified_on: [null]
      });
    }

  }

  save() {
    if (this.mode === 'Create') {
      this.createUnit();
    } else if (this.mode === 'Update') {
      this.updateUnit();
    }
  }

  createUnit() {
    const unit = this.form.value;
    unit.unit_code = this._genericfunction.trimData(unit.unit_code);
    unit.unit_name = this._genericfunction.trimData(unit.unit_name);
    if (this.validateModelData(unit)) {
      this.unitService.saveUnits(unit).subscribe(p => {
        this.dialogRef.close(p);
      });
    }
  }

  updateUnit() {
    const unit = this.form.value;
    unit.unit_code = this._genericfunction.trimData(unit.unit_code);
    unit.unit_name = this._genericfunction.trimData(unit.unit_name);
    if (this.validateModelData(unit)) {
      this.unitService.saveUnits(unit).subscribe(p => {
        this.dialogRef.close(p);
      });
    }
  }

  isCreateMode() {
    return this.mode === 'Create';
  }

  isUpdateMode() {
    return this.mode === 'Update';
  }

  validateModelData(unit) {
    var status = true;
    if (!(this._validateformdata.validateString(unit.unit_code).status)) {
      this.setTimer("unitcode", (this._validateformdata.validateString(unit.unit_code).message), true);
    }
    if (!(this._validateformdata.validateString(unit.unit_name).status)) {
      this.setTimer("unitname", (this._validateformdata.validateString(unit.unit_name).message), true);
    }
    else {
      if (!(this._validateformdata.validateUnitCode(unit.unit_code).status)) {
        this.setTimer("unitcodelength", (this._validateformdata.validateUnitCode(unit.unit_code).message), true);
      }
    }

    if (
      (this._validateformdata.validateString(unit.unit_name).status) &&
      (this._validateformdata.validateString(unit.unit_code).status) &&
      (this._validateformdata.validateUnitCode(unit.unit_code).status)) {
      return true;
    }
    else {
      return false;
    }
  }

  public setTimer(model, msg, isSuccess) {

    // set showloader to true to show loading div on view
    this.showMessage = true;
    if (model == "unitname") {
      this.unitNameValidationMessage = msg;
    }
    if (model == "unitcode") {
      this.unitCodeValidationMessage = msg;
    }
    if (model == "unitcodelength") {
      this.unitCodeLengthValidationMessage = msg;
    }

    else {
      this.message = msg;
    }
    this.timer = Observable.timer(5000); // 4000 millisecond means 4 seconds
    this.subscription = this.timer.subscribe(() => {
      // set showloader to false to hide loading div from view after 5 seconds
      this.showMessage = false;
      this.message = "";
    });
  }
}
