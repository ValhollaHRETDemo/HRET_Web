export class Unit {
  unit_id: number;
  unit_name: string;
  unit_code: string;
  is_active: string;
  created_by: string;
  created_on: string;
  modified_by: string;
  modified_on: string;

  constructor(unit) {
    this.unit_id = unit.unit_id;
    this.unit_code = unit.unit_code;
    this.unit_name = unit.unit_name;
    this.is_active = unit.is_active;
    this.created_by = unit.created_by;
    this.created_on = unit.created_on;
    this.modified_by = unit.modified_by;
    this.modified_on = unit.modified_on;
  }
}
