import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort } from '@angular/material';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/filter';
import { Observable } from 'rxjs/Observable';
import { List } from '../../core/list/list.interface';
import { Unit } from './unit-create-update/unit.model';
import { ListColumn } from '../../core/list/list-column.model';
import { ListDataSource } from '../../core/list/list-datasource';
import { ListDatabase } from '../../core/list/list-database';
import { componentDestroyed } from '../../core/utils/component-destroyed';
import { UnitCreateUpdateComponent } from './unit-create-update/unit-create-update.component';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { ROUTE_TRANSITION } from '../../app.animation';
import { UnitsService } from '../../providers/units.service';

@Component({
  selector: 'vr-all-in-one-table',
  templateUrl: './unitmanagement.component.html',
  styleUrls: ['./unitmanagement.component.scss'],
  animations: [...ROUTE_TRANSITION],
  host: { '[@routeTransition]': '' }
})

export class UnitManagementComponent implements List<Unit>, OnInit, OnDestroy {

  subject$: ReplaySubject<Unit[]> = new ReplaySubject<Unit[]>(1);
  data$: Observable<Unit[]>;
  units: Unit[];


  @Input()
  columns: ListColumn[] = [
    { name: 'Unit Code', property: 'unit_code', visible: true, isModelProperty: true },
    { name: 'Unit Name', property: 'unit_name', visible: true, isModelProperty: true },
    { name: 'Actions', property: 'actions', visible: true },
  ] as ListColumn[];
  pageSize = 10;
  resultsLength: number;
  dataSource: ListDataSource<Unit> | null;
  database: ListDatabase<Unit>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  constructor(private dialog: MatDialog, private unitService: UnitsService) {
  }

  ngOnInit() {
    this.getUnits();
  }

  getUnits() {
    this.unitService.getUnits().subscribe(unitsData => {
      this.units = unitsData.map(unit => new Unit(unit));
      this.subject$.next(this.units);
      this.data$ = this.subject$.asObservable();

      this.database = new ListDatabase<Unit>();
      this.data$.takeUntil(componentDestroyed(this))
        .filter(Boolean)
        .subscribe((units) => {
          this.units = units;
          this.database.dataChange.next(units);
          this.resultsLength = units.length;
        });

      this.dataSource = new ListDataSource<Unit>(this.database, this.sort, this.paginator, this.columns);
    });
  }

  createUnit() {
    this.dialog.open(UnitCreateUpdateComponent).afterClosed().subscribe((unit: Unit) => {
      if (unit) {
        this.units.unshift(new Unit(unit));
        this.subject$.next(this.units);
        this.unitService.showToast("success", "Unit created successfully");
      }
    });
  }

  updateUnit(unit) {
    this.dialog.open(UnitCreateUpdateComponent, {
      data: unit
    }).afterClosed().subscribe((unit) => {
      if (unit) {
        const index = this.units.findIndex((existingUnit) => existingUnit.unit_id === unit.unit_id);
        this.units[index] = new Unit(unit);
        this.subject$.next(this.units);
        this.unitService.showToast("success", "Unit updated successfully");

      }
    });
  }

  deleteUnit(unit) {
    this.unitService.deleteUnits(unit).subscribe(p => {
      this.units.splice(this.units.findIndex((existingUnit) => existingUnit.unit_id === unit.unit_id), 1);
      this.subject$.next(this.units);
      this.unitService.showToast("success", "Unit deleted successfully");
    });
  }

  onFilterChange(value) {
    if (!this.dataSource) {
      return;
    }
    this.dataSource.filter = value;
  }

  ngOnDestroy() {
  }
}
