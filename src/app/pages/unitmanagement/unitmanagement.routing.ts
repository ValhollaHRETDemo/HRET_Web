import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { UnitManagementComponent } from 'app/pages/unitmanagement/unitmanagement.component';

const routes: Routes = [
  {
    path: '',
    component: UnitManagementComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UnitManagementRoutingModule { }
