import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatCheckboxModule, MatDialogModule, MatIconModule, MatInputModule, MatMenuModule, MatPaginatorModule, MatSortModule, MatTableModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { ListModule } from '../../core/list/list.module';
import { PageHeaderModule } from '../../core/page-header/page-header.module';
import { BreadcrumbsModule } from '../../core/breadcrumbs/breadcrumbs.module';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { DatagridComponentRoutingModule } from './datagrid.routing';
import {DatagridComponent} from './datagrid.component'

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        FlexLayoutModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatCheckboxModule,
        MatInputModule,
        MatIconModule,
        MatMenuModule,
        MatButtonModule,
        MatDialogModule,
        AngularFontAwesomeModule, 
        DatagridComponentRoutingModule,
        // Core
        ListModule,
        PageHeaderModule,
        BreadcrumbsModule
    ],
    declarations: [DatagridComponent],
    exports: [DatagridComponent]
})
export class DatagridModule { }
