import { Component, OnInit, Input, ViewChild, OnDestroy, SimpleChanges, EventEmitter, Output, OnChanges, ElementRef } from '@angular/core';
import { DataGridViewModel } from '../../viewmodel/datagrid.viewmodel';
import { UserEvent } from '../../viewmodel/user.event';
import { ListColumn } from 'app/core/list/list-column.model';
import { ListDataSource } from 'app/core/list/list-datasource';
import { ListDatabase } from 'app/core/list/list-database';
import { MatPaginator, MatSort } from '@angular/material';
import { Observable } from '../../../../node_modules/rxjs';
import { componentDestroyed } from '../../core/utils/component-destroyed';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import * as _ from 'underscore'
import * as XLSX from 'xlsx';


@Component({
  selector: 'hret-datagrid',
  templateUrl: './datagrid.component.html',
  styleUrls: ['./datagrid.component.scss']
})
export class DatagridComponent implements OnInit, OnDestroy, OnChanges {

  @Input() viewModel: DataGridViewModel;
  columns: ListColumn[] = [] as ListColumn[];
  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  pageSize = 10;
  resultsLength: number;
  dataSource: ListDataSource<any> | null;
  database: ListDatabase<any>;
  subject$: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);
  data$: Observable<any[]>;
  data: any[];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('TABLE') table: any;

  constructor() { }

  ngOnInit() {
  }

  renderGrid() {
    this.data = this.viewModel.rows.map((row, rowIndex) => {
      const entity: any = {};
      this.viewModel.columns.forEach((column, index) => {
        entity[column.name.toString()] = row.data[index];
        entity.rowIndex = rowIndex;
      });
      return entity;
    });

    this.data$ = this.subject$.asObservable();
    this.subject$.next(this.data);
    this.database = new ListDatabase<any>();
    this.data$.takeUntil(componentDestroyed(this)).filter(Boolean).subscribe((dynamicData) => {
      this.data = dynamicData;
      this.database.dataChange.next(dynamicData);
      this.resultsLength = dynamicData.length;
    });
    this.dataSource = new ListDataSource<any>(this.database, this.sort, this.paginator, this.columns);
  }

  ngOnDestroy() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.viewModel !== undefined && changes.viewModel.currentValue.columns !== undefined) {
      this.buildColumns();
      this.renderGrid();
    }
  }


  buildColumns() {
    this.columns = this.viewModel.columns.map(p => {
      return { name: p.name, property: p.name, visible: !p.isHidden };
    }) as ListColumn[];
  }

  navigate(rowData) {
    debugger;
    this.viewModel.events.next({ eventData: this.viewModel.rows[rowData.rowIndex].data[this.viewModel.keyColumnIndex.toFixed()], eventType: "DataGridRowClicked" })
  }

  exportToExcel() {
    this.viewModel.events.next({ eventData: null, eventType: "ExportToExcel" })

  }

}

