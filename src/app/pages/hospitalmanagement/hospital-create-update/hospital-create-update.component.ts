import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../../reducers/index';
import { Observable } from 'rxjs/Observable';
import { StatesService } from '../../../providers/states.service';
import { HospitalInformation } from './hospital.model';
import { HospitalInformationService } from 'app/providers/hospitalinformation.service';
import { UnitsService } from 'app/providers/units.service';
import { HospitalUnitsService } from 'app/providers/hospitalunits.service';
import { debug } from 'util';
import { validateFormData } from '../../validateformdata';
import { genericFunctions } from '../../genericfunctions';
import { State } from '../../statemanagement/state-create-update/state.model';

@Component({
  selector: 'vr-hospital-create-update',
  templateUrl: './hospital-create-update.component.html',
  styleUrls: ['./hospital-create-update.component.scss']
})

export class HospitalCreateUpdateComponent implements OnInit {

  AvailableUnits = [];
  states = [];
  units = [];
  hospitalUnits = [];
  stateList: State;

  selectedStateId: any;

  form: FormGroup;
  mode: 'create' | 'update' = 'create';

  _validateformdata: validateFormData;
  _genericfunction: genericFunctions;

  showMessage: boolean;
  message: string;
  hospitalNameValidationMessage: string;
  contactNameValidationMessage: string;
  emailValidationMessage: string;
  stateValidationMessage: string;
  cityValidationMessage: string;
  zipValidationMessage: string;
  phoneValidationMessage: string;

  timer: Observable<number>;
  subscription: any;

  constructor( @Inject(MAT_DIALOG_DATA) public defaults: any,
    private dialogRef: MatDialogRef<HospitalCreateUpdateComponent>,
    private fb: FormBuilder,
    private store: Store<fromRoot.State>,
    private hospitalInformationService: HospitalInformationService,
    private statesService: StatesService,
    private unitService: UnitsService,
    private hospitalUnitsService: HospitalUnitsService) {
    this._validateformdata = new validateFormData();
    this._genericfunction = new genericFunctions();
  }

  ngOnInit() {

    this.fillDefaults();

  }

  fillDefaults() {

    this.statesService.getAllStates().subscribe(p => {
      p.forEach(element => {
        this.states.push({ name: element.state_name, id: element.state_id });
      });
    });

    if (this.defaults) {

      this.mode = 'update';
      this.form = this.fb.group({
        hospital_id: [this.defaults.hospital_id || null,],
        hospital_name: [this.defaults.hospital_name || '',],
        contact_person: [this.defaults.contact_person || ''],
        email_id: this.defaults.email_id || '',
        phone: this.defaults.phone || '',
        address_line_1: this.defaults.address_line_1 || '',
        address_line_2: this.defaults.address_line_2 || '',
        city: this.defaults.city || '',
        zip_code: this.defaults.zip_code || '',
        is_active: [this.defaults.is_active || '1'],
        created_by: [this.defaults.created_by || '1'],
        created_on: [this.defaults.created_on || new Date()],
        modified_by: [this.defaults.modified_by || null],
        modified_on: [this.defaults.modified_on || null],
        state_id: [this.defaults.state_id || null]
      });
      this.selectedStateId = this.defaults.state_id;

    } else {

      this.defaults = {} as HospitalInformation;
      this.form = this.fb.group({
        hospital_name: [this.defaults.hospital_name || '',],
        contact_person: [this.defaults.contact_person || ''],
        email_id: this.defaults.email_id || '',
        phone: this.defaults.phone || '',
        address_line_1: this.defaults.address_line_1 || '',
        address_line_2: this.defaults.address_line_2 || '',
        city: this.defaults.city || '',
        zip_code: this.defaults.zip_code || '',
        is_active: [this.defaults.is_active || '1'],
        created_by: [this.defaults.created_by || '1'],
        created_on: [this.defaults.created_on || new Date()],
        modified_by: [this.defaults.modified_by || null],
        modified_on: [this.defaults.modified_on || null]
      });

    }

    this.getHospitalUnits();

  }

  save() {
    if (this.mode === 'create') {
      this.createHospital();
    } else if (this.mode === 'update') {
      this.updateHospital();
    }
  }

  createHospital() {
    const hospital = this.form.value;
    hospital.state_id = this.selectedStateId;
    hospital.state_name = this.states.find(x => x.id === this.selectedStateId).name;
    hospital.hospital_name = this._genericfunction.trimData(hospital.hospital_name);
    hospital.contact_person = this._genericfunction.trimData(hospital.contact_person);
    hospital.email_id = this._genericfunction.trimData(hospital.email_id);
    hospital.city = this._genericfunction.trimData(hospital.city);
    //if (this.validateModelData(hospital)) {
    this.hospitalInformationService.saveHospitalInformation(hospital).subscribe(p => {
      this.setHospitalUnits(hospital, p, "create");
    });
    //}
  }

  updateHospital() {
    const hospital = this.form.value;
    hospital.state_id = this.selectedStateId;
    hospital.state_name = this.states.find(x => x.id === this.selectedStateId).name;
    hospital.hospital_name = this._genericfunction.trimData(hospital.hospital_name);
    hospital.contact_person = this._genericfunction.trimData(hospital.contact_person);
    hospital.email_id = this._genericfunction.trimData(hospital.email_id);
    hospital.city = this._genericfunction.trimData(hospital.city);
    //if (this.validateModelData(hospital)) {
    this.hospitalInformationService.saveHospitalInformation(hospital).subscribe(p => {
      this.setHospitalUnits(hospital, p, "update");
    });
    //}
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  getHospitalUnits() {
    this.unitService.getUnits().subscribe(units => {

      units.forEach(unit => {
        this.AvailableUnits.push({ id: unit.unit_id, name: unit.unit_name, selected: false, hosp_unit_id: 0 });
      });

      if (this.defaults.hospital_id !== undefined) {
        this.hospitalUnitsService.getHospitalUnitsByHospitalId(this.defaults.hospital_id).subscribe(selectedUnits => {
          this.AvailableUnits.forEach(availableUnit => {
            selectedUnits.forEach(selectedUnit => {
              if (availableUnit.id === selectedUnit.unit_id) {
                availableUnit.hosp_unit_id = selectedUnit.hosp_unit_id;
                if (selectedUnit.is_active === '1') {
                  availableUnit.selected = true;
                }
              }
            });
          });
        });
      }

    });
  }

  setHospitalUnits(hospital, savedHospitalInfomration, mode) {

    this.AvailableUnits.forEach(element => {
      if (mode === "update") {
        this.hospitalUnits.push({
          hosp_unit_id: element.hosp_unit_id !== 0 ? element.hosp_unit_id : 0
        });
      }
      this.hospitalUnits.push({
        hospital_id: savedHospitalInfomration.hospital_id,
        unit_id: element.id,
        is_active: element.selected === true ? '1' : '0',
        created_by: '1',
        created_on: new Date,
        modified_by: '1',
        modified_on: new Date
      });
    });

    this.hospitalUnitsService.saveHospitalUnits(this.hospitalUnits).subscribe(p => {
      this.dialogRef.close(savedHospitalInfomration);
    });

  }

  customAvailableUnitChange(id, value) {
    this.AvailableUnits.forEach(availableUnit => {
      if (availableUnit.id === id) { availableUnit.selected = !value; }
    });
  }

  setTimer(model, msg, isSuccess) {

    // set showloader to true to show loading div on view
    this.showMessage = true;
    if (model == "email") {
      this.emailValidationMessage = msg;
    }
    if (model == "name") {
      this.hospitalNameValidationMessage = msg;
    }
    if (model == "statecode") {
      this.stateValidationMessage = msg;
    }
    if (model == "contact") {
      this.contactNameValidationMessage = msg;
    }
    if (model == "city") {
      this.cityValidationMessage = msg;
    }
    if (model == "zip") {
      this.zipValidationMessage = msg;
    }
    if (model == "phone") {
      this.phoneValidationMessage = msg;
    }
    else {
      this.message = msg;
    }
    this.timer = Observable.timer(5000); // 4000 millisecond means 4 seconds
    this.subscription = this.timer.subscribe(() => {
      // set showloader to false to hide loading div from view after 5 seconds
      this.showMessage = false;
      this.message = "";
    });
  }

  validateModelData(hospital) {
    var status = true;
    if (!(this._validateformdata.validateEmail(hospital.email_id).status)) {
      this.setTimer("email", (this._validateformdata.validateEmail(hospital.email_id).message), true);
    }
    if (!(this._validateformdata.validateString(hospital.hospital_name).status)) {
      this.setTimer("name", (this._validateformdata.validateString(hospital.hospital_name).message), true);
    }
    if (!(this._validateformdata.validateDropdownSelection(hospital.state_code).status)) {
      this.setTimer("statecode", (this._validateformdata.validateDropdownSelection(hospital.state_code).message), true);
    }
    if (!(this._validateformdata.validateString(hospital.contact_person).status)) {
      this.setTimer("contact", (this._validateformdata.validateString(hospital.contact_person).message), true);
    }
    if (!(this._validateformdata.validateString(hospital.city).status)) {
      this.setTimer("city", (this._validateformdata.validateString(hospital.city).message), true);
    }
    //console.log("zip code validation", (this._validateformdata.validateZipCode(hospital.zip_code).status));
    if (!(this._validateformdata.validateZipCode(hospital.zip_code).status)) {
      this.setTimer("zip", (this._validateformdata.validateZipCode(hospital.zip_code).message), true);
    }
    if (!(this._validateformdata.validatePhoneNumber(hospital.phone).status)) {
      this.setTimer("phone", (this._validateformdata.validatePhoneNumber(hospital.phone).message), true);
    }
    if (
      (this._validateformdata.validateEmail(hospital.email_id).status) &&
      (this._validateformdata.validateString(hospital.hospital_name).status) &&
      (this._validateformdata.validateString(hospital.city).status) &&
      (this._validateformdata.validateString(hospital.contact_person).status) &&
      (this._validateformdata.validateDropdownSelection(hospital.state_code).status) &&
      (this._validateformdata.validateZipCode(hospital.zip_code).status) &&
      (this._validateformdata.validatePhoneNumber(hospital.phone).status)) {
      return true;
    }
    else {
      return false;
    }
  }

}
