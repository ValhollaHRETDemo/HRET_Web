import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HospitalCreateUpdateComponent } from './hospital-create-update.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatDialogModule, MatIconModule, MatInputModule, MatRadioModule, MatSelectModule, MatCheckboxModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatDialogModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatRadioModule,
    MatSelectModule,
    MatCheckboxModule
  ],
  declarations: [HospitalCreateUpdateComponent],
  entryComponents: [HospitalCreateUpdateComponent],
  exports: [HospitalCreateUpdateComponent]
})
export class HospitalCreateUpdateModule {
}
