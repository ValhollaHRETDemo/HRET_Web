import { State } from "../../statemanagement/state-create-update/state.model";

export class HospitalInformation {
  hospital_id: number;
  hospital_name: string;
  contact_person: string;
  email_id: string;
  phone: string;
  address_line_1: string;
  address_line_2: string;
  city: string;
  state_id: number;
  zip_code: number;
  is_active: string;
  created_by: number;
  created_on: Date;
  modified_by: number;
  modified_on: Date;
  state_name: string;
  hiin_id: string;
  constructor(hospitalInformation) {
    this.hospital_id = hospitalInformation.hospital_id;
    this.hospital_name = hospitalInformation.hospital_name;
    this.contact_person = hospitalInformation.contact_person;
    this.email_id = hospitalInformation.email_id;
    this.phone = hospitalInformation.phone;
    this.address_line_1 = hospitalInformation.address_line_1;
    this.address_line_2 = hospitalInformation.address_line_2;
    this.city = hospitalInformation.city;
    this.zip_code = hospitalInformation.zip_code;
    this.state_id = hospitalInformation.state_id;
    this.is_active = hospitalInformation.is_active;
    this.created_by = hospitalInformation.created_by;
    this.created_on = hospitalInformation.created_on;
    this.modified_by = hospitalInformation.modified_by;
    this.modified_on = hospitalInformation.modified_on;
    if (hospitalInformation.state_ !== undefined) {
      this.state_name = hospitalInformation.state_.state_name;
    } else {
      this.state_name = hospitalInformation.state_name;
    }
    this.hiin_id = hospitalInformation.hiin_id;
  }

}
