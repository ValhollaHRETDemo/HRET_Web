import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort } from '@angular/material';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/filter';
import { Observable } from 'rxjs/Observable';
import { List } from '../../core/list/list.interface';
import { HospitalInformation } from './hospital-create-update/hospital.model';
import { ListColumn } from '../../core/list/list-column.model';
import { ListDataSource } from '../../core/list/list-datasource';
import { ListDatabase } from '../../core/list/list-database';
import { componentDestroyed } from '../../core/utils/component-destroyed';
import { HospitalCreateUpdateComponent } from './hospital-create-update/hospital-create-update.component';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { ROUTE_TRANSITION } from '../../app.animation';
import { Subscription } from 'rxjs/Subscription';
//import { Hospital_Data } from './hospitalmanagement.demo';
import { StatesService } from 'app/providers/states.service';
import { HospitalInformationService } from 'app/providers/hospitalinformation.service';

@Component({
  selector: 'vr-all-in-one-table',
  templateUrl: './hospitalmanagement.component.html',
  styleUrls: ['./hospitalmanagement.component.scss'],
  animations: [...ROUTE_TRANSITION],
  host: { '[@routeTransition]': '' }
})
export class HospitalManagementComponent implements List<HospitalInformation>, OnInit, OnDestroy {

  subject$: ReplaySubject<HospitalInformation[]> = new ReplaySubject<HospitalInformation[]>(1);
  data$: Observable<HospitalInformation[]>;
  hospitals: HospitalInformation[];

  message: string = "";
  showMessage: boolean = false;
  private subscription: Subscription;
  private timer: Observable<any>;

  public states = [];

  @Input()
  columns: ListColumn[] = [
    { name: 'Hospital Name', property: 'hospital_name', visible: true, isModelProperty: true },
    { name: 'Email', property: 'email_id', visible: true, isModelProperty: true },
    { name: 'Phone number', property: 'phone', visible: true, isModelProperty: true },
    { name: 'State', property: 'state_name', visible: true, isModelProperty: true },
    { name: 'Actions', property: 'actions', visible: true }
  ] as ListColumn[];
  pageSize = 10;
  resultsLength: number;
  dataSource: ListDataSource<HospitalInformation> | null;
  database: ListDatabase<HospitalInformation>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  constructor(private dialog: MatDialog, private hospitalInformationService: HospitalInformationService,
    private stateservice: StatesService) {
  }

  ngOnInit() {

    this.hospitalInformationService.getHospitalInformation().subscribe(Hospital_Data => {
      this.hospitals = Hospital_Data.map(hospital => new HospitalInformation(hospital));
      this.subject$.next(this.hospitals);
      this.data$ = this.subject$.asObservable();
      this.database = new ListDatabase<HospitalInformation>();
      this.data$
        .takeUntil(componentDestroyed(this))
        .filter(Boolean)
        .subscribe((hospitals) => {
          this.hospitals = hospitals;
          this.database.dataChange.next(hospitals);
          this.resultsLength = hospitals.length;
        });

      this.dataSource = new ListDataSource<HospitalInformation>(this.database, this.sort, this.paginator, this.columns);
    });

    
    this.stateservice.getAllStates().subscribe(p => {
      p.forEach(element => {
        this.states.push({ name: element.state_name, id: element.state_id });
      });      
    });

  }

  createHospital() {
    this.dialog.open(HospitalCreateUpdateComponent).afterClosed().subscribe((hospital: HospitalInformation) => {
      if (hospital) {
        hospital.state_name = this.states.find(x => x.id === hospital.state_id).name;
        this.hospitals.unshift(new HospitalInformation(hospital));
        this.subject$.next(this.hospitals);
        this.hospitalInformationService.showToast("success", "Hospital information saved successfully")
      }
    });
  }

  updateHospital(hospital) {
    this.dialog.open(HospitalCreateUpdateComponent, {
      data: hospital
    }).afterClosed().subscribe((hospital) => {
      if (hospital) {
        hospital.state_name = this.states.find(x => x.id === hospital.state_id).name;
        const index = this.hospitals.findIndex((existingHospital) => existingHospital.hospital_id === hospital.hospital_id);
        this.hospitals[index] = new HospitalInformation(hospital);
        this.subject$.next(this.hospitals);
        this.hospitalInformationService.showToast("success", "Hospital information modified successfully");
      }
    });
  }

  deleteHospital(hospital) {
    this.hospitalInformationService.deleteHospitalInformation(hospital).subscribe(p => {
      this.hospitals.splice(this.hospitals.findIndex((existingHospital) => existingHospital.hospital_id === hospital.hospital_id), 1);
      this.subject$.next(this.hospitals);
      this.hospitalInformationService.showToast("success", "Hospital information deleted successfully");
    });
  }

  onFilterChange(value) {
    if (!this.dataSource) {
      return;
    }
    this.dataSource.filter = value;
  }

  ngOnDestroy() {
  }

  setTimer(msg, isSuccess) {

    // set showloader to true to show loading div on view
    this.showMessage = true;
    this.message = msg;

    this.timer = Observable.timer(4000); // 4000 millisecond means 4 seconds
    this.subscription = this.timer.subscribe(() => {
      // set showloader to false to hide loading div from view after 5 seconds
      this.showMessage = false;
      this.message = "";
    });
  }

}
