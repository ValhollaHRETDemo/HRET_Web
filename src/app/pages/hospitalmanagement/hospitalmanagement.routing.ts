import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { HospitalManagementComponent } from 'app/pages/hospitalmanagement/hospitalmanagement.component';

const routes: Routes = [
  {
    path: '',
    component: HospitalManagementComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HospitalManagementRoutingModule { }
