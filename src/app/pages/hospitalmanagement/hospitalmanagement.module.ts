import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatDialogModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatPaginatorModule,
  MatSortModule,
  MatTableModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { HospitalCreateUpdateModule } from './hospital-create-update/hospital-create-update.module';
import { ListModule } from 'app/core/list/list.module';
import { PageHeaderModule } from 'app/core/page-header/page-header.module';
import { BreadcrumbsModule } from 'app/core/breadcrumbs/breadcrumbs.module';
import { HospitalManagementComponent } from 'app/pages/hospitalmanagement/hospitalmanagement.component';
import { HospitalManagementRoutingModule } from 'app/pages/hospitalmanagement/hospitalmanagement.routing';


@NgModule({
  imports: [
    CommonModule,
    HospitalManagementRoutingModule,
    FormsModule,
    FlexLayoutModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatInputModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    MatDialogModule,

    // Core
    ListModule,
    HospitalCreateUpdateModule,
    PageHeaderModule,
    BreadcrumbsModule
  ],
  declarations: [HospitalManagementComponent],
  exports: [HospitalManagementComponent]
})
export class HospitalManagementModule { }
