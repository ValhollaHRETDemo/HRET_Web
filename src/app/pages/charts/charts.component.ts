import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ChartViewModel } from '../../viewmodel/charts.viewmodel';
import { UserEvent } from '../../viewmodel/user.event';
import { Chart } from 'angular-highcharts';
import * as _ from "underscore";
import { charts } from "highcharts";

@Component({
  selector: 'hret-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.scss']
})
export class ChartsComponent implements OnInit, OnChanges {

  constructor() { }
  @Input() viewModel: ChartViewModel;
  chart: Chart;


  ngOnInit() {
    debugger;
  }

  initChart() {
    this.chart = new Chart({
      chart: {
        type: 'line',
      },
      title: {
        text: ''
      },
      subtitle: {
        text: ''
      },
      xAxis: {
        categories: this.viewModel.xData
      },
      yAxis: {
        title: {
          text: 'Average'
        }
      },
      plotOptions: {
        line: {
          dataLabels: {
            enabled: true
          },
          enableMouseTracking: true
        }
      },
      series: this.viewModel.chartSeries.map(element => {
        return {
          name: element.name.toString(),
          data: element.data.map(parseFloat),
          dashStyle: element.dashStyle || "Line",
          lineWidth: element.lineWidth || 1
        }
      }),

      credits: {
        enabled: false
      }
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.viewModel != undefined && this.viewModel.chartSeries != undefined) {
      this.initChart();
    }
  }

}
