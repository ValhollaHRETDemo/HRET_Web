import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    MatButtonModule, MatCheckboxModule, MatDialogModule, MatIconModule, MatInputModule, MatMenuModule, MatPaginatorModule, MatSortModule, MatTableModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { ListModule } from '../../core/list/list.module';
import { PageHeaderModule } from '../../core/page-header/page-header.module';
import { BreadcrumbsModule } from '../../core/breadcrumbs/breadcrumbs.module';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { ChartsComponentRoutingModule } from './charts.routing';
import { ChartsComponent } from './charts.component';
import { ChartModule } from 'angular-highcharts';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        FlexLayoutModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatCheckboxModule,
        MatInputModule,
        MatIconModule,
        MatMenuModule,
        MatButtonModule,
        MatDialogModule,
        AngularFontAwesomeModule,
        ChartsComponentRoutingModule,
        // Core
        ListModule,
        PageHeaderModule,
        BreadcrumbsModule, ChartModule
    ],
    declarations: [ChartsComponent],
    exports: [ChartsComponent]
})
export class ChartsModule { }
