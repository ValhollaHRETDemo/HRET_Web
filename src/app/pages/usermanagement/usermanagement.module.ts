import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  MatButtonModule,
  MatCheckboxModule,
  MatDialogModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatPaginatorModule,
  MatSortModule,
  MatTableModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { ListModule } from 'app/core/list/list.module';
import { PageHeaderModule } from 'app/core/page-header/page-header.module';
import { BreadcrumbsModule } from 'app/core/breadcrumbs/breadcrumbs.module';
import { UserManagementComponent } from 'app/pages/usermanagement/usermanagement.component';
import { UserManagementRoutingModule } from 'app/pages/usermanagement/usermanagement.routing';
import { UserRoleManagementModule } from 'app/pages/usermanagement/user-role-management/userrole.module';


@NgModule({
  imports: [
    CommonModule,
    UserManagementRoutingModule,
    FormsModule,
    FlexLayoutModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatInputModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    MatDialogModule,

    // Core
    ListModule,
    UserRoleManagementModule,
    PageHeaderModule,
    BreadcrumbsModule
  ],
  declarations: [UserManagementComponent],
  exports: [UserManagementComponent]
})
export class UserManagementModule { }
