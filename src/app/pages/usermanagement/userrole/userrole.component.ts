import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../../reducers/index';

//for get info
import { UserService } from 'app/providers/user.service';
import { RoleService } from 'app/providers/role.service';
import { StatesService } from 'app/providers/states.service';
import { HealthSystemManagementService } from 'app/providers/healthsystemmanagement.service';
import { HospitalInformationService } from 'app/providers/hospitalinformation.service';
import { UnitsService } from 'app/providers/units.service';

//for set info
import { UserRole } from './userrole.model';
import { UserRoleService } from 'app/providers/userrole.service';

@Component({
  selector: 'vr-userrole',
  templateUrl: './userrole.component.html',
  styleUrls: ['./userrole.component.scss']
})

export class UserRoleComponent implements OnInit {

  form: FormGroup;

  //for get info
  user_id: string;
  user_name: string;

  selectedTempRoleName = "";

  Roles = [];
  selectedRoleId: any;

  States = [];
  selectedStateId: any;
  isStateShow: boolean = false;
  isStateDisabled: boolean = false;

  HealthSystems = [];
  selectedHealthSystemId: any;
  isHealthSystemShow: boolean = false;
  isHealthSystemDisabled: boolean = false;

  Hospitals = [];
  selectedHospitalId: any;
  isHospitalShow: boolean = false;
  isHospitalDisabled: boolean = false;

  Units = [];
  selectedUnitId: any;
  isUnitShow: boolean = false;

  //for set info
  UserRoles = [];

  constructor( @Inject(MAT_DIALOG_DATA) public defaults: any,
    private dialogRef: MatDialogRef<UserRoleComponent>,
    private fb: FormBuilder,
    private store: Store<fromRoot.State>,
    private userRoleService: UserRoleService,
    private userService: UserService,
    private roleService: RoleService,
    private healthSystemManagementService: HealthSystemManagementService,
    private hospitalInformationService: HospitalInformationService,
    private unitsService: UnitsService,
    private stateService: StatesService) {
  }

  ngOnInit() {

    var userDetails = JSON.parse(localStorage.getItem("userDetails"));

    this.user_name = this.defaults.user_name;
    this.user_id = this.defaults.user_id;

    this.roleService.getRoles().subscribe(roleResponse => {
      roleResponse.forEach(element => {
        if (parseInt(element.role_rank) > parseInt(userDetails.role_rank)) {
          this.Roles.push({ name: element.role_name, id: element.role_id });
        }
      });
    });

    this.userRoleService.getUserAndRolesByUserId(this.user_id).subscribe(userRoleResponse => {

      var optionsField;
      if (userRoleResponse) {
        optionsField = ({
          user_role_id: userRoleResponse.user_role_id,
          user_id: userRoleResponse.user_id,
          role_id: userRoleResponse.role_id,
          role_name: this.Roles.find(x => x.id === userRoleResponse.role_id).name,
          hospital_id: userRoleResponse.hospital_id,
          hs_id: userRoleResponse.hs_id,
          unit_id: userRoleResponse.unit_id,
          state_id: userRoleResponse.state_id,
          is_active: userRoleResponse.is_active,
          created_by: userRoleResponse.created_by,
          created_on: userRoleResponse.created_on,
          modified_by: null,
          modified_on: null
        });

        this.selectedRoleId = userRoleResponse.role_id;
        this.selectedTempRoleName = this.Roles.find(x => x.id === this.selectedRoleId).name;

        this.selectedStateId = userRoleResponse.state_id;
        if (this.selectedStateId !== null) { this.loadStates(); this.isStateShow = true; }

        this.selectedHealthSystemId = userRoleResponse.hs_id;
        if (this.selectedHealthSystemId !== null) { this.loadHealthSystems(this.selectedStateId); this.isHealthSystemShow = true; }

        this.selectedHospitalId = userRoleResponse.hospital_id;
        if (this.selectedHospitalId !== null) { this.loadHospitals(this.selectedStateId); this.isHospitalShow = true; }

        this.selectedUnitId = userRoleResponse.unit_id;
        if (this.selectedUnitId !== null) { this.loadUnits(this.selectedHospitalId); this.isUnitShow = true; }

        if (optionsField.role_name === "Hospital Admin" || optionsField.role_name === "Health System Admin") {
          this.isStateDisabled = true;
        }
        if (optionsField.role_name === "Unit Admin") {
          this.isStateDisabled = true;
          this.isHospitalDisabled = true;
          this.isHealthSystemDisabled = true;
        }

      } else {
        optionsField = ({
          user_id: "",
          role_id: "",
          role_name: "",
          hospital_id: userDetails.hospital_id,
          hs_id: userDetails.hs_id,
          unit_id: userDetails.unit_id,
          state_id: userDetails.state_id,
          is_active: "1",
          created_by: userDetails.user_id,
          created_on: new Date(),
          modified_by: null,
          modified_on: null
        });

        this.loadStates();

        this.selectedStateId = userDetails.state_id;
        this.selectedHospitalId = userDetails.hospital_id;
        this.selectedHealthSystemId = userDetails.hs_id;
        this.selectedUnitId = userDetails.unit_id;

        if (this.selectedStateId) {
          this.loadHealthSystems(this.selectedStateId);
          this.loadHospitals(this.selectedStateId);
          this.isStateDisabled = true;
        }
        if (this.selectedHospitalId) {
          this.loadUnits(this.selectedHospitalId);
          this.isHospitalDisabled = true;
        }
      }

      this.UserRoles.push(optionsField);
    });

  }

  save() {

    this.UserRoles[0].user_role_id = this.UserRoles[0].user_role_id === "" ? 0 : this.UserRoles[0].user_role_id;
    this.UserRoles[0].user_id = this.user_id;
    this.UserRoles[0].role_id = this.selectedRoleId;
    this.UserRoles[0].hospital_id = this.selectedHospitalId;
    this.UserRoles[0].hs_id = this.selectedHealthSystemId;
    this.UserRoles[0].unit_id = this.selectedUnitId;
    this.UserRoles[0].state_id = this.selectedStateId;
    this.userRoleService.saveUserAndRoles(this.UserRoles[0]).subscribe(p => {
      this.UserRoles[0].role_name = this.Roles.find(x => x.id === this.UserRoles[0].role_id).name;
      this.dialogRef.close(this.UserRoles[0]);
    });

  }

  cancel() {
    this.dialogRef.close();
  }

  onRoleSelect(data) {
    this.selectedTempRoleName = this.Roles.find(x => x.id === data.value).name;
    if (this.selectedTempRoleName === "HRET Admin") {
      this.isStateShow = false;
      this.isHealthSystemShow = false;
      this.isHospitalShow = false;
      this.isUnitShow = false;
    }
    if (this.selectedTempRoleName === "State Admin") {
      this.loadStates();
      this.isStateShow = true;
      this.isHealthSystemShow = false;
      this.isHospitalShow = false;
      this.isUnitShow = false;
    }
    if (this.selectedTempRoleName === "Health System Admin") {
      this.loadStates();
      this.isStateShow = true;
      this.isHealthSystemShow = true;
      this.isHospitalShow = false;
      this.isUnitShow = false;
    }
    if (this.selectedTempRoleName === "Hospital Admin") {
      this.loadStates();
      this.isStateShow = true;
      this.isHealthSystemShow = false;
      this.isHospitalShow = true;
      this.isUnitShow = false;
    }
    if (this.selectedTempRoleName === "Unit Admin") {
      this.loadStates();
      this.isStateShow = true;
      this.isHealthSystemShow = false;
      this.isHospitalShow = true;
      this.isUnitShow = true;
    }
  }

  onStateSelect(data) {

    if (this.selectedTempRoleName === "Health System Admin") {
      this.loadHealthSystems(this.selectedStateId);
    }
    if (this.selectedTempRoleName === "Hospital Admin") {
      this.loadHospitals(this.selectedStateId);
    }
    if (this.selectedTempRoleName === "Unit Admin") {
      this.loadHospitals(this.selectedStateId);
    }
  }

  onHospitalSelect(data) {
    if (this.selectedTempRoleName === "Unit Admin") {
      this.loadUnits(this.selectedHospitalId);
    }
  }

  loadStates() {
    this.stateService.getStates().subscribe(stateResponse => {
      this.States = [];
      stateResponse.forEach(element => {
        this.States.push({ name: element.state_name, id: element.state_id });
      });
    });
  }

  loadHealthSystems(state_id) {
    this.healthSystemManagementService.getHealthSystemsByStateId(state_id).subscribe(healthSystemResponse => {
      this.HealthSystems = [];
      healthSystemResponse.forEach(element => {
        this.HealthSystems.push({ name: element.health_system_name, id: element.health_system_id });
      });
    });
  }

  loadHospitals(state_id) {

    this.hospitalInformationService.getHospitalInformationByStateId(state_id).subscribe(hospitalInformationResponse => {
      this.Hospitals = [];
      hospitalInformationResponse.forEach(element => {
        this.Hospitals.push({ name: element.hospital_name, id: element.hospital_id });
      });
    });
  }

  loadUnits(hosp_id) {
    this.unitsService.getUnitsByHospId(hosp_id).subscribe(unitsResponse => {
      this.Units = [];
      unitsResponse.forEach(element => {
        this.Units.push({ name: element.unit_name, id: element.unit_id });
      });
    });
  }

}
