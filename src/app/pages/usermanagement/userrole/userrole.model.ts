import { State } from "../../statemanagement/state-create-update/state.model";

export class UserRole {
  user_role_id: number;
  user_id: number;
  role_id: number;
  hospital_id: number;
  unit_id: number;
  state_id: number;
  is_active: boolean;
  created_by: number;
  created_on: Date;
  modified_by: number | null;
  modified_on: Date | null;
  hs_id: number;
  role_name: string;
  constructor(healthsystem) {
    this.user_role_id = healthsystem.user_role_id;
    this.user_id = healthsystem.user_id;
    this.role_id = healthsystem.role_id;
    this.hospital_id = healthsystem.hospital_id;
    this.unit_id = healthsystem.unit_id;
    this.state_id = healthsystem.state_id;
    this.is_active = healthsystem.is_active;
    this.created_by = healthsystem.created_by;
    this.created_on = healthsystem.created_on;
    this.modified_by = healthsystem.modified_by;
    this.modified_on = healthsystem.modified_on;
    this.hs_id = healthsystem.hs_id;
    this.role_name = healthsystem.role_name;
  }
}
