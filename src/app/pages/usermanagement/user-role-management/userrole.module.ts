import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserRoleManagementComponent } from './user-role-management.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatDialogModule, MatIconModule, MatInputModule, MatRadioModule, MatSelectModule, MatCheckboxModule } from '@angular/material';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatDialogModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatRadioModule,
    MatSelectModule,
    MatCheckboxModule,
    NgMultiSelectDropDownModule.forRoot()
  ],
  declarations: [UserRoleManagementComponent],
  entryComponents: [UserRoleManagementComponent],
  exports: [UserRoleManagementComponent]
})
export class UserRoleManagementModule {
}
