import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { UserModel } from 'app/pages/usermanagement/user-role-management/userModel.model';
import { UserService } from 'app/providers/user.service';
import { RoleService } from 'app/providers/role.service';
import { StatesService } from 'app/providers/states.service';
import { UnitsService } from 'app/providers/units.service';
import { HospitalInformationService } from 'app/providers/hospitalinformation.service';
import { HealthSystemManagementService } from 'app/providers/healthsystemmanagement.service';
import { UserRoleService } from 'app/providers/userrole.service';
import { HospitalUnitsService } from 'app/providers/hospitalunits.service';
import { Subscription } from 'rxjs';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'vr-user-role-management',
  templateUrl: './user-role-management.component.html',
  styleUrls: ['./user-role-management.component.scss']
})
export class UserRoleManagementComponent implements OnInit {

  message: string = "";
  showMessage: boolean = false;
  private subscription: Subscription;
  private timer: Observable<any>;

  mode: 'create' | 'update' = 'create';
  userModel: any;

  firstName: string;
  lastName: string;
  emailId: string;
  phone: string;

  //for get info
  user_id: string;
  user_name: string;

  selectedTempRoleName = "";

  Roles = [];
  selectedRoleId: any;

  States = [];
  selectedStateId: any;
  isStateShow: boolean = false;
  isStateDisabled: boolean = false;

  HealthSystems = [];
  selectedHealthSystemId: any;
  isHealthSystemShow: boolean = false;
  isHealthSystemDisabled: boolean = false;

  Hospitals = [];
  selectedHospitalId: any;
  isHospitalShow: boolean = false;
  isHospitalDisabled: boolean = false;

  Units = [];
  selectedUnitId: any;
  isUnitShow: boolean = false;

  //for set info
  UserRoles = [];

  userDetails = JSON.parse(localStorage.getItem("userDetails"));

  constructor( @Inject(MAT_DIALOG_DATA) public defaults: any,
    private dialogRef: MatDialogRef<UserRoleManagementComponent>,
    private userService: UserService,
    private roleService: RoleService,
    private healthSystemManagementService: HealthSystemManagementService,
    private hospitalInformationService: HospitalInformationService,
    private unitsService: UnitsService,
    private hospitalUnitService: HospitalUnitsService,
    private stateService: StatesService,
    private userRoleService: UserRoleService) { }

  ngOnInit() {

    //for first module
    if (this.defaults.status === "update") {
      this.mode = 'update';
      this.userModel = this.defaults.user;
      this.firstName = this.userModel['first_name'];
      this.lastName = this.userModel['last_name'];
      this.emailId = this.userModel['email_id'];
      this.phone = this.userModel['phone'];
    } else {
      this.mode = 'create';
      this.userModel = {
        email_id: '',
        user_pwd: '',
        first_name: '',
        last_name: '',
        phone: '',
        is_locked: '1',
        password_expiry_date: new Date(),
        is_active: '1',
        created_on: new Date(),
        created_by: this.defaults.from_user_id,
        modified_on: null,
        modified_by: null,
        failure_count: '0',
        //is_auto_password: '0',
        role_id: null,
        role_name: null,
        role_rank: null,
        state_id: null,
        hs_id: null,
        hospital_id: null,
        unit_id: null
      }
      this.firstName = "";
      this.lastName = "";
      this.emailId = "";
      this.phone = "";
    }

    //for second module
    this.BindDropDowns();

    //if (this.userDetails.role_name === "HRET Admin") { }
    if (this.userDetails.role_name === "State Admin") { this.isStateDisabled = true; }
    if (this.userDetails.role_name === "Health System Admin") { this.isStateDisabled = true; this.isHealthSystemDisabled = true; }
    if (this.userDetails.role_name === "Hospital Admin") { this.isStateDisabled = true; this.isHospitalDisabled = true; }
    //if (this.userDetails.role_name === "Unit Admin") {}

  }

  BindDropDowns(): any {

    this.user_name = (this.defaults.user === undefined) ? "" : this.defaults.user.user_name;
    this.user_id = (this.defaults.user === undefined) ? "" : this.defaults.user.user_id;

    this.roleService.getRoles().subscribe(roleResponse => {
      roleResponse.forEach(element => {
        if (parseInt(element.role_rank) > parseInt(this.userDetails.role_rank)) {
          this.Roles.push({ name: element.role_name, id: element.role_id });
        }
      });
    });

    //for existing user - fetching values
    if (this.user_id) {
      this.userRoleService.getUserAndRolesByUserId(this.user_id).subscribe(userRoleResponse => {

        if (userRoleResponse) {
          var optionsField = ({
            user_role_id: userRoleResponse.user_role_id,
            user_id: userRoleResponse.user_id,
            role_id: userRoleResponse.role_id,
            role_name: this.Roles.find(x => x.id === userRoleResponse.role_id).name,
            hospital_id: userRoleResponse.hospital_id,
            hs_id: userRoleResponse.hs_id,
            unit_id: userRoleResponse.unit_id,
            state_id: userRoleResponse.state_id,
            is_active: userRoleResponse.is_active,
            created_by: userRoleResponse.created_by,
            created_on: userRoleResponse.created_on,
            modified_by: null,
            modified_on: null
          });

          this.selectedRoleId = userRoleResponse.role_id;
          this.selectedTempRoleName = this.Roles.find(x => x.id === this.selectedRoleId).name;

          this.selectedStateId = userRoleResponse.state_id;
          if (this.selectedStateId !== null) { this.loadStates(); this.isStateShow = true; }

          this.selectedHealthSystemId = userRoleResponse.hs_id;
          if (this.selectedHealthSystemId !== null) { this.loadHealthSystems(this.selectedStateId); this.isHealthSystemShow = true; }

          this.selectedHospitalId = userRoleResponse.hospital_id;
          if (this.selectedHospitalId !== null) { this.loadHospitals(this.selectedStateId); this.isHospitalShow = true; }

          this.selectedUnitId = userRoleResponse.unit_id;
          if (this.selectedUnitId !== null) { this.loadUnits(this.selectedHospitalId); this.isUnitShow = true; }

          this.UserRoles.push(optionsField);

        }

      });
    } else {
      var optionsField = ({
        user_id: null,
        role_id: null,
        role_name: null,
        hospital_id: null,
        hs_id: null,
        unit_id: null,
        state_id: null,
        is_active: "1",
        created_by: this.userDetails.user_id,
        created_on: new Date(),
        modified_by: null,
        modified_on: null
      });

      this.UserRoles.push(optionsField);

    }

  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  save() {

    if (this.validation()) {

      //for first module
      this.userModel['first_name'] = this.firstName;
      this.userModel['last_name'] = this.lastName;
      this.userModel['email_id'] = this.emailId;
      this.userModel['phone'] = this.phone;
      this.userModel['role_id'] = this.selectedRoleId;
      this.userModel['role_name'] = this.Roles.find(x => x.id === this.selectedRoleId).name;
      this.userService.saveUser(this.userModel).subscribe(userResponse => {
        userResponse["role_id"] = this.userModel['role_id'];
        userResponse["role_name"] = this.userModel['role_name'];

        //for second module
        if (this.UserRoles[0].user_role_id !== undefined) {
          this.UserRoles[0].user_role_id = (this.UserRoles[0].user_role_id === "") ? 0 : this.UserRoles[0].user_role_id;
        }
        this.UserRoles[0].user_id = userResponse.user_id;
        this.UserRoles[0].role_id = this.selectedRoleId;
        this.UserRoles[0].hospital_id = this.selectedHospitalId;
        this.UserRoles[0].hs_id = this.selectedHealthSystemId;
        this.UserRoles[0].unit_id = this.selectedUnitId;
        this.UserRoles[0].state_id = this.selectedStateId;
        this.userRoleService.saveUserAndRoles(this.UserRoles[0]).subscribe(p => {
          this.UserRoles[0].role_name = this.Roles.find(x => x.id === this.UserRoles[0].role_id).name;
          this.dialogRef.close(userResponse);
        });

      });

    }
  }

  cancel() {
    this.dialogRef.close();
  }

  validation() {
    if (this.firstName.trim().toString() === "") {
      this.validationMessage("Invalid first name");
      return false;
    }
    if (this.emailId.trim().toString() === "") {
      this.validationMessage("Invalid email");
      return false;
    }
    if (this.selectedRoleId === undefined) {
      this.validationMessage("Invalid role");
      return false;
    }
    if (this.selectedTempRoleName === "State Admin" && this.selectedStateId === null) {
      this.validationMessage("Invalid state");
      return false;
    }
    if (this.selectedTempRoleName === "Health System Admin" && this.selectedHealthSystemId === null) {
      this.validationMessage("Invalid health system");
      return false;
    }
    if (this.selectedTempRoleName === "Hospital Admin" && this.selectedHospitalId === null) {
      this.validationMessage("Invalid hospital");
      return false;
    }
    if (this.selectedTempRoleName === "Unit Admin" && this.selectedUnitId === null) {
      this.validationMessage("Invalid unit");
      return false;
    }
    return true;
  }

  onRoleSelect(data) {

    //set value
    this.selectedStateId = null;
    this.States = [];
    this.selectedHealthSystemId = null;
    this.HealthSystems = [];
    this.selectedHospitalId = null;
    this.Hospitals = [];
    this.selectedUnitId = null;
    this.Units = [];

    this.selectedTempRoleName = this.Roles.find(x => x.id === data.value).name;

    if (this.mode === 'create') {

      if (this.selectedTempRoleName === "HRET Admin") {
        this.isStateShow = false;
        this.isHealthSystemShow = false;
        this.isHospitalShow = false;
        this.isUnitShow = false;
      }
      if (this.selectedTempRoleName === "State Admin") {
        this.isStateShow = true;
        this.loadStates();
        this.isStateDisabled = false;

        this.isHealthSystemShow = false;
        this.isHospitalShow = false;
        this.isUnitShow = false;
      }
      if (this.selectedTempRoleName === "Health System Admin") {

        this.isStateShow = true;
        this.loadStates();
        if (this.userDetails.state_id !== null) { this.selectedStateId = this.userDetails.state_id; }
        this.isStateDisabled = false;

        this.isHealthSystemShow = true;
        if (this.userDetails.state_id !== null) { this.loadHealthSystems(this.selectedStateId); }
        this.isHealthSystemDisabled = false;

        this.isHospitalShow = false;

        this.isUnitShow = false;
      }
      if (this.selectedTempRoleName === "Hospital Admin") {
        this.isStateShow = true;
        this.loadStates();
        if (this.userDetails.state_id !== null) { this.selectedStateId = this.userDetails.state_id; }
        this.isStateDisabled = false;

        this.isHealthSystemShow = false;

        this.isHospitalShow = true;
        if (this.userDetails.state_id !== null) { this.loadHospitals(this.selectedStateId); }
        this.isHospitalDisabled = false;

        this.isUnitShow = false;

      }
      if (this.selectedTempRoleName === "Unit Admin") {
        this.isStateShow = true;
        this.loadStates();
        if (this.userDetails.state_id !== null) { this.selectedStateId = this.userDetails.state_id; }
        this.isStateDisabled = false;

        this.isHealthSystemShow = false;

        this.isHospitalShow = true;
        if (this.userDetails.state_id !== null) { this.loadHospitals(this.selectedStateId); }
        if (this.userDetails.state_id !== null) { this.selectedHospitalId = this.userDetails.hospital_id; }
        this.isHospitalDisabled = false;

        this.isUnitShow = true;
        if (this.userDetails.hospital_id !== null) { this.loadUnits(this.selectedHospitalId); }
      }

    }

    //if (this.userDetails.role_name === "HRET Admin") { }
    if (this.userDetails.role_name === "State Admin") { this.isStateDisabled = true; }
    if (this.userDetails.role_name === "Health System Admin") { this.isStateDisabled = true; this.isHealthSystemDisabled = true; }
    if (this.userDetails.role_name === "Hospital Admin") { this.isStateDisabled = true; this.isHospitalDisabled = true; }
    //if (this.userDetails.role_name === "Unit Admin") {}

  }

  onStateSelect(data) {

    //set value
    this.selectedHealthSystemId = null;
    this.HealthSystems = [];
    this.selectedHospitalId = null;
    this.Hospitals = [];
    this.selectedUnitId = null;
    this.Units = [];

    if (this.selectedTempRoleName === "Health System Admin") {
      this.loadHealthSystems(this.selectedStateId);
    }
    if (this.selectedTempRoleName === "Hospital Admin") {
      this.loadHospitals(this.selectedStateId);
    }
    if (this.selectedTempRoleName === "Unit Admin") {
      this.loadHospitals(this.selectedStateId);
    }
  }

  onHealthSystemSelect(data) {

    //set value
    this.selectedHospitalId = null;
    this.Hospitals = [];
    this.selectedUnitId = null;
    this.Units = [];

    if (this.selectedTempRoleName === "Unit Admin") {
      this.loadUnits(this.selectedHospitalId);
    }
  }

  onHospitalSelect(data) {

    //set value
    this.selectedHealthSystemId = null;
    this.HealthSystems = [];
    this.selectedUnitId = null;
    this.Units = [];

    if (this.selectedTempRoleName === "Unit Admin") {
      this.loadUnits(this.selectedHospitalId);
    }
  }

  loadStates() {
    this.stateService.getStates().subscribe(stateResponse => {
      this.States = [];
      stateResponse.forEach(element => {
        this.States.push({ name: element.state_name, id: element.state_id });
      });
    });
  }

  loadHealthSystems(state_id) {
    this.healthSystemManagementService.getHealthSystemsByStateId(state_id).subscribe(healthSystemResponse => {
      this.HealthSystems = [];
      healthSystemResponse.forEach(element => {
        this.HealthSystems.push({ name: element.health_system_name, id: element.health_system_id });
      });
    });
  }

  loadHospitals(state_id) {
    this.hospitalInformationService.getHospitalInformationByStateId(state_id).subscribe(hospitalInformationResponse => {
      this.Hospitals = [];
      hospitalInformationResponse.forEach(element => {
        this.Hospitals.push({ name: element.hospital_name, id: element.hospital_id });
      });
    });
  }

  loadUnits(hosp_id) {
    this.unitsService.getUnits().subscribe(unitResponse => {
      var units = unitResponse;
      this.hospitalUnitService.getHospitalUnitsByHospitalId(hosp_id).subscribe(hospUnitResponse => {
        this.Units = [];
        hospUnitResponse.forEach(element => {
          var unitname = units.find(x => x.unit_id === element.unit_id).unit_name;
          this.Units.push({ name: unitname, id: element.unit_id });
        });
      });

    });
  }

  public validationMessage(message) {

    // set showloader to true to show loading div on view
    this.showMessage = true;
    this.message = message;

    this.timer = Observable.timer(4000); // 4000 millisecond means 4 seconds
    this.subscription = this.timer.subscribe(() => {
      // set showloader to false to hide loading div from view after 5 seconds
      this.showMessage = false;
      this.message = "";
    });
  }

}
