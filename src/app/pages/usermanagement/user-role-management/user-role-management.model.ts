import { State } from "../../statemanagement/state-create-update/state.model";

export class UserRole {
  user_role_id: number;
  user_id: number;
  role_id: number;
  hospital_id: number;
  unit_id: number;
  state_id: number;
  is_active: boolean;
  created_by: number;
  created_on: Date;
  modified_by: number | null;
  modified_on: Date | null;
  hs_id: number;
  role_name: string;
  constructor(userrole) {
    this.user_role_id = userrole.user_role_id;
    this.user_id = userrole.user_id;
    this.role_id = userrole.role_id;
    this.hospital_id = userrole.hospital_id;
    this.unit_id = userrole.unit_id;
    this.state_id = userrole.state_id;
    this.is_active = userrole.is_active;
    this.created_by = userrole.created_by;
    this.created_on = userrole.created_on;
    this.modified_by = userrole.modified_by;
    this.modified_on = userrole.modified_on;
    this.hs_id = userrole.hs_id;
    this.role_name = userrole.role_name;
  }
}
