import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort } from '@angular/material';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/filter';
import { Observable } from 'rxjs/Observable';
import { List } from '../../core/list/list.interface';
import { User } from './user-create-update/user.model';
import { ListColumn } from '../../core/list/list-column.model';
import { ListDataSource } from '../../core/list/list-datasource';
import { ListDatabase } from '../../core/list/list-database';
import { componentDestroyed } from '../../core/utils/component-destroyed';
import { User_Data } from './usermanagement.demo';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { ROUTE_TRANSITION } from '../../app.animation';
import { UserService } from '../../providers/user.service';
import { RoleService } from 'app/providers/role.service';
import { UserRoleService } from 'app/providers/userrole.service';
import { UserRole } from 'app/pages/usermanagement/userrole/userrole.model';
import { UserRoleManagementComponent } from 'app/pages/usermanagement/user-role-management/user-role-management.component';

@Component({
  selector: 'vr-all-in-one-table',
  templateUrl: './usermanagement.component.html',
  styleUrls: ['./usermanagement.component.scss'],
  animations: [...ROUTE_TRANSITION],
  host: { '[@routeTransition]': '' }
})

export class UserManagementComponent implements List<User>, OnInit, OnDestroy {

  subject$: ReplaySubject<User[]> = new ReplaySubject<User[]>(1);
  data$: Observable<User[]>;
  users = [];
  totalUsers: User[];
  roles = [];
  userroles: UserRole[];

  @Input()
  columns: ListColumn[] = [
    { name: 'First Name', property: 'first_name', visible: true, isModelProperty: true },
    { name: 'Last Name', property: 'last_name', visible: true, isModelProperty: true },
    { name: 'Email Id', property: 'email_id', visible: true, isModelProperty: true },
    { name: 'Role', property: 'role_name', visible: true, isModelProperty: true },
    { name: 'Actions', property: 'actions', visible: true }
  ] as ListColumn[];
  pageSize = 10;
  resultsLength: number;
  dataSource: ListDataSource<User> | null;
  database: ListDatabase<User>;

  showMessage: boolean;
  message: string;
  timer: Observable<number>;
  subscription: any;

  userDetails = (JSON.parse(localStorage.getItem("userDetails")));

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  constructor(private dialog: MatDialog,
    private userservice: UserService,
    private roleService: RoleService,
    private userroleService: UserRoleService) {
  }

  ngOnInit() {

    this.roleService.getRoles().subscribe(roleResponse => {
      roleResponse.forEach(element => {
        this.roles.push({ role_name: element.role_name, role_id: element.role_id, role_rank: element.role_rank });
        //, state_id: element.state_id, hospital_id: element.hospital_id, hs_id: element.hs_id, unit_id: element.unit_id 
      });
      this.userroleService.getUserAndRoles().subscribe(userroleResponse => {
        this.userroles = userroleResponse.map(userrole => new UserRole(userrole));

        this.userroles.forEach(element => {
          element.role_name = this.roles.find(x => x.role_id === element.role_id).role_name;
        });

        this.userservice.getUsers().subscribe(userResponse => {
          this.totalUsers = userResponse.map(user => new User(user));

          this.totalUsers.forEach(element => {
            if (this.userroles.find(x => x.user_id === element.user_id) !== undefined) {
              element.role_id = this.userroles.find(x => x.user_id === element.user_id).role_id.toString();
              element.role_name = this.userroles.find(x => x.user_id === element.user_id).role_name;
              element.role_rank = this.roles.find(x => x.role_id === parseInt(element.role_id)).role_rank.toString();
              element.state_id = (this.userroles.find(x => x.user_id === element.user_id).state_id) ? this.userroles.find(x => x.user_id === element.user_id).state_id.toString() : "";
              element.hospital_id = (this.userroles.find(x => x.user_id === element.user_id).hospital_id) ? this.userroles.find(x => x.user_id === element.user_id).hospital_id.toString() : "";
              element.hs_id = (this.userroles.find(x => x.user_id === element.user_id).hs_id) ? this.userroles.find(x => x.user_id === element.user_id).hs_id.toString() : "";
              element.unit_id = (this.userroles.find(x => x.user_id === element.user_id).unit_id) ? this.userroles.find(x => x.user_id === element.user_id).unit_id.toString() : "";
            } else {
              element.role_id = "";
              element.role_name = "-";
              element.role_rank = "0";
            }
          });

          this.totalUsers.forEach(element => {
            if (element.role_id === "") {
              if (element.created_by === this.userDetails.user_id) {
                this.users.push(element);
              }
            } else {

              if (parseInt(element.role_rank) > parseInt(this.userDetails.role_rank)) {
                if (this.userDetails.role_name === "State Admin"
                  && element.state_id.toString() === this.userDetails.state_id.toString()) {
                  this.users.push(element);
                }
                if (this.userDetails.role_name === "Health System Admin"
                  && element.state_id.toString() === this.userDetails.state_id.toString()
                  && element.hs_id.toString() === this.userDetails.hs_id.toString()) {
                  this.users.push(element);
                }
                if (this.userDetails.role_name === "Hospital Admin"
                  && element.state_id.toString() === this.userDetails.state_id.toString()
                  && element.hospital_id.toString() === this.userDetails.hospital_id.toString()) {
                  this.users.push(element);
                }
                if (this.userDetails.role_name === "Unit Admin"
                  && element.state_id.toString() === this.userDetails.state_id.toString()
                  && element.hospital_id.toString() === this.userDetails.hospital_id.toString()
                  && element.unit_id.toString() === this.userDetails.unit_id.toString()) {
                  this.users.push(element);
                }
                if (this.userDetails.role_name === "HRET Admin" || this.userDetails.role_name === "Super Admin") {
                  this.users.push(element);
                }
              }
            }
          });

          this.subject$.next(this.users);
          this.data$ = this.subject$.asObservable();
          this.database = new ListDatabase<User>();
          this.dataSource = new ListDataSource<User>(this.database, this.sort, this.paginator, this.columns);
          this.data$
            .takeUntil(componentDestroyed(this))
            .filter(Boolean)
            .subscribe((users) => {
              this.users = users;
              this.database.dataChange.next(users);
              this.resultsLength = users.length;
            });
        });

      });

    });

  }

  createUser() {
    this.dialog.open(UserRoleManagementComponent, {
      data: { from_role_rank: this.userDetails.role_rank, from_user_id: this.userDetails.user_id, status: "create" }
    }).afterClosed().subscribe((user: User) => {
      if (user) {
        this.users.unshift(new User(user));
        this.subject$.next(this.users);
        this.userservice.showToast("success", "User saved successfully");
      }
    });
  }

  updateUser(user) {
    this.dialog.open(UserRoleManagementComponent, {
      data: { user: user, from_role_rank: this.userDetails.role_rank, from_user_id: this.userDetails.user_id, status: "update" }
    }).afterClosed().subscribe((user) => {
      if (user) {
        debugger;
        const index = this.users.findIndex((existingUser) => existingUser.user_id === user.user_id);
        this.users[index] = new User(user);
        this.subject$.next(this.users);
        this.userservice.showToast("success", "User modified successfully");
      }
    });

  }

  deleteUser(user) {
    this.userservice.deleteUser(user).subscribe(p => {
      this.users.splice(this.users.findIndex((existingUser) => existingUser.user_id === user.user_id), 1);
      this.subject$.next(this.users);
      this.userservice.showToast("success", "User deleted successfully");
    });

  }

  onFilterChange(value) {
    if (!this.dataSource) {
      return;
    }
    this.dataSource.filter = value;
  }

  ngOnDestroy() {
  }

  setTimer(msg, isSuccess) {

    // set showloader to true to show loading div on view
    this.showMessage = true;
    this.message = msg;

    this.timer = Observable.timer(4000); // 4000 millisecond means 4 seconds
    this.subscription = this.timer.subscribe(() => {
      // set showloader to false to hide loading div from view after 5 seconds
      this.showMessage = false;
      this.message = "";
    });
  }

}
