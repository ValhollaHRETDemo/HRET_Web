export class User {
  user_id: number;
  email_id: string;
  user_pwd: string;
  first_name: string;
  last_name: string;
  phone: string;
  is_locked: string;
  password_expiry_date: string;
  is_active: string;
  created_on: string;
  created_by: string;
  modified_on: string;
  modified_by: string;
  failure_count: string;
  is_auto_password: string;
  role_id: string;
  role_name: string;
  role_rank: string;
  state_id: string;
  hs_id: string;
  hospital_id: string;
  unit_id: string;
  constructor(user) {
    this.user_id = user.user_id;
    this.email_id = user.email_id;
    this.user_pwd = user.user_pwd;
    this.first_name = user.first_name;
    this.last_name = user.last_name;
    this.phone = user.phone;
    this.is_locked = user.is_locked;
    this.password_expiry_date = user.password_expiry_date;
    this.is_active = user.is_active;
    this.created_on = user.created_on;
    this.created_by = user.created_by;
    this.modified_on = user.modified_on;
    this.modified_by = user.modified_by;
    this.failure_count = user.failure_count;
    this.is_auto_password = user.is_auto_password;
    this.role_id = user.role_id;
    this.role_name = user.role_name;
    this.role_rank = user.role_rank;
    this.state_id = user.state_id;
    this.hs_id = user.hs_id;
    this.hospital_id = user.hospital_id;
    this.unit_id = user.unit_id;
  }

}
