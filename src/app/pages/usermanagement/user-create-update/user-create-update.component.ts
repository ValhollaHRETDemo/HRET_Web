import { Component, Inject, OnInit, } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import * as fromRoot from '../../../reducers/index';
import { User } from './user.model';
import { UserService } from '../../../providers/user.service';
import { DomainManagementService } from '../../../providers/domainmanagement.service';
import { validateFormData } from '../../validateformdata';
import { genericFunctions } from '../../genericfunctions';
const _ = require("underscore");

@Component({
  selector: 'vr-user-create-update',
  templateUrl: './user-create-update.component.html',
  styleUrls: ['./user-create-update.component.scss']
})
export class UserCreateUpdateComponent implements OnInit {

  userOutput = [];

  form: FormGroup;
  mode: 'create' | 'update' = 'create';
  _validateformdata: validateFormData;
  _genericfunction: genericFunctions;

  firstNameValidationMessage: string;
  lastNameValidationMessage: string;
  emailValidationMessage: string;
  cityValidationMessage: string;
  domainNameValidationMessage: string;
  domainList: any;
  selectedDomain: any;

  showMessage: boolean;
  message: string;
  timer: Observable<number>;
  subscription: any;



  constructor(@Inject(MAT_DIALOG_DATA) public defaults: any,
    private dialogRef: MatDialogRef<UserCreateUpdateComponent>,
    private fb: FormBuilder,
    private userservice: UserService,
    private domainservice: DomainManagementService,
    private store: Store<fromRoot.State>) {
    this._validateformdata = new validateFormData();
    this._genericfunction = new genericFunctions();
  }

  ngOnInit() {
    this.fillDefaults();
  }

  fillDefaults() {

    if (this.defaults.status === "update") {

      this.mode = 'update';
      this.form = this.fb.group({
        user_id: [this.defaults.user.user_id],
        email_id: [this.defaults.user.email_id || '',],
        user_pwd: [this.defaults.user.user_pwd || '',],
        first_name: [this.defaults.user.first_name || '',],
        last_name: [this.defaults.user.last_name || '',],
        phone: [this.defaults.user.phone || '',],
        zip_code: [this.defaults.user.zip_code || '',],
        is_locked: [this.defaults.user.is_locked || '1',],
        password_expiry_date: [this.defaults.user.password_expiry_date || new Date()],
        is_active: [this.defaults.user.is_active || '1'],
        created_by: [this.defaults.user.created_by || ''],
        created_on: [this.defaults.user.created_on || new Date()],
        modified_by: [this.defaults.user.modified_by || ''],
        modified_on: [this.defaults.user.modified_on || new Date()],
        failure_count: [this.defaults.user.failure_count || '0'],
        is_auto_password: [this.defaults.user.is_auto_password || '0'],
        role_id: [this.defaults.user.role_id],
        role_name: [this.defaults.user.role_name]
      });

    }
    else {
      var byUserId = this.defaults.user_id;
      this.defaults = {} as User;
      this.form = this.fb.group({
        email_id: [this.defaults.email_id || '',],
        user_pwd: [this.defaults.user_pwd || '',],
        first_name: [this.defaults.first_name || '',],
        last_name: [this.defaults.last_name || '',],
        phone: [this.defaults.phone || '',],
        is_locked: [this.defaults.is_locked || '1',],
        password_expiry_date: [this.defaults.password_expiry_date || new Date()],
        is_active: [this.defaults.is_active || '1'],
        created_by: [this.defaults.created_by || byUserId],
        created_on: [this.defaults.created_on || new Date()],
        modified_by: [this.defaults.modified_by || byUserId],
        modified_on: [this.defaults.modified_on || new Date()],
        failure_count: [this.defaults.failure_count || '0'],
        is_auto_password: [this.defaults.is_auto_password || '0'],
        role_id: [this.defaults.role_id || ''],
        role_name: [this.defaults.role_name || '-']
      });

    }
  }

  save() {
    debugger;
    if (this.mode === 'create') {
      this.createUser();
    } else if (this.mode === 'update') {
      this.updateUser();
    }
  }

  createUser() {
    const user = this.form.value;
    user.email_id = this._genericfunction.trimData(user.email_id);
    user.first_name = this._genericfunction.trimData(user.first_name);
    user.last_name = this._genericfunction.trimData(user.last_name);

    if (this.validateModelData(user)) {
      this.userservice.saveUser(user).subscribe(userResponse => {
        userResponse["role_id"] = user.role_id;
        userResponse["role_name"] = user.role_name;
        this.dialogRef.close(userResponse);
      });
    }
  }

  updateUser() {
    const user = this.form.value;
    user.email_id = this._genericfunction.trimData(user.email_id);
    user.first_name = this._genericfunction.trimData(user.first_name);
    user.last_name = this._genericfunction.trimData(user.last_name);

    if (this.validateModelData(user)) {
      this.userservice.saveUser(user).subscribe(userResponse => {
        userResponse["role_id"] = user.role_id;
        userResponse["role_name"] = user.role_name;
        this.dialogRef.close(userResponse);
      });
    }
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  public setTimer(model, msg, isSuccess) {

    // set showloader to true to show loading div on view
    this.showMessage = true;
    if (model == "email") {
      this.emailValidationMessage = msg;
    }
    if (model == "firstname") {
      this.firstNameValidationMessage = msg;
    }
    if (model == "lastname") {
      this.lastNameValidationMessage = msg;
    }

    if (model == "domainname") {
      this.domainNameValidationMessage = msg;
    }
    else {
      this.message = msg;
    }
    this.timer = Observable.timer(5000); // 4000 millisecond means 4 seconds
    this.subscription = this.timer.subscribe(() => {
      // set showloader to false to hide loading div from view after 5 seconds
      this.showMessage = false;
      this.message = "";
    });
  }

  validateDomainName(userEmail) {
    //Should be validated from db entries
    if (this._validateformdata.validateEmail(userEmail).status) {
      var email_domain_name = ((userEmail)).split('@');

      if (email_domain_name[1].toLocaleLowerCase() == "ventechsolutions.com" || email_domain_name[1].toLocaleLowerCase() == "globalmantrai.com" || email_domain_name[1].toLocaleLowerCase() == "yahoo.in") {
        return true;
      }
      else {
        this.setTimer("domainname", "Domain is not registered", true);
        return false;
      }
    }
  }

  validateModelData(user) {
    var status = true;

    if (!(this._validateformdata.validateEmail(user.email_id).status)) {
      this.setTimer("email", (this._validateformdata.validateEmail(user.email_id).message), true);
    }
    if (!(this._validateformdata.validateString(user.first_name).status)) {
      this.setTimer("firstname", (this._validateformdata.validateString(user.first_name).message), true);
    }
    if (!(this._validateformdata.validateString(user.last_name).status)) {
      this.setTimer("lastname", (this._validateformdata.validateString(user.last_name).message), true);
    }

    if (
      (this._validateformdata.validateEmail(user.email_id).status) &&
      (this._validateformdata.validateString(user.first_name).status) &&
      (this._validateformdata.validateString(user.last_name).status) &&
      (this.validateDomainName(user.email_id))
    ) {
      return true;
    }
    else {
      return false;
    }
  }

}
