import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/filter';
import 'rxjs/add/observable/timer';
import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { List } from '../../core/list/list.interface';
import { State } from './state-create-update/state.model';
import { ListColumn } from '../../core/list/list-column.model';
import { ListDataSource } from '../../core/list/list-datasource';
import { ListDatabase } from '../../core/list/list-database';
import { componentDestroyed } from '../../core/utils/component-destroyed';
import { StateCreateUpdateComponent } from './state-create-update/state-create-update.component';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { ComponentAnimation } from '../../app.animation';
import { StatesService } from '../../providers/states.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'vr-all-in-one-table',
  templateUrl: './statemanagement.component.html',
  styleUrls: ['./statemanagement.component.scss'],
  animations: [ComponentAnimation]
})

export class StateManagementComponent implements List<State>, OnInit, OnDestroy {

  subject$: ReplaySubject<State[]> = new ReplaySubject<State[]>(1);
  data$: Observable<State[]>;
  states: State[];
  message: string = "";
  showMessage: boolean = false;
  @Input()
  columns: ListColumn[] = [
    { name: 'State', property: 'state_name', visible: true, isModelProperty: true },
    { name: 'State Code', property: 'state_code', visible: true, isModelProperty: true },
    { name: 'Actions', property: 'actions', visible: true }
  ] as ListColumn[]; 

  pageSize = 10;

  resultsLength: number;
  dataSource: ListDataSource<State> | null;
  database: ListDatabase<State>;
  private subscription: Subscription;
  private timer: Observable<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private dialog: MatDialog,
    private statesService: StatesService) {
  }

  ngOnInit() {
    this.getStates();
  }

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  createState() {
    this.dialog.open(StateCreateUpdateComponent).afterClosed().subscribe((state: State) => {
      if (state) {
        this.states.unshift(new State(state));
        this.subject$.next(this.states);
        this.statesService.showToast("success", "State saved successfully");
      }
    });
  }

  updateState(state) {
    this.dialog.open(StateCreateUpdateComponent, {
      data: state
    }).afterClosed().subscribe((state) => {
      if (state) {
        const index = this.states.findIndex((existingState) => existingState.state_id === state.state_id);
        this.states[index] = new State(state);
        this.subject$.next(this.states);
        this.statesService.showToast("success", "State modifed successfully");
      }
    });
  }

  deleteState(state) {
    this.statesService.deleteState(state).subscribe(p => {
      this.states.splice(this.states.findIndex((existingState) => existingState.state_id === state.state_id), 1);
      this.subject$.next(this.states);
      this.statesService.showToast("success", "State deleted successfully");
    });
  }

  getStates() {
    var data = this.statesService.getStates().subscribe(p => {

      this.states = p.map(state => new State(state));
      this.subject$.next(this.states);
      this.data$ = this.subject$.asObservable();
      this.database = new ListDatabase<State>();

      this.dataSource = new ListDataSource<State>(this.database, this.sort, this.paginator, this.columns);
      this.data$.takeUntil(componentDestroyed(this))
        .filter(Boolean)
        .subscribe((states) => {
          this.states = states;
          this.database.dataChange.next(states);
          this.resultsLength = states.length;
        });
      this.dataSource = new ListDataSource<State>(this.database, this.sort, this.paginator, this.columns);
    });
  }

  onFilterChange(value) {
    if (!this.dataSource) {
      return;
    }
    this.dataSource.filter = value;
  }

  public setTimer(msg, isSuccess) {

    // set showloader to true to show loading div on view
    this.showMessage = true;
    this.message = msg;

    this.timer = Observable.timer(4000); // 4000 millisecond means 4 seconds
    this.subscription = this.timer.subscribe(() => {
      // set showloader to false to hide loading div from view after 5 seconds
      this.showMessage = false;
      this.message = "";
    });
  }

  ngOnDestroy() {
  }

}
