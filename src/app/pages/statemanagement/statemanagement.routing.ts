import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { StateManagementComponent } from 'app/pages/statemanagement/statemanagement.component';

const routes: Routes = [
  {
    path: '',
    component: StateManagementComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StateManagementRoutingModule { }
