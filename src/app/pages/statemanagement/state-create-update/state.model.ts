export class State {
  state_code: string;
  state_name : string;
  is_hret : string;
  is_active : boolean;
  created_on: Date;
  created_by: number;
  modified_on: Date;
  modified_by: number;
  state_id:number;

  constructor(state) {
    this.state_code = state.state_code;
    this.state_name = state.state_name;
    this.is_hret = state.is_hret;
    this.is_active = state.is_active;
    this.created_on = state.created_on;
    this.created_by = state.created_by;
    this.modified_on = state.modified_on;
    this.modified_by = state.modified_by;
    this.state_id = state.state_id;
  }
}
