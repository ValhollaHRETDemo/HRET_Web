import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import * as fromRoot from '../../../reducers/index';
import { StatesService } from '../../../providers/states.service';
import * as _ from 'underscore';
import { validateFormData } from '../../validateformdata';
import { genericFunctions } from '../../genericfunctions';

@Component({
  selector: 'vr-state-create-update',
  templateUrl: './state-create-update.component.html',
  styleUrls: ['./state-create-update.component.scss']
})
export class StateCreateUpdateComponent implements OnInit {

  form: FormGroup;
  statesModel: any;
  states = [];
  selectedStateCode: any;

  stateValidationMessage: string;
  _validateformdata: validateFormData;

  showMessage: boolean;
  message: string;
  timer: Observable<number>;
  subscription: any;

  constructor( @Inject(MAT_DIALOG_DATA) public defaults: any,
    private dialogRef: MatDialogRef<StateCreateUpdateComponent>,
    private fb: FormBuilder,
    private store: Store<fromRoot.State>, private statesService: StatesService) {
    this._validateformdata = new validateFormData();
  }

  ngOnInit() {
    this.fillDefaults();
  }


  // createState() {

  //   const state = this.selectedStateCode;


  //   this.statesService.saveState(state).subscribe(p => {
  //     this.dialogRef.close(this.selectedStateCode);
  //   });

  // }


  isCreateMode() {
    var state: any = _.where(this.statesModel, { state_code: this.selectedStateCode });
    state.is_hret = true;
    state.modified_by = 1;
    if (state[0] != undefined) {
      this.statesService.saveState(state[0]).subscribe(p => {
        this.dialogRef.close(state[0]);
      });
    }
    else {
      this.setTimer("state", "Please select a state", true);
    }

  }


  fillDefaults() {
    var data = this.statesService.getAllStates().subscribe(p => {
      this.statesModel = p;
      p.forEach(element => {
        var optionsField = { name: element.state_name, id: element.state_code };
        this.states.push(optionsField);
      });
      this.form = this.fb.group(this.states);
    });
  }

  public setTimer(model, msg, isSuccess) {

    // set showloader to true to show loading div on view
    this.showMessage = true;
    if (model == "state") {
      this.stateValidationMessage = msg;
    }
    else {
      this.message = msg;
    }
    this.timer = Observable.timer(5000); // 4000 millisecond means 4 seconds
    this.subscription = this.timer.subscribe(() => {
      // set showloader to false to hide loading div from view after 5 seconds
      this.showMessage = false;
      this.message = "";
    });
  }



}
