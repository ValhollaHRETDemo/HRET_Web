import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatDialogModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatPaginatorModule,
  MatSortModule,
  MatTableModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { StateCreateUpdateModule } from './state-create-update/state-create-update.module';
import { ListModule } from 'app/core/list/list.module';
import { PageHeaderModule } from 'app/core/page-header/page-header.module';
import { BreadcrumbsModule } from 'app/core/breadcrumbs/breadcrumbs.module';
import { StateManagementComponent } from 'app/pages/statemanagement/statemanagement.component';
import { StateManagementRoutingModule } from 'app/pages/statemanagement/statemanagement.routing';
import { AngularFontAwesomeModule } from 'angular-font-awesome';


@NgModule({
  imports: [
    CommonModule,
    StateManagementRoutingModule,
    FormsModule,
    FlexLayoutModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatInputModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    MatDialogModule,
    AngularFontAwesomeModule,
    // Core
    ListModule,
    StateCreateUpdateModule,
    PageHeaderModule,
    BreadcrumbsModule
  ],
  declarations: [StateManagementComponent],
  exports: [StateManagementComponent]
})
export class StateManagementModule { }
