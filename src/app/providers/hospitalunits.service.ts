import 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';


@Injectable()
export class HospitalUnitsService extends BaseService {

    private hospitalUnits = {};
    private hospitalUnitsArray: any[];
    
    constructor(http: HttpClient, toastyService: ToastyService, toastyConfig: ToastyConfig) {
        super(http, "hospitalUnits", toastyService, toastyConfig)
    }

    getHospitalUnitsByHospitalId(hospitalId): Observable<any> {
        return this.http.get(this.getApi() + "/hospitalUnitsByHospital/" + hospitalId)
            .map((res: Response) => res);
    }

    saveHospitalUnits(data): Observable<any> {
        return this.http.post(this.getApi() + "/", data);
    }

}