import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/Rx';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';

let counter = 0;

let apiEndPoint = 'http://54.255.243.89:3000';
//let apiEndPoint = "https://hret-dev-1855823045.ap-southeast-1.elb.amazonaws.com/api"
//let apiEndPoint = "https://hret-test-1966793776.ap-southeast-1.elb.amazonaws.com/api"


@Injectable()
export class BaseService {

    constructor(protected http: HttpClient, protected entity: string,
        protected toastyService: ToastyService, protected toastyConfig: ToastyConfig) { }
    id: any;
    numbers: any;

    getRawApi(): string {
        return apiEndPoint
    }
    getApi(): string {
        return apiEndPoint + "/" + this.entity
    }
    get(id): Observable<any> {
        return this.http.get(this.getApi() + "/" + id)
            .map((res: Response) => res);
    }
    create(data): Observable<any> {
        return this.http.post(this.getApi() + "/", data);
    }

    delete(id): Observable<any> {
        return this.http.delete(this.getApi() + "/" + id)
            .map((res: Response) => res);
    }

    update(id, data): Observable<any> {
        return this.http.put(this.getApi() + "/" + id, data)
            .map((res: Response) => res);
    }

    query(filter, select?, options?): Observable<any> {
        return this.http.post(this.getApi() + "/query", {
            filter: filter, select: select, options: options
        });
    }

    setId(selectedid: number) {
        this.id = selectedid;
    }
    getId() {
        return this.id;
    }

    showToast(type, message) {

        // Or create the instance of ToastOptions
        var toastOptions: ToastOptions = {
            title: "",
            msg: message,
            showClose: true,
            timeout: 5000,
            theme: 'default',
            // onAdd: (toast: ToastData) => {
            //     console.log('Toast ' + toast.id + ' has been added!');
            // },
            // onRemove: function (toast: ToastData) {
            //     console.log('Toast ' + toast.id + ' has been removed!');
            // }
        };

        switch (type) {
            case 'default': this.toastyService.default(toastOptions); break;
            case 'info': this.toastyService.info(toastOptions); break;
            case 'success': this.toastyService.success(toastOptions); break;
            case 'wait': this.toastyService.wait(toastOptions); break;
            case 'error': this.toastyService.error(toastOptions); break;
            case 'warning': this.toastyService.warning(toastOptions); break;
        }

        return null;
    }
}
