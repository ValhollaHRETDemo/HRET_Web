import 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';


@Injectable()
export class HospitalInformationService extends BaseService {

    private users = {};

    private userArray: any[];

    constructor(http: HttpClient, toastyService: ToastyService, toastyConfig: ToastyConfig) {
        super(http, "hospitalInformation", toastyService, toastyConfig)
    }

    getHospitalInformation(): Observable<any> {
        return this.http.get(this.getApi())
            .map((res: Response) => res);
    }

    getHospitalInformationById(id): Observable<any> {
        return this.http.get(this.getApi() + "/" + id)
            .map((res: Response) => res);
    }

    getHospitalInformationByStateId(state_id): Observable<any> {
        return this.http.get(this.getApi() + "/getHospitalInformationByStateId/" + state_id)
            .map((res: Response) => res);
    }

    saveHospitalInformation(data): Observable<any> {
        return this.http.post(this.getApi() + "/", data);
    }

    deleteHospitalInformation(data): Observable<any> {
        return this.http.post(this.getApi() + "/delete", data);
    }
}