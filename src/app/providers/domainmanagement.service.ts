import 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';


@Injectable()
export class DomainManagementService extends BaseService {


    private userArray: any[];

    constructor(http: HttpClient, toastyService: ToastyService, toastyConfig: ToastyConfig) {
        super(http, "domain", toastyService, toastyConfig)
    }

    getDomain(): Observable<any> {
        return this.http.get(this.getApi())
            .map((res: Response) => res);
    }

    getDomainById(id): Observable<any> {
        return this.http.get(this.getApi() + "/" + id)
            .map((res: Response) => res);
    }

    saveDomain(data): Observable<any> {
        return this.http.post(this.getApi() + "/", data);
    }

    deleteDomain(data): Observable<any> {
        return this.http.post(this.getApi() + "/delete", data);
    }

}