import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { BaseService } from './base.service';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';

let counter = 0;

@Injectable()
export class RoleService extends BaseService {

    private roles = {};

    private roleArray: any[];

    constructor(http: HttpClient, toastyService: ToastyService, toastyConfig: ToastyConfig) {
        super(http, "roles", toastyService, toastyConfig)
    }

    getRoles(): Observable<any> {
        return this.http.get(this.getApi())
            .map((res: Response) => res);
    }

    getRolesById(id): Observable<any> {
        return this.http.get(this.getApi() + "/" + id)
            .map((res: Response) => res);
    }

    saveRoles(data): Observable<any> {
        return this.http.post(this.getApi() + "/", data);
    }

    deleteRoles(data): Observable<any> {
        return this.http.post(this.getApi() + "/delete", data);
    }
}
