import 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';


@Injectable()
export class UnitsService extends BaseService {

    private units = {};
    private unitsArray: any[];

    constructor(http: HttpClient, toastyService: ToastyService, toastyConfig: ToastyConfig) {
        super(http, "units", toastyService, toastyConfig)
    }

    getUnits(): Observable<any> {
        return this.http.get(this.getApi())
            .map((res: Response) => res);
    }

    getUnitsById(id): Observable<any> {
        return this.http.get(this.getApi() + "/" + id)
            .map((res: Response) => res);
    }

    getUnitsByHospId(hosp_id): Observable<any> {
        return this.http.get(this.getApi() + "/getUnitsByHospId/" + hosp_id)
            .map((res: Response) => res);
    }

    saveUnits(data): Observable<any> {
        return this.http.post(this.getApi() + "/", data);
    }

    deleteUnits(data): Observable<any> {
        return this.http.post(this.getApi() + "/delete", data);
    }
}