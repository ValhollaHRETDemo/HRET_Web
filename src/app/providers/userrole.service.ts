import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { BaseService } from './base.service';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';

let counter = 0;

@Injectable()
export class UserRoleService extends BaseService {

    private userroles = {};

    private userroleArray: any[];

    constructor(http: HttpClient, toastyService: ToastyService, toastyConfig: ToastyConfig) {
        super(http, "userAndRoles", toastyService, toastyConfig)
    }

    getUserAndRoles(): Observable<any> {
        return this.http.get(this.getApi())
            .map((res: Response) => res);
    }

    getUserAndRolesById(id): Observable<any> {
        return this.http.get(this.getApi() + "/" + id)
            .map((res: Response) => res);
    }

    getUserAndRolesByUserId(id): Observable<any> {
        return this.http.get(this.getApi() + "/userAndRolesByUserId/" + id)
            .map((res: Response) => res);
    }

    saveUserAndRoles(data): Observable<any> {
        return this.http.post(this.getApi() + "/", data);
    }

    deleteUserAndRoles(data): Observable<any> {
        return this.http.post(this.getApi() + "/delete", data);
    }
}
