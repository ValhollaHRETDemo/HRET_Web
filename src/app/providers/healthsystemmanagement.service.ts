import 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';


@Injectable()
export class HealthSystemManagementService extends BaseService {


    private userArray: any[];

    constructor(http: HttpClient, toastyService: ToastyService, toastyConfig: ToastyConfig) {
        super(http, "healthSystems", toastyService, toastyConfig)
    }

    getHealthSystems(): Observable<any> {
        return this.http.get(this.getApi())
            .map((res: Response) => res);
    }

    getHealthSystemsByStateId(state_id): Observable<any> {
        return this.http.get(this.getApi() + "/getHealthSystemsByStateId/" + state_id)
            .map((res: Response) => res);
    }

    getHealthSystemById(id): Observable<any> {
        return this.http.get(this.getApi() + "/" + id)
            .map((res: Response) => res);
    }

    saveHealthSystem(data): Observable<any> {
        return this.http.post(this.getApi() + "/", data);
    }

    deleteHealthSystem(data): Observable<any> {
        return this.http.post(this.getApi() + "/delete", data);
    }
}