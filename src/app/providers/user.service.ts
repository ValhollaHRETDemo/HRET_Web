import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { BaseService } from './base.service';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';

let counter = 0;

@Injectable()
export class UserService extends BaseService {

    private users = {};

    private userArray: any[];

    constructor(http: HttpClient, toastyService: ToastyService, toastyConfig: ToastyConfig) {
        super(http, "users", toastyService, toastyConfig)
    }

    getUsers(): Observable<any> {
        return this.http.get(this.getApi())
            .map((res: Response) => res);
    }

    getUserById(id): Observable<any> {
        return this.http.get(this.getApi() + "/" + id)
            .map((res: Response) => res);
    }

    login(data): Observable<any> {
        return this.http.post(this.getRawApi() + "/login", data)
            .map((res: Response) => res);
    }

    verification(data): Observable<any> {
        return this.http.post(this.getRawApi() + "/verification", data)
            .map((res: Response) => res);
    }

    generateOTP(data): Observable<any> {
        return this.http.post(this.getRawApi() + "/generateOTP", data)
            .map((res: Response) => res);
    }

    changePassword(data): Observable<any> {
        return this.http.post(this.getRawApi() + "/changePassword", data)
            .map((res: Response) => res);
    }

    resetPassword(data): Observable<any> {
        return this.http.post(this.getRawApi() + "/resetPassword", data)
            .map((res: Response) => res);
    }

    saveUser(data): Observable<any> {
        return this.http.post(this.getApi() + "/", data);
    }

    deleteUser(data): Observable<any> {
        return this.http.post(this.getApi() + "/delete", data);
    }

    getUsersByUserId(id): Observable<any> {
        return this.http.get(this.getApi() + "/getUsersByUserId/" + id)
            .map((res: Response) => res);
    }

    getUserDetails(id): Observable<any> {
        return this.http.get(this.getApi() + "/getUserDetails/" + id)
            .map((res: Response) => res);
    }
}
