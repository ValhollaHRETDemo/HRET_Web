import 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';


@Injectable()
export class HealthSystemHospitalService extends BaseService {


    private userArray: any[];

    constructor(http: HttpClient, toastyService: ToastyService, toastyConfig: ToastyConfig) {
        super(http, "healthSystemsAndHospital", toastyService, toastyConfig)
    }

    getHealthSystemsHospital(): Observable<any> {
        return this.http.get(this.getApi())
            .map((res: Response) => res);
    }

    getHealthSystemHospitalById(id): Observable<any> {
        return this.http.get(this.getApi() + "/" + id)
            .map((res: Response) => res);
    }

    getHealthSystemHospitalByHealthSystemId(id): Observable<any> {
        return this.http.get(this.getApi() + "/HealthSystemHospitalByHealthSystemId/" + id)
            .map((res: Response) => res);
    }

    saveHealthSystemHospital(data): Observable<any> {
        return this.http.post(this.getApi() + "/", data);
    }

    deleteHealthSystem_Hospital(data): Observable<any> {
        return this.http.post(this.getApi() + "/delete", data);
    }
}