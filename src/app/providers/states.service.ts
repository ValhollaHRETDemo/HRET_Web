import 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';


@Injectable()
export class StatesService extends BaseService {

    private users = {};

    private userArray: any[];

    constructor(http: HttpClient, toastyService: ToastyService, toastyConfig: ToastyConfig) {
        super(http, "states", toastyService, toastyConfig)
    }

    getStates(): Observable<any> {
        return this.http.get(this.getApi())
            .map((res: Response) => res);
    }

    getAllStates(): Observable<any> {
        return this.http.get(this.getApi() + "/allStates")
            .map((res: Response) => res);
    }

    getStateById(id): Observable<any> {
        return this.http.get(this.getApi() + "/" + id)
            .map((res: Response) => res);
    }

    saveState(data): Observable<any> {
        return this.http.post(this.getApi() + "/", data);
    }

    deleteState(data): Observable<any> {
        return this.http.post(this.getApi() + "/delete", data);
    }
}