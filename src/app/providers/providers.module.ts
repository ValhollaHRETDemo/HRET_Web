import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseService } from './base.service';
import { UserService } from './user.service';
import { StatesService } from './states.service';
import { HospitalInformationService } from './hospitalinformation.service';
import { UnitsService } from './units.service';
import { ReportsService } from './reports.service';
import { DomainManagementService } from './domainmanagement.service';
import { HospitalUnitsService } from 'app/providers/hospitalunits.service';
import { HealthSystemManagementService } from './healthsystemmanagement.service'
import { HealthSystemHospitalService } from './healthsystem-hospital.service'
import { HttpModule } from '@angular/http';
import { GoogleMapsComponent } from '../../app/pages/google-maps/google-maps.component'
import { UserRoleService } from 'app/providers/userrole.service';
import { RoleService } from 'app/providers/role.service';

const SERVICES = [
  UserService, HospitalInformationService, ReportsService,
  BaseService, StatesService, UnitsService,
  HospitalInformationService,
  HealthSystemManagementService,
  HealthSystemHospitalService,
  UserRoleService,
  RoleService,
  DomainManagementService,
  GoogleMapsComponent,
  HospitalUnitsService
];

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
  ],
  providers: [
    ...SERVICES

  ],
})
export class ProvidersModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: ProvidersModule,
      providers: [
        ...SERVICES
      ],
    };
  }
}
