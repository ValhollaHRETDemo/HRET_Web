import 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';

@Injectable()
export class ReportsService extends BaseService {

    constructor(http: HttpClient, toastyService: ToastyService, toastyConfig: ToastyConfig) {
        super(http, "reports", toastyService, toastyConfig)
    }

    getVersion() {
        return this.http.get(this.getApi() + "/", { responseType: 'text' });
    }
    getReports(param_user_id, param_measure_id, param_sub_measure_id, param_start_date, param_end_date, param_state_code, param_hospital_id
        , param_report_level, param_is_rural, param_bed_range, param_peer_group, param_comparable, param_no_of_random_samples): Observable<any> {

        const data = {
            'param_measure_id': param_measure_id,
            'param_sub_measure_id': param_sub_measure_id,
            'param_start_date': param_start_date,
            'param_end_date': param_end_date,
            'param_state_code': param_state_code,
            'param_hospital_id': param_hospital_id,
            'param_report_level': param_report_level,
            'param_is_rural': param_is_rural,
            'param_bed_range': param_bed_range,
            'param_peer_group': param_peer_group,
            'param_comparable': param_comparable,
            'param_no_of_random_samples': param_no_of_random_samples,
            'param_user_id': param_user_id
        };

        return this.http.post(this.getApi() + "/", data);
    }


    exportToExcel(hretReport, userFilterValues) {
        return this.http.get(this.getApi() + "/" + "reports/exporttoexcel")
            .map((res: Response) => res);
    }
    getFiltersData(id, reportLevel, hospital): Observable<any> {
        return this.http.get(this.getApi() + "/" + "getFilterData/" + id, {
            params: {
                level: reportLevel, hospital: hospital
            }
        })
            .map((res: Response) => res);
    }

}
