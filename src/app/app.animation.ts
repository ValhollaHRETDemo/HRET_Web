import { animate, state, style, transition, trigger } from '@angular/animations';

export const SLIDE_RIGHT_ROUTE_TRANSITION = [
  trigger('routeTransition', [
    state('void', style({ width: '100%', height: '100%'}) ),
    state('*', style({ width: '100%', height: '100%'}) ),
    transition(':enter', [
      style({
        transform: 'translateX(-100%)',
        position: 'fixed'
      }),
      animate('0.5s cubic-bezier(0.35, 0, 0.25, 1)', style({transform: 'translateX(0%)'}))
    ]),
    transition(':leave', [
      style({
        transform: 'translateX(0%)',
        position: 'fixed'
      }),
      animate('0.5s cubic-bezier(0.35, 0, 0.25, 1)', style({transform: 'translateX(100%)'}))
    ])
  ])
];

export const FADE_IN_OUT_ROUTE_TRANSITION = [
  trigger('routeTransition', [
    state('void', style({ width: '100%', height: '100%', display: 'block', position: 'absolute' }) ),
    state('*', style({ width: '100%', height: '100%', display: 'block', position: 'absolute' }) ),
    transition(':enter', [
      style({
      })
    ]),
    transition(':leave', [
      style({
        opacity: '1',
        overflow: 'hidden'
      }),
      animate('0.5s linear', style({
        opacity: '0',
        overflow: 'hidden'
      }))
    ])
  ])
];



export const FADE_IN_ROUTE_TRANSITION = [
  trigger('routeTransition', [
    state('void', style({ width: '100%', height: '100%', display: 'block', position: 'absolute' }) ),
    state('*', style({ width: '100%', height: '100%', display: 'block', position: 'absolute' }) ),
    transition(':enter', [
      style({
        opacity: '0',
      }),
      animate('0.5s linear', style({
        opacity: '1',
      }))
    ]),
    transition(':leave', [

    ])
  ])
];



export const ComponentAnimation =
    // trigger name for attaching this animation to an element using the [@triggerName] syntax
    trigger('ComponentAnimation1', [
 
        // end state styles for route container (host)
        state('*', style({
            // the view covers the whole screen with a semi tranparent background
            position: 'fixed',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            backgroundColor: 'rgba(0, 0, 0, 0.8)'
        })),
 
        // route 'enter' transition
        transition(':enter', [
 
            // styles at start of transition
            style({
                // start with the content positioned off the right of the screen,
                // -400% is required instead of -100% because the negative position adds to the width of the element
                right: '-400%',
 
                // start with background opacity set to 0 (invisible)
                backgroundColor: 'rgba(0, 0, 0, 0)'
            }),
 
            // animation and styles at end of transition
            animate('.5s ease-in-out', style({
                // transition the right position to 0 which slides the content into view
                right: 0,
 
                // transition the background opacity to 0.8 to fade it in
                backgroundColor: 'rgba(0, 0, 0, 0.8)'
            }))
        ]),
 
        // route 'leave' transition
        transition(':leave', [
            // animation and styles at end of transition
            animate('.5s ease-in-out', style({
                // transition the right position to -400% which slides the content out of view
                right: '-400%',
 
                // transition the background opacity to 0 to fade it out
                backgroundColor: 'rgba(0, 0, 0, 0)'
            }))
        ])
    ]);

export const ROUTE_TRANSITION = FADE_IN_ROUTE_TRANSITION;
