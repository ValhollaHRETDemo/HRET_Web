import { ReportMasterViewModel } from "../viewmodel/reportmaster.viewmodel";
import { DataGridViewModel } from "../viewmodel/datagrid.viewmodel";
import { FilterViewModel } from "../viewmodel/filter.viewmodel";
import { ChartViewModel } from "../viewmodel/charts.viewmodel";
import { element } from "../../../node_modules/protractor";
import * as _ from "underscore";

export class ReportsDataAdapter {

    constructor() {
    }

    public transformToDataGridViewModel(data) {

        var dgViewModel: DataGridViewModel = new DataGridViewModel();
        dgViewModel.rows = data.rows.map(row => { return { data: row, isHidden: false } });
        dgViewModel.columns = _.flatten([data.key_columns, data.property_columns, data.time_window_columns]);
        dgViewModel.keyColumnIndex = data.key_columns[0].index;
        return dgViewModel;
    }

    public transformToChartViewModel(data) {
        debugger;
        var chViewModel: ChartViewModel = new ChartViewModel();
        chViewModel.chartSeries = data.keys.map((legend, index) => {
            return {
                seriesId: legend, name: legend,
                data: data.rows[index].slice(data.time_window_columns[0].index),
                isHidden: false
            }
        })
        chViewModel.xData = _.flatten([data.time_window_columns]).map(t => { return t.name; });
        return chViewModel;
    }
}