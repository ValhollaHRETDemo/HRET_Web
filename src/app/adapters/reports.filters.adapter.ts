import { ReportMasterViewModel } from "../viewmodel/reportmaster.viewmodel";
import { DataGridViewModel } from "../viewmodel/datagrid.viewmodel";
import { FilterViewModel } from "../viewmodel/filter.viewmodel";
import { ChartViewModel } from "../viewmodel/charts.viewmodel";
import { element } from "../../../node_modules/protractor";

export class ReportsFilterAdapter {

    constructor() {
    }

    transform(filterData) {
        return filterData;
    }
}