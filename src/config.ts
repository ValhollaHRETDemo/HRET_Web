const JWT = require("jsonwebtoken");

module.exports = {
    tokenSecret: "dsafewtyeryhshtydzgzvzfw43546eyq643qatedfh34a",

    decodeToken(authToken) {
        var decodedData = JWT.decode(authToken, this.tokenSecret);
        return decodedData;
    }
}

